## Quad Generation ##

| Function | Module | Description |
|:---------|:-------|:------------|
| `quadParent` | `Base` | Determine the parent for the given quad |
| `quadSibling` | `Base` | Compute a quad belonging to the same parent |
| `quadFirstDescendant` | `Base` | Build first descendant for given level |
| `quadLastDescendant` | `Base` | Build last descendant for given level |
| | | |
| `quadFaceNeighbour` | `Base` | Compute quad across face |
| `quadHalfFaceNeighbours` | `Base` | Get both the half-sized quads across the given face |
| `quadCornerNeighbour` | `Base` | Compute quad across from the indicated corner |
| `quadHalfCornerNeighbour` | `Base` | Compute half-size quad across from corner |

## Quad Queries ##

| Function | Module | Description |
|:---------|:-------|:------------|
| `quadExists` | `Forest` | Checks for quadrant in the local forest or ghost layer |
| `quadFindOwner` | `Forest` | Search forest for quad and return MPI rank if found |
| `quadFaceNeighbour` | `Base` | Compute quad across face |
| `quadFaceNeighbourExtra` | `Forest` | Compute quad across face, and tree boundaries |
| `quadCornerNeighbourExtra` | `Forest` | Compute quad across corner, and tree boundaries |
| `quadIsCoarse` | `Forest` | True if the quad is adjacent to finer cells |
| `quadCoarseInfo` | `Forest` | Compute coarse-values for each face and corner |

## List of Modules ##

```
src/Data/Forest/Local.hs
src/Data/Forest/P4est.hs
src/Data/Forest/Mesh.hs
src/Data/Forest/P4est/Traverse.hs
src/Data/Forest/P4est/Types.hsc
src/Data/Forest/P4est/Communication.hs
src/Data/Forest/P4est/Connectivity.hs
src/Data/Forest/P4est/Nodes.hs
src/Data/Forest/P4est/P4est.hs
src/Data/Forest/P4est/Legacy.hs
src/Data/Forest/P4est/Base.hs
src/Data/Forest/P4est/Export.hs
src/Data/Forest/P4est/Ghost.hs
src/Data/Scientific/SC.hs
src/Data/Scientific/SC/Types.hsc
src/Data/Scientific/SC/Recycle.hs
src/Data/Scientific/SC/Array.hs
```
