{-# LANGUAGE DataKinds, DeriveGeneric, FlexibleContexts, FlexibleInstances,
             GeneralizedNewtypeDeriving, InstanceSigs, KindSignatures,
             MultiParamTypeClasses, NoMonomorphismRestriction,
             OverloadedStrings, PolyKinds, ScopedTypeVariables,
             StandaloneDeriving, TemplateHaskell, TupleSections,
             TypeFamilies #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Local.Rebase
-- Copyright   : (C) Patrick Suggate, 2020
-- License     : BDS3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Basic functionality for local (sub)forests and nodes.
--
------------------------------------------------------------------------------

module Data.Forest.Local.Rebase
  ( module Data.Forest.Mesh
  , Rebaseable (..)
  , BoundaryTestable (..)
    -- traversal and navigation:
  , moveInDir
  , stepInDir
  , leafProper
  , rebaseLeaf
  , leafTransform
  , numCellsOf
  , cellsOfNode
  , cellsOfNode'
    -- helper functions:
  , mkKey
  )
where

import           Control.Applicative
import           Control.Lens                   ((.~))
import           Data.AffineSpace               (AffineSpace (..))
import           Data.Bits.Handy                (i32, int, testBit, (>>%))
import           Data.Bool                      (bool)
import           Data.Foldable                  (toList)
import           Data.Forest.Local.Base
import           Data.Forest.Mesh
import           Data.Forest.Mesh.Macro2D
import           Data.Forest.Node               (leavesOfNode)
import           Data.Forest.P4est.Base
import           Data.Forest.P4est.Connectivity
import           Data.Forest.P4est.P4est
import           Data.IntSet.Sets
import           Data.Maybe                     (catMaybes)
import           Data.Tree.Linear               (Translatable (..))
import           Linear.Handy                   (V2 (..))


-- ** Rebasing
------------------------------------------------------------------------------
-- | Rebase a node to a new owner-tree, if necessary.
instance Rebaseable (Node 2) where
  type MeshOf (Node 2) = MacroMesh 2
--   rebase = normaliseNode
  rebase = normalateNode
  {-# INLINE rebase #-}

-- | Rebase a leaf to a new owner-tree, if necessary.
instance Rebaseable (Leaf 2) where
  type MeshOf (Leaf 2) = MacroMesh 2
  rebase = rebaseLeaf

-- | Rebase a leaf-index to a new owner-tree, if necessary.
instance Rebaseable (Fkey 2) where
  type MeshOf (Fkey 2) = MacroMesh 2
  rebase mm = fmap reify . rebaseLeaf mm . lpath

-- ** Instances for neighbour operations
------------------------------------------------------------------------------
instance HasFaceNeighbours (Leaf 2) where
  faceNbrs mm l = catMaybes ls where
    ls = rebaseLeaf mm . stepInDir l <$> [minBound..maxBound]
  halfFaceNbrs mm l d = catMaybes $ toList ns where
    ns = rebaseLeaf mm . q2leaf <$> quadFaceNbrsHalf (l2quad l) d

instance HasFaceNeighbours (Fkey 2) where
  faceNbrs     mm   = fmap reify . faceNbrs     mm . lpath
  halfFaceNbrs mm p = fmap reify . halfFaceNbrs mm (lpath p)
  {-# INLINE faceNbrs     #-}
  {-# INLINE halfFaceNbrs #-}

instance HasDiagNeighbours (Leaf 2) where
  diagNbrs     mm l = catMaybes ds where
    ds = rebaseLeaf mm . q2leaf . cornerNbr     (l2quad l) <$> [0..3]
  halfDiagNbrs mm l = catMaybes ds where
    ds = rebaseLeaf mm . q2leaf . cornerNbrHalf (l2quad l) <$> [0..3]

-- ** Simple transformations
------------------------------------------------------------------------------
instance Translatable (Leaf 2) where
  type InDir (Leaf 2) = Dir 2
  translate n = Just . stepInDir n
  {-# INLINE translate #-}

instance Translatable (Fkey 2) where
  type InDir (Fkey 2) = Dir 2
  translate n = Just . reify . stepInDir (lpath n)
  {-# INLINE translate #-}


-- * Traversal and navigation
------------------------------------------------------------------------------
-- | Move across the face in the indicated direction, to find the adjacent
--   leaf-cell.
--
--   NOTE: this function is capable of crossing tree-boundaries.
--   TODO: needs to return an @Orient 2@, not a @Dir 2@!?
moveInDir :: LocalForest 2 -> Dir 2 -> Leaf 2 -> Maybe (Leaf 2)
moveInDir lf d = leafProper lf . flip stepInDir d

-- ** Direction helpers
------------------------------------------------------------------------------
-- | Step the leaf in the indicated direction from the starting leaf.
--
--   NOTE: does not check tree boundaries.
stepInDir :: Leaf 2 -> Dir 2 -> Leaf 2
stepInDir l d = l .+^ ds
  where
    ds = i32 <$> [V2 dw 0, V2 0 dw]!!(int d >>% 1)
    dw = bool negate id (_dir d `testBit` 0) $ width l
{-# INLINE stepInDir #-}


-- * Leaf-cell transformations
------------------------------------------------------------------------------
-- | Make the leaf-cell into a proper leaf-cell, if possible.
--
--   TODO: needs to handle non-face-only transforms.
leafProper :: LocalForest 2 -> Leaf 2 -> Maybe (Leaf 2)
leafProper  = rebaseLeaf . _macroStruct
{-# INLINABLE leafProper #-}

-- ** Low-level leaf-cell transformations
------------------------------------------------------------------------------
-- | If the given leaf is outside of its marked owning-tree, then rebase that
--   leaf in a new tree, if possible.
rebaseLeaf :: MacroMesh 2 -> Leaf 2 -> Maybe (Leaf 2)
rebaseLeaf mm l@(Leaf (V2 x y) _ t)
  | closure l  = Just l
  | f y && f x = rebaseLeafDiag mm l
  | otherwise  = leafTransform l . fst <$> (t, d) <~ mm
  where
    f a = a < 0 || a >= w
    d | x <  0 = West
      | x >= w = East
      | y <  0 = South
      | y >= w = North
      | otherwise = error "rebaseLeaf: internal error"
    w = i32 $ maxWidth l

-- | Try to find an adjacent tree in the north/south direction, and then in
--   the east/west direction -- if the given leaf is diagonally outside of its
--   owner-tree.
rebaseLeafDiag :: MacroMesh 2 -> Leaf 2 -> Maybe (Leaf 2)
rebaseLeafDiag mm l@(Leaf (V2 x y) _ t)
  = case ly of
      Just l' -> rebaseLeaf mm l'
      Nothing -> lx >>= rebaseLeaf mm
  where
    dx | x <  0    = West
       | x >= w    = East
       | otherwise = error "rebaseLeafDiag: invalid x coordinate"
    dy | y <  0    = South
       | y >= w    = North
       | otherwise = error "rebaseLeafDiag: invalid y coordinate"
    w  = i32 $ maxWidth l
    lx = leafTransform l . fst <$> (t, dx) <~ mm
    ly = leafTransform l . fst <$> (t, dy) <~ mm
{-# INLINE rebaseLeafDiag #-}

-- | Apply a face-transform to the given leaf.
leafTransform :: Leaf 2 -> FTransform -> Leaf 2
leafTransform l = toleaf . quadFaceTransform (l2quad l)

-- ** Canonicalisation functions
------------------------------------------------------------------------------
-- | If the given node is outside of the unit tree that it belongs to, then
--   compute new coordinates, and re-base the node to a new tree.
--
--   TODO:
--    + needs to do a tree-search, or test all permutations of the search-
--      directions, and also when reaching-around, to cover all (simple) face-
--      traversals?
--
normalateNode :: MacroMesh 2 -> Node 2 -> Maybe (Node 2)
normalateNode mm p
  | null ds   = Just p
  | otherwise = applyNodeOps mm p ds <|>
                applyNodeOps mm p sd <|>
                reachAround  mm p ds
  where
    (ds, sd)  = (searchDirs p, reverse ds)

searchDirs :: Node 2 -> [Dir 2]
searchDirs p@(Node (V2 x y) _ _) = catMaybes ds where
  w = maxWidth p
  f g d = if g then Just d else Nothing
  {-# INLINE f #-}
  ds = uncurry f <$> [x < 0, x > w, y < 0, y > w] `zip` [West ..North]
{-# INLINE searchDirs #-}

-- | Apply a sequence of rebase-ops, if possible.
applyNodeOps :: MacroMesh 2 -> Node 2 -> [Dir 2] -> Maybe (Node 2)
applyNodeOps  _ p    []  = Just p
applyNodeOps mm p (d:ds) = rebaseNode mm p d >>= applyNodeOps mm `flip` ds

------------------------------------------------------------------------------
-- | When we can not take a direct path due to missing cells/trees, when
--   rebasing a node, then try traversing around the obstacle.
--
--   NOTE:
--    + assumes that one of the ordinates is outside of its current parent-
--      tree;
--    + also assumes that @applyNodeOps@ has already been attempted, so this
--      function will return @Nothing@ for these cases;
--
reachAround :: MacroMesh 2 -> Node 2 -> [Dir 2] -> Maybe (Node 2)
reachAround  _ _ [] = Nothing
reachAround mm p@(Node (V2 x y) _ _) ds
  | x==0 = applyNodeOps mm p ( West:ds)
  | x==w = applyNodeOps mm p ( East:ds)
  | y==0 = applyNodeOps mm p (South:ds)
  | y==w = applyNodeOps mm p (North:ds)
  | True = Nothing
  where
    w = maxWidth p
{-# INLINE reachAround #-}

rebaseNode :: MacroMesh 2 -> Node 2 -> Dir 2 -> Maybe (Node 2)
rebaseNode mm p@(Node _ l t) d =
  let mf = (toEnum t, d) <~ mm
  in  (lvl .~ l) . q2node . quadFaceTransform (n2quad p) . fst <$> mf

------------------------------------------------------------------------------
-- | Calculates how many cells the given node is a member of.
--   NOTE: makes certain assumptions of the sanity of the given node.
numCellsOf :: MacroMesh 2 -> Node 2 -> Int
numCellsOf mm = length . cellsOfNode mm
{-# INLINE numCellsOf #-}

cellsOfNode :: MacroMesh 2 -> Node 2 -> [Leaf 2]
cellsOfNode mm = catMaybes . cellsOfNode' mm
{-# INLINE cellsOfNode #-}

cellsOfNode' :: MacroMesh 2 -> Node 2 -> [Maybe (Leaf 2)]
cellsOfNode' mm = fmap (rebase mm) . leavesOfNode
{-# INLINE cellsOfNode' #-}


-- * Helper functions
------------------------------------------------------------------------------
-- | Replaces @Nothing@ values with keys that do not belong to the forest.
mkKey :: LocalForest 2 -> Leaf 2 -> Fkey 2
mkKey lf l = case leafProper lf l of
  Nothing -> _treeId .~ (-1) $ reify l
  Just l' -> reify l'
