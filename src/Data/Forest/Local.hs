{-# LANGUAGE DataKinds, DeriveGeneric, FlexibleContexts, FlexibleInstances,
             GeneralizedNewtypeDeriving, InstanceSigs, KindSignatures,
             MultiParamTypeClasses, NoMonomorphismRestriction,
             OverloadedStrings, PolyKinds, ScopedTypeVariables,
             StandaloneDeriving, TemplateHaskell, TupleSections,
             TypeFamilies #-}

{-# OPTIONS_GHC -Wall -fno-warn-orphans #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Local
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Functions for working with `p4est_t` @P4est@ objects.
--
-- TODO:
--
------------------------------------------------------------------------------

module Data.Forest.Local
  ( module Data.Forest.Mesh
  , Rebaseable (..)
  , BoundaryTestable (..)
  , LocalForest (..)
  , macroStruct
  , localForest
  , firstLeaves
  , ghostsLayer
  , mirrorLayer
  , forestNodes
  , LocalNodes (..)
  , localNodes
  , localCells
  , cellOfNode
  , globalIdxs
  , nodeOwners
  , ownedCount
  , sharedWith
  , RankSet
  , rankSet
  -- Constructors:
  , mkLocalForest
  , mkLforest
  , macroLShape
  , emptyLocalNodes
  , buildLocalNodes
  --   assembly helpers:
  , mkLocals
  , mkCellProps
  , fixNodeCoords
  --   updates:
  , shareNodesAndUpdate
  --   import & export:
  , saveLocalForest
  , loadLocalForest
  , yamlSaveLocalForest
  , yamlLoadLocalForest
  , saveLocalNodes
  , loadLocalNodes
  , yamlSaveLocalNodes
  , yamlLoadLocalNodes
  --   transforms across tree-faces:
  , FTransforms (..)
  , ftransforms
  , buildFaceTransforms
  , buildFaceTransforms'
  --   traversal and navigation:
  , moveInDir
  , stepInDir
  , leafProper
  , rebaseLeaf
  , leafTransform
  --   generators:
  , makeNodeOf
  , nodesOfLeaf
  , leavesOfNode
  , numCellsOf
  , cellsOfNode
  , firstLeaf
  , lastLeaf
  --   queries:
  , incidentTo
  , numOwned
  , mirrorsOwnedNodes
  , mirrorsNodes
  , leafRankOverlaps
  , leafRankOverlaps'
  , findRanksOf
  -- Parametric coordinates:
  , Parametric (..)
  , PCoord (..)
  , pcoordVec
  , PCoords (..)
  , pcoordMap
  , dofPCoords
  --   parametric coordinate helpers:
  , leafPCoords
  , cellPCoords
  , mkPCoord
  , fromPCoord
  , toPCoord
  , pequiv
  , nodeFromPCoord
  , leafCentrePCoord
  --   parametric coordinates to trees:
  , makeTreeMap
  , treeMapFromVertices
  -- Helper Functions:
  -- -----------------
  , mkKey
  ) where

import           Control.Arrow                  (first, second)
import           Control.DeepSeq
import           Control.Lens                   ((%~), (&), (.~), (^.))
import           Control.Monad.State
import           Data.Bool                      (bool)
import           Data.Foldable                  (foldl', foldlM, toList)
import qualified Data.IntMap                    as Map
import           Data.IntMap.Maps
import qualified Data.IntSet                    as Set
import           Data.IntSet.Sets
import           Data.Maybe                     (catMaybes)
import           Data.Vector.Helpers
import           Data.Vector.Storable           (Vector)
import qualified Data.Vector.Storable           as Vec
import           GHC.Int
import           Linear.Handy                   (V4 (..))

import           Data.Forest.Local.Base
import           Data.Forest.Local.PCoord
import           Data.Forest.Local.Rebase
import           Data.Forest.Local.Sharing
import           Data.Forest.Mesh
import           Data.Forest.Mesh.Macro2D
import           Data.Forest.Node
import           Data.Forest.P4est.Base
import           Data.Forest.P4est.Connectivity
import           Data.Forest.P4est.Nodes
import           Data.Forest.P4est.P4est
import qualified Data.Scientific.SC             as SC


-- * Local forest constructors
------------------------------------------------------------------------------
-- | Constructs a @LocalForest@ from the local,  parallel subforest.
--
--   NOTE:
--    + this function is strict so that `p4est` can be closed afterwards, if
--      desired.
--
mkLocalForest :: P4estM (LocalForest 2)
mkLocalForest  = do
  (lf, ls, em) <- mkLforest
  co <- getConnectivityT
  let fx = buildFaceTransforms co
      (vx, m2) = first macroGrid $ decomposeConn2D em
      mm = MacroMesh m2 fx $ Just vx
  (gs, ms) <- mkGhosts
  ( _, hs) <- mkCellProps lf
  ns <- mkLocals lf hs
  lx <- fixNodeCoords `flip` LocalForest mm lf ls gs ms ns <$> SC.mpiRank
  lx `deepseq` pure lx

-- ** Constructor helpers
------------------------------------------------------------------------------
-- | Build the initial 'Lforest' structure, as this is used by subsequent
--   steps, while constructing the 'LocalForest'.
mkLforest :: P4estM (Lforest 2, Vector (Leaf 2), Conn2D)
mkLforest  = do
  frst <- getP4raw
  let ts = _fTrees frst
      t0 = fromIntegral $ frst^.fFirstLocalTree
      t1 = fromIntegral $ frst^.fLastLocalTree
      go lf i | i  <=  t1 = Vec.foldl' (\ms -> (~>ms) . tofkey) lf qs `go` succ i
              | otherwise = lf
        where qs = Vec.map (qTree .~ fromIntegral i) $ ts!i^.tQuads
      ls = Vec.map toleaf $ frst^.fGlobalFirstPos
  pure (go bare t0, ls, frst^.fConnectivity)


-- * User API functions
------------------------------------------------------------------------------
-- | Find the leaf-cells that are incident to the given independent-node.
--
--   NOTE: the returned 'Diag'-values represent the (Z-index) quadrant/
--     direction of each leaf-cell, relative to the given, central node.
--   NOTE: the convention used is that a node always stores the level of the
--     coarsest leaf-cell that it belongs to (as its level); therefore, only
--     same- and finer- level leaf-cells need to be searched for.
--
incidentTo :: LocalForest 2 -> Node 2 -> [(Diag 2, Fkey 2)]
incidentTo lf p = catMaybes ks where
  -- build sets of same- and finer- level leaves:
  ls = mkKey lf <$> leavesOfNode p
  ms = mkKey lf <$> leavesOfNode (p & lvl %~ succ)
  -- keep/drop rules for the leaf-cells of the support:
  go (j, k)
    | k <? lf^.localForest = Just k  -- fine cell found in local subforest
    | k <? lf^.ghostsLayer = Just k  -- other process has this leaf-cell
    | j <? lf^.localForest = Just j  -- same-level cell found
    | j <? lf^.ghostsLayer = Just j  -- other process has this leaf-cell
    | otherwise            = Nothing -- not found, so outside-of-boundary?
  -- compute the (Maybe-)tuples of diag-leaf pairs:
  ks = zipWith (\c -> fmap (c,)) [SW ..NE] $ go <$> zip ls ms


-- * Constructors
------------------------------------------------------------------------------
-- | Empty local-nodes info.
emptyLocalNodes :: LocalNodes 2
emptyLocalNodes  =
  LocalNodes Vec.empty bare bare Vec.empty Vec.empty Vec.empty bare

-- | Build the dof-nodes data from the given (local) region of the forest, and
--   the nodes data.
buildLocalNodes :: Rank -> Lforest 2 -> Xforest 2 InterpInfo -> Nodes
                -> LocalNodes 2
buildLocalNodes rank quds hngs nods = LocalNodes lz qs oz gs rs ns sw where
  -- Build an array of (independent) 'Node' objects:
  ls = fixLevels qs hngs $ nods^.nodesIndep
  lz = Vec.map indep2node ls
  -- Store the set of independent-/dof- nodes owned by this process:
  -- NOTE: also stores the cells that each node is incident upon.
  js = Vec.enumFromN (nods^.offsetOwnedIndeps) (nods^.numOwnedIndeps)
  os = nodeQuads qs $ Set.fromList $ Vec.toList js
  oz = map (second toleaf) <$> os
  -- Build the map from trees to local-quads:
  cs = Vec.toList $ nods^.nodesLocal  -- vertex-indices for each cell
  qs = foldl' (flip (~>)) bare $ flat quds `zip` cs
  -- Global indices for each independent node:
  gs = calcGlobalIndices rs nods
  -- Rank/owner of each independent node:
  rs = findIndepRanks rank nods
  -- Number of owned, independent nodes per MPI process:
  ns = nods^.globalOwnedIndeps
  -- For the locally-owned independent nodes, store the set of MPI processes
  -- that each node has been shared with:
  sw = findSharedIndeps nods

-- ** Construction helpers
------------------------------------------------------------------------------
-- | Sometimes a node on the border between two trees is placed into a tree
--   where it is incident only to leaves of a different refinement-level, than
--   the node. In these cases, it makes more sense to move the node to a tree
--   that contains (incident) leaves of the same level.
--
fixNodeCoords :: Rank -> LocalForest 2 -> LocalForest 2
fixNodeCoords rk lf = lf & forestNodes %~ (localNodes .~ ns) where
  ns = Vec.imap upd $ lf^.forestNodes^.localNodes
  no = lf^.forestNodes^.nodeOwners
  upd j p | no!j == rk && border p = findBestTree lf p
          | otherwise              = p

findBestTree :: LocalForest 2 -> Node 2 -> Node 2
findBestTree lf p
  | null ks   = p'
  | otherwise = p
  where
    -- Check that the given leaf(-key) can be found within the @LocalForest@:
    chk l = l<?lf^.localForest || l<?lf^.ghostsLayer
    -- Get the list of leaves incident to @p@, and of the same level:
    ls = filter (chk . snd) . zip [SW ..NE] $ mkKey lf <$> leavesOfNode p
    -- Keep only those leaves of the same level AND in the same tree:
    ks = filter ((treeId p==) . treeId . snd) ls
    er = error . ("findBestTree: internal error: " ++)
    -- Search in each of the directions for the leaf that was found:
    p' = case ls of
      []       -> er "node-level differs from that of any incident leaf"
      (d, l):_ -> flipDiag d `makeNodeOf` l

-- ** Local-nodes assembly helpers
------------------------------------------------------------------------------
-- | Traverse the local forest and determine whether each cell is a coarse-/
--   interp- cell, or not.
--
--   OBSOLETE?
--
mkCellProps ::
     Lforest 2
  -> P4estM (Xforest 2 CoarseInfo, Xforest 2 InterpInfo)
mkCellProps ts = do
  let go (cs, fs) k = do
        (mc, mf) <- quadPropsInfo $ toquad k
        let cs' = maybe cs ((~>cs) . (k,)) mc
            fs' = maybe fs ((~>fs) . (k,)) mf
        pure (cs', fs')
  foldlM go (bare, bare) $ flat ts

------------------------------------------------------------------------------
mkLocals :: Lforest 2 -> Xforest 2 InterpInfo -> P4estM (LocalNodes 2)
mkLocals fs hs = do
  rank <- SC.mpiRank
  get >>= maybe
    (pure emptyLocalNodes)
    (fmap (buildLocalNodes rank fs hs) . fromNodesT) . _nodes

-- ** Construction helpers
------------------------------------------------------------------------------
-- | Compute the (local-)quads that each locally-owned, independent node
--   belongs to.
--
--   TODO:
--    + how to compute (and represent) ghost-quads adjacent to owned nodes?
--    + requires hanging-node information?
nodeQuads :: Xforest 2 (V4 Int) -> IntSet -> IntMap [(Dir 2, Quad)]
nodeQuads xf ns = foldl' go bare . fmap (first toquad) $ flat xf
  where
    go :: IntMap [(Dir 2, Quad)] -> (Quad, V4 Int) -> IntMap [(Dir 2, Quad)]
    go qs (q, ni) = snd $ foldl' (upd q) (maxBound, qs) ni
    upd q (c, nq) i
      | i  <?  ns = (pred c, Map.insertWith (++) i [(c, q)] nq)
      | otherwise = (pred c, nq)

-- | Extract the (minimum quad-)level for each local, independent node (where
--   possible).
fixLevels :: Xforest 2 (V4 Int) -> Xforest 2 InterpInfo -> Vector Indep ->
             Vector Indep
fixLevels qs hs = Vec.imap (\i n -> maybe n (\l -> ilevel .~ l $ n) $ i <~ xs)
  where
    -- compute coarsest level of each local-node:
    fn :: Fkey 2 -> V4 Int -> [(Int, Int8)]
    fn q vi = toList vi `zip` ls where
      ls = bool l (l-1) . isInterpNode hs (toquad q) <$> [0..3]
      l  = fromIntegral $ depth q
    -- extract the local-node indices, and level of each quad:
    xs = foldl' go bare . concat $ uncurry fn <$> flat qs
    go lm (i, l) = Map.insertWith min i l lm
