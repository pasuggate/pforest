{-# LANGUAGE CPP, DeriveGeneric, FlexibleInstances, GeneralizedNewtypeDeriving,
             OverloadedStrings, PatternSynonyms, TemplateHaskell,
             TypeSynonymInstances #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.Ghost
-- Copyright   : (C) Patrick Suggate 2019
-- License     : GPL3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Ghosts-layer functions and data-types.
--
-- TODO:
--
------------------------------------------------------------------------------

module Data.Forest.P4est.Ghost
  ( GhostInfo (..)
  , whichTree
  , localNum
  , ownerRank
  , GhostMap
  -- low-level ghost functions:
  , ghostNew
  , ghostDestroy
  , ghostExpand
  --   queries:
  , quadrantExists
  --   helper-functions:
  , quadrantFindOwner
  -- mid-level functions:
  , Ghost (..)
  , gMpisize
  , gNumTrees
  , gBtype
  , gGhosts
  , gTreeOffsets
  , gProcOffsets
  , gMirrors
  , gMirrorTreeOffsets
  , gMirrorProcMirrors
  , gMirrorProcOffsets
  , gMirrorProcFronts
  , gMirrorProcFrontOffsets
  --   conversions and import/export:
  , fromGhostT
  , saveGhost
  , loadGhost
  --   helper-functions:
  , setQuadToTree
  , setQuadToProc
  ) where

import           Control.Lens                 (makeLenses, (%~), (.~), (^.))
import           Control.Monad.IO.Class
import           Data.Aeson
import qualified Data.ByteString.Lazy         as Lazy
import           Data.IntMap.Maps
import           Data.Vector.Storable         (Vector)
import qualified Data.Vector.Storable         as Vec
import qualified Data.Vector.Storable.Mutable as Mut
import           Foreign.C.Types
import           Foreign.Marshal.Utils        (copyBytes, toBool)
import           Foreign.Ptr
import           Foreign.Storable
import           GHC.Generics                 (Generic)

import           Bindings.P4est.P4estGhost
import           Data.Forest.P4est.Base
import           Data.Scientific.SC.Array     as SC


-- * Ghost data-types
------------------------------------------------------------------------------
-- TODO: move to separate module?
data Ghost
  = Ghost
      { _gMpisize                :: CInt
      , _gNumTrees               :: TInt
      , _gBtype                  :: P4estConnectType
      , _gGhosts                 :: Vector Quad
        -- numTrees + 1
      , _gTreeOffsets            :: Vector LInt -- numTrees + 1
        -- mpisize + 1
      , _gProcOffsets            :: Vector LInt -- mpisize + 1
      , _gMirrors                :: Vector Quad
      , _gMirrorTreeOffsets      :: Vector LInt
      , _gMirrorProcMirrors      :: Vector LInt
      , _gMirrorProcOffsets      :: Vector LInt
      , _gMirrorProcFronts       :: Vector LInt
      , _gMirrorProcFrontOffsets :: Vector LInt
      }
  deriving (Eq, Show, Generic)

data GhostInfo
  = GhostInfo
      { _whichTree :: Int
      , _localNum  :: Int
      , _ownerRank :: Int
      }
  deriving (Eq, Show, Generic)

type GhostMap = IntMap (IntMap GhostInfo)


-- * JSON import/export
------------------------------------------------------------------------------
instance ToJSON   Ghost
instance FromJSON Ghost

instance ToJSON   GhostInfo
instance FromJSON GhostInfo


-- * Lenses
------------------------------------------------------------------------------
makeLenses ''Ghost
makeLenses ''GhostInfo


-- * Ghost-layer functions
------------------------------------------------------------------------------
ghostNew :: MonadIO m => P4estT -> P4estConnectType -> m GhostT
ghostNew (P4estT p) t = liftIO $ do
  GhostT <$> c'p4est_ghost_new p t

ghostDestroy :: MonadIO m => GhostT -> m ()
ghostDestroy (GhostT g) = liftIO $ c'p4est_ghost_destroy g

ghostExpand :: MonadIO m => P4estT -> GhostT -> m GhostT
ghostExpand (P4estT p) (GhostT g) = liftIO $ do
  c'p4est_ghost_expand p g
  return $ GhostT g


-- * User-level API functions
------------------------------------------------------------------------------
{-- }
ghostMap :: MonadIO m => P4estT -> Ghost -> m GhostMap
ghostMap (P4estT p) (GhostT g) = liftIO $ do
  gs <- toVector . ScArray $ p'p4est_ghost_t'ghosts g
  let ks = Vec.map (Ltree.unKey . quadToKey . fromQuadrantT) gs
      go return . Ltree.unKey . quadToKey
--}

-- * Queries
------------------------------------------------------------------------------
-- | Checks if quadrant exists in the local forest or the ghost layer.
quadrantExists :: MonadIO m => P4estT -> GhostT -> CInt -> QuadrantT -> m Bool
quadrantExists (P4estT p) (GhostT g) t (QuadrantT q) = liftIO $ do
  ScArray ar <- SC.fromVector (Vec.replicate 4 0 :: Vector CInt)
  res <- c'p4est_quadrant_exists p g t q ar nullPtr nullPtr
  SC.destroy $ ScArray ar
  return $ toBool res

{-- }
-- | Checks if quadrant exists in the local forest or the ghost layer.
--   TODO:
faceQuadrantExists :: MonadIO m => P4estT -> GhostT -> CInt -> QuadrantT -> m Bool
faceQuadrantExists (P4estT p) (GhostT g) t (QuadrantT q) = liftIO $ do
  ScArray ar <- SC.fromVector (Vec.replicate 4 0 :: Vector CInt)
  res <- c'p4est_face_quadrant_exists p g t q ar nullPtr nullPtr
  SC.destroy $ ScArray ar
  return $ toBool res
--}


-- * Import/export
------------------------------------------------------------------------------
fromGhostT :: MonadIO m => GhostT -> m Ghost
fromGhostT (GhostT p) = liftIO $ do
--   let p = castPtr p' :: Ptr C'p4est_ghost_t
  mn <- peek $ p'p4est_ghost_t'mpisize p
  nt <- peek $ p'p4est_ghost_t'num_trees p
  bt <- peek $ p'p4est_ghost_t'btype p
  let mn' = fromIntegral mn + 1
      nt' = fromIntegral nt + 1
  -- ghosts:
  gs <- toVector . ScArray $ p'p4est_ghost_t'ghosts p
  gt <- Mut.new nt' >>= \ar -> Mut.unsafeWith ar $ \q -> do
    s <- castPtr <$> peek (p'p4est_ghost_t'tree_offsets p)
    copyBytes q s (nt'*sizeOf (undefined :: LInt))
    Vec.unsafeFreeze ar
  gp <- Mut.new mn' >>= \ar -> Mut.unsafeWith ar $ \q -> do
    s <- castPtr <$> peek (p'p4est_ghost_t'proc_offsets p)
    copyBytes q s (mn'*sizeOf (undefined :: LInt))
    Vec.unsafeFreeze ar
  -- mirrors:
  ms <- toVector . ScArray $ p'p4est_ghost_t'mirrors p
  mt <- Mut.new nt' >>= \ar -> Mut.unsafeWith ar $ \q -> do
    s <- castPtr <$> peek (p'p4est_ghost_t'mirror_tree_offsets p)
    copyBytes q s (nt'*sizeOf (undefined :: LInt))
    Vec.unsafeFreeze ar
  mp <- Mut.new mn' >>= \ar -> Mut.unsafeWith ar $ \q -> do
    s <- castPtr <$> peek (p'p4est_ghost_t'mirror_proc_offsets p)
    copyBytes q s (mn'*sizeOf (undefined :: LInt))
    Vec.unsafeFreeze ar
  let nm = fromIntegral $ Vec.last mt
--       np = fromIntegral $ Vec.last mp
  mm <- Mut.new nm >>= \ar -> Mut.unsafeWith ar $ \q -> do
    s <- castPtr <$> peek (p'p4est_ghost_t'mirror_proc_mirrors p)
    copyBytes q s (nm*sizeOf (undefined :: LInt))
    Vec.unsafeFreeze ar
  mq <- peek $ p'p4est_ghost_t'mirror_proc_front_offsets p
  (mo, mf) <- if castPtr mq == nullPtr then
      return (Vec.empty, Vec.empty)
    else do
      mo <- Mut.new mn' >>= \ar -> Mut.unsafeWith ar $ \q -> do
        s <- castPtr <$> peek (p'p4est_ghost_t'mirror_proc_front_offsets p)
        copyBytes q s (mn'*sizeOf (undefined :: LInt))
        Vec.unsafeFreeze ar
      let nq = fromIntegral $ Vec.last mo
      mf <- Mut.new nq >>= \ar -> Mut.unsafeWith ar $ \q -> do
        s <- castPtr <$> peek (p'p4est_ghost_t'mirror_proc_fronts p)
        copyBytes q s (nq*sizeOf (undefined :: LInt))
        Vec.unsafeFreeze ar
      return (mo, mf)
  return $ Ghost mn nt bt gs gt gp ms mt mm mp mf mo

-- ** JSON save/load
------------------------------------------------------------------------------
saveGhost :: MonadIO m => FilePath -> Ghost -> m ()
saveGhost fp = liftIO . Lazy.writeFile fp . encode

loadGhost :: MonadIO m => FilePath -> m (Maybe Ghost)
loadGhost  = liftIO . fmap decode . Lazy.readFile


-- * Helper functions
------------------------------------------------------------------------------
quadrantFindOwner :: MonadIO m => P4estT -> LInt -> CInt -> QuadrantT -> m CInt
quadrantFindOwner (P4estT p) t f (QuadrantT q) = liftIO $ do
  c'p4est_quadrant_find_owner p t f q

------------------------------------------------------------------------------
-- | These fields are already set, so this function is pointless?
setQuadToTree :: Ghost -> Ghost
setQuadToTree ghst =
  let js = Vec.map fromIntegral $ p2i $ ghst^.gTreeOffsets
--       ks = Vec.map fromIntegral $ p2i $ ghst^.gMirrorTreeOffsets
      go = Vec.zipWith (qTree .~)
--   in  gMirrors %~ go ks $ gGhosts %~ go js $ ghst
  in  gGhosts %~ go js $ ghst

-- | Use this function to overwrite the 'local_num' values with the MPI rank
--   of the quadrant, instead.
setQuadToProc :: Ghost -> Ghost
setQuadToProc ghst =
  let js = Vec.map fromIntegral $ p2i $ ghst^.gProcOffsets
--       ks = Vec.map fromIntegral $ p2i $ ghst^.gMirrorProcOffsets
      go = Vec.zipWith (qRank .~)
--   in  gMirrors %~ go ks $ gGhosts %~ go js $ ghst
  in  gGhosts %~ go js $ ghst
