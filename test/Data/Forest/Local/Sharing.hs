{-# LANGUAGE DataKinds, MultiWayIf, ScopedTypeVariables, TupleSections #-}

module Data.Forest.Local.Sharing where

import           Control.Concurrent                (threadDelay)
import           Control.DeepSeq
import           Control.Distributed.MPI.Handy
import           Control.Lens                      ((^.))
import           Control.Monad.IO.Class
import           Control.Monad.State
import           Data.Bits.Handy                   (int, (<<%))
import           Data.Coerce
import qualified Data.List                         as List
import           Data.Maybe
import qualified Data.Set                          as Set
import           Data.Set.Sets
import           Foreign.C.Types
import           Foreign.Marshal
import           Foreign.Ptr
import           Foreign.Storable
import           GHC.Int
import           Linear                            (V2 (..))
import           System.Environment
import           System.Random
import           Test.Tasty
import           Test.Tasty.HUnit
import           Text.Printf

import           Bindings.P4est.P4est
import           Bindings.P4est.P4estBits
import           Bindings.P4est.P4estCommunication
import           Data.Forest.Local                 as Lo
import           Data.Forest.P4est                 as P4
import           Data.Scientific.SC                as SC

import           Data.Vector.Extras                (bsearch)


-- * Test helper functions
------------------------------------------------------------------------------
p4estRankOverlaps :: P4estT -> Leaf 2 -> IO (Rank, Rank)
p4estRankOverlaps p4 q = with (l2quad q) $ \pq -> do
  let t = fromIntegral $ q^._treeId :: CInt
      f = toEnum . fromIntegral :: CInt -> Rank
      g = fromIntegral . fromEnum :: Rank -> CInt
  rk <- commRank commWorld

  lq <- alloca $ \(qq :: Ptr Quad) -> do
    c'p4est_quadrant_last_descendant (castPtr pq) (castPtr qq) QuadMaxLevel
    peek qq

  -- compare first and last quads
  fr <- c'p4est_comm_find_owner (coerce p4) t (castPtr pq) (g rk)
  lr <- with lq $ \pl -> do
    c'p4est_comm_find_owner (coerce p4) t (castPtr pl) (g rk)

  pure (f fr, f lr)

p4estCompare :: Leaf 2 -> Leaf 2 -> IO Ordering
p4estCompare l0 l1 = do
  let q0 = l2quad l0
      q1 = l2quad l1
  with q0 $ \p0 -> with q1 $ \p1 -> do
    r <- c'p4est_quadrant_compare (castPtr p0) (castPtr p1)
    if | r < 0 -> pure LT
       | r > 0 -> pure GT
       | True  -> pure EQ

------------------------------------------------------------------------------
-- | Refine the given quadrant if the result from this callback is `True`.
--
refineFn :: Int -> Int -> P4.Quad -> IO Bool
refineFn lmin lmax quad
  | depth quad <lmin = pure True
  | depth quad>=lmax = pure False
  | otherwise        = randomIO

------------------------------------------------------------------------------
-- | Generate @lnum@ random leaves within the (first tree of the) forest.
--
randomLeaves :: Int -> Int -> Int -> IO [Leaf 2]
randomLeaves lmin lmax lnum = do
  let go :: Int -> IO [Leaf 2]
      go 0 = pure []
      go n = do
        l <- randomRIO (lmin, lmax)
        x <- randomRIO (0, 2^l-1)
        y <- randomRIO (0, 2^l-1)
        (Leaf (fn l <$> V2 x y) (fromIntegral l) 0:) <$> go (n-1)
      fn l = (<<% (QuadMaxLevel - l + 1))
      {-# INLINE fn #-}
  go lnum

leafRanks :: MonadIO m => P4estT -> Leaf 2 -> m (Leaf 2, (Rank, Rank))
leafRanks p4 l = liftIO $ (l,) <$> p4estRankOverlaps p4 l

------------------------------------------------------------------------------
createForest :: IO ([(Leaf 2, (Rank, Rank))], LocalForest 2)
createForest  = do
  args <- getArgs

  -- Use 'mpi-hs' and 'p4est' for the initial mesh construction
--   SC.withLoggedSC "out/tests" args $ do
  SC.withSC args $ do

    -- Build the initial "connectivity" (i.e., "macro-mesh")
    mesh <- P4.unitSquare

    -- Build the initial parallel-forest
    vals <- P4.withP4estM mesh $ do
      -- Refine
      let (lmin, lmax) = (2, 4)
      refineWithIO (refineFn lmin lmax) True

      -- Balance, and partition
      P4.partition
      P4.balance P4.ConnectFull
      P4.partition

      -- Add ghosts and node-labels, then share nodes and build subforests
      P4.addGhost P4.ConnectFull
      P4.addNodes

      P4.svgWriteFile "out/p4est"

      -- TODO: randomly generate some leaves and then compute the rank-ranges
      p4st <- _p4est <$> get
      lefs <- liftIO $ randomLeaves (lmin-1) (lmax+1) 2000
      lers <- leafRanks p4st `mapM` lefs

      -- Return a local-forest
      deepseq lers $ (lers,) <$> Lo.mkLocalForest

{-- }
    rank <- fromEnum <$> SC.mpiRank
    let lfn = printf "out/p4est%04d.yaml" rank :: FilePath
    Lo.yamlSaveLocalForest lfn $ Lo.fixNodeCoords (toEnum rank) (snd vals)
--}

    vals `deepseq` P4.meshDestroy mesh
    pure vals

------------------------------------------------------------------------------
manualTests :: IO ()
manualTests  = do
  --  1. Create a new parallel forest
  --  2. Construct a @LocalForest@ from the 'p4est' object
  ----------------------------------------------------------------------------
  (lefs, lofo) <- createForest
  print $ lofo^.firstLeaves
  printf "Max quad level: %d\n" (QuadMaxLevel :: Int)

  --  3. Randomly generate quads, and check @leafRankOverlaps@ results against
  --     those produced by 'p4est' routines
  ----------------------------------------------------------------------------
  mapM_ `flip` zip [0..] lefs $ \(j :: Int, (l, (r, s))) -> do
    let (b, u) = Lo.leafRankOverlaps lofo l
    if (b == r && u == s)
      then pure () -- putStrLn "OK"
      else do
        shleaf j l
        shleaf j (firstLeaf l)
        shleaf j (lastLeaf l)
        printf "%04d: rank(Leaf): %s\n" j . show $ bsearch (lofo^.firstLeaves) l
        printf "%04d: firstLeaf < Leaf: %s\n" j . show $ firstLeaf l < l
        printf "%04d: Leaf <  lastLeaf: %s\n" j . show $ l < lastLeaf l
        oa <- p4estCompare (firstLeaf l) l
        ob <- p4estCompare l (lastLeaf l)
        printf "%04d: firstLeaf < Leaf: %s\n" j $ show oa
        printf "%04d: Leaf <  lastLeaf: %s\n" j $ show ob
        printf "%04d: failed: [p4est] (%d, %d) /== (%d, %d) [pforest]\n" j
          (fromEnum r) (fromEnum s) (fromEnum b) (fromEnum u)
        error $ printf "%04d: exiting ..." j

shleaf :: Int -> Leaf 2 -> IO ()
shleaf j l = do
  printf "%04d: Leaf\n" j
  printf "%04d:   %s\n" j . show $ l^._coord
  printf "%04d:   %s\n" j . show $ l^._level
  printf "%04d:   %s\n" j . show . int $ l^._treeId

------------------------------------------------------------------------------
-- | Test overlaps calculated with 'p4est' routines, vs my own.
testOverlaps :: LocalForest 2 -> Leaf 2 -> (Rank, Rank) -> Assertion
testOverlaps lofo l (r, s) = do
  let (b, u) = Lo.leafRankOverlaps lofo l
  b @?= r
  u @?= s


-- * Unit tests
------------------------------------------------------------------------------
-- | Make sure that normalising and rebasing of parametric coordinates works.
--
--   FIXME: does not work due to memory-allocation problems (with the SC
--     library).
--
test_leaf_overlaps :: Assertion
test_leaf_overlaps  = do
  --  1. Create a new parallel forest
  --  2. Construct a @LocalForest@ from the 'p4est' object
  ----------------------------------------------------------------------------
  (lefs, lofo) <- createForest

  --  3. Randomly generate quads, and check @leafRankOverlaps@ results against
  --     those produced by 'p4est' routines
  ----------------------------------------------------------------------------
  mapM_ `flip` lefs $ \(l, (r, s)) -> do
    putStrLn "Hi"
    testOverlaps lofo l (r, s)


-- * Top-level testbench
------------------------------------------------------------------------------
tests :: TestTree
tests  = testGroup "pforest_sharing"
  [ testCase "test_leaf_overlaps" test_leaf_overlaps
  ]
