------------------------------------------------------------------------------
-- |
-- Module      : Main
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- A very basic demonstration of some parallel-forest functionality.
-- 
-- Changelog:
--  + 15/06/2019  --  initial file;
-- 
------------------------------------------------------------------------------

module Main where

import System.Environment (getArgs)
import Control.Monad.IO.Class
import Text.Printf
import Data.Scientific.SC as SC
import Data.Forest.P4est as P4

main :: IO ()
main  = do
  _ <- getArgs >>= \args -> P4.withSC args $ do
    P4.withSquare $ do
      rank <- fromEnum <$> SC.mpiRank
      liftIO $ printf "basic 'square' example: MPI rank = %d\n" rank
      P4.vtkWriteFile "out/square"
  pure ()
