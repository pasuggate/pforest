{-# LANGUAGE DataKinds, DeriveFoldable, DeriveFunctor, DeriveGeneric,
             FlexibleContexts, FlexibleInstances, GeneralizedNewtypeDeriving,
             MultiParamTypeClasses, OverloadedStrings, PatternSynonyms,
             RankNTypes, ScopedTypeVariables, StandaloneDeriving,
             TemplateHaskell, TupleSections, TypeFamilies, TypeOperators,
             ViewPatterns #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Mesh
-- Copyright   : (C) Patrick Suggate 2019
-- License     : GPL3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Functions for working with the "connectivities" of parallel forests.
--
-- == Changelog:
--  - 2019-05-26  --  initial file;
--
-- == TODO:
--  - additional queries for working with boundaries and corners;
--
------------------------------------------------------------------------------

module Data.Forest.Mesh
  ( module Data.Forest.Linear
  -- type-families for sized objects:
  , FaceVec
  , NodeVec
  -- type-classes for finding neighbours:
  , HasFaceNeighbours (..)
  , HasDiagNeighbours (..)
  , HasEdgeNeighbours (..)
  -- nodes & coordinates:
  , Node (..)
  , pos
  , lvl
  , tId
  , CoordMap (..)
  , _coordMap
  -- directions & orientations:
  , Dir (..)
  , dir
  , pattern West
  , pattern East
  , pattern South
  , pattern North
  , Diag (..)
  , diag
  , pattern SW
  , pattern SE
  , pattern NW
  , pattern NE
  , Orient (..)
  , orient
  , flipDir
  , flipDiag
  , flipOrient
  , mirrorOrient
  -- Export and import:
  -- ------------------
  , saveCoordMap
  , loadCoordMap
  , yamlCoordMap
  -- Helper data-types:
  -- ------------------
  , Box
  --   axis-aligned bounding-boxes:
  , AABB (..)
  , lower
  , delta
--   , aabb
  --   constants:
  , treeMaxLevel
  , quadMaxLevel
  ) where

import           Control.DeepSeq
import           Control.Lens         (makeLenses, (%~), (&), (^.))
import           Data.Aeson
import           Data.Bits.Handy      (int, testBit, xor, (>>%))
import           Data.Bool            (bool)
import qualified Data.Vector          as Box
import           Foreign.Storable
import           GHC.Generics         (Generic)
import           GHC.Int
import           GHC.TypeNats
import           Linear.Handy         hiding (unit)

import           Bindings.P4est.P4est
import           Data.Forest.Linear
import           Data.Forest.Node
import           Data.IntMap.CoordMap
import           Data.Tree.Linear     (Translatable (..))


-- * Convenience type-families
------------------------------------------------------------------------------
type family   FaceVec (d :: Nat) :: * -> *
type instance FaceVec  1          = V2
type instance FaceVec  2          = V4
type instance FaceVec  3          = V6

type family   NodeVec (d :: Nat) :: * -> *
type instance NodeVec  1          = V2
type instance NodeVec  2          = V4
type instance NodeVec  3          = V6

type instance Vec 2 Int32 = V2 Int32
type instance Vec 3 Int32 = V3 Int32

type instance DimsOf (Node   d) = d
type instance DimsOf (Dir    d) = d
type instance DimsOf (Orient d) = d
type instance DimsOf (Diag   d) = d


-- * Type-classes for finding adjacent cells in meshes
------------------------------------------------------------------------------
class HasFaceNeighbours p where
  faceNbrs     :: MeshOf p -> p -> [p]
  halfFaceNbrs :: MeshOf p -> p -> Dir (DimsOf p) -> [p]

class HasDiagNeighbours p where
  diagNbrs     :: MeshOf p -> p -> [p]
  halfDiagNbrs :: MeshOf p -> p -> [p]

class HasEdgeNeighbours p where
  edgeNbrs     :: MeshOf p -> p -> [p]
  halfEdgeNbrs :: MeshOf p -> p -> Dir (DimsOf p) -> [p]


-- * Types for RUB-splines meshes
------------------------------------------------------------------------------

-- ** Directional data types
------------------------------------------------------------------------------
newtype Dir (d :: Nat) = Dir { _dir :: Int8 }
  deriving (Eq, Ord, Enum, Num, Real, Integral, Storable, Show, Generic)

------------------------------------------------------------------------------
newtype Orient (d :: Nat) = Orient { _orient :: Int8 }
  deriving (Eq, Ord, Enum, Num, Real, Integral, Storable, Show, Generic)

------------------------------------------------------------------------------
-- | Z-index representations for diagonals/corners.
newtype Diag (d :: Nat) = Diag { _diag :: Int8 }
  deriving (Eq, Ord, Enum, Num, Real, Integral, Storable, Show, Generic)

-- ** Additional data types
------------------------------------------------------------------------------
-- | Axis-Aligned Bounding-Box.
data AABB a
  = AABB
      { _lower, _delta :: V3 a
      }
  deriving (Eq, Show, Generic)

-- ** Convenience aliases
------------------------------------------------------------------------------
-- | Alias for vectors of boxed objects.
type Box a = Box.Vector a


-- * Convenience patterns
------------------------------------------------------------------------------
-- | Directions across faces, and Z-index ordering.
pattern West :: Dir 2
pattern West <- ((== Dir 0) -> True)
  where West  = Dir 0

pattern East :: Dir 2
pattern East <- ((== Dir 1) -> True)
  where East  = Dir 1

pattern South :: Dir 2
pattern South <- ((== Dir 2) -> True)
  where South  = Dir 2

pattern North :: Dir 2
pattern North <- ((== Dir 3) -> True)
  where North  = Dir 3

------------------------------------------------------------------------------
-- | Directions for diagonals and corners, relative to the node, or cell-
--   centre, and stored using Z-index ordering.
pattern SW :: Diag 2
pattern SW <- ((== Diag 0) -> True)
  where SW  = Diag 0

pattern SE :: Diag 2
pattern SE <- ((== Diag 1) -> True)
  where SE  = Diag 1

pattern NW :: Diag 2
pattern NW <- ((== Diag 2) -> True)
  where NW  = Diag 2

pattern NE :: Diag 2
pattern NE <- ((== Diag 3) -> True)
  where NE  = Diag 3


-- * Lenses and instances
------------------------------------------------------------------------------
makeLenses ''Dir
makeLenses ''Diag
makeLenses ''Orient
makeLenses ''AABB

-- ** Derived instances for JSON import/export
------------------------------------------------------------------------------
instance ToJSON   (Dir 2)
instance FromJSON (Dir 2)

instance ToJSON   (Dir 3)
instance FromJSON (Dir 3)

------------------------------------------------------------------------------
instance ToJSON   (Orient 2)
instance FromJSON (Orient 2)

instance ToJSON   (Orient 3)
instance FromJSON (Orient 3)

------------------------------------------------------------------------------
instance ToJSON   a => ToJSON   (AABB a)
instance FromJSON a => FromJSON (AABB a)

-- ** Strictness instances
------------------------------------------------------------------------------
instance NFData (Diag 2)
instance NFData (Dir  2)
instance NFData (Diag 3)
instance NFData (Dir  3)

-- ** Standard instances
------------------------------------------------------------------------------
instance Bounded (Dir 2) where
  minBound = Dir 0
  maxBound = Dir 3
  {-# INLINE minBound #-}
  {-# INLINE maxBound #-}

instance Bounded (Dir 3) where
  minBound = Dir 0
  maxBound = Dir 7
  {-# INLINE minBound #-}
  {-# INLINE maxBound #-}

------------------------------------------------------------------------------
instance Bounded (Diag 2) where
  minBound = Diag 0
  maxBound = Diag 3
  {-# INLINE minBound #-}
  {-# INLINE maxBound #-}

instance Bounded (Diag 3) where
  minBound = Diag 0
  maxBound = Diag 7
  {-# INLINE minBound #-}
  {-# INLINE maxBound #-}

-- ** Simple transformations
------------------------------------------------------------------------------
instance Translatable (Node 2) where
  type InDir (Node 2) = Dir 2
  translate n (Dir d) = Just n' where
    n' = n & pos %~ (^+^ ow)
    ow = [V2 dw 0, V2 0 dw]!!(int d >>% 1)
    dw = bool negate id (d `testBit` 0) $ width n


-- * Direction and orientation functions
------------------------------------------------------------------------------
-- | Flip the LSB to also flip the direction by 180 degress.
flipDir :: Dir d -> Dir d
flipDir  = dir %~ xor 1
{-# INLINE flipDir #-}

-- | Flip all bits to point in the opposite (diagonal) direction.
flipDiag :: forall d. Bounded (Diag d) => Diag d -> Diag d
flipDiag  = diag %~ xor ((maxBound :: Diag d)^.diag)

------------------------------------------------------------------------------
-- | Flip the LSB to also flip the direction by 180 degress.
flipOrient :: Orient d -> Orient d
flipOrient  = orient %~ xor 1
{-# INLINE flipOrient #-}

-- | Flip the orientation bit, to perform a mirror-flip of the orientation.
mirrorOrient :: Orient d -> Orient d
mirrorOrient  = orient %~ xor 4
{-# INLINE mirrorOrient #-}


-- * Constants
------------------------------------------------------------------------------
-- | Maximum level that a leaf-node can have.
quadMaxLevel :: Integral i => i
quadMaxLevel  = c'P4EST_QMAXLEVEL
{-# INLINE quadMaxLevel #-}

-- | Maximum number of levels that a tree can have.
treeMaxLevel :: Integral i => i
treeMaxLevel  = c'P4EST_MAXLEVEL
{-# INLINE treeMaxLevel #-}
