{-# LANGUAGE BangPatterns, DataKinds, FlexibleContexts, FlexibleInstances,
             ScopedTypeVariables, TupleSections, TypeFamilies #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.Traverse
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Traversal of local regions of a forest, as if they are "Cartesian."
--
-- TODO:
--
------------------------------------------------------------------------------

module Data.Forest.P4est.Traverse
  ( floodFillM
  , gatherBy
    -- more searches and accumulations:
  , euclidSearchL
  , euclidAccumLR
  , euclidAccumL
    -- adjacent-cell operations:
  , findFaceNbr
  , findNbrsBy
    -- helpers:
  , coordDir
  , orientDir
  ) where

import           Control.Arrow                     (second)
import           Data.Bits.Handy                   (testBit, (.&.), (.|.))
import           Data.Bool                         (bool)
import           Data.Foldable                     (foldl')
import qualified Data.Map                          as Map
import           Data.Map.Maps                     ()
import           Data.Maybe                        (catMaybes, fromJust)
import           Data.Set.Sets
import           Data.Tuple                        (swap)
import           Data.Vector.Fusion.Stream.Monadic (Step (..))
import           GHC.Types                         (SPEC (..))
import           Linear                            (V2 (..), V3 (..), (!*))

import           Data.Forest.Mesh.Macro
import           Data.Forest.P4est.P4est
import           Data.Tree.Linear                  (Translatable (..))


-- * Space-cadre stuff
------------------------------------------------------------------------------
-- | Given the index of the starting-leaf, gather its neighbours using a
--   breadth-first traversal, and filtering using the given predicate.
--
--   Output:
--   + subforest built from adjacent leaves;
--   + remaining subforest with the gathered-region removed.
--
gatherBy ::
     InDir        (Fkey d) ~ Dir d
  => Bounded      (Dir  d)
  => Translatable (Fkey d)
  => Rebaseable   (Fkey d)
  => SetInsert    (Fkey d) (Lforest d)
  => SetIndex     (Fkey d) (Lforest d)
  => SetBuild     (Fkey d) (Lforest d)
  => MeshOf       (Fkey d) ~ MacroMesh d
  => MacroMesh d
  -> (Fkey d -> Bool)
  -> Fkey d
  -> Lforest d
  -> (Lforest d, Lforest d)
gatherBy mm inc k0 lf = go SPEC (bare, lf, [k0]) where
  go !_ (gf, rf, qs@(q:rs))
    | null qs   = (gf, rf)
    | dry rf    = (gf, rf)
    | q <? rf   = go SPEC (q~>gf, rf', qs')
    | otherwise = go SPEC (   gf, rf', qs')
    where
      rf' = q <\ rf
      qs' = rs ++ findNbrsBy mm inc q
  go _ _ = error "P4est.Traverse.gatherBy: invalid input"
{-# INLINE gatherBy #-}

unStep :: Monad m => (s -> m (Step s a)) -> s -> m [a]
unStep step = go where
  go s = step s >>= \r -> case r of
    Done       -> pure []
    Skip    s' -> go s'
    Yield x s' -> (x:) <$> go s'
{-# INLINE unStep #-}


-- * User API functions
------------------------------------------------------------------------------
floodFillM :: (V3 Int -> Bool) -> V3 Int -> Quad -> P4estM [Quad]
floodFillM inc x0 q0 = do
  let step (qs, vs)
        | dry qs    = pure Done
        | q1 <? vs  = pure $ Skip (qr, vs)
        | otherwise = do
            -- calculate positions for -/+ x & y, and apply inclusion-test
            let go d
                  | inc x'    = fmap (second (x',)) <$> findFaceNbr q1 o1 d
                  | otherwise = pure Nothing
                  where x'    = coordDir x1 d
            -- construct quads for [West,East,South,North]
            -- compute the orientations, if they cross tree boundaries
            rs' <- catMaybes <$> go `mapM` [West ..North]
            -- yield the quadrant, and update the queue:
            pure $ Yield q1 (flip (~>) `foldl'` qr $ rs', q1~>vs)
        where
          -- pop the "top"-quad:
          ((q1, (x1, o1)), qr) = Map.deleteFindMin qs
  unStep step (unit (q0,(x0,Orient 0)), bare :: Set Quad)

------------------------------------------------------------------------------
-- | Perform a local, Euclidean search while accumulating the result of each
--   evaluation.
euclidSearchL ::
  (V3 Int -> Bool) -> (a -> Quad -> a) -> V3 Int -> a -> Quad -> P4estM a
euclidSearchL inc acc x0 s0 = fmap head . euclidAccumLR inc acc x0 s0

-- | Perform a local, Euclidean search while accumulating the result of each
--   evaluation.
--
--   NOTE: this version accumulates all intermediate products as well.
euclidAccumL :: (V3 Int -> Bool) -> (a -> Quad -> a) -> V3 Int -> a -> Quad ->
                P4estM [a]
euclidAccumL inc acc x0 s0 = fmap reverse . euclidAccumLR inc acc x0 s0

-- | Perform a local, Euclidean search while accumulating the result of each
--   evaluation.
--
--   NOTE: this version accumulates all intermediate products as well, and the
--     returned list is in reverse-ordering.
euclidAccumLR :: (V3 Int -> Bool) -> (a -> Quad -> a) -> V3 Int -> a -> Quad ->
                 P4estM [a]
euclidAccumLR inc acc x0 s0 q0 = do
  let step (qs, st, vs)
        | dry qs    = pure Done
        | q1 <? vs  = pure $ Skip (qr, st, vs)
        | otherwise = do
            -- calculate positions for -/+ x & y, and apply inclusion-test
            let go d
                  | inc x'    = fmap (second (x',)) <$> findFaceNbr q1 o1 d
                  | otherwise = pure Nothing
                  where x'    = coordDir x1 d
                st' = acc st q1
            -- construct quads for [West,East,South,North]
            -- compute the orientations, if they cross tree boundaries
            rs' <- catMaybes <$> go `mapM` [West ..North]
            -- yield the quadrant, and update the queue:
            pure $ Yield st' (flip (~>) `foldl'` qr $ rs', st', q1~>vs)
        where
          -- pop the "top"-quad:
          ((q1, (x1, o1)), qr) = Map.deleteFindMin qs
      eval qv = step qv >>= \r -> case r of
        Done        -> pure []
        Skip    qv' -> eval qv'
        Yield x qv' -> (x:) <$> eval qv'
  eval (unit (q0, (x0, Orient 0)), s0, bare :: Set Quad)


-- ** Additional API functions
------------------------------------------------------------------------------
-- | Move in direction `d`, for the quad with the given orientation.
--   Output:
--     Just (q, o) -- if there is a valid quad in the computed direction, and
--                    return the updated orientation as well.
--
--   TODO: does not handle mirror-flips.
findFaceNbr :: Quad -> Orient 2 -> Dir 2 -> P4estM (Maybe (Quad, Orient 2))
findFaceNbr q0 o0 d0 = do
  let d'  = orientDir o0 d0
      upd = second (orientUpd o0 d' . oppDir) . swap
--   res <- quadFaceOnBoundary q0 d' >>= \b -> if b
--     then pure Nothing
--     else fmap upd <$> quadFaceNeighbourExtra q0 d'
--   pure res
  fmap upd <$> quadFaceNeighbourExtra q0 d'


-- * Helper functions
------------------------------------------------------------------------------
-- | Compute the "adjacent" coordinate in the indicated direction.
--   NOTE: uses 'p4est'-depth (of 29), instead of 'Ltree'-depth (of 15).
coordDir :: V3 Int -> Dir 2 -> V3 Int
coordDir (V3 x y l) d = case d of
  West  -> V3 (x-s) y l
  East  -> V3 (x+s) y l
  South -> V3 x (y-s) l
  North -> V3 x (y+s) l
  _     -> error "P4est.Traverse.coordDir: invalid direction"
  where s = quadrantWidth l -- 1 <<% (maxDepth - l)
{-# INLINE coordDir #-}

-- | Transform the given direction, using a coordinate-operator built from
--   the orientation-value.
orientDir :: Orient 2 -> Dir 2 -> Dir 2
orientDir o = mkDir . (toTransform o !*) . toAxes where
  mkDir :: V2 Int -> Dir 2
  mkDir  = fromJust . fromAxes
  {-# INLINE mkDir #-}

-- TODO: does not handle mirror-flips.
orientUpd :: Orient 2 -> Dir 2 -> Dir 2 -> Orient 2
orientUpd (Orient o) d0 d1
  | d0 == d1  = Orient o
  | otherwise = orientUpd (Orient o') d0 $ ccw d1
  where
    m = o `testBit` 2
    r = o .&. 3
    o' = bool 0 4 m .|. succ r `mod` 4
    ccw West  = South
    ccw South = East
    ccw East  = North
    ccw North = West
    ccw _     = error "P4est.Traverse.orientUpd: invalid direction"
    {-# INLINE ccw #-}
{-# INLINE orientUpd #-}
