{-# LANGUAGE DataKinds #-}
module Data.Forest.Local.Tests where

import Test.Tasty
import Test.Tasty.HUnit
-- import           Test.HUnit              (Assertion, (@?=))
import Data.Maybe
import Data.Set.Sets
import qualified Data.Set as Set
import qualified Data.List as List
import Linear (V2 (..))

import Data.Forest.Local


tests :: TestTree
tests  = testGroup "pforest_local"
  [ testCase "test_rebase_simple" test_rebase_simple
  , testCase "test_rebase_harder" test_rebase_harder
  , testCase "test_leaves_query"  test_leaves_query
  ]


-- * Unit tests
------------------------------------------------------------------------------
-- | Make sure that normalising and rebasing of parametric coordinates works.
test_rebase_simple :: Assertion
test_rebase_simple  = do
  let ds = maxWidth (undefined :: Leaf 2)
      dh = ds `div` 2
      n0 = Node (V2 0 0) 0 0 :: Node 2
      n1 = Node (V2 (-ds) 0) 0 0 :: Node 2
      -- node above tree-0
      n2 = Node (V2 dh (ds+dh)) 1 0 :: Node 2
      r2 = Node (V2 dh dh) 1 1 :: Node 2
      -- node left of tree-1
      n3 = Node (V2 (ds+dh) dh) 1 1 :: Node 2
      r3 = Node (V2 dh dh) 1 2 :: Node 2
      -- node right of tree-2
      n4 = Node (V2 (dh-ds) dh) 1 2 :: Node 2
      r4 = Node (V2 dh dh) 1 1 :: Node 2
      -- test data & functions
      mesh = macroLShape
      purm = pure . rebase mesh
  -- origin
  o0 <- purm n0
  o0 @?= Just n0
  -- out-of-domain
  o1 <- purm n1
  o1 @?= Nothing
  -- re-centre in tree 1
  o2 <- purm n2
  o2 @?= Just r2
  -- re-centre in tree 2
  o3 <- purm n3
  o3 @?= Just r3
  -- re-centre in tree 1
  o4 <- purm n4
  o4 @?= Just r4

-- | Make sure that normalising and rebasing of parametric coordinates works.
test_rebase_harder :: Assertion
test_rebase_harder  = do
  let ds = maxWidth (undefined :: Leaf 2)
      dh = ds `div` 2
      -- node in 3rd tree, based in 1st:
      n5 = Node (V2 (ds+dh) ds) 1 0 :: Node 2
      r5 = Node (V2 dh 0) 1 2 :: Node 2
      -- node in 1st tree, based in 3rd:
      n6 = Node (V2 0 (-dh)) 1 2 :: Node 2
      r6 = Node (V2 ds dh) 1 0 :: Node 2
      -- COC-point test:
      -- test data & functions
      mesh = macroLShape
      purm = pure . rebase mesh
  -- rebase node to 3rd tree, from 1st:
  o5 <- purm n5
  o5 @?= Just r5
  -- rebase node to 1st tree, from 3rd:
  o6 <- purm n6
  o6 @?= Just r6

------------------------------------------------------------------------------
-- | Make sure that normalising and rebasing of parametric coordinates works.
test_leaves_query :: Assertion
test_leaves_query  = do
  let ds = maxWidth (undefined :: Leaf 2)
      dh = ds `div` 2
      -- find the leaves at the left-side T-junction:
      n0 = Node (V2 0 ds) 1 0 :: Node 2
      r0 = [ Leaf (fromIntegral <$> V2 (-dh) dh) 1 0
           , Leaf (fromIntegral <$> V2   0   dh) 1 0
           , Leaf (fromIntegral <$> V2 (-dh) ds) 1 0
           , Leaf (fromIntegral <$> V2   0   ds) 1 0
           ] :: [Leaf 2]
      -- find the OoD leaves at the left-side T-junction:
      n1 = n0
      r1 = [ Leaf (fromIntegral <$> V2 (-dh) dh) 1 0
           , Leaf (fromIntegral <$> V2 (-dh) ds) 1 0
           ] :: [Leaf 2]
      -- find the leaves at the top, left corner:
      n2 = Node (V2 0 ds) 1 1 :: Node 2
      r2 = [ Leaf (fromIntegral <$> V2 (-dh) dh) 1 1
           , Leaf (fromIntegral <$> V2   0   dh) 1 1
           , Leaf (fromIntegral <$> V2 (-dh) ds) 1 1
           , Leaf (fromIntegral <$> V2   0   ds) 1 1
           ] :: [Leaf 2]
      -- find the OoD leaves at the top, left corner:
      n3 = n2
      r3 = [ Leaf (fromIntegral <$> V2 (-dh) dh) 1 1
           , Leaf (fromIntegral <$> V2 (-dh) ds) 1 1
           , Leaf (fromIntegral <$> V2   0   ds) 1 1
           ] :: [Leaf 2]
      -- find the leaves at the concave corner:
      n4 = Node (V2 ds ds) 1 0 :: Node 2
      r4 = [ Leaf (fromIntegral <$> V2 dh dh) 1 0
           , Leaf (fromIntegral <$> V2 ds dh) 1 0
           , Leaf (fromIntegral <$> V2 dh ds) 1 0
           , Leaf (fromIntegral <$> V2 ds ds) 1 0
           ] :: [Leaf 2]
      -- find the leaves at the concave corner:
      n5 = n4
      r5 = [ Leaf (fromIntegral <$> V2 ds dh) 1 0
           ] :: [Leaf 2]
      -- find the "outers" at the left-side T-junction:
      n6 = n0
      r6 = [ Node (V2 (-dh) ds) 1 0, n6 ] :: [Node 2]
      -- test data & functions
      mesh = macroLShape
      purl = pure . leavesOfNode
      purf = pure . filter (isNothing . rebase mesh) . leavesOfNode
      furp = flat . List.foldl1' (/\) . map (Set.fromList . nodesOfLeaf)

  -- find the leaves at the left-side T-junction:
  o0 <- purl n0
  o0 @?= r0
  -- find the OoD leaves at the left-side T-junction:
  o1 <- purf n1
  o1 @?= r1
  -- find the leaves at the top, left corner:
  o2 <- purl n2
  o2 @?= r2
  -- find the OoD leaves at the top, left corner:
  o3 <- purf n3
  o3 @?= r3
  -- find the leaves at the concave corner:
  o4 <- purl n4
  o4 @?= r4
  -- find the OoD leaves at the concave corner:
  o5 <- purf n5
  o5 @?= r5
  -- find the "outers" at the left-side T-junction:
  o6 <- furp <$> purf n6
  o6 @?= r6

{-- }
-- TODO: move to `rubs-mesh`
test_pcoord :: Assertion
test_pcoord  = do
  let ds = maxWidth (undefined :: Leaf 2)
      dh = ds `div` 2
      n0 = Node (V2 0 0) 0 0 :: Node 2
      r0 = PCoord (V3 0 0 0)
      -- test data & functions
      mesh = macroLShape
      p2pm = pure . toPCoord mesh
  -- origin
  o0 <- p2pm n0
  o0 @?= Just n0
--}
