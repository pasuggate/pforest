{-# LANGUAGE GeneralizedNewtypeDeriving, CPP, TemplateHaskell,
  PolyKinds, OverloadedStrings, PatternSynonyms, ViewPatterns,
  RankNTypes, DeriveGeneric, FlexibleInstances, StandaloneDeriving,
  TypeFamilies, TypeOperators, MultiParamTypeClasses,
  FlexibleContexts, ScopedTypeVariables, NoMonomorphismRestriction,
  TupleSections, DataKinds
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Mesh.Macro
-- Copyright   : (C) Patrick Suggate 2019
-- License     : BSD3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Functions for working with the "connectivities" of parallel forests.
-- 
-- Changelog:
--  + 26/05/2019  --  initial file;
-- 
------------------------------------------------------------------------------

module Data.Forest.Mesh.Macro
  ( Relations (..)
  , Layered (..)
  , Lindex (..)
  , HasWidth (..)
  , HasCoords (..)
  , HasTreeID (..)
  --   type-families:
  , FaceVec
  , NodeVec
  , Vec
  -- Embeddings:
  -- ===========
  , MacroType
  , MacroMesh (..)
  , macroMesh
  , transforms
  , embedding
  , FTransforms (..)
  , ftransforms
  , Bounds (..)
  , bounds
  , Vertices (..)
  , coordinates
  , vertIndices
  -- constructors:
  , buildFaceTransforms
  , buildFaceTransforms'
  , makeFaceTransforms
  -- queries:
  , nbrsOf
  , findNbrsBy
  ) where

import GHC.Generics (Generic)
import GHC.Int
import GHC.TypeNats
-- import Foreign.Storable
-- import Foreign.Ptr
import System.IO.Unsafe
import Control.Lens (makeLenses)
import Control.Monad ((<=<))
-- import Control.Monad.IO.Class
-- import Control.Distributed.MPI as MPI
import Control.DeepSeq
import Data.Maybe (fromMaybe, catMaybes)
import qualified Data.Vector as Box
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import Data.Aeson
import Linear.Handy hiding (unit)

import Data.Index.ZCurve
-- import Data.IntMap.CoordMap
import Data.Tree.Linear.Coords (Translatable (..))
import Data.Forest.Linear
-- import Data.Forest.Node
import Data.Forest.Mesh
import Data.Forest.P4est.Base
import Data.Forest.P4est.Connectivity
-- import Bindings.P4est.P4est


-- * Convenience type-families
------------------------------------------------------------------------------
-- | Select the macro-mesh data-type for the desired number of parametric
--   dimensions.
type family   MacroType (d :: Nat) :: *

------------------------------------------------------------------------------
type family   OrientType (d :: Nat) :: *
type instance OrientType 2 = Int8
type instance OrientType 3 = Int8

type instance DimsOf (MacroMesh d) = d
type instance DimsOf (FTransforms d) = d
type instance DimsOf (Bounds d) = d
type instance DimsOf (Vertices d a) = d


-- * Macro-mesh data types
------------------------------------------------------------------------------
-- | Stores the connectivity-structure of a parallel forest, along with the
--   face-crossing transforms, and the vertex information that is needed to
--   draw/export an embedding of the mesh.
data MacroMesh (d :: Nat)
  = MacroMesh { _macroMesh  :: MacroType d
              , _transforms :: Box (FTransforms d)
              , _embedding  :: Maybe (Vertices d Int32)
              } deriving (Generic)

-- ** Data types for face-crossing coordinate transforms
------------------------------------------------------------------------------
-- | Stores the face(-to-face) transforms for each face of a tree.
newtype FTransforms (d :: Nat)
  = FTransforms { _ftransforms :: FaceVec d FTransform }
  deriving (Generic)

deriving instance Eq   (FTransforms 2)
deriving instance Show (FTransforms 2)

deriving instance Eq   (FTransforms 3)
deriving instance Show (FTransforms 3)

-- ** Data types for boundary information
------------------------------------------------------------------------------
-- | Stores (for each face) whether boundary nodes count towards the total
--   number of dofs.
--   
--   NOTE: a corner dof, that is incident to two boundaries, is only counted
--     if _both_ boundaries are non-Dirichlet.
--   TODO: support more boundary-types than just homogeneous, Dirichlet
--     boundary-conditions.
--   TODO: add support for octree meshes.
newtype Bounds (d :: Nat) = Bounds { _bounds :: FaceVec d Bool }
  deriving (Generic)

deriving instance Eq       (Bounds 2)
deriving instance Show     (Bounds 2)
deriving instance Storable (Bounds 2)

deriving instance Eq       (Bounds 3)
deriving instance Show     (Bounds 3)
deriving instance Storable (Bounds 3)

-- ** Additional types for meshes
------------------------------------------------------------------------------
-- | Geometry information for a mesh, if required.
--   
--   NOTE: this is probably most useful to store the embedding information,
--     which may be useful when exporting to SVG, or perhaps VTK.
data Vertices d a
  = Vertices { _coordinates :: Vector (Vec d a)
             , _vertIndices :: Vector (NodeVec d Int)
             } deriving (Generic)

deriving instance (Storable a, Eq   a) => Eq   (Vertices 2 a)
deriving instance (Storable a, Show a) => Show (Vertices 2 a)

deriving instance (Storable a, Eq   a) => Eq   (Vertices 3 a)
deriving instance (Storable a, Show a) => Show (Vertices 3 a)


-- * Lenses and instances
------------------------------------------------------------------------------
makeLenses ''MacroMesh
makeLenses ''FTransforms
makeLenses ''Bounds
makeLenses ''Vertices

-- ** Strictness instances
------------------------------------------------------------------------------
instance             NFData (FTransforms 2)
instance             NFData (FTransforms 3)

instance NFData a => NFData (Vertices 2 a)
instance NFData a => NFData (Vertices 3 a)

-- ** Import/export instances
------------------------------------------------------------------------------
instance ToJSON   (FTransforms 2)
instance FromJSON (FTransforms 2)

instance ToJSON   (FTransforms 3)
instance FromJSON (FTransforms 3)

instance ToJSON   (Bounds 2)
instance FromJSON (Bounds 2)

instance ToJSON   (Bounds 3)
instance FromJSON (Bounds 3)

------------------------------------------------------------------------------
instance (Storable a, ToJSON   a) => ToJSON   (Vertices 2 a)
instance (Storable a, FromJSON a) => FromJSON (Vertices 2 a)

instance (Storable a, ToJSON   a) => ToJSON   (Vertices 3 a)
instance (Storable a, FromJSON a) => FromJSON (Vertices 3 a)


-- * Transform constructors
------------------------------------------------------------------------------
-- | Build the face-transforms array, and store the indices for each set of
--   face-transforms in the 'Conn2D'.
buildFaceTransforms' :: Conn2D -> Box (FTransforms 2)
buildFaceTransforms' em = fs where
  fs = unsafePerformIO $ do
    co <- fromConn2D em
    let ts = buildFaceTransforms co
    connDestroy co
    pure ts

-- | Algorithm:
--    for every tree, 't':
--     1) for each of its faces, 'f':
--         a) compute the tree-to-tree transform of 'f';
--     2) store the 4 (or 6) face-transforms in the array at position 't';
--     3) return the array;
--   
--   TODO: needs to be monadic?
buildFaceTransforms :: ConnectivityT -> Box (FTransforms 2)
buildFaceTransforms co =
  makeFaceTransforms co <$> Box.enumFromN 0 (connNumTrees co)

makeFaceTransforms :: ConnectivityT -> TreeID -> FTransforms 2
makeFaceTransforms co t = FTransforms $ go <$> faceDirs where
  go d = FTransform (-1) Vec.empty `fromMaybe` findFaceTransform co (fromEnum t) d


-- * Helper functions
------------------------------------------------------------------------------
-- | Find neighbours by applying the given predicate.
--   
--   TODO: remove, as is pointless?
findNbrsBy ::
     Rebaseable p
  => Translatable p
  => Enum    (InDir p)
  => Bounded (InDir p)
  => MeshOf p
  -> (p -> Bool)
  -> p
  -> [p]
findNbrsBy mm f = filter f . nbrsOf mm
{-# INLINE findNbrsBy #-}

-- | Calculate the neighbours of the leaf-node (for the given index), and
--   computing coordinate transforms as required, when crossing faces between
--   adjacent trees.
{-- }
nbrsOf ::
     forall d.
     SetElems (Fkey d, Dir d) (Maybe (Fkey d)) (MacroMesh d)
  => Bounded (Dir d)
  => MacroMesh d -> Fkey d -> [Fkey d]
nbrsOf mm k = catMaybes $ (<~ mm) . (k,) <$> [minBound..maxBound :: Dir d]
--}
nbrsOf ::
     Rebaseable p
  => Translatable p
  => Enum    (InDir p)
  => Bounded (InDir p)
  => MeshOf p
  -> p
  -> [p]
nbrsOf m p = catMaybes $ fmap (rebase m <=< translate p) [minBound..maxBound]
{-# INLINE nbrsOf #-}
