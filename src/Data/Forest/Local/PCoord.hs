{-# LANGUAGE DataKinds, DeriveGeneric, FlexibleContexts, FlexibleInstances,
             GeneralizedNewtypeDeriving, InstanceSigs, KindSignatures,
             MultiParamTypeClasses, NoMonomorphismRestriction,
             OverloadedStrings, PolyKinds, ScopedTypeVariables,
             StandaloneDeriving, TemplateHaskell, TupleSections,
             TypeFamilies #-}
{-# OPTIONS_GHC -Wall -fno-warn-orphans #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Local.PCoord
-- Copyright   : (C) Patrick Suggate, 2020
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Functions for working with the parametric coordinates of local nodes.
--
------------------------------------------------------------------------------

module Data.Forest.Local.PCoord
  ( module Data.Forest.PCoords
  , mkPCoord
  , leafPCoords
  , cellPCoords
  , fromPCoord
  , dofPCoords
    -- Low-level conversion functions
  , toPCoord
  , pequiv
  , leafCentrePCoord
  , nodeFromPCoord
    -- Tree-map functions
  , makeTreeMap
  , treeMapFromVertices
  )
where

import           Control.Arrow            ((&&&))
import           Control.Lens             ((%~), (&), (^.))
import           Data.Bits.Handy          ((<<%))
import           Data.Coerce
import           Data.Foldable            (foldl', toList)
import           Data.Forest.Local.Base
import           Data.Forest.Local.Rebase ()
import           Data.Forest.Mesh.Macro2D
import           Data.Forest.Node
import           Data.Forest.PCoords
import           Data.Forest.TreeMap
import           Data.IntSet.Sets
import           Data.Vector.Helpers
import qualified Data.Vector.Storable     as Vec
import           GHC.Int
import           Linear.Handy             hiding (unit)


-- ** Parametric coordinates instances
------------------------------------------------------------------------------
instance Parametric (MacroMesh 2) where
  pcoord :: MacroMesh 2 -> Node 2 -> PCoord 2
  pcoord  = toPCoord
  {-# INLINE pcoord #-}


-- * Parametric coordinates functions
------------------------------------------------------------------------------
-- | Construct a parametric coordinate from a forest's leaf-key.
--
--   TODO: this needs to be a function-family?
--
mkPCoord :: MacroMesh 2 -> Fkey 2 -> PCoord 2
mkPCoord mm = toPCoord mm . firstNodeOfLeaf . lpath
{-# INLINE mkPCoord #-}

------------------------------------------------------------------------------
-- | Compute the [4x4] set of parametric coordinates of nodes that have
--   support within the given leaf-cell.
--
--   TODO: generalise to support 3D as well.
--
cellPCoords :: MacroMesh 2 -> Fkey 2 -> [PCoord 2]
cellPCoords mm k =
  let p  = mkPCoord mm k
      h  = width k
      ds = [-h, 0, h, h+h]
  in  [ p & pcoordVec %~ _xy %~ (^+^ V2 x y) | y <- ds, x <- ds ]

------------------------------------------------------------------------------
-- | Construct a parametric coordinate from all of the corner-nodes of a leaf-
--   cell (key).
leafPCoords :: MacroMesh 2 -> Fkey 2 -> [PCoord 2]
leafPCoords mm = fmap (pcoord mm) . nodesOfLeaf . lpath
{-# INLINE leafPCoords #-}

-- | Transform an "integral" parametric coordinate into a floating-point
--   representation, which is a tuple of leaf-width (2^-depth) and coordinates
--   of the lower-left corner of the leaf.
--
--   TODO: this needs to be a function-family, for (isogeometric) geometry-
--     maps?
--
fromPCoord :: PCoord 2 -> (R, V2 R)
fromPCoord  =
  let s = recip . fromIntegral $ maxWidth (undefined :: Leaf 2) :: Double
  in  (*s) . fromIntegral . width &&& ((*s) . fromIntegral <$>) . (^._xy) . _pcoordVec
{-# INLINE fromPCoord #-}

-- ** Mapping-functions from parametric coordinates
------------------------------------------------------------------------------
-- | Build a map from parametric-coordinates to local-node indices, using the
--   given @LocalForest@ .
--
--   NOTE: the computed map does not contain "hanging nodes", since these do
--     not have local-indices (in 'p4est').
--
dofPCoords :: LocalForest 2 -> PCoords 2 Int
dofPCoords lf =
  let maco  = lf^.macroStruct :: MacroMesh 2
      nods  = lf^.forestNodes :: LocalNodes 2
      -- Compute the list of "ghost-DoF's" (since we do not know their indices
      -- yet)
      gpts  = (,-1) . toPCoord maco . oddNodeOfLeaf . lpath <$> flat ghst
      ghst  = lf^.ghostsLayer :: Lforest 2
      gcds  = foldl' (flip (~>)) bare gpts
      -- Find the parametric coordinates for each DoF, and then insert into
      -- the @PCoords@ map
      pfun :: PCoords 2 Int -> Int -> Node 2 -> PCoords 2 Int
      pfun  = \qs j q -> (toPCoord maco q, (nods^.globalIdxs)!j) ~> qs
      {-# INLINE pfun #-}
      -- Build the set of DoF parametric-coordinates, for the owned and pre-
      -- shared DoF's
  in  pfun `Vec.ifoldl'` gcds $ nods^.localNodes :: PCoords 2 Int
--   in  pfun `Vec.ifoldl'` bare $ nods^.localNodes :: PCoords 2 Int

-- ** Parametric coordinate helpers
------------------------------------------------------------------------------
-- | Convert the given node (represented using local-tree coordinates) to
--   (global) parametric coordinates.
--
--   FIXME:
--    + this gives the wrong parametric coordinates when the coordinate axis
--      of a tree does not line up with those of the parameter-space;
--
toPCoord :: MacroMesh 2 -> Node 2 -> PCoord 2
toPCoord mm n = case rebase mm n of
  Nothing -> toPCoord' mm n
  Just n' -> toPCoord' mm n'

toPCoord' :: MacroMesh 2 -> Node 2 -> PCoord 2
toPCoord' mm n@(Node (V2 j i) l t) =
  let V2 x y = (<<% maxDepth n) <$> originOf mm t
      V2 w h = scaleOf mm t
  in  PCoord (V4 (x+j*w) (y+i*h) l t)

-- | Find the origin for the tree with the given id.
originOf :: MacroMesh 2 -> Int -> V2 Int
originOf mm t =
  let Just (Vertices cs vi) = mm^.embedding
  in  fromIntegral <$> cs!(vi!t^._x)
{-# INLINE[1] originOf #-}

scaleOf :: MacroMesh 2 -> Int -> V2 Int
scaleOf mm t =
  let Just (Vertices cs vi) = mm^.embedding
      og = originOf mm t
      tc = fromIntegral <$> cs!(vi!t^._w)
  in  tc ^-^ og
{-# INLINE[2] scaleOf #-}

------------------------------------------------------------------------------
-- | Are two nodes "parameter(-space) equivalent"?
pequiv :: MacroMesh 2 -> Node 2 -> Node 2 -> Bool
pequiv mm a b =
  let a' = toPCoord mm a
      b' = toPCoord mm b
  in  a' == b'

-- | Compute the parametric coordinates of a node within the centre of the
--   given leaf-cell, if possible.
leafCentrePCoord :: MacroMesh 2 -> Leaf 2 -> Maybe (PCoord 2)
leafCentrePCoord mm = fmap (pcoord mm) . leafCentreNode
{-# INLINE leafCentrePCoord #-}

-- ** Tree-map functions
------------------------------------------------------------------------------
-- | Construct a map from parametric coordinates to the identifiers of the
--   owning tree.
makeTreeMap :: MacroMesh 2 -> TreeMap 2
makeTreeMap mm = case mm^.embedding of
  Nothing -> error "can not make TreeMap, no embedding information exists"
  Just vs -> treeMapFromVertices vs

-- | Construct a map from macromesh coordinates to tree identifiers.
treeMapFromVertices :: Vertices 2 Int32 -> TreeMap 2
treeMapFromVertices vs@(Vertices _ vi) =
  let go :: Int -> TreeMap 2 -> TreeMap 2
      go i = treeMap %~ ((minTreeCoordOf vs i, toEnum i) ~>)
  in  Vec.foldl' (flip go) bare $ 0 `efn` len vi
{-# INLINABLE treeMapFromVertices #-}

minTreeCoordOf :: Vertices 2 Int32 -> Int -> V2 Int
minTreeCoordOf (Vertices cs vi) t =
  let js = toList $ vi!t :: [Int]
      xy = fmap fromIntegral . (cs!) <$> js :: [V2 Int]
      xs = (^._x) <$> xy
      ys = (^._y) <$> xy
  in  minimum xs `V2` minimum ys
{-# INLINE[2] minTreeCoordOf #-}

------------------------------------------------------------------------------
-- | Invert the 'pcoord' transformation, if possible. Roughly:
--
--   > nodeFromPCoord tr . pcoord mm = Just
--
--   TODO: transform @_xy@ into local-tree coordinates.
--
nodeFromPCoord :: MacroMesh 2 -> PCoord 2 -> Node 2
nodeFromPCoord mm p =
  let V4 i j k l = coerce p
      x0 = (<<% maxDepth p) <$> originOf mm l
  in  Node (V2 i j ^-^ x0) k l
{-# INLINE nodeFromPCoord #-}
