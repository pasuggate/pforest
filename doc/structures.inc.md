Each (MPI) process(or) stores the full *'connectivity'* [@burstedde2011p4est] [@meyers1998hex] for the forest, but only the local quadrants.

## Connectivities ## {#sec:connectivities}

Figure @fig:p4est-corner-brick shows two examples of inbuilt `Connectivity` objects, `corner` & `brick`, and their indices/labels. The *'face'* (edge, in 2D) indices correspond to the face number for the quadrant opposite (from the range 0-3 in 2D, plus an extra bit for direction-reversal, but not shown here). The *'corner'* indices correspond to the 'corner' number for this quadrant that is incident to the corner.

![Two inbuilt `Connectivity` objects, `corner` & `brick`, and their indices. The 'face' and 'corner' indices correspond to the 'face'/'corner' number for the quadrant opposite/itself (respectively).](images/p4est-corner-brick.png "Two inbuilt 'Connectivity' objects and their indices"){#fig:p4est-corner-brick}

### Topology and Embedding ### {#sec:embed-types}

Using the `p4est` library allows arbitrary topology to be represented. The `EmbeddingT`{.haskell} data type is defined within `Data.Forest.Mesh`, and it has type-level parameters so that it can support $d$-cube meshes, where $d \in \mathbb{N}$^[Though the `p4est` library only supports 2D and 3D meshes, therefore so does this library.]

```haskell
-- | The mesh (which also defines the topology), along with the geometry info
--   forms an embedding of the Connectivity/Forest within R^3.
data EmbeddingT (d :: Nat) a
  = EmbeddingT { _vertexCoords   :: Vector (V3 Double)
               , _treeVertices   :: Vector (NodeVec d Int32) -- 4x/8x #trees
               , _treeAttribs    :: Vector a                 -- #trees
               , _treeToTree     :: Vector (FaceVec d Int32) -- 4x/6x #trees
               , _treeToFace     :: Vector (FaceVec d  Int8) -- 4x/6x #trees
               , _treeToCorner   :: Vector (NodeVec d Int32) -- 4x/8x #trees
               , _cttOffset      :: Vector Int32
               , _cornerToTree   :: Vector Int32
               , _cornerToCorner :: Vector Int8
               } deriving (Generic)
```


Defined within the `pforest` library, and belonging to `module Data.Forest.Mesh`{.haskell}.

If the `vertexCoords` entry is not empty (refer to Section \ref{sec:embed-types} for more information on the related data structures), then an `EmbeddingT`{.haskell} contains geometry information that can also be used for Isogeometric Analysis.

The free type-level parameters of the `EmbeddingT`{.haskell} type are specialised to either:

#. `EmbeddingT 2 Int`{.haskell} -- for *'forests of quadtrees'*; OR

#. `EmbeddingT 3 Int`{.haskell} -- for *'forests of octrees'*,

where the `Int`{.haskell} value is used to index into a map of face-transforms, `IntMap (FTransforms d)`{.haskell}. The `FTransforms (d :: Nat)`{.haskell} data type stores the transforms that are used to perform coordinate-transformations that are required when converting a set of coordinates from one tree, to that used by another tree. These transforms are needed when crossing faces between the trees, when navigating the mesh.

## L-shaped Domain Example ## {#sec:lshape-example}

The `EmbeddingT`{.haskell} data type is closely based on the `p4est_connectivity_t` type from the `p4est` library. To understand this data type, we will use the [(inverted) L-shaped domain example](#sec:lshape-example) (in the following subsection) to define a forest containing three trees. This is the L-shpaed domain that was shown earlier in Figures \ref{fig:subforests} & @fig:lshape-mesh. The 'connectivity' data-structure can represent far more mesh-configurations than are supported by `rubs-mesh`, and is based on a structure described in a paper on the CUBIT hexahedral meshing library [@meyers1998hex]. For example, it allows a *'tube'* to be defined by identifying the relevant faces of a unit-square (and a torus, as well).

The `p4est_connectivity` data-structures are very fussy when making adjacency-queries. For example, the `p4est_quadrant_find_owner()` function uses some of the "unneeded" *tree-to-<x>* entries to test for particular scenaries. So, in following definition:

```haskell
-- | Inverted L-shape composed from three unit-squares.
--   NOTE: when a face of a tree is not adjacent to another tree, then set:
--      + the Tree-to-Tree (tt) field to itself (for that face); and
--      + the Tree-to-Face (tf) field to the current face, to be consistent
--        with the 'tt' values,
--     so that 'quadExists' works correctly across boundaries.
embedLShape :: Embedding a
embedLShape  = Embedding vs tx tt tf tc co ct cc where
  vs = Vec.fromList [ V3 0 0 0, V3 1 0 0, V3 0 1 0, V3 1 1 0
                    , V3 2 1 0, V3 0 2 0, V3 1 2 0, V3 2 2 0
                    ] :: Vector (V3 Double)
  tx = Vec.fromList [ V4 0 1 2 3, V4 2 3 5 6, V4 3 4 6 7
                    ] :: Vector (V4 CInt)
  tt = Vec.fromList [ V4 0 0 0 1, V4 1 2 0 1, V4 1 2 2 2
                    ] :: Vector (V4 CInt)
  m  = -1 :: CInt
  tf = Vec.fromList [ V4 0 1 2 2, V4 0 0 3 3, V4 1 1 2 3 ] :: Vector (V4 CSChar)
  tc = Vec.fromList [ V4 m m m 0, V4 m 0 m m, V4 0 m m m ] :: Vector (V4 CInt)
  co = Vec.fromList [ 0, 3 ] :: Vector CInt
  ct = Vec.fromList [ 0, 1, 2 ] :: Vector CInt
  cc = Vec.fromList [ 3, 1, 0 ] :: Vector CSChar
```

the *tree-to-tree* (`tt`) field uses its own tree-index to fill in entries for faces[^face-edge] that are not adjacent to other trees. And the corresponding *tree-to-face* (`tf`) values are just the face indices for itself, again, when not adjacent to other trees. This allows `p4est_quadrant_find_owner()` to terminate early if a tree is "adjacent to itself" *AND* its $x^{th}$ face is adjacent to itself.

[^face-edge]: In 2D, the `p4est` library calls edges *'faces'*, presumably as they're the $(d-1)^{th}$-dimensional primitive, like a face is to a cube, so that it makes it easier to generalise the code to work with $d \in {2, 3} \subset \mathbb{Z}$?

### Notes ###

#. Another point of note, from the definition given above, is that *tree-to-corner* (`tc`) entries must be $-1$ when they do not map to *'corner'* object -- and the `p4est` authors only seem to explicitly create/use *'corners'* when there are three or more $d$-cubes meeting at a shared vertex(?).

#. Also, there is not a significant level of "sanity-checking" of connectivities passed to `p4est` functions. Therefore, slightly-wrong connectivities will cause many `p4est` routines to fail, and often just by hanging, with no error-messages.

#. **Allowable Meshes**: Only *"flat" meshes* (e.g., see Figure \ref{fig:subforests}) can be supported in general, due to the extra math required, and reduction of continuity, that is needed to support *'extraordinary points'* [@stam1998exact]. General-purpose, stationary subdivision schemes are limited to $C^1$ continuity at the extraordinary points. And the *"subdivision masks"* have not been computed for these yet, but it may be possible by using the methods that were presented by Jos Stam [@stam1998exact].

<!--

\begin{figure}
\centering
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{../rubs-base/images/region000.pdf}
  \label{fig:region000}
\end{minipage}%
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{../rubs-base/images/region001.pdf}
  \label{fig:region001}
\end{minipage}%
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{../rubs-base/images/region002.pdf}
  \label{fig:region002}
\end{minipage}\\
\vspace{5mm}
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{../rubs-base/images/region003.pdf}
  \label{fig:region003}
\end{minipage}%
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{../rubs-base/images/region004.pdf}
  \label{fig:region004}
\end{minipage}%
\begin{minipage}{.32\textwidth}
  \mbox{}%
\end{minipage}%
\caption{\label{fig:subforests}An inverted, subdivided L-shaped domain that has been (2:1, corner-) balanced, partitioned, and distributed across five processes.}
\end{figure}

-->
