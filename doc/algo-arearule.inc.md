**Synopsis**: *The* "area rule" *constraint is used to ensure that only (up to) two levels of basis-function have support within any given element.*

### Tasks ###

#. Perhaps only a single pass would be required if the algorithm processes cells from finest to coarsest?

#. Can the algorithm from `tree-linear` be used?

### Algorithm ###

Input:

#. Balanced mesh.

Output:

#. Area-rule constrained mesh.

Bad algorithm:

#. For each cell, check the level of its neighbours.

#. If any of the neighbours are more than one level finer, then mark the current cell for subdivision.

#. Subdivide all marked cells.

#. Repeat until there are no more cells to subdivide.
