{-# LANGUAGE FlexibleContexts, GADTs, OverloadedStrings, TypeFamilies #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.Export.VTK
-- Copyright   : (C) Patrick Suggate, 2020
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- VTK exporters for `p4est` meshes; i.e., forests of linear-trees.
--
-- Changelog:
--  + 14/08/2020  --  initial file;
--
-- TODO:
--  + move to `rubs-mesh` as this module makes assumptions about supported
--    mesh topologies, and that are more restrictive than that of 'p4est';
--
--  + still too many magic-numbers throughout the code -- mostly related to
--    scaling & depths -- and I need to clean this up;
--
--  + need to be able to set the geometry;
--
--  + helper-functions for building correct arrays of field-coefficients;
--
------------------------------------------------------------------------------

module Data.Forest.P4est.Export.VTK
  ( FieldList (..)
    -- VTK exporters
  , vtkWriteFile
  , vtkWriteFile'
  , vtkWriteScalarField
  , vtkWriteVectorField
  , vtkWriteFieldList
  , vtkWriteFields
    -- Field-list functions
  , scalarFields
  , vectorFields
  ) where

import           Bindings.P4est.P4estVtk
import           Control.Monad.IO.Class
import           Control.Monad.State
import           Data.Coerce              (coerce)
import           Data.Forest.P4est.P4est  (HasP4estT (..), P4estM, WithP4est,
                                           _p4est)
import           Data.Forest.P4est.Types  hiding (pos)
import           Data.Scientific.SC.Array
import           Data.Vector.Extras
import           Data.Vector.Storable     (Vector)
import           Foreign.C.String
import           Linear                   (V3 (..))


-- * Data types
------------------------------------------------------------------------------
data FieldList where
  ScalarField :: String -> Vector     R  -> FieldList -> FieldList
  VectorField :: String -> Vector (V3 R) -> FieldList -> FieldList
  EndOfList   ::                                         FieldList


{-- }
-- * VTK export
------------------------------------------------------------------------------
-- | Export the given forest and (optionally) its geometry to the indicated
--   file.
vtkWriteFile :: MonadIO m => P4estT -> GeometryT -> FilePath -> m ()
vtkWriteFile (P4estT p4) (GeometryT pg) fpath = liftIO $ do
  withCString fpath $ \pf -> c'p4est_vtk_write_file p4 pg pf
--}

-- ** Mesh-only VTK export
------------------------------------------------------------------------------
-- | Export a mesh for the given forest, but only its "parametric geometry".
vtkWriteFile :: FilePath -> P4estM ()
vtkWriteFile fp = do
  p4st <- _p4est <$> get
  vtkWriteFile' p4st GeomNull fp

-- | Export the given forest and its geometry to the indicated file.
vtkWriteFile' :: MonadIO m => P4estT -> GeometryT -> FilePath -> m ()
vtkWriteFile' (P4estT p4) (GeometryT pg) fpath = liftIO $ do
  withCString fpath $ c'p4est_vtk_write_file p4 pg
{-# INLINE vtkWriteFile' #-}

-- ** Support for field data
------------------------------------------------------------------------------
{-- }
foreign import ccall "p4est_vtk_write_point_dataf"
  c'p4est_vtk_write_point_data_scalar1_vector0
    :: Ptr C'p4est_vtk_context -> CInt -> CInt
    -> CString -> ScArray R
    -> IO (Ptr C'p4est_vtk_context)

foreign import ccall "p4est_vtk_write_point_dataf"
  c'p4est_vtk_write_point_data_scalar0_vector1
    :: Ptr C'p4est_vtk_context -> CInt -> CInt
    -> CString -> ScArray (V3 R)
    -> IO (Ptr C'p4est_vtk_context)
--}

------------------------------------------------------------------------------
-- | Export a mesh along with the scalar- and vector- fields from the given
--   @FieldList@ .
--
--   TODO:
--    + need to be able to set the geometry;
--    + a better data-type than the @FieldList@?
--
vtkWriteFieldList :: WithP4est m => FilePath -> FieldList -> m ()
vtkWriteFieldList fp ls = vtkWriteFields fp fs vs where
  fs = scalarFields ls
  vs = vectorFields ls

------------------------------------------------------------------------------
-- | Export the fields of a parallel forest mesh to VTK.
vtkWriteFields
  :: WithP4est m
  => FilePath
  -> [(String, Vector R)]
  -> [(String, Vector (V3 R))]
  -> m ()
vtkWriteFields fp fs vs = do
  P4estT p4 <- askP4estT
  liftIO $ withCString fp $ \pf -> do
    cx0 <- c'p4est_vtk_context_new p4 pf
    cx1 <- c'p4est_vtk_write_header cx0

    -- write scalar fields
    forM_ fs $ \(l, x) -> withCString l $ \pl -> do
      ar <- fromVector x
      _  <- c'p4est_vtk_write_point_scalar (coerce cx1) pl (coerce ar)
      pure ()

    -- write vector fields
    forM_ vs $ \(l, x) -> withCString l $ \pl -> do
      ar <- fromVector x
      _  <- c'p4est_vtk_write_point_vector (coerce cx1) pl (coerce ar)
      pure ()

    c'p4est_vtk_write_footer cx1 >> pure ()

-- ** Simple field-export functions
------------------------------------------------------------------------------
-- | Export a parallel forest mesh that has a single scalar field, and writing
--   as VTK files.
vtkWriteScalarField :: WithP4est m => FilePath -> String -> Vector R -> m ()
vtkWriteScalarField fp ls xs = do
  P4estT p4 <- askP4estT
  liftIO $ withCString fp $ \pf -> withCString ls $ \pl -> do
    cx0 <- c'p4est_vtk_context_new p4 pf
    cx1 <- c'p4est_vtk_write_header cx0
    ar  <- fromVector xs
    cx2 <- c'p4est_vtk_write_point_scalar cx1 pl (coerce ar)
    c'p4est_vtk_write_footer cx2 >> pure ()

-- | Export a parallel forest mesh that has a single vector field, and writing
--   as VTK files.
vtkWriteVectorField
  :: WithP4est m
  => FilePath -> String -> Vector (V3 R)
  -> m ()
vtkWriteVectorField fp ls xs = do
  P4estT p4 <- askP4estT
  liftIO $ withCString fp $ \pf -> withCString ls $ \pl -> do
    cx0 <- c'p4est_vtk_context_new p4 pf
    cx1 <- c'p4est_vtk_write_header cx0
    ar  <- fromVector xs
    cx2 <- c'p4est_vtk_write_point_vector cx1 pl (coerce ar)
    c'p4est_vtk_write_footer cx2 >> pure ()


-- * Functions for @FieldList@'s
------------------------------------------------------------------------------
-- | Extract all of the scalar-fields from the list.
scalarFields :: FieldList -> [(String, Vector R)]
scalarFields (ScalarField l x n) = (l, x):scalarFields n
scalarFields (VectorField _ _ n) = scalarFields n
scalarFields  EndOfList          = []

-- | Extract all of the vector-fields from the list.
vectorFields :: FieldList -> [(String, Vector (V3 R))]
vectorFields (VectorField l x n) = (l, x):vectorFields n
vectorFields (ScalarField _ _ n) = vectorFields n
vectorFields  EndOfList          = []
