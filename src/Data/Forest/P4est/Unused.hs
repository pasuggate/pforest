{-# LANGUAGE DataKinds, FlexibleContexts, FlexibleInstances,
             MultiParamTypeClasses, ScopedTypeVariables, TupleSections,
             TypeFamilies #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.Unused
-- Copyright   : (C) Patrick Suggate, 2020
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Unused functions for working with `p4est_t` "forest" objects.
--
-- Changelog:
--  + 15/07/2020  --  refactored from '.Base';
--
------------------------------------------------------------------------------

module Data.Forest.P4est.Unused where

import           Control.Arrow          (first)
import           Control.Lens           ((^.))
import qualified Data.IntMap            as Map
import           Data.IntMap.Maps
import           Data.Maybe             (isJust)
import           Data.Vector.Storable   (Vector)
import qualified Data.Vector.Storable   as Vec

import           Data.Forest.P4est.Base
import           Data.Tree.Linear       (Ltree (..))
import           Data.Tree.Linear.Xtree (Xtree (..))


------------------------------------------------------------------------------
toQuads :: Rank -> IntMap (Ltree 2) -> Vector Quad
toQuads rank = Vec.fromList . toQuads' rank
{-# INLINE toQuads #-}

toQuads' :: Rank -> IntMap (Ltree 2) -> [Quad]
toQuads' rank quds = uncurry go `concatMap` flat quds where
  go t = fmap (toEnum t `keyToQuad` rank) . flat
{-# INLINE toQuads' #-}

insQuad :: Quad -> IntMap (Ltree 2) -> IntMap (Ltree 2)
insQuad q ts | t  <?  ts = Map.adjust (k~>) t ts
             | otherwise = (t, unit k) ~> ts
  where (t,k) = (fromIntegral $ q^.qTree, quadToKey q)
{-# INLINE insQuad #-}

hasQuad :: Quad -> IntMap (Ltree 2) -> Bool
hasQuad q = maybe False (quadToKey q <?) . (t<~) where
  t = fromIntegral $ q^.qTree
{-# INLINE hasQuad #-}

delQuad :: Quad -> IntMap (Ltree 2) -> IntMap (Ltree 2)
delQuad q lf = maybe lf ((~>lf) . (t,) . (quadToKey q <\)) $ t<~lf where
  t = fromIntegral $ q^.qTree
{-# INLINE delQuad #-}

------------------------------------------------------------------------------
toQuadsX :: Rank -> IntMap (Xtree 2 a) -> [(Quad, a)]
toQuadsX rank quds = uncurry go `concatMap` flat quds where
  go t = fmap (first $ toEnum t `keyToQuad` rank) . flat
{-# INLINE toQuadsX #-}

insQuadX :: Quad -> a -> IntMap (Xtree 2 a) -> IntMap (Xtree 2 a)
insQuadX q x ts | t  <?  ts = Map.adjust ((k,x)~>) t ts
                | otherwise = (t, unit (k,x)) ~> ts
  where (t,k) = (fromIntegral $ q^.qTree, quadToKey q)
{-# INLINE insQuadX #-}

getQuadX :: Quad -> IntMap (Xtree 2 a) -> Maybe a
getQuadX q ts = t<~ts >>= (quadToKey q<~) where
  t = fromIntegral $ q^.qTree
{-# INLINE getQuadX #-}

hasQuadX :: Quad -> IntMap (Xtree 2 a) -> Bool
hasQuadX q = isJust . getQuadX q
{-# INLINE hasQuadX #-}

delQuadX :: Quad -> IntMap (Xtree 2 a) -> IntMap (Xtree 2 a)
delQuadX q lf = maybe lf ((~>lf) . (t,) . (quadToKey q <\)) $ t<~lf where
  t = fromIntegral $ q^.qTree
{-# INLINE delQuadX #-}
