------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.Communication
-- Copyright   : (C) Patrick Suggate 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Communication functionality for querying and  sharing information with
-- subforests that belong to other (MPI) processes.
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Forest.P4est.Communication
  ( commFindOwner
  ) where

import Foreign.C.Types
import Control.Monad.IO.Class
import Bindings.P4est.P4estCommunication
import Data.Forest.P4est.Types


-- | Find the MPI rank of the owner of the given quadrant.
--   
--   TODO: what is the difference between this and 'quadFindOwner' from the
--     'P4est' module?
commFindOwner :: MonadIO m => P4estT -> LInt -> QuadrantT -> CInt -> m CInt
commFindOwner (P4estT p) t (QuadrantT q) g = liftIO $ do
  c'p4est_comm_find_owner p t q g

{-- }
findOwner :: MonadIO m => P4estT -> LInt -> QuadrantT -> CInt -> m CInt
findOwner (P4estT p) t (QuadrantT q) g = liftIO $ do
  c'p4est_comm_find_owner p t q g

neighborhoodOwned :: MonadIO m =>
  P4estT -> LInt -> Ptr CInt -> Ptr CInt -> QuadrantT -> m CInt
neighborhoodOwned (P4estT p) t (QuadrantT q) g = liftIO $ do
  c'p4est_comm_neighborhood_owned , p -> CInt -> aa -> bb -> q -> IO CInt
--}
