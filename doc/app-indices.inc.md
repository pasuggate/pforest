## Z-Indices ## {#app:z-indices}

Figure @fig:p4est-indexing gives the indexing convention used by te `p4est` library. This convention is *X* before *Y* before *Z*, or sometimes called *"Z-indices"*.

![The p4est library uses Z-indexing for corners, edges, faces, and cells. (Image Courtesy of *Burstedde et. al.*, 2011 .)](../rubs-base/images/p4est-indexing.png "The p4est library uses Z-indexing for corners, edges, faces, and cells"){#fig:p4est-indexing}

## Z-Index Representation ## {#app:z-index-reprs}

In the following diagram:

```
    63 <-- l*d -->            0
    +-+-+-     -+-+-+-+---- -+-+
    |0|    ...    |1|0 ...    0|
    +-+-+-     -+-+-+-+---- -+-+
```

so there are `l` groups of child-indices, where `l` is the *'level'* of the cell, and `d` is the *number of parametric dimensions*.

## Exposed API Functions ##

Most of this stuff is covered in `doc/common-api.inc.md` of the `pforest` package, and therefore in Section \ref{sec:common-api}:

+ [The Common API](#sec:common-api)

## Linear $2^d$-Tree Representations ##

Linear $2^d$-tree implemented as a set of leaf-cell Z-indices:

```haskell
  newtype Ltree (d :: Nat)   = Ltree { _leaves :: IntSet }
```

And when the leaf-nodes contain data:

```haskell
  newtype Xtree (d :: Nat) a = Xtree { _leaves :: IntMap a }
```
