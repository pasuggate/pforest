{-# LANGUAGE DataKinds, DeriveGeneric, FlexibleContexts, FlexibleInstances,
             GeneralizedNewtypeDeriving, InstanceSigs, KindSignatures,
             MultiParamTypeClasses, NoMonomorphismRestriction,
             OverloadedStrings, PolyKinds, ScopedTypeVariables,
             StandaloneDeriving, TemplateHaskell, TupleSections,
             TypeFamilies #-}

{-# OPTIONS_GHC -Wall -fno-warn-orphans #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Local.Sharing
-- Copyright   : (C) Patrick Suggate, 2020
-- License     : BDS3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Node-sharing, and local/global node, leaf, and (sub)forest functionality.
--
------------------------------------------------------------------------------

module Data.Forest.Local.Sharing
  ( calcGlobalIndices
  , findIndepRanks
  , findRanksOf
  , findSharedIndeps
  , leafRankOverlaps
  , leafRankOverlaps'
  , mirrorsNodes
  , mirrorsOwnedNodes
  , mkGhosts
  , numOwned
  , shareNodesAndUpdate
  )
where

import           Control.Arrow                 ((&&&), (***), (<<<))
import           Control.Distributed.MPI.Handy as MPI
import           Control.Lens                  ((%~), (.~), (^.))
import           Control.Monad.State
import           Data.Bits.Handy               (int)
import           Data.Bool                     (bool)
import           Data.Foldable                 (foldl', toList)
import           Data.Function
import qualified Data.IntMap                   as Map
import           Data.IntMap.Maps
import qualified Data.IntSet                   as Set
import           Data.IntSet.Sets
import           Data.Maybe                    (catMaybes, fromJust)
import           Data.MonoTraversable          (MonoFoldable (..))
import qualified Data.Vector.Algorithms.Search as Vec
import           Data.Vector.Extras
import           Data.Vector.Storable          (Storable, Vector)
import qualified Data.Vector.Storable          as Vec
import           GHC.TypeNats
import           System.IO.Unsafe
import           Text.Printf

import           Data.Forest.Local.Base
import           Data.Forest.P4est.P4est       hiding (trace', trc)
import qualified Data.Scientific.SC            as SC


mkGhosts :: P4estM (Lforest 2, Lforest 2)
mkGhosts  = do
  get >>= maybe
    (pure (bare, bare))
    (fmap (toGhostsLayer &&& toMirrorLayer) . fromGhostT) . _ghost

-- ** Local-forest assembly helpers
------------------------------------------------------------------------------
-- | Assembly helper-function that computes the ghost-layer.
toGhostsLayer :: Ghost -> Lforest 2
toGhostsLayer  = Vec.foldl' (\ms -> (~>ms) . tofkey) bare . _gGhosts

-- | Assembly helper-function that computes the mirror-layer.
toMirrorLayer :: Ghost -> Lforest 2
toMirrorLayer  = Vec.foldl' (\ms -> (~>ms) . tofkey) bare . _gMirrors

------------------------------------------------------------------------------
-- | Rank/owner of each independent node.
findIndepRanks :: Rank -> Nodes -> Vector Rank
findIndepRanks rank nods =
  let nr = nods^.nonLocalRanks
      ns = nods^.nodesIndep
      s  = nods^.offsetOwnedIndeps
      n  = nods^.numOwnedIndeps
      fn i | i <  s    = toEnum $ nr!i
           | i >= s+n  = toEnum $ nr!(i-n)
           | otherwise = rank
  in  Vec.imap (\i _ -> fn i) ns
{-# INLINE findIndepRanks #-}

-- | Calculate the global indices for each independent node.
calcGlobalIndices :: Vector Rank -> Nodes -> Vector Int
calcGlobalIndices rs nods =
  let ns = nods^.nodesIndep
      ps = Vec.scanl' (+) 0 $ nods^.globalOwnedIndeps
      fn i x = ps!fromEnum (rs!i) + fromEnum (x^.ilocal)
  in  Vec.imap fn ns
{-# INLINE calcGlobalIndices #-}

-- | For the locally-owned independent nodes, store the set of MPI processes
--   that each node has been shared with.
--
--   TODO:
--    + is this function useful?
--
findSharedIndeps :: Nodes -> IntMap RankSet
findSharedIndeps nods =
  let si = nods^.sharedIndeps
      s  = nods^.offsetOwnedIndeps
      n  = nods^.numOwnedIndeps
      ss = slc s n $ nods^.nodesIndep
      fn i | j<0 || o<0 = Nothing
           | otherwise  = (i,) . unit . toEnum . g <$> j <~ si
        where
          g a | len a > o = a!o
              | otherwise = error $ printf "findSharedIndeps: x = %s, j = %d, o = %d, a = %s\n" (show x) j o (show a)
          x = ss!i
          j = int $ x^.ipad8 - 1
          o = int $ x^.ipad16
  in  Map.fromList . catMaybes . map fn . Vec.toList $ efn 0 n
{-# INLINE findSharedIndeps #-}


-- * Queries
------------------------------------------------------------------------------
-- | Return the number of locally-owned, independent nodes.
numOwned :: Storable (Node d) => LocalForest d -> Int
numOwned  = len . _localNodes . _forestNodes
{-# INLINE numOwned #-}

------------------------------------------------------------------------------
-- | Gather the set of the locally-owned, independent nodes that are incident
--   to any leaf-node of the "mirrors layer," adding its index to the set of
--   such nodes.
mirrorsOwnedNodes ::
     MonoFoldable (Lforest d)
  => Foldable (NodeVec d)
  => SetElems (Fkey d) (Maybe (NodeVec d Int)) (Xforest d (NodeVec d Int))
  => LocalForest d
  -> IntSet
mirrorsOwnedNodes lf = Set.filter (<?ownd) $ mirrorsNodes lf where
  ownd = lf^.forestNodes^.cellOfNode
{-# INLINABLE mirrorsOwnedNodes #-}

-- | Gather together each of the local, independent nodes that are referenced
--   by any leaf-nodes of the "mirrors layer."
--
--   FIXME:
--    + the @localCells@ array stores both independent nodes, and hanging
--      nodes, so the hanging nodes need to be filtered out from the returned
--      set of indices.
--
mirrorsNodes :: forall (d :: Nat).
     MonoFoldable (Lforest d)
  => Foldable (NodeVec d)
  => SetElems (Fkey d) (Maybe (NodeVec d Int)) (Xforest d (NodeVec d Int))
  => LocalForest d
  -> IntSet
mirrorsNodes lf = Set.filter (<ln) . ofoldl' go bare $ lf^.mirrorLayer where
  cs = lf^.forestNodes^.localCells
  ln = len $ lf^.forestNodes^.globalIdxs
  -- find the node-indices of the given @Leaf@, and add them to the set:
  go :: IntSet -> Fkey d -> IntSet
  go js = foldl' (flip (~>)) js . maybe [] toList . (<~ cs)
  {-# INLINE go #-}

-- ** Leaf-cell ownership queries
------------------------------------------------------------------------------
-- | Find the ranks of any local-regions of the forest that intersect with the
--   given leaf-cell.
--
--   TODO:
--    + build a version that does not use `unsafePerformIO` (perhaps by using
--      my `bsearch` function instead)?
--    + unit and property-based tests for this function.
--    + benchmark the performance of this function.
--
leafRankOverlaps
  :: forall (d :: Nat). d ~ 2
  => Storable (Leaf d)
  => Ord (Leaf d)
  => LocalForest d
  -> Leaf d
  -> (Rank, Rank)
leafRankOverlaps  = leafRankOverlaps' . _firstLeaves
{-# INLINE[2] leafRankOverlaps #-}

leafRankOverlaps'
  :: forall (d :: Nat). d ~ 2
  => Storable (Leaf d)
  => Ord (Leaf d)
  => Vector (Leaf d)
  -> Leaf d
  -> (Rank, Rank)
leafRankOverlaps' lefs c = go c0 c1 where
  c0 = firstLeaf c
  c1 = lastLeaf c
  go = (,) `on` toEnum . bsearch lefs
{-# INLINABLE leafRankOverlaps' #-}

-- | Compute the (MPI) ranks of the processes that own (or overlap with) any
--   of the given list of leaf-cells.
findRanksOf :: LocalForest 2 -> [Leaf 2] -> RankSet
findRanksOf lf = foldl' (\/) bare . map go where
  go c = let (l, u) = leafRankOverlaps lf c
         in  RankSet $ Set.fromList $ map fromEnum [l..u]


-- * Node operations
------------------------------------------------------------------------------
-- | Share nodes and then update the arrays of stored nodes.
--   Inputs:
--    + sets of node-indices for nodes to be shared with other MPI processes;
--    + data structure that stores the parallel-forest information;
--   Output:
--    + updated parallel-forest, using the received nodes information.
--
--   NOTE: also updates the array that stores which rank owns each independent
--     node, to support gather/scatter operations.
--
shareNodesAndUpdate
  :: MonadIO m
  => Sharers
  -> LocalForest 2
  -> m (LocalForest 2)
shareNodesAndUpdate ls lf = do
  rank <- SC.mpiRank
  cs   <- shareSharedCounts ls
  let js = buildIndeps rank (lf^.forestNodes) ls
      go = Vec.map (fromJust . (<~js)) . Vec.fromList . flat
      ss = go <$> invert (rankSet <$> ls)

  let rs = lf^.forestNodes^.nodeOwners
  trace' "Sharers:"
  mapM_ (trc . (' ':) . show) $ flat ls
  trc . printf "Local offset: %d" . fromJust $ rank `Vec.elemIndex` rs
  trc . printf "Global offset: %d" $ Vec.scanl' (+) 0 (lf^.forestNodes^.ownedCount)!fromEnum rank

  trace' "sharing shared-nodes"
  (trc . (' ':) . show) `mapM_` Map.elems js
  xs   <- shareShareds commWorld MpiTagNodesShared ss cs

  trace' "applying shared-updates ..."
  Vec.mapM_ (trc . (' ':) . show) `mapM_` Map.elems xs
  lf'  <- pure $ lf & forestNodes %~ applySharedUpdates ls xs

  trace' "DoF nodes:"
  let nods = lf'^.forestNodes
      -- gidx = Vec.ifilter (\i _ -> _nodeOwners nods!i == rank) $ nods^.globalIdxs
      -- nval = Vec.ifilter (\i _ -> _nodeOwners nods!i == rank) $ nods^.localNodes
      gidx = nods^.globalIdxs
      nval = nods^.localNodes
      idxs = 0 `efn` len gidx :: Vector Z
  Vec.mapM_ `flip` idxs $ \j ->
    trc . printf " Dnode %3d: %s" (gidx!j) . show $ nval!j
  trace' "done (shareNodesAndUpdate)"

  pure lf'

------------------------------------------------------------------------------
-- | Build a map from DoF global-indices to independent-nodes data.
buildIndeps :: Rank -> LocalNodes 2 -> Sharers -> IntMap Indep
buildIndeps rank nods = Map.mapWithKey $ curry (fn . fst) where
  ns = nods^.localNodes
  gs = nods^.globalIdxs
--   rs = nods^.nodeOwners
--   p  = fromJust $ Vec.elemIndex rank rs
--   fn k = ilocal .~ toEnum (k - p) <<< node2indep $ ns!k
  p  = Vec.scanl' (+) 0 (nods^.ownedCount)!fromEnum rank
  fn k = ilocal .~ toEnum ((gs!k) - p) <<< node2indep $ ns!k
{-# INLINE buildIndeps #-}

-- ** Sharing helpers
------------------------------------------------------------------------------
-- | Compute the updates to the local nodes information, using the received
--   independent nodes from other MPI processes.
--
--   NOTE: reorders the received nodes (by global index), when merging into
--     'nods :: LocalNodes 2', and also updates the @localCells@ data.
--
applySharedUpdates :: Sharers -> Shareds -> LocalNodes 2 -> LocalNodes 2
applySharedUpdates shar shaz nods =
  let js = Vec.concat $ Map.elems shaz
      ps = Vec.scanl' (+) 0 $ nods^.ownedCount
      -- build an array of the MPI ranks of each shared node:
      rs = Vec.concat $ (\(k, x) -> len x `rep` toEnum k) <$> flat shaz
      -- compute globals using the array of ranks:
      gs = Vec.imap (\i x -> ps!fromEnum (rs!i) + int (x^.ilocal)) js
      ns = Vec.map indep2node js
      -- eliminate duplicates by overwriting old values with updates:
      (gx, nx, rx) = (nods^.globalIdxs, nods^.localNodes, nods^.nodeOwners)
      no  = Vec.ifoldl' (\xs j g -> (g, (nx!j, rx!j)) ~> xs) bare gx
      no' = Vec.ifoldl' (\xs j g -> (g, (ns!j, rs!j)) ~> xs) no gs
      -- construct new node-properties arrays:
      gx' = Vec.fromList $ Map.keys no'
      (nx', rx') = Vec.fromList *** Vec.fromList <<< unzip $ Map.elems no'
      -- update the local-indices stored for each cell:
      go :: Int -> Int -> [Int]
      go i j | i >= len gx = []
             | gx!i==gx'!j = j:go i' j'
             | otherwise   = go i j'
        where
          (i', j') = (i+1, j+1)
      lx  = Vec.fromList $ go 0 0
      -- shift the indices up for hanging-nodes, or else return the new local-
      -- node index:
      fn j | j >= len gx = j + (len gx' - len gx)
           | otherwise   = lx!j
      cx' = fmap fn <$> nods^.localCells
      -- update the indices for the shared-with sets, and node-cell info:
      wx' = Map.mapKeysMonotonic fn $ Map.unionWith (\/) shar $ nods^.sharedWith
      ox' = Map.mapKeysMonotonic fn $ nods^.cellOfNode
  in  nods & localNodes .~ nx' & globalIdxs .~ gx' & nodeOwners .~ rx'
           & localCells .~ cx' & sharedWith .~ wx' & cellOfNode .~ ox'
