{-# LANGUAGE DataKinds, PatternSynonyms #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Parallel
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Top-level export for parallel forests.
--
-- Changelog:
--  + 03/06/2019  --  initial file;
--
------------------------------------------------------------------------------

module Data.Forest.Parallel
  ( module Data.Forest.Mesh
  , uniform
  , corner
  )
where

import           Data.Forest.Local
import           Data.Forest.Mesh
import qualified Data.Forest.P4est       as P4
import           Data.Forest.P4est.Types (pattern ConnectFull, Quad (..))
import           Data.Scientific.SC      (SCM)
import           Linear                  (V2 (..))


-- * Some pre-made meshes
------------------------------------------------------------------------------
uniform :: Int -> SCM (LocalForest 2)
uniform l = do
  sq <- P4.unitSquare
  lf <- P4.withP4estM sq $ do
    -- refine to the requested level:
    P4.refineWith ((<l) . depth) True
    -- partition and balance mesh:
    P4.partition
    P4.balance ConnectFull
    P4.partition
    -- add ghosts and label the nodes:
    P4.addGhost ConnectFull
    P4.addNodes
    -- construct a @LocalForest@ from the 'p4est':
--     P4.svgWriteFile "out/forest"
    mkLocalForest
  P4.meshDestroy sq
  pure lf

corner :: Int -> SCM (LocalForest 2)
corner l = do
  sq <- P4.unitSquare
  lf <- P4.withP4estM sq $ do
    -- refine SW corner:
    let go (Quad (V2 0 0) m _ _ _ _) = fromIntegral m < l
        go                        _  = False
    P4.refineWith go True
    -- partition and balance mesh:
    P4.partition
    P4.balance ConnectFull
    P4.partition
    -- add ghosts and label the nodes:
    P4.addGhost ConnectFull
    P4.addNodes
    -- construct a @LocalForest@ from the 'p4est':
--     P4.svgWriteFile "out/forest"
    mkLocalForest
  P4.meshDestroy sq
  pure lf
