{-# LANGUAGE DataKinds #-}
module Data.Forest.Query.Tests where

import Test.Tasty
import Test.Tasty.HUnit
-- import           Test.HUnit              (Assertion, (@?=))
import Data.Maybe (isJust)
import Linear (V2 (..))

import Data.Forest.Query
import Data.Forest.Local


tests :: TestTree
tests  = testGroup "pforest_query"
  [ testCase "test_query_face"    test_query_face
  , testCase "test_query_corner"  test_query_corner
  ]


-- * Boundary tests
------------------------------------------------------------------------------
-- | Basic face-tests.
test_query_face :: Assertion
test_query_face  = do
  let ds = maxWidth (undefined :: Leaf 2)
      dh = ds `div` 2
      -- test data & functions
      mesh = macroLShape
      purr = pure . isOnFace
      purm = pure . flip onBoundaryOf mesh
      -- easy face:
      n0 = Node (V2 0 dh) 1 0 :: Node 2
      r0 = True
      -- limitations of these face-tests:
      n1 = Node (V2 ds dh) 1 1 :: Node 2
      r1 = True
      -- boundary test should be better:
      r2 = False
      -- corners are not excluded:
      n3 = Node (V2 0 ds) 1 1 :: Node 2
      r3 = True
  -- make sure on face:
  o0 <- purr n0
  o0 @?= r0
  -- node is on "internal" face:
  o1 <- purr n1
  o1 @?= r1
  -- node is on "internal" face:
  o2 <- purm n1
  o2 @?= r2
  -- corners are not excluded:
  o3 <- purr n3
  o3 @?= r3

-- | Basic corner-tests.
test_query_corner :: Assertion
test_query_corner  = do
  let ds = maxWidth (undefined :: Leaf 2)
      dh = ds `div` 2
      -- test data & functions
      mesh = macroLShape
      purr = pure . isCorner
      purm = pure . flip onBoundaryOf mesh
      purf = pure . isJust . flip findCorner mesh
      -- easy corner:
      n0 = Node (V2 0 ds) 1 1 :: Node 2
      r0 = True
      -- limitations of these corner-tests:
      n1 = Node (V2 0 ds) 1 0 :: Node 2
      r1 = True
      -- boundary test should agree:
      r2 = True
      -- check against face:
      n3 = Node (V2 0 dh) 1 0 :: Node 2
      r3 = False
      -- check using @findCorner@ (which is unreliable):
      n4 = n0
      r4 = False
  -- make sure on corner:
  o0 <- purr n0
  o0 @?= r0
  -- node is on "internal" face:
  o1 <- purr n1
  o1 @?= r1
  -- node is on "internal" face:
  o2 <- purm n1
  o2 @?= r2
  -- node is strictly on a face:
  o3 <- purr n3
  o3 @?= r3
  -- check using @findCorner@ (which is unreliable):
  o4 <- purf n4
  o4 @?= r4
