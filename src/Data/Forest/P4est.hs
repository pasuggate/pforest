------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Top-level exporter of functions and data-structures for the high-level API
-- for 2D parallel forests.
--
------------------------------------------------------------------------------

module Data.Forest.P4est
  ( module Data.Forest.P4est.Types
  , module Data.Forest.P4est.Base
  , module Data.Forest.P4est.P4est
  , module Data.Forest.P4est.Connectivity
  , module Data.Forest.P4est.Ghost
  , module Data.Forest.P4est.Nodes
  , module Data.Forest.P4est.Traverse
  , vtkWriteFile
  , svgWriteFile
  , writeP4estAsSVG
  , localForestToSVG
  , mkLocalExport
  -- good re-exports:
  , module Data.Forest.Mesh
  , module Data.Forest.Query
  , testPforest
  ) where

import           Data.Forest.Mesh
import           Data.Forest.P4est.Base
import           Data.Forest.P4est.Connectivity
import           Data.Forest.P4est.Export
import           Data.Forest.P4est.Ghost
import           Data.Forest.P4est.Nodes
import           Data.Forest.P4est.P4est
import           Data.Forest.P4est.Traverse
import           Data.Forest.P4est.Types
import           Data.Forest.Query
import           Text.Printf


-- * Testing & examples
------------------------------------------------------------------------------
testPforest :: IO ()
testPforest  = do
  printf "Creating connectivity\n"
  conn <- connCreate UnitSquare
  printf "Connectivity:\n%s\n" $ show conn
  printf "Destroying connectivity\n"
  connDestroy conn
  printf "Exiting\n\n"
