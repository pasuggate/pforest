# Appendix: Global Index Calculations # {#sec:glob-idx}

The `p4est` library provides to API's for generating global (and local) nodes:

+ [`nodes`](#nodes)

+ [`lnodes`](#lnodes)

## `nodes` Version ## {#nodes}

The member `global_owned_indeps` of `p4est_nodes` is the array of the number of *"independent nodes"* per processor. Is this of any use, since many of these independent nodes will be *"inferred nodes"* of RUB-spline meshes -- therefore they can be computed and should not be globally-indexed?

Just use this initial data-structure while assembling my own, and I should be able to determine whether a node is "inferred" or a "degree-of-freedom"?

The local, independent-nodes arrays are arranged as:

#. independent nodes from lower-ranked processors;

#. locally-owned independent nodes (starting at `offset_owned_indeps`); then

#. independent nodes from higher-ranked processors.

The `shared_indeps` structure stores `sc_recycle_array`'s of the (MPI) ranks of all of the shared independent-nodes -- whether they are owned by this processor, or not. Each independent-node's `pad8` stores the `i+1` index into the `shared_indeps` array, so a `pad8` value of zero indicates that this independent-node is not shared, and also owned by the current processor.

### Tasks ###

#. `nodes` does not store level-info in the `Indep` (or `Hang2`) data-structures -- add this info myself?

#. `nodes` uses an ownership rule for dof-nodes that may not be ideal -- can I do better?

## `lnodes` Version ## {#lnodes}

Does the following code fragment (from `p4est_lnodes.h`):

```c
/** Compute the global number of a local node number */
/*@unused@*/
static inline       p4est_gloidx_t
p4est_lnodes_global_index (p4est_lnodes_t * lnodes, p4est_locidx_t lidx)
{
  p4est_locidx_t      owned = lnodes->owned_count;
  P4EST_ASSERT (lidx >= 0 && lidx < lnodes->num_local_nodes);

  return (lidx < owned) ? lnodes->global_offset + lidx :
    lnodes->nonlocal_nodes[lidx - owned];
}
```

give the correct answers? Where `owned_count`, `nonlocal_nodes`, and `num_local_nodes` are related via:

```
 * element_nodes indexes into the set of local nodes, layed out as follows:
 * local nodes = [< ----owned_count---- >|< ----nonlocal_nodes---- >]
 *             = [< ---------------num_local_nodes---------------- >]
```

where this was taken from `p4est_lnodes.h`.

\clearpage

# Appendix: MPI # {#app:mpi}

## `p4est` MPI Tags ## {#mpi-tags}

From `p4est_base.h`:

```c
/** Tags for MPI messages */
typedef enum p4est_comm_tag
{
  P4EST_COMM_TAG_FIRST = SC_TAG_FIRST,
  P4EST_COMM_COUNT_PERTREE = SC_TAG_LAST,
  P4EST_COMM_BALANCE_FIRST_COUNT,
  P4EST_COMM_BALANCE_FIRST_LOAD,
  P4EST_COMM_BALANCE_SECOND_COUNT,
  P4EST_COMM_BALANCE_SECOND_LOAD,
  P4EST_COMM_PARTITION_GIVEN,
  P4EST_COMM_PARTITION_WEIGHTED_LOW,
  P4EST_COMM_PARTITION_WEIGHTED_HIGH,
  P4EST_COMM_PARTITION_CORRECTION,
  P4EST_COMM_GHOST_COUNT,
  P4EST_COMM_GHOST_LOAD,
  P4EST_COMM_GHOST_EXCHANGE,
  P4EST_COMM_GHOST_EXPAND_COUNT,
  P4EST_COMM_GHOST_EXPAND_LOAD,
  P4EST_COMM_GHOST_SUPPORT_COUNT,
  P4EST_COMM_GHOST_SUPPORT_LOAD,
  P4EST_COMM_GHOST_CHECKSUM,
  P4EST_COMM_NODES_QUERY,
  P4EST_COMM_NODES_REPLY,
  P4EST_COMM_SAVE,
  P4EST_COMM_LNODES_TEST,
  P4EST_COMM_LNODES_PASS,
  P4EST_COMM_LNODES_OWNED,
  P4EST_COMM_LNODES_ALL,
  P4EST_COMM_TAG_LAST
}
p4est_comm_tag_t;
```

I will need to add some extra MPI tags?

## MPI Commands ##

E.g., from `p4est_communication.c`, 
```c
void
p4est_comm_count_pertree (p4est_t * p4est, p4est_gloidx_t * pertree)
{
    ...
        /* Processor p has part of this tree too and needs to tell me */
        mpiret = sc_MPI_Irecv (&recvbuf, 1, P4EST_MPI_LOCIDX, p,
                               P4EST_COMM_COUNT_PERTREE, p4est->mpicomm,
                               &req_recv);
    ...
    mpiret = sc_MPI_Isend (&sendbuf, 1, P4EST_MPI_LOCIDX, p,
                           P4EST_COMM_COUNT_PERTREE, p4est->mpicomm,
                           &req_send);
    SC_CHECK_MPI (mpiret);
  }

  /* Complete MPI operations and cumulative count */
  if (addtomytree >= 0) {
    mpiret = sc_MPI_Wait (&req_recv, &status);
    SC_CHECK_MPI (mpiret);
    mypertree[addtomytree] += (p4est_gloidx_t) recvbuf;
  }
  pertree[0] = 0;
  mpiret = sc_MPI_Allgatherv (mypertree, mycount, P4EST_MPI_GLOIDX,
                              pertree + 1, treecount, treeoffset,
                              P4EST_MPI_GLOIDX, p4est->mpicomm);
  SC_CHECK_MPI (mpiret);
  for (c = 0; c < (int) num_trees; ++c) {
    pertree[c + 1] += pertree[c];
  }
  if (sendbuf >= 0) {
    mpiret = sc_MPI_Wait (&req_send, &status);
    SC_CHECK_MPI (mpiret);
  }
  ...
}
```

## Useful MPI API Commands ##

The [`mpi-hs`](http://hackage.haskell.org/package/mpi-hs) library was used by the code that was written for [sharing the "sharers"](#sec:sharing).

| MPI function  | Args                   | Description                          |
|:--------------|:-----------------------|:-------------------------------------|
| `MPI_Isend`   | comm,buf,tag,dst,req   | Non-blocking send a message          |
| `MPI_Irecv`   | comm,buf,tag,dst,req   | Non-blocking receive a message       |
| `MPI_Wait`    | req,stat               | Waits for an MPI request to complete |
| `MPI_Waitall` | cnt,req[],stat[]       | Waits for all given MPI requests to complete |
| `MPI_Iprobe`  | comm,src,tag,flag,stat | Non-blocking test for a message      |

### `mpi-hs` Data Types ###

```haskell
{#pointer *MPI_Comm as Comm foreign newtype#}
{#pointer *MPI_Datatype as Datatype foreign newtype#}
{#pointer *MPI_Request as Request foreign newtype#}
{#pointer *MPI_Status as Status foreign newtype#}
{#pointer *MPI_Op as Op foreign newtype#}

{#enum ThreadSupport {} deriving (Eq, Ord, Read, Show, Generic)#}

newtype Count = Count CInt
  deriving (Eq, Ord, Enum, Generic, Integral, Num, Real, Storable)

newtype Rank = Rank CInt
  deriving (Eq, Ord, Enum, Integral, Num, Real, Storable, Generic)

newtype Tag = Tag CInt
  deriving (Eq, Ord, Read, Show, Generic, Enum, Num, Storable)
```

## Tasks ##

#. Add tags for the additional sharing operations, and of the `Tag`{.haskell} type.
   #. use `c2hs` to build an `enum`{.c} from the `p4est` tags?
   #. *'pattern synonyms'* and maybe *'CPP'* macros?

#. Testbenches and unit-tests.

#. Go through the source-code for `mpi-hs` and figure out if there is any missing functionality.

\clearpage

# Appendix: Connectivities and `p4est` Adjacency Queries # {#app:adjacency}

The `p4est_connectivity` data-structures are very fussy when making adjacency-queries. For example, the `p4est_quadrant_find_owner()` function uses some of the "unneeded" *tree-to-<x>* entries to test for particular scenaries. So, in following definition:

```haskell
-- | Inverted L-shape composed from three unit-squares.
--   NOTE: when a face of a tree is not adjacent to another tree, then set:
--      + the Tree-to-Tree (tt) field to itself (for that face); and
--      + the Tree-to-Face (tf) field to the current face, to be consistent
--        with the 'tt' values,
--     so that 'quadExists' works correctly across boundaries.
embedLShape :: Embedding a
embedLShape  = Embedding vs tx tt tf tc co ct cc where
  vs = Vec.fromList [ V3 0 0 0, V3 1 0 0, V3 0 1 0, V3 1 1 0
                    , V3 2 1 0, V3 0 2 0, V3 1 2 0, V3 2 2 0
                    ] :: Vector (V3 Double)
  tx = Vec.fromList [ V4 0 1 2 3, V4 2 3 5 6, V4 3 4 6 7
                    ] :: Vector (V4 CInt)
  tt = Vec.fromList [ V4 0 0 0 1, V4 1 2 0 1, V4 1 2 2 2
                    ] :: Vector (V4 CInt)
  m  = -1 :: CInt
  tf = Vec.fromList [ V4 0 1 2 2, V4 0 0 3 3, V4 1 1 2 3 ] :: Vector (V4 CSChar)
  tc = Vec.fromList [ V4 m m m 0, V4 m 0 m m, V4 0 m m m ] :: Vector (V4 CInt)
  co = Vec.fromList [ 0, 3 ] :: Vector CInt
  ct = Vec.fromList [ 0, 1, 2 ] :: Vector CInt
  cc = Vec.fromList [ 3, 1, 0 ] :: Vector CSChar
```

the *tree-to-tree* (`tt`) field uses its own tree-index to fill in entries for faces[^face-edge] that are not adjacent to other trees. And the corresponding *tree-to-face* (`tf`) values are just the face indices for itself, again, when not adjacent to other trees. This allows `p4est_quadrant_find_owner()` to terminate early if a tree is "adjacent to itself" *AND* its $x^{th}$ face is adjacent to itself.

[^face-edge]: In 2D, the `p4est` library calls edges *'faces'*, presumably as they're the $(d-1)^{th}$-dimensional primitive, like a face is to a cube, so that it makes it easier to generalise the code to work with $d \in {2, 3} \subset \mathbb{Z}$?

The previous listing describes the 'connectivity' for an (inverted) L-shaped domain, consisting of a forest containing three trees, as shown in Figure @fig:lshape-mesh. The 'connectivity' data-structure is very flexible and is based on a structure described in a paper on the CUBIT hexahedral meshing library [@meyers1998hex]. For example, it allows a *'tube'* to be defined by identifying the relevant faces of a unit-square (and a torus, as well).

![*ParaView* screen-capture that shows an L-shaped mesh, via VTK export from `p4est`. The mesh has been (randomly, recursively) refined, and showing the local-mesh regions for each of five processors.](rubs-base/images/lshape-mesh.png "L-shaped mesh that has been refined"){#fig:lshape-mesh}

Another point of note, from the definition given above, is that *tree-to-corner* (`tc`) entries must be $-1$ when they do not map to *'corner'* object -- and the `p4est` authors only seem to use corners when there are three or more $d$-cubes meeting at a shared vertex(?).

Also, there is not a significant level of "sanity-checking" of connectivities passed to `p4est` functions. Therefore, slightly-wrong connectivities will cause many `p4est` routines to fail, and often just by hanging, with no error-messages.

## Tasks ##

#. Figure out the source of this data-structure, and what tooling already exists.

#. Improve the `Embedding` data type, by using more descriptive types for some of the data-structures.

#. Add support for *'reflected'* orientations, and generalise the coordinate, face, and neighbour operations to work in both 2D and 3D.

#. Better tree-to-tree queries, transforms, data-structures, and operators.

\clearpage

# DONE: Determine Suitability of `p4est` # {#sec:p4est}

#. Determine whether I can obtain the adjacency information (from `p4est`) that I need to build *Index Sets* for scatter operations (in `petsc`)

   + Within `p4est_lnodes.c`, regarding the `p4est_ghost_support_lnodes(..)` function, what is the *"support of all nodes"*, that is mentioned in the header, `p4est_lnodes.h`? I.e., what is considered to be the support of a node, and is it fixed or parameterised?

   + Can I use my own `support(..)` function, to build up `send`/`recv` lists? (Even if I need to code this in C.) This may need several iterations of ghost-expand operations, if the mesh-element size changes quickly? And until after a ghost-expansion, may not be able to determine whether all cells in the support of a node have been fetched (and included within the ghost-layer)?

   + Perhaps this'll require a custom `p4est_ghost_new(..)` constructor, as well?
   
   + OR, I should be able to use:

        ```c
        p4est_locidx_t     *mirror_proc_fronts;
        ```
    
     to store extra quads when extending the support? And it should be straight-forward (but a lot of work?) to *parameterise it by polynomial degree*? Does `p4est_ghost_expand(..)` work by adding quads to the above lists? And can I mess with `lnodes` before calling `p4est_ghost_expand_by_lnodes(..)`?
   
   + Will it be possible to have the *polynomial degree*, $\mu$, as a *parameter* when calculating node *support*?

#. Is the mapping that is already included within `p4est` for obtaining global indices any use, and of the form $\operatorname{index} : \mathbf{Quad} \rightarrow \mathbf{Node}$?

   + ~~How is this map calculated?~~ Basically, an array (of length $N+1$, for $N$ MPI processes) of *"offsets"* (but this just forms an ascending list of global node indices) is stored for each process's `lnodes` structure, so local-to-global indices can be computed using:

       ```c
           p4est_locidx_t      owned_count;
           p4est_gloidx_t      global_offset;
       ```

     and see [Appendix: Global Index Calculations](#sec:glob-idx) for the algorithm.

   + Quadrants do not have much info, but `lnodes` do, and they store the map of elements to indices, for a local partition, which contains the local- and shared- indices of nodes.

#. Decide whether it is possible to compute neighbourhood information in a form suitable for `petsc`:

   + Are *Index Sets* the (`petsc`) data-structure that I need to use for scatters?
   
   + How do I use `petsc` [`VecScatter`](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecScatter.html) objects?
   + Can I get my SMURCCS meshes into `petsc`, and while being able to maintain the local-to-global indexing that I built earlier? Use `DMPLEX`?
   
   + What about [`deal.II`](https://www.dealii.org/), as this is used for FEM, apparently?
   
   + Differences between `lnodes` and `nodes`?

#. ~~Understand `p4est_step4.c` -- This example uses the FEM to solve Poisson's problem. Therefore, uses quantities that are stored at nodes, and assembles (the *Laplacian* and *mass*) system matrices.~~ **DONE**

#. Understand the `p4est_connectivity_t` representation, and document it:

   + My *"L"*-shape connectivity is wrong, as `p4est` sometimes hangs?

   + Where to put the note on connectivities?

   + Finish reading the `p4est` paper [@burstedde2011p4est].
