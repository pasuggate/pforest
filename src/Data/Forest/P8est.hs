------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P8est
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Functions and data structures for working with `p8est_t` @P8est@ objects.
-- 
------------------------------------------------------------------------------

module Data.Forest.P8est
  ( module Data.Forest.P8est.Types
{-- }
  , module Data.Forest.P8est.Base
  , module Data.Forest.P8est.P8est
  , module Data.Forest.P8est.Connectivity
  , module Data.Forest.P8est.Ghost
  , module Data.Forest.P8est.Nodes
  , module Data.Forest.P8est.Traverse
--}
  ) where

-- import Text.Printf

import Data.Forest.P8est.Types

{-- }
import Data.Forest.P8est.Base
import Data.Forest.P8est.P8est
import Data.Forest.P8est.Connectivity
import Data.Forest.P8est.Ghost
import Data.Forest.P8est.Nodes
import Data.Forest.P8est.Traverse


testPforest :: IO ()
testPforest  = do
  printf "Creating connectivity\n"
  conn <- connCreate UnitSquare
  printf "Connectivity:\n%s\n" $ show conn
  printf "Destroying connectivity\n"
  connDestroy conn
  printf "Exiting\n\n"
--}
