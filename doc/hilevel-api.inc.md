The high-level API is intended to support building RUB-spline mesh libraries, and its mesh -building, -traversing, and -querying operations. There is nothing to explicitly force the mesh to represent a RUB-spline mesh, but much of the `p4est` generality is not exposed by this abstraction layer and API.

Therefore, this high-level API is somewhat less general than the mid-level API (and therefore less general than the `p4est` library as well), as it makes certain assumptions regarding 2:1 balance, and what the ghost-layers are. Additionally, mesh balancing operations are now "hidden" from the user, as are partioning operations.

The files that are considered to be part of `pforest`'s *high-level API* are:

#. `module Data.Forest.Parallel`{.haskell} \
   The top-level re-exporter for all parallel-forest functions and data-types, and for 2D (`p4est`) as well as 3D (`p8est`) implementations. There are also modules for re-exporting just the 2D or 3D subset of the API's:

   #. `module Data.Forest.P4est`{.haskell} -- for parallel forests containing quadrilateral (2D, or square) leaf-nodes; and

   #. `module Data.Forest.P8est`{.haskell} -- re-exports parallel forests containing hexahedral (3D, or cube) leaf-nodes.

#. `module Data.Forest.Local`{.haskell} \
   Exposes the top-level functions and data-types for working with local subforests.

#. `module Data.Forest.Mesh`{.haskell} \
   This module is intended to (re-)export the general-purpose and [Common API](#sec:common-api) functions and data-types for $2^d$-trees and -forests.

The following subsections cover:

#. [High-level API overview](#sec:hi-overview) for the simplified, monadic interface to the parallel forests functionality provided by the `p4est` backend.

#. [The API for working with subforests](#sec:local-forests) as these functions and data structures are useful for interfacing with other libraries; for example, the *'PETSc'* numerical-solvers library [@petsc-user-ref], and for exporting to other mesh formats.

#. [Linear $2^d$-forests API](#sec:lforests) which have very general-purpose representations, use only pure functions, and are used when building more specialised mesh and forest data-structures and API's.

#. [General overview of import and export](#sec:highlevel-io) functions and supported representations.

#. [Examples for using these API's](#sec:api-examples) and including a "grading" example where a parallel forest is decomposed into local, connected regions of the forest that each contain elements from just a single mesh-level.

\clearpage

## API Overview ## {#sec:hi-overview}

The API presents functions and data types to handle:

#. Construction of parallel forests and their meshes:

   + `withForest :: MacroT 2 -> ForestM 2 a -> SCM a`{.haskell}

   + `addGhost :: ForestM 2 ()`{.haskell}

   + `addNodes :: ForestM 2 ()`{.haskell}

#. Mesh traversals, refinement, and coarsening:

   + `refineWith :: (Leaf 2 -> Bool) -> Bool -> ForestM 2 ()`{.haskell}

   + `coarsenBy :: (Leaf 2 -> Bool) -> Bool -> ForestM 2 ()`{.haskell}

#. Parallel forest queries:

   + `existsIn :: Leaf 2 -> LocalForest 2 -> Bool`{.haskell}

   + `findAdjacent :: Leaf 2 -> Dir 2 -> LocalForest 2 -> Maybe (Leaf 2)`{.haskell}

The core data type is essentially a hash-map (by tree-index) of linear $2^d$-trees, and this is covered in more detail in a later section.

Many of the data-structures from the high-level API have instances for [type classes of the *'Common API'*](#sec:common-api) (see Section \ref{sec:common-api}), which includes many commonly-used functions for working with meshes, sets, maps, coordinates, and indices.

<!--
As a first, simple example:

```haskell
  simpleExample :: IO ()
  simpleExample  = do
```
-->

### The `ForestM` Environment ### {#sec:forestm}

Monadic `ForestM`{.haskell} actions are evaluated in an environment that contains stateful parallel-forest data, and can also include ghost-layers and mesh-nodes information. The data type has the following definition:

```haskell
  newtype ForestM (d :: Nat) a = ForestM { unForestM :: StateT (ForestState d) SCM a }
```

and more information can be found in:

```haskell
  -- TODO: belongs in `pforest`
  module Data.Mesh.RUBS.Forest
```

though its actual implementation should be considered "abstract".

### Constructors ###

+ `withForest :: MacroT 2 -> ForestM 2 a -> SCM a`{.haskell} \
  Perform the given action within a parallel-forest context.

+ `addGhost :: ForestM 2 ()`{.haskell} \
  Adds a *'ghost layer'* that contains all of the cells adjacent to the local subforest, as well as a layer of *'mirrors'* which are the cells adjacent to the boundary of the local subforest.

+ `addNodes :: ForestM 2 ()`{.haskell} \
  Generates a unique node-labelling for every *'independent node'* of the subforest. Every local subforest stores its *'owned'* independent nodes, as well as any *'shared'* independent nodes, that it received from other (MPI) processes, due to them having *"support"* within any of the local cells.

### Shape Modification ###

The macro-mesh is not typically refined, once built/imported. But the various trees of the forest (each corresponding to an element of the macro-mesh) may have leaves that need to be refined or coarsened. The API functions for these operations are:

+ `refineWith :: (Leaf 2 -> Bool) -> Bool -> ForestM 2 ()`{.haskell} \
  A [traversal](#sec:traverse-api) that uses a *'predicate'* to determine whether each cell should be subdivided, or not. The operation is applied recursively if the boolean argument is `True`{.haskell}.

+ `coarsenBy :: (Leaf 2 -> Bool) -> Bool -> ForestM 2 ()`{.haskell} \
  Apply the predicate to each leaf-node, and coarsen it (and its *'siblings'*) if it evaluates to `True`{.haskell}.

### Mesh Navigation ###

There is no general-purpose indexing scheme that conveniently maps global-to-local coordinates, but this library uses tree-indices to select a tree, and then Z-indices (or Morton indices) to then select sub-regions and leaf-nodes.

The following functions are used to calculate adjacent forest $d$-cells:

+ `nbrsOf :: MacroMesh d -> Fkey d -> [Fkey d]`{.haskell} \
  Calculate the neighbours of the leaf-node (for the given index), and computing coordinate transforms as required, when crossing faces between adjacent trees. \
  *Inputs*:
  + `MacroMesh d`{.haskell} -- macro mesh containing the forest's connectivity structure;
  + `Fkey d`{.haskell} -- index of the leaf-node that the neighbours are being computed for;

  *Outputs*:
  + `[Fkey d]`{.haskell} -- list of the forest indices for the cells that are adjacent to the input leaf-node.

These functions are defined within:

```haskell
  -- TODO: belongs in `pforest`
  module Data.Mesh.RUBS.Regions
```

### Traversals ###

Tree-traversal is by face adjacency queries, and face-crossing transforms.

+ `floodFillM :: (Leaf 2 -> Bool) -> Leaf 2 -> ForestM 2 (Lforest 2)`{.haskell} \
  Perform a "flood-fill" search from the given leaf-node (cell-coordinate), and using the supplied predicate to determine inside/outside.

\clearpage

## Usage Example ##

\begin{figure}[htb]
\centering
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/forest000.pdf}
  \label{fig:region000}
\end{minipage}%
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/forest001.pdf}
  \label{fig:region001}
\end{minipage}%
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/forest002.pdf}
  \label{fig:region002}
\end{minipage}%
\caption{\label{fig:square-forest}A subdivided, square domain that has been (2:1, corner-) balanced, partitioned, and distributed across three MPI processes.}
\end{figure}

\clearpage

## Local (Sub-)Forests ## {#sec:local-forests}

The `LocalForest`{.haskell} data type stores the local subforest, which is typically a strict subforest as the parallel forest is likely to have been partitioned over many MPI processes (by the `p4est` backend). The subforest is represented using [linear $2^d$ *'forests'*](#sec:lforests) that contain one or more linear $2^d$-trees, as well as information for if/how these trees are connected.

Both 2D and 3D local subforests (of a parallel forest) are supported, and this data structure has the following definition:

```haskell
  data LocalForest (d :: Nat) = LocalForest { _macroStruct :: MacroMesh d
                                            , _localForest :: Lforest d
                                            , _firstLeaves :: Vector (Leaf d)
                                            , _ghostsLayer :: Lforest d
                                            , _mirrorLayer :: Lforest d
                                            , _forestNodes :: LocalNodes d
                                            } deriving (Generic)
```

### Getters and Setters ###

The getters and setters are generated automatically using the `makeLenses` template function, from the `lens` library, and via:

```haskell
  makeLenses ''LocalForest
```

and this is possible for any data type that has a `Generic`{.haskell} instance. The following list describes each of the generated getters and setters:

+ `macroStruct :: Lens' (LocalForest d) (EmbeddingT d Int)`{.haskell} \
  Get/set the full macro-mesh of the forest, and this is duplicated/shared amongst all (MPI) processes.

+ `localForest :: Control.Lens.Type.Lens' (LocalForest d) (Lforest d)`{.haskell} \
  Get/set the (sparse) linear $2^d$-forest representation of the local subforest.

+ `ghostsLayer :: Control.Lens.Type.Lens' (LocalForest d) (Lforest d)`{.haskell} \
  Get/set the ghosts layer of the local subforest, which is represented as a (sparse) linear $2^d$-forest.

+ `mirrorLayer :: Control.Lens.Type.Lens' (LocalForest d) (Lforest d)`{.haskell} \
  Get/set the mirror-layer of the local subforest, which is represented as a (sparse) linear $2^d$-forest.

### Leaf-Cell Functions ### {#sec:leaf-functions}

#### Ownership Queries ####

+ `findRanksOf :: LocalForest 2 -> [Leaf 2] -> RankSet`{.haskell} \
  Compute the (MPI) ranks of the processes that own (or overlap with) any of the given list of leaf-cells. \
  *Inputs*: 
  + `LocalForest 2`{.haskell} -- local subforest data, including the mirrors-layer and the local independent nodes information;
  + `[Leaf 2]`{.haskell} -- list of forest-/leaf- cells that cover the subforest of interest; and

  *Output*:
  + `RankSet`{.haskell} -- the set of MPI ranks that the given list of nodes overlaps with. \

+ `leafRankOverlaps :: LocalForest 2 -> Leaf 2 -> (Rank, Rank)`{.haskell} \
  Find the ranks of any local-regions of the forest that intersect with the
  given leaf-cell.

### Nodes Labeling ### {#sec:node-labels}

Stores information about the globally-unique lableling, and which nodes are owned by the current process, and which are needed but stored elsewhere. API functions include:

+ `share :: Sharers -> ForestM d ()`{.haskell} \
  Share the local nodes information.

+ `shareNodesAndUpdate :: Sharers -> LocalForest 2 -> P4estM (LocalForest 2)`{.haskell} \
  Share nodes and then update the arrays of stored nodes. Also updates the array that stores which rank owns each independent node, to support gather/scatter operations.


#### Data Structures ####

Locally, the independent nodes information (both for those owned by this MPI process, and those that are shared) is stored within the following data structure:

```haskell
  data LocalNodes (d :: Nat) = LocalNodes { _localNodes :: Vector (Node d)
                                          , _localCells :: Xforest d (NodeVec d Int)
                                          , _cellOfNode :: IntMap [(Dir d, Leaf d)]
                                          , _globalIdxs :: Vector Int
                                          , _nodeOwners :: Vector Rank
                                          , _ownedCount :: Vector Int
                                          , _sharedWith :: IntMap RankSet
                                          } deriving (Generic)
```

which has fields for:

#. the array of local, independent nodes;

#. local-indices for cell nodes;

#. local-index to its cells; and

#. MPI-rank to local-indices.

Note that *interpolated-/hanging- nodes are not stored* within this data-structure, even though the `localCells :: Xforest d (NodeVec d Int)`{.haskell} may contain indices for interpolated nodes.

And when sets of local, independent nodes are shared with other MPI processes, the following data type:

```haskell
  type Sharers = IntMap RankSet
```

is used to represent this set. This data type represents a map from independent node label to the set of MPI ranks, for each node.

#### Queries ####

+ `incidentTo :: LocalForest 2 -> Node 2 -> [(Diag 2, Fkey 2)]`{.haskell} \
  Find the leaf-cells that are incident to the given independent-node. The returned `Diag`{.haskell}-values represent the (Z-indexed) quadrant/direction for each leaf-cell, relative to the given, central node. \
  *Inputs*: 
  + `LocalForest 2`{.haskell} -- local subforest data, including the mirrors-layer and the local independent nodes information;
  + `Node 2`{.haskell} -- independent node that we are looking for leaves incident to; and
  
  *Output*:
  + `[(Diag 2, Fkey 2)]`{.haskell} -- list of indices of leaf-cells that are incident to the given node, along with their direction from the node. \

+ `mirrorsOwnedNodes :: LocalForest 2 -> IntSet`{.haskell} \
  Gather the set of the locally-owned, independent nodes that are incident to any leaf-node of the "mirrors layer," adding its index to the set of such nodes. \
  *Inputs*: 
  + `LocalForest 2`{.haskell} -- local subforest data, including the mirrors-layer and the local independent nodes information; and

  *Output*:
  + `IntSet`{.haskell} -- the set of local-indices for the nodes that belong to leaf-nodes within the mirrors layer. \

+ `mirrorsNodes :: LocalForest 2 -> IntSet`{.haskell} \
  Gather together each of the local, independent nodes that are referenced by any leaf-nodes of the "mirrors layer."
  *Inputs*: 
  + `LocalForest 2`{.haskell} -- local subforest data, including the mirrors-layer and the local independent nodes information; and

  *Output*:
  + `IntSet`{.haskell} -- the set of local-indices for the nodes that belong to leaf-nodes within the mirrors layer.

#### Import and Export ####

For FEA applications, this structure tracks the degrees-of-freedom that need to be gathered/scattered.

+ `saveLocalNodes :: FilePath -> LocalNodes d -> m ()`{.haskell} \
  Save the local node-info to a JSON file of the given name.

+ `loadLocalNodes :: FilePath -> m (Maybe LocalNodes)`{.haskell} \
  Load the local node-info from a JSON file of the given name.

\clearpage

## Macro-Meshes ## {#sec:macro-meshes}

The macro-mesh structure is needed for traversing the mesh, performing queries, and when computing an embedding. The high-level API uses following data type:

```haskell
  data MacroMesh (d :: Nat) = MacroMesh { _macroMesh  :: MacroType d
                                        , _transforms :: Box (FTransforms d)
                                        , _boundaries :: Vector (Bounds d)
                                        , _embedding  :: Maybe (Vertices d Double)
                                        } deriving (Generic)
```

This data structure stores the connectivity-structure of a parallel forest, along with the face-crossing transforms, boundary information, and possibly the vertex information that is needed to draw/export an embedding of the mesh. It is shared across all MPI processes, and its members are:

#. `_macroMesh :: MacroType d`{.haskell} is based on the `p4est_connectivity_t` structure of the backend `p4est` library, except with macro-cell or embedding information.

#. The `_transforms :: Box (FTransforms d)`{.haskell} member stores the coordinate-transforms for performing the coordinate-conversions needed when changing the coordinate system to that of a face-adjacent tree.

#. `_boundaries :: Vector (Bounds d)`{.haskell} stores information as to whether the boundary nodes of a given face contain coefficients (or DoF's), or not.

#. `_embedding  :: Maybe (Vertices d Double)`{.haskell} is useful when drawing to `SVG`, or exporting to `VTK`.

### Neighbourhood and Adjacency Queries ### {#sec:nbr-queries}

Find the face-crossing transform for the face in the given direction:

```haskell
  instance SetElems (TreeID, Dir 2) (Maybe (FTransform, Dir 2)) (MacroMesh 2) where
    (<~) :: (TreeID, Dir 2) -> MacroMesh 2 -> Maybe (FTransform 2, Dir 2)
```

### Boundary Queries {#sec:macro-boundaries}

The following type-class:

```haskell
  class NodeQueries t where
    onBoundaryOf :: Node (DimsOf t) -> t -> Bool

  instance NodeQueries (MacroMesh 2)
    ...
```

is to facilitate boundary operations; e.g., imposing boundary conditions.

### Computing an Embedding {#sec:make-an-embedding}

\clearpage

## Linear $2^d$-Forests ## {#sec:lforests}

Expanding on the idea of *'linear $2^d$-trees'* [@gargantini1982effective], linear $2^d$ *'forests'* contain one or more linear $2^d$-trees, as well as information for if/how these trees are connected -- which the `p4est` calls its *"macro mesh"*, or its *'connectivity'* structure [@burstedde2011p4est].

### Data Types ###

Briefly:

+ `Lforest (d :: Nat)`{.haskell} -- forests of linear $2^d$-trees.

+ `Xforest (d :: Nat) a`{.haskell} -- linear $2^d$-forests with data (of type `a`) stored within the leaves of trees.

+ `Fkey (d :: Nat)`{.haskell} -- data type for (hash/map) keys, for forest nodes and cells.

+ `Leaf (d :: Nat)`{.haskell} -- represents a leaf-cell of a forest, as linear $2^d$-trees typically only store leaves.

+ `Node (d :: Nat)`{.haskell} -- coordinates for vertices of a parallel forest mesh.

These extend the `Ltree`{.haskell} and `Xtree`{.haskell} data types from the `tree-linear` library, so that the index of each tree can be supported. Local-region-of-forest data-type has the following definition:

```haskell
  newtype Lforest (d :: Nat) = Lforest { _ltrees :: IntMap (Ltree d) }
    deriving (Eq, Generic)
```

Local-region-of-forest data-type, with storage in the leaves:

```haskell
  newtype Xforest (d :: Nat) a = Xforest { _xtrees :: IntMap (Xtree d a) }
    deriving (Eq, Generic)
```

### Z-Indices ###

Data types for keys that represent tuples containing tree-ID and Z-index:

```haskell
  data Fkey (d :: Nat) = Fkey { _ktree :: !Int32, _kindx :: !Int64 }
    deriving (Eq, Ord, Show, Generic)
```

For 2D forests:

+ `keyToLeaf :: Fkey 2 -> Leaf 2`{.haskell} \
  Convert the given key (representing a Z-index) into a cell-coordinate. \
  *Inputs*:
  + `Fkey 2`{.haskell} -- product data type that contains the Z-index of the cell (within the tree), along with the tree ID;

  *Outputs*:
  + `Leaf 2`{.haskell} -- leaf-node of the forest. \

+ `leafToKey :: Leaf 2 -> Fkey 2`{.haskell} \
  Compute the Z-index 'key' in a form that is compatible with linear quadtrees (and particularly for use with `Lforest`{.haskell}/`Xforest`{.haskell} objects). \
  *Inputs*:
  + `Leaf 2`{.haskell} -- leaf-node of the forest.

  *Outputs*:
  + `Fkey 2`{.haskell} -- tree ID and Z-index of the cell (within the forest), and typically to be used for hash/map operations;

### Mesh Elements/Cells ###

Mesh cells:

```haskell
  data Leaf (d :: Nat) = Leaf { _coord :: !(Vec d Int32)
                              , _level :: !Int8
                              , _ctree :: !Int32
                              } deriving (Generic)
```

#### Constructors and Generators ####

+ `leavesOfNode :: Node 2 -> [Leaf 2]`{.haskell} \
  Generate the (same-level) leaf-cells that could be incident to the given (independent) node. \
  *Inputs*:
  + `Node 2`{.haskell} -- independent node (location) to find the leaf-cells of.

  *Outputs*:
  + `[Leaf 2]`{.haskell} -- generated list of leaf-cells that are incident to the given node.

### Node Coordinates ###

Independent nodes have the following representation:

```haskell
  data Node (d :: Nat) = Node { _pos :: Vec d Z  -- ^ position
                              , _lvl :: Z        -- ^ node level
                              , _tId :: Z        -- ^ tree ID
                              } deriving (Generic)
```

And for working with the coordinates of nodes (edges, leaves, etc.), there is a family of functions that can be used to transform between the coordinates of the trees of the forest. To compute new owner/tree for the given node, edge, cell, etc, if it does not lie strictly within its current owner-tree:

```haskell
  class Rebaseable p where
    type MeshOf p :: *
    rebase :: MeshOf p -> p -> Maybe p
```

#### Constructors and Generators ####

+ `makeNodeOf :: Diag 2 -> Fkey 2 -> Node 2`{.haskell} \
  Construct one of the four possible corner-nodes of the indicated leaf-cell, using the given Z-index representation of the corner-index. \
  *Inputs*:
  + `Diag 2`{.haskell} -- Z-encoded corner index/direction, relative to the centre of a leaf-cell;
  + `Fkey 2`{.haskell} -- tree ID and Z-index of the leaf-cell that will have one of its nodes built for;

  *Outputs*:
  + `Node 2`{.haskell} -- generated independent node (location), of the given leaf-cell.

\clearpage

## Import and Export ## {#sec:hilevel-io}

Import from `JSON`, and export to `JSON`, `VTK`, and `SVG`.

\clearpage

## Examples ## {#sec:api-examples}

### Example: Find Connected-Regions of the Same Level ### {#sec:level-example}

Using the `traverse` function along with a suitable predicate.
