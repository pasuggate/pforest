Haskell's *'type classes'* [@hall1994type;@wadler1989make] are used to give a common interface to many functions. This also increases the uniformity of the exposed API functions, whether working with the high-/mid-/low- level interfaces, or with 2D/3D meshes.

The common API documentation covers these subsections:

#. [Coordinate Functions](#coordinate-functions)

#. [Indexing Functions](#indexing-functions)

#. [Operators for Set-like Objects](#sec:set-ops)

#. [Common Tree/Node Operations](#sec:common-ops)

and more detail and examples are also given throughout this text.

\clearpage

## Coordinate Functions ##

This section contains type classes that are used to present a uniform interface for getters that extract coordinates. These classes are either imported from the `tree-linear` package, and belonging to its:

```haskell
  module Data.Index.ZCurve
```

or defined within the `pforest` file for:

```haskell
  module Data.Forest.Mesh
```

and many instances make heavy use of data types from the `linear` package; for example, `data V3 a`{.haskell}, and operators from the `lens` library.

### Cell-Width Functions ###

Get the (mesh) cell-width for the given cell/coordinate/object:

```haskell
  class HasWidth t where
    maxWidth :: t -> Int
    width    :: t -> Int
```

where the `maxWidth` function typically ignores the *value* of its input, as it usually only needs its *type* to index into the *constant value for the maximum cell-width*.

### Getting the Level of a Layered Data-Type ###

For data-types that have layers:

```haskell
  class Layered t where
    maxDepth :: t -> Int
    depth    :: t -> Int
```

where the `maxDepth` function typically ignores the *value* of its input, as it usually only needs its *type* to index into the *constant value for the maximum cell-depth*.

### Object Coordinates ###

For data-types that have coordinates^[Just computes the (integral) Cartesian coordinates, and excludes tree -level/-depth.]:

```haskell
  class HasCoords t where
    type CoordVec t :: *
    coords :: t -> CoordVec t
```

\clearpage

## Indexing Functions ##

Tree and Z-indices are frequently used to index the leaves belonging to forests of $2^d$-trees.

### Z-Indices for $2^d$-Tree Leaves ###

As was popularised by a paper on quadtrees [@gargantini1982effective], quadtrees can be *"linearised"* and we use *'Z-indices'* which can be used to generate *'space-filling curves'*. For data-structures that support Z-indices:

```haskell
  class Lindex t k | t->k, k->t where
    reify :: t -> k
    lpath :: k -> t
    lroot :: t
```

### Tree Indices ###

Forests are typically composed from multiple trees, and like the `p4est` library, this library assigns an index to each tree of the forest. The following type class is for generating, extracting, and converting linear $2^d$-tree indices:

```haskell
  class HasTreeID t where
    treeId :: t -> Int
```

\clearpage

## Operators for Set-like Objects ## {#sec:set-ops}

These classes are from an external library, `set-extras`, and defined within the module:

```haskell
  module Math.Sets where
```

There are functions and operators for building, querying, and flattening set-like objects, as well as for inserting to, and deleting from, them. The fixities for these operators are:

```haskell
  infixr 7 ~>
  infixl 6 <~
  infixl 6 /\
  infixl 5 \/,\\
  infixr 4 <\
  infixl 4 <?, >?
```

and they are used throughout the various modules and libraries of the RUB-spline meshes and FEA tools reference implementation, examples, and projects.

### Empty and Singleton Constructors ###

A family of functions to construct empty and singleton sets:

```haskell
  class SetBuild e s | s -> e where
    bare :: s
    unit :: e -> s
```

### Set Conversions ###

Flatten set to a list:

```haskell
  class SetFlat e s | s -> e where
    flat :: s -> [e]
```

### Set Operations ###

A class of some standard set operations:

```haskell
  class SetOps a where
    (/\) :: a -> a -> a     -- ^ Intersection
    (\/) :: a -> a -> a     -- ^ Union
    (\\) :: a -> a -> a     -- ^ Difference
    dry  :: a -> Bool
    card :: a -> Int
```

The fixities for these operators are:

```haskell
  infixl 6 /\
  infixl 5 \/
  infixl 5 \\
```

### Element-Insert Operator ###

Construct a new set by inserting a new element into an existing set.

```haskell
  class SetInsert e s | s -> e where
    (~>) :: e -> s -> s     -- ^ Insert
```

The fixities for this operator is:

```haskell
  infixr 7 ~>
```

### Queries and Deletions by Element or Index ###

Query and deletion of elements of a set, using either element value or element index:

```haskell
  class SetIndex i s | s -> i where
    (<\) :: i -> s -> s     -- ^ Delete
    (<?) :: i -> s -> Bool  -- ^ Member?

  (>?) :: SetIndex i s => i -> s -> Bool
  (>?)  = (not .) . (<?)    -- ^ Not a member?
```

The fixities for these operators are:

```haskell
  infixr 4 <\
  infixl 4 <?, >?
```

### Element-Retrieval Operators ###

Retrieves the element at the given position/index:

```haskell
  class SetElems i e s | s -> i, s -> e where
    (<~) :: i -> s -> e     -- ^ Get
```

The fixities for this operator is:

```haskell
  infixl 6 <~
```

\clearpage

## Common Tree/Node Operations ## {#sec:common-ops}

### Tree-Cell Relations ###

```haskell
  class Relations t where
    parent        :: t -> Maybe t
    progeny       :: t -> [t]
    lineage       :: t -> [t]
    adjacent      :: t -> [t]
    neighbourhood :: t -> [t]
```

Where these perform the following functions:

+ `parent        :: t -> Maybe t`{.haskell} \
  Calculate the parent of the given cell, if it exists.

+ `progeny       :: t -> [t]`{.haskell} \
  Compute the progeny (or, the $2^d$ children) of the given cell.

+ `lineage       :: t -> [t]`{.haskell} \
  Compute lineage (i.e., all of the ancestors) of the given cell.

+ `adjacent      :: t -> [t]`{.haskell} \
  Calculates the indices of the cells that touch the given cell, and excluding the given cell.

+ `neighbourhood :: t -> [t]`{.haskell} \
  Calculates the indices of all cells that touch the given cell, including the given cell.

### Node/Cell Inclusion Tests ###

The type class for these tests has the following definition:

```haskell
  class BoundaryTestable t where
    inside  :: t -> Bool
    inside   = not . ghost
    ghost   :: t -> Bool
    ghost    = not . inside
    border  :: t -> Bool
    outside :: t -> Bool
    outside  = not . closure
    closure :: t -> Bool
    closure  = not . outside
```

Where these perform the following functions:

+ `inside  :: t -> Bool`{.haskell} \
  Does the given node-coordinate lie strictly inside the boundary?

+ `ghost   :: t -> Bool`{.haskell} \
  Does the given node-coordinate lie on the boundary, or outside, of the domain?

+ `border  :: t -> Bool`{.haskell} \
  Does the given node-coordinate lie on the boundary of the domain?

+ `outside :: t -> Bool`{.haskell} \
  Does the given node-coordinate lie strictly outside the boundary?

+ `closure :: t -> Bool`{.haskell} \
  Within the "closure" of the domain?

There are standard instances for `Node`{.haskell}, `Leaf`{.haskell}, and `Fkey`{.haskell} data types.

### Affine Operations ###

From the `vector-space` package, the `AffineSpace`{.haskell} type-class can be used to update node/leaf positions:

```haskell
class AdditiveGroup (Diff p) => AffineSpace p where
  type Diff p
  (.-.)  :: p -> p -> Diff p
  (.+^)  :: p -> Diff p -> p  -- ^ Point plus vector
```

For objects cabable of being translated, and in the indicated direction, and by some amount determined by the instance:

```haskell
  class Translatable a where
    type InDir a :: *
    translate :: a -> InDir a -> Maybe a
```
