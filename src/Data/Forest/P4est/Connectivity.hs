{-# LANGUAGE CPP, DataKinds, DeriveGeneric, FlexibleContexts, FlexibleInstances,
             GeneralizedNewtypeDeriving, NoMonomorphismRestriction,
             OverloadedStrings, PatternSynonyms, PolyKinds, RankNTypes,
             StandaloneDeriving, TemplateHaskell, TupleSections,
             TypeSynonymInstances #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.Connectivity
-- Copyright   : (C) Patrick Suggate 2019
-- License     : GPL3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Functions for working with the "connectivities" of parallel forests.
--
-- TODO:
--  + additional queries for working with boundaries and corners;
--
------------------------------------------------------------------------------

module Data.Forest.P4est.Connectivity
  ( ConnTopology (..)
  , ConnectivityT (..)
  -- low-level ops:
  , connCreate
  , connDestroy
  , withConn
  --   connectivity constructors:
  , connSquare
  , connStar
  , connCorner
  , connLShape
--   , withSquare
  --   queries:
  , connNumTrees
  --   conversions and import/export:
  , connLoad
  , connSave
  , fromConnectivityT
  -- embedding data-types and lenses:
--   , module Data.Forest.Mesh
  , embedLShape
  , embedSquare
  , embedCorner
  --   embedding conversions and import/export:
  , Conn2D (..)
  , toConn2D
  , fromConn2D
  -- helper data-types:
  , CornerInfo (..)
  , icorner
  , cxforms
  , FXformT (..)
  , CXformT (..)
  -- transformation data-types and functions:
  , FTransform (..)
  , fnewTreeId
  , ftransform
  --   queries:
  , findFaceTransform
  , findCornerTransform
  --   apply transforms:
  , quadFaceTransform
  , quadCornerTransform
  -- helper functions:
  , testConnectivity
  )
where

import           Control.DeepSeq
import           Control.Lens                     (makeLenses, (.~))
import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Vector.Storable             (Vector)
import qualified Data.Vector.Storable             as Vec
import qualified Data.Vector.Storable.Mutable     as Mut
import           Foreign.C.String
import           Foreign.C.Types
import           Foreign.ForeignPtr
import           Foreign.Marshal.Alloc            (alloca)
import           Foreign.Marshal.Utils            (copyBytes, new, with)
import           Foreign.Ptr
import           Foreign.Storable
import           GHC.Generics                     (Generic)
import           GHC.Int
import           Linear                           (V3 (..), V4 (..))
import           System.IO.Unsafe
import           Text.Printf

import           Bindings.P4est.P4estBits
import           Bindings.P4est.P4estConnectivity
import           Data.Forest.P4est.Base
import           Data.Scientific.SC.Array         as SC


-- * Connectivity data-types.
------------------------------------------------------------------------------
data ConnTopology
  = ForestConn TInt TInt TInt TInt
  | UnitSquare | Periodic | RotWrap | Corner | Pillow | Moebius | Star
  | Cubed | Disk
  | Brick2D CInt CInt      PBool PBool
--   | Brick3D CInt CInt CInt PBool PBool PBool
  | ByName CString
  deriving (Eq, Show, Generic)

-- ** Data types used for coordinate-transforms
------------------------------------------------------------------------------
-- | Stores the adjacent tree, and the required coordinate transform, when
--   crossing from one tree to another.
--
--   TODO: can this also work in 3D?
-- data FTransform (d :: Int) =
data FTransform
  = FTransform
      { _fnewTreeId :: Int
      , _ftransform :: Vector CInt
      }
  deriving (Eq, Show, Generic)

-- | Helper data-type for marshalling.
newtype FXformT =
  FXformT { getFXformT :: ForeignPtr CInt }
  deriving (Eq, Show, Generic)

------------------------------------------------------------------------------
-- | Info about corner transformations.
-- data CornerInfo (d :: Nat) =
data CornerInfo
  = CornerInfo
      { _icorner :: Int
      , _cxforms :: Vector CXformT
      }
  deriving (Eq, Show, Generic)

newtype CXformT =
  CXformT { getCXformT :: C'p4est_corner_transform_t }
  deriving (Eq, Show, Generic, Storable)

------------------------------------------------------------------------------
data Conn2D
  = Conn2D
      { _vertexCoords   :: Vector (V3 Double)
      , _treeVertices   :: Vector (V4 Int32)
        --            , _treeAttribs    :: Vector a            -- #trees
      , _treeToTree     :: Vector (V4 Int32) -- 4x/6x #trees
        -- 4x/6x #trees
      , _treeToFace     :: Vector (V4 Int8) -- 4x/6x #trees
        -- 4x/8x #trees
      , _treeToCorner   :: Vector (V4 Int32) -- 4x/8x #trees
      , _cttOffset      :: Vector Int32
      , _cornerToTree   :: Vector Int32
      , _cornerToCorner :: Vector Int8
      }
  deriving (Eq, Show, Generic)

-- * Instances
------------------------------------------------------------------------------
-- | Storable instance of the same size as each member of the array of face-
--   transforms (used by corner-transforms).
instance Storable FXformT where
  sizeOf  _ = sizeOf (undefined :: CInt)*c'P4EST_FTRANSFORM
  alignment = const 8
--   alignment = const $ alignment (undefined :: CInt)
  peek p = do
    f <- mallocForeignPtrBytes $ sizeOf (undefined :: FXformT)
    withForeignPtr f $ \q -> do
      copyBytes q (castPtr p) $ sizeOf (undefined :: FXformT)
    pure $ FXformT f
  poke p (FXformT f) = withForeignPtr f $ \q -> do
    copyBytes (castPtr p) q $ sizeOf (undefined :: FXformT)

-- ** JSON import/export
------------------------------------------------------------------------------
instance ToJSON   FTransform
instance FromJSON FTransform

instance ToJSON   Conn2D
instance FromJSON Conn2D

-- ** Strictness instances
------------------------------------------------------------------------------
instance NFData Conn2D
instance NFData FTransform


-- * Lenses
------------------------------------------------------------------------------
makeLenses ''CornerInfo
makeLenses ''FTransform
-- makeLenses ''FTransforms


-- * Constructors & destructors.
------------------------------------------------------------------------------
connCreate :: MonadIO m => ConnTopology -> m ConnectivityT
connCreate topo = liftIO $ fmap ConnectivityT $ case topo of
  ForestConn verts trees corns ctt -> c'p4est_connectivity_new verts trees corns ctt
  Brick2D n m a_wrap b_wrap    -> c'p4est_connectivity_new_brick n m a_wrap b_wrap
  ByName cname                 -> c'p4est_connectivity_new_byname cname
  UnitSquare -> c'p4est_connectivity_new_unitsquare
  Periodic   -> c'p4est_connectivity_new_periodic
  RotWrap    -> c'p4est_connectivity_new_rotwrap
  Corner     -> c'p4est_connectivity_new_corner
  Pillow     -> c'p4est_connectivity_new_pillow
  Moebius    -> c'p4est_connectivity_new_moebius
  Star       -> c'p4est_connectivity_new_star
  Cubed      -> c'p4est_connectivity_new_cubed
  Disk       -> c'p4est_connectivity_new_disk

connDestroy :: MonadIO m => ConnectivityT -> m ()
connDestroy (ConnectivityT conn) = liftIO $ c'p4est_connectivity_destroy conn

-- ** Quick-constructors
------------------------------------------------------------------------------
-- | Constructs a "2-cube."
connSquare :: MonadIO m => m ConnectivityT
connSquare  = connCreate UnitSquare

connStar :: MonadIO m => m ConnectivityT
connStar  = connCreate Star

connCorner :: MonadIO m => m ConnectivityT
connCorner  = connCreate Corner

-- ** Conversions
------------------------------------------------------------------------------
fromConnectivityT :: MonadIO m => ConnectivityT -> m Conn2D
fromConnectivityT (ConnectivityT p) = liftIO $ do
    nv <- fromIntegral <$> peek (p'p4est_connectivity'num_vertices p)
    nt <- fromIntegral <$> peek (p'p4est_connectivity'num_trees p)
    nc <- fromIntegral <$> peek (p'p4est_connectivity'num_corners p)
    -- read the vertices:
    vs <- Mut.new nv >>= \ar -> Mut.unsafeWith ar $ \q -> do
      s <- castPtr <$> peek (p'p4est_connectivity'vertices p)
      copyBytes q s (nv*sizeOf (undefined :: V3 Double))
      Vec.unsafeFreeze ar
    vi <- Mut.new nt >>= \ar -> Mut.unsafeWith ar $ \q -> do
      s <- castPtr <$> peek (p'p4est_connectivity'tree_to_vertex p)
      copyBytes q s (nt*sizeOf (undefined :: V4 Int32))
      Vec.unsafeFreeze ar
    tt <- Mut.new nt >>= \ar -> Mut.unsafeWith ar $ \q -> do
      s <- castPtr <$> peek (p'p4est_connectivity'tree_to_tree p)
      copyBytes q s (nt*sizeOf (undefined :: V4 Int32))
      Vec.unsafeFreeze ar
    tf <- Mut.new nt >>= \ar -> Mut.unsafeWith ar $ \q -> do
      s <- castPtr <$> peek (p'p4est_connectivity'tree_to_face p)
      copyBytes q s (nt*sizeOf (undefined :: V4 Int8))
      Vec.unsafeFreeze ar
--     printf "Verts:\n%s\n" (show vs)
--     printf "Quads:\n%s\n" (show vi)
--     printf "Trees:\n%s\n" (show tt)
--     printf "Faces:\n%s\n" (show tf)
    -- corner stuff only when nc > 0:
    tc <- if nc<=0 then return Vec.empty else do
      Mut.new nt >>= \ar -> Mut.unsafeWith ar $ \q -> do
        s <- castPtr <$> peek (p'p4est_connectivity'tree_to_corner p)
        copyBytes q s (nt*sizeOf (undefined :: V4 Int32))
        Vec.unsafeFreeze ar
    co <- Mut.new (nc+1) >>= \ar -> Mut.unsafeWith ar $ \q -> do
      s <- peek $ p'p4est_connectivity'ctt_offset p
      copyBytes q s ((fromIntegral nc+1)*sizeOf (undefined :: Int32))
      Vec.map fromIntegral <$> Vec.unsafeFreeze ar
    let cn = if nc<=0 then 0 else fromIntegral (Vec.last co)
    ct <- if nc<=0 then return Vec.empty else do
      Mut.new cn >>= \ar -> Mut.unsafeWith ar $ \q -> do
        s <- peek $ p'p4est_connectivity'corner_to_tree p
        copyBytes q s (cn*sizeOf (undefined :: Int32))
        Vec.map fromIntegral <$> Vec.unsafeFreeze ar
    cc <- if nc<=0 then return Vec.empty else do
      Mut.new cn >>= \ar -> Mut.unsafeWith ar $ \q -> do
        s <- peek $ p'p4est_connectivity'corner_to_corner p
        copyBytes q s (cn*sizeOf (undefined :: CSChar))
        Vec.map fromIntegral <$> Vec.unsafeFreeze ar
--     printf "Coffs:\n%s\n" (show co)
    return $ Conn2D vs vi tt tf tc co ct cc


-- * Export & import
------------------------------------------------------------------------------
connSave :: MonadIO m => FilePath -> ConnectivityT -> m Int
connSave fp (ConnectivityT pc) = liftIO $ withCString fp $ \ps -> do
  fromIntegral <$> c'p4est_connectivity_save ps pc

connLoad :: MonadIO m => FilePath -> m ConnectivityT
connLoad fp = liftIO $ withCString fp $ \ps -> do
  ConnectivityT <$> c'p4est_connectivity_load ps nullPtr

------------------------------------------------------------------------------
toConn2D :: MonadIO m => ConnectivityT -> m Conn2D
toConn2D  = fromConnectivityT
{-# INLINE toConn2D #-}

-- | Construct a `p4est` "connectivity" structure that stores the geometric
--   coordinates of each of the trees, and also how they connect together.
fromConn2D :: MonadIO m => Conn2D -> m ConnectivityT
fromConn2D (Conn2D vs vi tt tf tc co ct cc) = liftIO $ do
  let fi = fromIntegral :: forall i. Integral i => i -> CInt
      nv = Vec.length vs
      nt = Vec.length vi
      nc = Vec.length co - 1
      no = Vec.last co -- :: TInt
  p <- c'p4est_connectivity_new (fi nv) (fi nt) (fi nc) (fi no)

  -- copy vertex coordinates into a new array:
  do let nb = nv*sizeOf (undefined :: Double)*3
     q <- peek $ p'p4est_connectivity'vertices p
     Vec.unsafeWith vs $ \r -> do
       copyBytes q (castPtr r) nb
  -- copy quad->vertex indices into a new array:
  do let nb = nt*sizeOf (undefined :: TInt)*4
     q <- peek $ p'p4est_connectivity'tree_to_vertex p
     Vec.unsafeWith vi $ \r -> do
       copyBytes q (castPtr r) nb
  -- copy the neighbours-across-faces into a new array:
  do let nb = nt*sizeOf (undefined :: TInt)*4
     q <- peek $ p'p4est_connectivity'tree_to_tree p
     Vec.unsafeWith tt $ \r -> do
       copyBytes q (castPtr r) nb
  -- copy the face-to-face+orientation into a new array:
  do let nb = nt*sizeOf (undefined :: Int8)*4
     q <- peek $ p'p4est_connectivity'tree_to_face p
     Vec.unsafeWith tf $ \r -> do
       copyBytes q (castPtr r) nb
  -- copy the tree-to-corner maps into a new array:
  do let nb = nt*sizeOf (undefined :: TInt)*4
     q <- peek $ p'p4est_connectivity'tree_to_corner p
     Vec.unsafeWith tc $ \r -> do
       copyBytes q (castPtr r) nb
  -- copy the corner-to-x offsets into a new array:
  do let nb = Vec.length co*sizeOf (undefined :: TInt)
     q <- peek $ p'p4est_connectivity'ctt_offset p
     Vec.unsafeWith co $ \r -> do
       copyBytes q (castPtr r) nb
  -- copy the list of corner-to-tree maps into a new array:
  do let nb = Vec.length ct*sizeOf (undefined :: TInt)
     q <- peek $ p'p4est_connectivity'corner_to_tree p
     Vec.unsafeWith ct $ \r -> do
       copyBytes q (castPtr r) nb
  -- copy the list of corner-to-tree maps into a new array:
  do let nb = Vec.length cc*sizeOf (undefined :: Int8)
     q <- peek $ p'p4est_connectivity'corner_to_corner p
     Vec.unsafeWith cc $ \r -> do
       copyBytes q (castPtr r) nb

  return $ ConnectivityT p


-- * Evaluation within ConnectivityT-Contexts
------------------------------------------------------------------------------
withConn :: MonadIO m => ConnectivityT -> (ConnectivityT -> m a) -> m a
withConn conn action = action conn

{-- }
withSquare :: MonadIO m => (ConnectivityT -> m a) -> m a
withSquare action = connSquare >>= \sq -> withConn sq action
--}


-- * Queries
------------------------------------------------------------------------------
connNumTrees :: ConnectivityT -> Int
connNumTrees (ConnectivityT p) = unsafePerformIO $ do
  fromIntegral <$> peek (p'p4est_connectivity'num_trees p)


-- * Additional constructors
------------------------------------------------------------------------------
connLShape :: MonadIO m => m ConnectivityT
connLShape  = liftIO $ fromConn2D embedLShape

-- ** 'Embedding' constructors
------------------------------------------------------------------------------
-- | Inverted L-shape composed from three unit-squares.
--
--   NOTE: when a face of a tree is not adjacent to another tree, then set:
--      + the Tree-to-Tree (tt) field to itself (for that face); and
--      + the Tree-to-Face (tf) field to the current face, to be consistent
--        with the 'tt' values,
--     so that 'quadExists' works correctly across boundaries.
--
embedLShape :: Conn2D
embedLShape  =
  let vs = Vec.fromList [ V3 0 0 0, V3 1 0 0, V3 0 1 0, V3 1 1 0
                        , V3 2 1 0, V3 0 2 0, V3 1 2 0, V3 2 2 0
                        ] :: Vector (V3 Double)
      tx = Vec.fromList [ V4 0 1 2 3, V4 2 3 5 6, V4 3 4 6 7
                        ] :: Vector (V4 Int32)
      tt = Vec.fromList [ V4 0 0 0 1, V4 1 2 0 1, V4 1 2 2 2
                        ] :: Vector (V4 Int32)
      m  = -1 :: Int32
      tf = Vec.fromList [ V4 0 1 2 2, V4 0 0 3 3, V4 1 1 2 3
                        ] :: Vector (V4 Int8) -- TTF
      tc = Vec.fromList [ V4 m m m 0, V4 m 0 m m, V4 0 m m m
                        ] :: Vector (V4 Int32)
      co = Vec.fromList [ 0, 3 ] :: Vector Int32
      ct = Vec.fromList [ 0, 1, 2 ] :: Vector Int32
      cc = Vec.fromList [ 3, 1, 0 ] :: Vector Int8 -- CTC
      -- embedding:
  in  Conn2D vs tx tt tf tc co ct cc

embedSquare :: Conn2D
embedSquare  =
  let vs = Vec.fromList [ V3 0 0 0, V3 1 0 0, V3 0 1 0, V3 1 1 0
                        ] :: Vector (V3 Double)
      tx = Vec.fromList [ V4 0 1 2 3
                        ] :: Vector (V4 Int32)
      tt = Vec.fromList [ V4 0 0 0 0
                        ] :: Vector (V4 Int32)
      tf = Vec.fromList [ V4 0 1 2 3 ] :: Vector (V4 Int8) -- TTF
--       tc = Vec.fromList [ V4 m m m m ] :: Vector (V4 Int32)
      tc = Vec.fromList [ ] :: Vector (V4 Int32)
      co = Vec.fromList [ 0 ] :: Vector Int32
      ct = Vec.fromList [ ] :: Vector Int32
      cc = Vec.fromList [ ] :: Vector Int8 -- CTC
      -- embedding:
  in  Conn2D vs tx tt tf tc co ct cc

{-- }
embedSquare :: Conn2D
embedSquare  = unsafePerformIO . liftIO $ connSquare >>= fromConnectivityT
-- embedSquare  = unsafePerformIO $ withSC [] $ do
--   connSquare >>= fromConnectivityT
--}

embedCorner :: Conn2D
embedCorner  = unsafePerformIO . liftIO $ connCorner >>= fromConnectivityT


-- * Face & corner transforms
------------------------------------------------------------------------------
-- | Search the connectivity for the tree adjacent to the given tree, and
--   in the direction given by 'Dir'. Then compute the transform, if found.
findFaceTransform :: ConnectivityT -> Int -> Dir 2 -> Maybe FTransform
findFaceTransform (ConnectivityT c) t d = unsafePerformIO $ do
  let n = c'P4EST_FTRANSFORM
  ar <- Mut.new n
  t' <- fmap fromIntegral $ Mut.unsafeWith ar $ \p -> do
    c'p4est_find_face_transform c (fromIntegral t) (fromDir d) p
  if t' >= 0
    then Just . FTransform t' <$> Vec.freeze ar
    else pure Nothing

-- | Transfrom the quad to coordinates of the new tree, using the given face-
--   transform.
quadFaceTransform :: Quad -> FTransform -> Quad
quadFaceTransform q (FTransform ti ft) = unsafePerformIO $ do
  with q $ \p -> new q >>= \r -> Vec.unsafeWith ft $ \t -> do
    c'p4est_quadrant_transform_face (castPtr p) (castPtr r) t
    fmap (qTree .~ fromIntegral ti) $ peek r

-- #ccall p4est_expand_face_transform , CInt -> CInt -> Ptr CInt -> IO ()
-- c'p4est_expand_face_transform

------------------------------------------------------------------------------
-- | Search for the indicated opposite-corner, for the given tree, and then
--   compute the information needed for transformations between coordinates
--   for each tree.
findCornerTransform :: ConnectivityT -> Int -> Int -> Maybe CornerInfo
findCornerTransform (ConnectivityT c) t d = unsafePerformIO $ do
  alloca $ \p -> do
    -- setup the 'sc_array_t' object for the corner-transforms:
    let ar = ScArray (p'p4est_corner_info_t'corner_transforms p)
    SC.setup (ar :: ScArray CXformT)
    c'p4est_find_corner_transform c (fromIntegral t) (fromIntegral d) p
    -- marshal into Haskell data-type, and then deallocate:
    q <- fmap fromIntegral $ peek (p'p4est_corner_info_t'icorner p)
    x <- toVector ar
    SC.reset ar
    pure . Just $ CornerInfo q x

-- | Move a quadrant inside or diagonally outside a corner position.
--
--   NOTE: not a true coordinate-transform; e.g., if the given quad is one
--     tile in from the corner, the computed quad will still be right in the
--     corner of the new tree.
quadCornerTransform :: Quad -> CornerInfo -> Quad
quadCornerTransform q (CornerInfo _ cx) = unsafePerformIO $ new q >>= \p -> do
  qs <- flip Vec.mapM cx $ \(CXformT x) -> do
    let i = fromIntegral $ c'p4est_corner_transform_t'ntree x
        z = fromIntegral $ c'p4est_corner_transform_t'ncorner x
    c'p4est_quadrant_transform_corner (castPtr p) z 1
    (qTree .~ i) <$> peek p
  pure $ Vec.last qs


-- * Testing & examples
------------------------------------------------------------------------------
testConnectivity :: IO ()
testConnectivity  = do
  let vs = Vec.fromList [ V3 0 0 0, V3 1 0 0, V3 0 1 0, V3 1 1 0
                        , V3 2 1 0, V3 0 2 0, V3 1 2 0, V3 2 2 0
                        ] :: Vector (V3 Double)
      tx = Vec.fromList [ V4 0 1 2 3, V4 2 3 5 6, V4 3 4 6 7
                        ] :: Vector (V4 Int32)
--       ax = Vec.empty :: Vector ()
      tt = Vec.fromList [ V4 0 0 0 1, V4 1 2 0 1, V4 1 2 2 2
                        ] :: Vector (V4 Int32)
      m  = -1 :: Int32
      tf = Vec.fromList [ V4 0 0 0 2, V4 0 0 3 3, V4 1 1 3 3 ] :: Vector (V4 Int8) -- TTF
      tc = Vec.fromList [ V4 m m m 0, V4 m 0 m m, V4 0 m m m ] :: Vector (V4 Int32)
      co = Vec.fromList [ 0, 3 ] :: Vector Int32
      ct = Vec.fromList [ 0, 1, 2 ] :: Vector Int32
      cc = Vec.fromList [ 3, 1, 0 ] :: Vector Int8 -- CTC
      -- embedding:
      em = Conn2D vs tx tt tf tc co ct cc
  printf "em: %s\n" $ show em
