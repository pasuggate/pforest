{-# LANGUAGE DataKinds, DeriveGeneric, FlexibleContexts,
             GeneralizedNewtypeDeriving, KindSignatures, OverloadedStrings,
             PatternSynonyms, PolyKinds, ScopedTypeVariables, TemplateHaskell,
             TupleSections, ViewPatterns #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.Nodes
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Functions for working with `p4est_t` "nodes" objects.
--
-- TODO:
--  + store the
--
------------------------------------------------------------------------------

module Data.Forest.P4est.Nodes
  ( Nodes (..)
  , numLocalQuadrants
  , numOwnedIndeps
  , numOwnedShared
  , offsetOwnedIndeps
  , nodesIndep
  , nodesHangs
  , nodesLocal
  , sharedIndeps
  , sharedOffsets
  , nonLocalRanks
  , globalOwnedIndeps
  -- create/destroy/convert:
  , nodesNew
  , nodesDestroy
  , fromNodesT
  , CoarseInfo (..)
  , refinedFaces
  , refinedDiags
  , InterpInfo (..)
  , hangingFaces
  , coarserDiags
  -- queries:
  , doesFaceHang
  , doesDiagHang
  , isInterpNode
  -- nodes-sharing:
  , pattern MpiTagNodesShared
  , Sharers
  , Shareds
  , sharedCounts
  , shareSharedCounts
  , shareShareds
  , newShareShareds
  -- export:
  , saveNodes
  , loadNodes
  , yamlSaveNodes
  -- miscellaneous:
  , invert
  ) where

import           Control.Arrow                 (first)
import           Control.Distributed.MPI.Array as MPI
import           Control.Distributed.MPI.Handy as MPI
import           Control.Lens                  (makeLenses, (^.))
import           Control.Monad.IO.Class
import           Data.Aeson
import qualified Data.ByteString               as BS
import qualified Data.ByteString.Lazy          as Lazy
import           Data.Foldable                 (foldl', toList)
import qualified Data.IntMap                   as Map
import           Data.IntMap.Maps
import qualified Data.IntSet                   as Set
import           Data.IntSet.Sets
import           Data.Maybe                    (catMaybes)
import           Data.Vector.Extras
import           Data.Vector.Storable          (Vector)
import qualified Data.Vector.Storable          as Vec
import qualified Data.Vector.Storable.Mutable  as Mut
import qualified Data.Yaml.Pretty              as Y
import           Foreign.C.Types
import           Foreign.Marshal.Utils
import           Foreign.Ptr
import           Foreign.Storable
import           GHC.Generics                  (Generic)
import           Linear                        (V4 (..))
import           Text.Printf

import           Bindings.P4est.P4estNodes
import           Data.Forest.P4est.Base
import           Data.Scientific.SC            as SC
import           Data.Scientific.SC.Array      as Ar
import           Data.Scientific.SC.Recycle    as Re


-- * Nodes data-types
------------------------------------------------------------------------------
-- | Nodes data-structure using Haskell data-types.
--
--   NOTE:
--    + contains complete information about local, shared, and (any) ghost
--      nodes.
--
data Nodes
  = Nodes
      { _numLocalQuadrants :: Int
      , _numOwnedIndeps    :: Int
      , _numOwnedShared    :: Int
      , _offsetOwnedIndeps :: Int
      , _nodesIndep        :: Vector Indep
      , _nodesHangs        :: Vector Hangs
      , _nodesLocal        :: Vector (V4 Int)
      , _sharedIndeps      :: IntMap (Vector Int)
      , _sharedOffsets     :: Vector Int
      , _nonLocalRanks     :: Vector Int
      , _globalOwnedIndeps :: Vector Int
      }
  deriving (Eq, Show, Generic)

------------------------------------------------------------------------------
-- | A cell is "coarse" if adjacent to refined (or, "hanger") cells, as a
--   coarse cell needs to be refined prior to quadrature, etc.
--
--   TODO: store the "hanging nodes" on any corresponding faces?
--
data CoarseInfo
  = CoarseInfo
      { _refinedFaces :: V4 Bool
      , _refinedDiags :: V4 Bool
      }
  deriving (Eq, Show, Generic)

-- | The "interp" cells are those that have "hanging nodes"; i.e., they are
--   incident to coarser cells.
data InterpInfo
  = InterpInfo
      { _hangingFaces :: V4 Bool
      , _coarserDiags :: V4 Bool
      }
  deriving (Eq, Show, Generic)

-- ** Nodes-sharing patterns and data types
------------------------------------------------------------------------------
-- | MPI tag for sharing (additional) shared-nodes.
pattern MpiTagNodesShared :: MPI.Tag
pattern MpiTagNodesShared <- ((== succ MpiTagP4estLast) -> True)
  where MpiTagNodesShared  = succ MpiTagP4estLast

------------------------------------------------------------------------------
-- | For each MPI rank, associate an array of independent nodes to share with
--   it.
--
--   TODO: find a better home for this.
--
type Shareds = IntMap (Vector Indep)


-- * Instances for node and related data-types
------------------------------------------------------------------------------
instance ToJSON   Nodes
instance FromJSON Nodes

instance ToJSON   CoarseInfo
instance FromJSON CoarseInfo

instance ToJSON   InterpInfo
instance FromJSON InterpInfo


-- * Lenses
------------------------------------------------------------------------------
makeLenses ''Nodes
makeLenses ''CoarseInfo
makeLenses ''InterpInfo


-- * Nodes-layer functions
------------------------------------------------------------------------------
-- | Determine whether the indicated face of the given quad is adjacent to a
--   larger quad.
doesFaceHang :: Xforest 2 InterpInfo -> Quad -> Dir 2 -> Bool
doesFaceHang hs q d = maybe False chk (tofkey q<~hs) where
  chk = (!!fromDir d) . toList . _hangingFaces
{-# INLINE doesFaceHang #-}

doesDiagHang :: Xforest 2 InterpInfo -> Quad -> Int -> Bool
doesDiagHang hs q z = maybe False chk (tofkey q<~hs) where
  chk = (!!z) . toList . _coarserDiags
{-# INLINE doesDiagHang #-}

isInterpNode :: Xforest 2 InterpInfo -> Quad -> Int -> Bool
isInterpNode hs q z =
  or (doesDiagHang hs q z:doesFaceHang hs q `fmap` sectorDirs z)
{-# INLINABLE isInterpNode #-}


-- * Low-level bindings to 'p4est' local-nodes functions
------------------------------------------------------------------------------
nodesNew :: MonadIO m => P4estT -> GhostT -> m NodesT
nodesNew (P4estT p) (GhostT g) = liftIO $ do
  NodesT <$> c'p4est_nodes_new p g
{-# INLINE nodesNew #-}

nodesDestroy :: MonadIO m => NodesT -> m ()
nodesDestroy (NodesT g) = liftIO $ c'p4est_nodes_destroy g
{-# INLINE nodesDestroy #-}

{-- }
nodesIsValid :: MonadIO m => P4estT -> NodesT -> m Bool
nodesIsValid (P4estT p) (NodesT g) = liftIO $ do
  c'p4est_nodes_is valid p g
  return $ NodesT g
--}


-- * Import/export
------------------------------------------------------------------------------
fromNodesT :: MonadIO m => NodesT -> m Nodes
fromNodesT (NodesT p) = do
  mn <- fromEnum <$> SC.mpiSize
  liftIO $ do
    let fi = fromIntegral :: Integral i => i -> Int
    lq <- fmap fi . peek $ p'p4est_nodes'num_local_quadrants p
    oi <- fmap fi . peek $ p'p4est_nodes'num_owned_indeps p
    os <- fmap fi . peek $ p'p4est_nodes'num_owned_shared p
    oo <- fmap fi . peek $ p'p4est_nodes'offset_owned_indeps p
    -- fetch independent and hanging nodes:
    ir <- Ar.toVector . ScArray $ p'p4est_nodes'indep_nodes p
    hr <- Ar.toVector . ScArray $ p'p4est_nodes'face_hangings p
    -- read in the node-indices for each quad:
    ll <- Mut.new lq >>= \ar -> Mut.unsafeWith ar $ \q -> do
      s <- castPtr <$> peek (p'p4est_nodes'local_nodes p)
      copyBytes q s (lq*sizeOf (undefined :: V4 LInt))
      Vec.map (fmap fi) <$> Vec.unsafeFreeze ar
    -- read node-sharing info as an array of recycle-arrays:
    sx <- peekSharers $ ScArray $ p'p4est_nodes'shared_indeps p
    let ni = Vec.length ir
        nn = ni - oi
    so <- peek (p'p4est_nodes'shared_offsets p) >>= \q0 -> if q0 == nullPtr
      then pure Vec.empty
      else Mut.new ni >>= \ar -> Mut.unsafeWith ar $ \q1 -> do
        s <- peek (p'p4est_nodes'shared_offsets p)
        copyBytes q1 s (ni*sizeOf (undefined :: CInt))
        Vec.map fi <$> Vec.unsafeFreeze ar
    -- get the array of non-local ranks:
    nl <- Mut.new nn >>= \ar -> Mut.unsafeWith ar $ \q2 -> do
      s <- peek (p'p4est_nodes'nonlocal_ranks p)
      copyBytes q2 s (nn*sizeOf (undefined :: CInt))
      Vec.map fi <$> Vec.unsafeFreeze ar
    go <- Mut.new mn >>= \ar -> Mut.unsafeWith ar $ \q3 -> do
      s <- peek (p'p4est_nodes'global_owned_indeps p)
      copyBytes q3 s (mn*sizeOf (undefined :: LInt))
      Vec.map fi <$> Vec.unsafeFreeze ar
    return $ Nodes lq oi os oo ir hr ll sx so nl go


-- ** JSON save/load
------------------------------------------------------------------------------
saveNodes :: MonadIO m => FilePath -> Nodes -> m ()
saveNodes fp = liftIO . Lazy.writeFile fp . encode

loadNodes :: MonadIO m => FilePath -> m (Maybe Nodes)
loadNodes  = liftIO . fmap decode . Lazy.readFile

yamlSaveNodes :: MonadIO m => FilePath -> Nodes -> m ()
yamlSaveNodes fp = liftIO . BS.writeFile fp . Y.encodePretty ycfg
  where
    ycfg = Y.setConfCompare compare Y.defConfig


-- * Nodes-sharing between MPI processes
------------------------------------------------------------------------------
-- | Count the number of independent nodes that will be shared to each MPI
--   process.
sharedCounts :: Sharers -> RankMap CInt
sharedCounts  = fmap (fromIntegral . card) . invertSharers
{-# INLINE sharedCounts #-}

-- ** MPI functions
------------------------------------------------------------------------------
-- | Send all of the shared-dofs counts, for each MPI process, amongst all MPI
--   processes.
--
--   TODO:
--    + more efficient communication, than O(n^2)?
--
shareSharedCounts :: MonadIO m => Sharers -> m (Vector CInt)
shareSharedCounts ls = do
  size <- fromEnum <$> SC.mpiSize
  let cs = sharedCounts ls
      cx = Vec.replicate size 0 Vec.// fmap (first fromEnum) (flat cs)
  MPI.allToAll 1 cx

makeCounts :: Vector CInt -> RankMap CInt
makeCounts  = Vec.ifoldl' (\x i c -> (fromIntegral i, c) ~> x) bare

newShareShareds
  :: forall m a. MonadIO m
  => Storable a
  => MPI.Tag
  -> RankMap (Vector a)
  -> Vector CInt
  -> m (IntMap (Vector a))
newShareShareds tag arrays dests = do
  let counts = fromIntegral <$> makeCounts dests ^. _rankMap
  tx <- mpiSendArraysWithTag tag arrays
  rx <- mpiRecvArraysWithTag tag counts
  xs <- mpiWaitArrays tx rx
  pure $ foldl' (\ys (r, x) -> (fromEnum r, x) ~> ys) bare xs

------------------------------------------------------------------------------
-- | Using MPI, @isend@/@irecv@ all shared nodes needed by this process, from
--   other ranks, and vice versa.
--
--   Inputs:
--    + MPI communicator;
--    + MPI tag used to represent this operation;
--    + map of arrays of independent nodes, to be shared with each of the MPI
--      processes with a corresponding map-key;
--    + array containing the number of nodes to receive from other processes;
--   Output:
--    + map from ranks to an array of received nodes.
--
shareShareds ::
     MonadIO m
  => Comm
  -> MPI.Tag
  -> Shareds
  -> Vector CInt
  -> m Shareds
shareShareds comm tag shaz nums = liftIO $ do
  trace' "Sharing independent nodes"
  -- issue the send-requests:
  let tx r x
        | Vec.null x = pure Nothing
        | otherwise  = do
            let x' = Vec.unsafeCast x :: Vector CInt
                r' = fromIntegral r
                n' = Vec.length x'
            Vec.unsafeWith x' $ \p' -> do
              Just <$> MPI.isend (p', n') (Rank r') tag comm
      {-# INLINE tx #-}
  trc . printf "Nodes being sent to each process: %s" . show . Map.elems $ fmap len shaz
  ts <- catMaybes <$> uncurry tx `mapM` flat shaz

  -- setup the recv-buffers, and then issue the recv-requests:
  let rx _ 0 = pure Nothing
      rx r n = do
        ar <- Mut.new (fromIntegral n)
        rq <- Mut.unsafeWith ar $ \p -> do
          let r' = fromIntegral r
              p' = castPtr p :: Ptr CInt
              n' = n*s
          MPI.irecv (p', n') (Rank r') tag comm
        pure $ Just (rq, (r, ar))
      {-# INLINE rx #-}
      ns' = [0..] `zip` Vec.toList nums
      s   = fromIntegral $ sizeOf (undefined :: Indep) `div` sizeOf (undefined :: CInt)
  trc . printf "Receiving nodes: %s" . show $ Vec.toList nums
  rs <- catMaybes <$> uncurry rx `mapM` ns'

  -- wait for the Tx/Rx to finish:
  let go q (r, ar) = do
        MPI.wait_ q >> (r,) <$> Vec.freeze ar
      {-# INLINE go #-}
  MPI.wait_ `mapM_` ts
  Map.fromList <$> uncurry go `mapM` rs


-- * Helper functions
------------------------------------------------------------------------------
-- | Inverts the map from `i :-> j` to `j :-> i`.
invert :: IntMap IntSet -> IntMap IntSet
invert  = Map.foldlWithKey' (\x k -> go k `Set.foldl'` x) bare where
  go j y l = Map.alter (Just . maybe (unit j) (j~>)) l y
  {-# INLINE go #-}

-- | Helper-function for reading the "sharers" array from the nodes object.
peekSharers :: ScArray (ArRecycle CInt) -> IO (IntMap (Vector Int))
peekSharers ar = do
  xs <- Vec.toList <$> Ar.toVector ar
  ys <- flip mapM xs $ \x -> with (Re.unRecycle x) $ \p -> do
    Vec.map fromIntegral <$> Re.toVector (ScRecycle p :: ScRecycle CInt)
--   print . Map.fromList $ [0..] `zip` ys
  pure . Map.fromList $ [0..] `zip` ys
{-# INLINE peekSharers #-}
