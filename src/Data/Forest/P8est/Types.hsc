{-# LANGUAGE CPP, DeriveGeneric, GeneralizedNewtypeDeriving,
             OverloadedStrings #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P8est.Types
-- Copyright   : (C) Patrick Suggate 2019
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Mappings between Haskell and C representations of `p8est` data-types.
--
-- Changelog:
--  + 03/06/2019  --  initial file;
--
------------------------------------------------------------------------------

#include <p8est.h>
#include <sc.h>
#include <bindings.dsl.h>
module Data.Forest.P8est.Types
  ( module Data.Forest.Mesh
  , module Data.Forest.P8est.Types
  , module Data.Scientific.SC
  ) where

import           Bindings.P8est.P8estConnectivity
import           Control.Lens                     (Lens', lens)
import           Data.Forest.Mesh
import           Data.Scientific.SC
import           Data.Typeable                    (Typeable)
import           Foreign.Storable
import           GHC.Generics                     (Generic)

{-- }
import           Bindings.P8est.P8est
import           Bindings.P8est.P8estBase
import           Bindings.P8est.P8estBits
import           Bindings.P8est.P8estGeometry
import           Bindings.P8est.P8estGhost
import           Bindings.P8est.P8estNodes
import           Bindings.SC.ScContainers         (C'sc_array, C'sc_mempool)
import           Control.Distributed.MPI          as MPI
import           Data.Index.ZCurve
import           Data.Scientific.SC.Array
import           Foreign.C.Types
import           Linear.Handy
--}


-- * Core data-types
------------------------------------------------------------------------------
-- | Used when computing 2:1 balance.
newtype ConnectT =
  ConnectT { unConnectT :: C'p8est_connect_type_t }
  deriving (Eq, Show, Storable, Generic, Typeable)


-- * Accessor functions
------------------------------------------------------------------------------
-- | Lens/accessor for the @Region@ (sub-)mesh of a field.
_unConnectT :: Lens' ConnectT C'p8est_connect_type_t
_unConnectT  = lens unConnectT (\r s -> r { unConnectT = s })
{-# INLINE _unConnectT #-}
