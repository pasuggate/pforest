{-# LANGUAGE BangPatterns, DataKinds, FlexibleContexts, FlexibleInstances,
             MultiParamTypeClasses, OverloadedStrings, PatternSynonyms,
             RankNTypes, ScopedTypeVariables, TypeFamilies #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.Base
-- Copyright   : (C) Patrick Suggate, 2017
-- License     : GPL3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Functions for working with `p4est_t` "forest" objects.
--
-- TODO:
--
------------------------------------------------------------------------------

module Data.Forest.P4est.Base
  ( module Data.Forest.P4est.Types
  , fromQuadrantT
  , withQuadrantT
  , showQuad
    -- quadrant getters:
  , quadrantX
  , quadrantY
  , quadrantLevel
  , getLevel
  , getCoord
    -- quad parent/child operations:
  , quadParent
  , quadSibling
  , quadChild
  , quadFirstDescendant
  , quadLastDescendant
  , lastQuad
--   , firstQuad
  , zeroQuad
    -- quad faces:
  , quadFaceNeighbour
  , quadHalfFaceNeighbours
  , quadHalfFaceNeighbours'
  , quadAllFaceNeighbours
  , quadFaceNbr
  , quadFaceNbrs
  , quadFaceNbrsHalf
  , quadFaceNbrsAll
    -- quad corners:
  , quadCornerNeighbour
  , quadHalfCornerNeighbour
  , cornerNbr
  , cornerNbrHalf
  , cornerNbrTwice
    -- quad tests:
  , overlaps
  , disjoint
  , isInside
  , outsideFace
  , outsideCorner
    -- quad coords & keys:
  , toquad
  , tofkey
  , toleaf, q2leaf
  , l2quad
  , quadToKey
  , keyToQuadUnsafe
  , keyToQuad
  , coordToQuadUnsafe
  , coordToQuad
  , lastCoordOffset
    -- conversions:
  , buildLtree
  , quadsToLtree
  , ltreeToQuadsUnsafe
    -- face directions:
  , toDir
  , fromDir
  , oppDir
  , faceDirs
  , sectorDirs
  , dirElem
    -- face transforms:
  , toAxes
  , fromAxes
  , toTransform
    -- low-level quad functions:
  , p2i
  , p2j
    -- logging helpers:
  , trace'
  , trc
  )
where

import           Control.Lens                 ((^.))
import           Control.Logging
import           Control.Monad.IO.Class
import           Control.Monad.ST             (runST)
import           Data.Bits.Handy              (i32, testBit, (.&.), (<<%),
                                               (>>%))
import           Data.IntMap.Maps
import           Data.Text                    (Text, pack)
import           Data.Vector.Storable         (Vector, (!))
import qualified Data.Vector.Storable         as Vec
import qualified Data.Vector.Storable.Mutable as Mut
import           Foreign.C.Types
import           Foreign.Marshal.Alloc        (alloca)
import           Foreign.Marshal.Utils
import           Foreign.Ptr
import           Foreign.Storable
import           GHC.Int
import           GHC.Types                    (SPEC (..))
import           GHC.Word
import           Linear.Handy                 (M22, V2 (..), V3 (..), V4 (..),
                                               identity, (!*!), _w, _x, _y, _z)
import           System.IO.Unsafe
import           Text.Printf

import           Bindings.P4est.P4est
import           Bindings.P4est.P4estBits
import           Data.Forest.Linear
import           Data.Forest.P4est.Types
import           Data.Index.ZCurve            (Key (..), maxDepth2)
import           Data.Tree.Linear             (Ltree (..))
import qualified Data.Tree.Linear             as Ltree


-- * Instances
------------------------------------------------------------------------------


-- * User API functions
------------------------------------------------------------------------------
fromQuadrantT :: MonadIO m => QuadrantT -> m Quad
fromQuadrantT (QuadrantT p') = liftIO $ let p = castPtr p' in peek p

-- | Allocate some memory, perform the action, and then deallocate the memory.
withQuadrantT :: MonadIO m => Quad -> (QuadrantT -> IO a) -> m a
withQuadrantT q k = liftIO $ alloca $ \p -> do
  poke p q
  let q' = QuadrantT $ castPtr p
  k q'

-- ** Quadrant setters & getters.
------------------------------------------------------------------------------
quadrantX :: QuadrantT -> IO QInt
quadrantX (QuadrantT ptr) = peek $ p'p4est_quadrant'x ptr
{-# INLINE quadrantX #-}

quadrantY :: QuadrantT -> IO QInt
quadrantY (QuadrantT ptr) = peek $ p'p4est_quadrant'y ptr
{-# INLINE quadrantY #-}

quadrantLevel :: QuadrantT -> IO Word8
quadrantLevel (QuadrantT ptr) = fmap fromIntegral $ peek $ p'p4est_quadrant'level ptr
{-# INLINE quadrantLevel #-}

------------------------------------------------------------------------------
-- TODO: Should still use `IO`, as `QuadrantT's are mutable?
getLevel :: QuadrantT -> Int
getLevel  = fromIntegral . unsafePerformIO . quadrantLevel
{-# INLINE getLevel #-}

getCoord :: QuadrantT -> (Int, Int)
getCoord quad = unsafePerformIO $ do
  x <- fromIntegral <$> quadrantX quad
  y <- fromIntegral <$> quadrantY quad
  return (x,y)
{-# INLINE getCoord #-}

-- ** Quadrant modifiers
------------------------------------------------------------------------------
-- | Generate the parent of the given quadrant.
quadParent :: Quad -> Quad
quadParent q = unsafePerformIO $ with q $ \qp -> do
  rp <- new q
  c'p4est_quadrant_parent (castPtr qp) (castPtr rp)
  peek rp
{-# INLINE quadParent #-}

-- | Generate the quadrant for the inidicated sibling [0..3].
quadSibling :: Quad -> Int -> Quad
quadSibling q j = unsafePerformIO $ with q $ \qp -> do
  rp <- new q
  c'p4est_quadrant_sibling (castPtr qp) (castPtr rp) (fromIntegral j)
  peek rp
{-# INLINE quadSibling #-}

------------------------------------------------------------------------------
-- | Compute the child corresponding to the given quadrant Z-index.
--   NOTE: from a future version of 'p4est'.
quadChild :: Quad -> Int -> Quad
quadChild q z = unsafePerformIO $ with q $ \qp -> do
  rp <- new q
  c'p4est_quadrant_child (castPtr qp) (castPtr rp) (fromIntegral z)
  peek rp
{-# INLINE quadChild #-}

------------------------------------------------------------------------------
-- | Generate the quadrant for the first descendant of the indicated level.
--   NOTE: level must be greater than that of the given quad.
quadFirstDescendant :: MonadIO m => Quad -> Int -> m Quad
quadFirstDescendant q l = liftIO $ with q $ \qp -> do
  rp <- new q
  let q' = castPtr qp
      r' = castPtr rp
  c'p4est_quadrant_first_descendant q' r' (fromIntegral l)
  peek rp
{-# INLINE quadFirstDescendant #-}

-- | Generate the quadrant for the last descendant of the inidicated level.
--   NOTE: level must be greater than that of the given quad.
quadLastDescendant :: MonadIO m => Quad -> Int -> m Quad
quadLastDescendant q l = liftIO $ with q $ \qp -> do
  rp <- new q
  let q' = castPtr qp
      r' = castPtr rp
  c'p4est_quadrant_last_descendant q' r' (fromIntegral l)
  peek rp
{-# INLINE quadLastDescendant #-}

------------------------------------------------------------------------------
-- | Compute quad across from the indicated face.
quadFaceNeighbour :: MonadIO m => Quad -> Dir 2 -> m Quad
quadFaceNeighbour q d = liftIO $ with q $ \qp -> do
  rp <- new q
  let q' = castPtr qp
      r' = castPtr rp
  c'p4est_quadrant_face_neighbor q' (fromDir d) r'
  peek rp
{-# INLINE quadFaceNeighbour #-}

-- | Get both the half-sized quads across the given face.
--   Returns:
--    x[0] -- first half-sized quad;
--    x[1] -- second half-sized quad;
--    x[2] -- first smallest-sized, upper-right quad;
--    x[3] -- second smallest-sized, upper-right quad;
--
--   NOTE: x[2] and x[3] may be useful for testing the whether other quads are
--     still within either of the two half-quads?
quadHalfFaceNeighbours' :: MonadIO m => Quad -> Dir 2 -> m (Vector Quad)
quadHalfFaceNeighbours' q d = liftIO $ with q $ \qp -> do
  ar <- Vec.unsafeThaw $ Vec.replicate 4 q
  Mut.unsafeWith ar $ \rp -> do
    let q' = castPtr qp
        r' = castPtr rp
        w' = plusPtr rp $ 2*sizeOf (undefined :: Quad)
    c'p4est_quadrant_half_face_neighbors q' (fromDir d) r' w'
  Vec.freeze ar
{-# INLINE quadHalfFaceNeighbours' #-}

-- | Get both the half-sized quads across the given face.
--   Returns:
--    V2.x -- first half-sized quad;
--    V2.y -- second half-sized quad;
quadHalfFaceNeighbours :: MonadIO m => Quad -> Dir 2 -> m (V2 Quad)
quadHalfFaceNeighbours q d = liftIO $ with q $ \qp -> do
  with (V2 q q) $ \rp -> do
    let q' = castPtr qp
        r' = castPtr rp
    c'p4est_quadrant_half_face_neighbors q' (fromDir d) r' nullPtr
    peek rp
{-# INLINE quadHalfFaceNeighbours #-}

-- | Compute all of the face-neighbours for the given direction. The returned
--   (four) faces are ordered from finest to coarsest:
--    a) & b) two finer (half-sized) neighbours touching this face;
--    c) the same level, across the 'Dir' face; and
--    d) the next level coarser, and adjacent.
--   NOTE: if the there is no quad possible for some level, then the returned
--     quad will be initialized to 'P4EST_QUADRANT_INIT'.
quadAllFaceNeighbours :: MonadIO m => Quad -> Dir 2 -> m (V4 Quad)
quadAllFaceNeighbours q d = liftIO $ with q $ \qp -> do
  with (V4 q q q q) $ \rp -> do
    c'p4est_quadrant_all_face_neighbors (castPtr qp) (fromDir d) (castPtr rp)
    peek rp
{-# INLINE quadAllFaceNeighbours #-}

------------------------------------------------------------------------------
-- | Compute quad across from the indicated corner.
quadCornerNeighbour :: MonadIO m => Quad -> Int -> m Quad
quadCornerNeighbour q c = liftIO $ with q $ \qp -> do
  rp <- new q
  c'p4est_quadrant_corner_neighbor (castPtr qp) (fromIntegral c) (castPtr rp)
  peek rp
{-# INLINE quadCornerNeighbour #-}

-- | Compute half-sized quad across from the indicated corner.
quadHalfCornerNeighbour :: MonadIO m => Quad -> Int -> m Quad
quadHalfCornerNeighbour q c = liftIO $ with q $ \qp -> new q >>= \rp -> do
  let q' = castPtr qp
      r' = castPtr rp
  c'p4est_quadrant_half_corner_neighbor q' (fromIntegral c) r'
  peek rp
{-# INLINE quadHalfCornerNeighbour #-}

------------------------------------------------------------------------------
{-- }
-- | Generate the quadrant for the inidicated sibling [0..3].
--   TODO: requires a later version of 'p4est'.
quadChild :: Quad -> Int -> IO Quad
quadChild q j = with q $ \qp -> do
  rp <- new q
  let q' = castPtr qp
      r' = castPtr rp
  c'p4est_quadrant_child q' r' (fromIntegral j)
  peek rp
--}

-- ** UnsafeIO versions
--    These should OK, in general.
--    TODO: analyse.
------------------------------------------------------------------------------
-- | Compute quad across from the indicated face.
quadFaceNbr :: Quad -> Dir 2 -> Quad
quadFaceNbr  = (unsafePerformIO .) . quadFaceNeighbour
{-# INLINE quadFaceNbr #-}

-- | Construct the four neighbours adjacent to each of the given quad's faces
--   (Z-order).
quadFaceNbrs :: Quad -> V4 Quad
quadFaceNbrs q = quadFaceNbr q <$> faceDirs
{-# INLINE quadFaceNbrs #-}

-- | Get both the half-sized quads across the given face.
--   Returns:
--    V2.x -- first half-sized quad;
--    V2.y -- second half-sized quad;
quadFaceNbrsHalf :: Quad -> Dir 2 -> V2 Quad
quadFaceNbrsHalf  = (unsafePerformIO .) . quadHalfFaceNeighbours
{-# INLINE quadFaceNbrsHalf #-}

-- | Compute all of the face-neighbours for the given direction. The returned
--   (four) faces are ordered from finest to coarsest:
--    a) & b) two finer (half-sized) neighbours touching this face;
--    c) the same level, across the 'Dir' face; and
--    d) the next level coarser, and adjacent.
--   NOTE: if the there is no quad possible for some level, then the returned
--     quad will be initialized to 'P4EST_QUADRANT_INIT'.
quadFaceNbrsAll :: Quad -> Dir 2 -> V4 Quad
quadFaceNbrsAll  = (unsafePerformIO .) . quadAllFaceNeighbours
{-# INLINE quadFaceNbrsAll #-}

------------------------------------------------------------------------------
-- | Compute quad across from the indicated corner.
cornerNbr :: Quad -> Int -> Quad
cornerNbr  = (unsafePerformIO .) . quadCornerNeighbour
{-# INLINE cornerNbr #-}

-- | Compute half-sized quad across from the indicated corner.
cornerNbrHalf :: Quad -> Int -> Quad
cornerNbrHalf  = (unsafePerformIO .) . quadHalfCornerNeighbour
{-# INLINE cornerNbrHalf #-}

-- | Compute quad across from the indicated corner.
cornerNbrTwice :: Quad -> Int -> Maybe Quad
cornerNbrTwice q z
  | qc == q   = Just $ cornerNbr qp z
  | otherwise = Nothing
  where
    (qp, qc) = (quadParent q, quadChild qp z)
{-# INLINE cornerNbrTwice #-}

------------------------------------------------------------------------------
-- | Compute the quadrant with the maximum possible Z-index; i.e., the last
--   possible quadrant of a tree.
lastQuad :: Quad -> Quad
lastQuad q = unsafePerformIO $ quadLastDescendant q QuadMaxLevel
{-# INLINE lastQuad #-}

-- | Build the quadrant such that no other quadrant (from this tree) can be
--   ordered before this quadrant.
--   TODO: not of any use?
zeroQuad :: Quad -> Quad
zeroQuad q = q { _qLocation = 0, _qLevel = 0 }
{-# INLINE zeroQuad #-}

{-- }
-- | Compute
--   TODO/FIXME: not useful in its current form, due to the way the 'p4est'
--     Z-indices work (where the shallowest quad with SW corner at a given
--     location has the lowest index).
firstQuad :: Quad -> Quad
-- firstQuad q = unsafePerformIO $ quadFirstDescendant q QuadMaxLevel
firstQuad q = unsafePerformIO $ quadFirstDescendant q RubsMaxLevel
--}

------------------------------------------------------------------------------
-- | Test if two quadrants overlap.
--   NOTE: modified to compare trees as well.
overlaps :: Quad -> Quad -> Bool
overlaps q0 q1
  | t0 /= t1  = False
  | otherwise = unsafePerformIO $ do
      with q0 $ \p0 -> with q1 $ \p1 -> do
        (0/=) <$> c'p4est_quadrant_overlaps (castPtr p0) (castPtr p1)
  where (t0, t1) = (_qTree q0, _qTree q1)
{-# INLINE overlaps #-}

-- | Compare two quadrants in their Morton ordering, with equivalence if the
--   two quadrants overlap.
--   NOTE: modified to compare trees as well.
disjoint :: Quad -> Quad -> Ordering
disjoint q0 q1
  | t0 /= t1  = compare t0 t1
  | otherwise = unsafePerformIO $ do
      with q0 $ \p0 -> with q1 $ \p1 ->
        flip compare 0 <$> c'p4est_quadrant_disjoint (castPtr p0) (castPtr p1)
  where (t0, t1) = (_qTree q0, _qTree q1)
{-# INLINE disjoint #-}

------------------------------------------------------------------------------
-- | Returns @True@ if the given quad is inside the unit-tree.
--   NOTE: just a coordinate-test.
isInside :: Quad -> Bool
isInside q = unsafePerformIO $ with q $ \p ->
  (/=0) <$> c'p4est_quadrant_is_inside_root (castPtr p)
{-# INLINE isInside #-}

outsideFace :: Quad -> Bool
outsideFace q = unsafePerformIO $ with q $ \p ->
  (/=0) <$> c'p4est_quadrant_is_outside_face (castPtr p)
{-# INLINE outsideFace #-}

outsideCorner :: Quad -> Bool
outsideCorner q = unsafePerformIO $ with q $ \p ->
  (/=0) <$> c'p4est_quadrant_is_outside_corner (castPtr p)
{-# INLINE outsideCorner #-}

-- ** Quadrant conversions
------------------------------------------------------------------------------
-- | Compute the Z-index 'key' in a form that is compatible with `Ltree`.
quadToKey :: Quad -> Key 2
quadToKey  = reify . quadToCoord
{-# INLINE quadToKey #-}

keyToQuadUnsafe :: Key 2 -> Quad
keyToQuadUnsafe  = coordToQuadUnsafe . lpath
{-# INLINE keyToQuadUnsafe #-}

keyToQuad :: TreeID -> Rank -> Key 2 -> Quad
keyToQuad t r = coordToQuad t r . lpath
{-# INLINE keyToQuad #-}

------------------------------------------------------------------------------
-- | Build a 'Quad' using the given coordinate, but without rank and tree
--   information.
coordToQuad :: TreeID -> Rank -> V3 Int -> Quad
coordToQuad t r (V3 x y l) = Quad (V2 x' y') l' 0 0 t' r'
  where
    (x', y') = (fromIntegral x<<%o, fromIntegral y<<%o)
    (o , l') = (treeMaxLevel - maxDepth2, fromIntegral l)
    (t', r') = (_unTreeID t, fromIntegral r)
{-# INLINE coordToQuad #-}

-- | Build a 'Quad' using the given coordinate, but without rank and tree
--   information.
coordToQuadUnsafe :: V3 Int -> Quad
coordToQuadUnsafe  = toEnum (-1) `coordToQuad` (-1 :: Rank)
{-# INLINE coordToQuadUnsafe #-}

-- | For the given quadrant-level, calculate the greatest possible offset for
--   a quadrant at that level, for it to fit within the unit tree.
lastCoordOffset :: (Integral i, Num a) => i -> a
lastCoordOffset  = fromIntegral . c'P4EST_LAST_OFFSET . fromIntegral
{-# INLINE lastCoordOffset #-}

-- ** Tree conversions
------------------------------------------------------------------------------
-- | As long as the given tree only contains quads from
--   FIXME:
buildLtree :: Tree -> Ltree 2
buildLtree  = Ltree.fromList . map quadToKey . Vec.toList . _tQuads
-- buildLtree (Tree qs fq lq qo lc ml) =
--   let ks = quadToKey `Vec.map` qs

quadsToLtree :: Vector Quad -> Ltree 2
quadsToLtree  = Ltree.fromList . map quadToKey . Vec.toList
{-# INLINABLE quadsToLtree #-}

ltreeToQuadsUnsafe :: Ltree 2 -> Vector Quad
ltreeToQuadsUnsafe  = Vec.fromList . fmap keyToQuadUnsafe . flat


-- * Helper functions
------------------------------------------------------------------------------
-- | Convert "column-pointers" (or, "offset sums") into a list of "column"
--   indices.
p2j :: Vector Int -> Vector Int
p2j cp = runST $ do
  let nnz = Vec.unsafeLast cp
  ci <- Mut.new nnz
  let go !p !q !j
        | p  >= nnz = return ()
        | p  ==  q  = go p (cp!(j+2)) (j+1)
--         | otherwise = Mut.write ci p j >> go (p+1) q j
        | otherwise = Mut.unsafeWrite ci p j >> go (p+1) q j
  go 0 (cp!1) 0
  Vec.unsafeFreeze ci
{-# INLINE p2j #-}

p2i :: forall i. (Integral i, Storable i) => Vector i -> Vector i
p2i cp = runST $ do
  let nnz = Vec.unsafeLast cp
      fi  = fromIntegral :: Integral i => i -> Int
  ci <- Mut.new $ fi nnz
  let go !_ !p !q !j
        | p  >= nnz = return ()
        | p  ==  q  = go SPEC p (cp!fi (j+2)) (j+1)
--         | otherwise = Mut.write ci (fi p) j >> go SPEC (p+1) q j
        | otherwise = Mut.unsafeWrite ci (fi p) j >> go SPEC (p+1) q j
  go SPEC 0 (cp!1) 0
  Vec.unsafeFreeze ci
{-# INLINE p2i #-}

-- ** Face-direction functions
------------------------------------------------------------------------------
-- | Convert a 'Dir'-value to an integral, and typically a 'CInt' when calling
--   into the 'p4est' C library.
fromDir :: Integral i => Dir 2 -> i
fromDir  = fromIntegral . fromEnum
{-# INLINE fromDir #-}

-- | Build the 'Dir'-value for an integral, and often from a 'CInt' when from
--   the 'p4est' C library.
toDir :: Integral i => i -> Dir 2
toDir  = toEnum . fromIntegral
{-# INLINE toDir #-}

-- | Flip a direction.
oppDir :: Dir 2 -> Dir 2
oppDir West  = East
oppDir East  = West
oppDir South = North
oppDir North = South
oppDir d     = error $ printf "P4est.Base.oppDir: invalid direction: %d" (fromEnum d)
{-# INLINE oppDir #-}

-- | Ascending array of face-indices.
faceDirs :: V4 (Dir 2)
faceDirs  = V4 West East South North
{-# INLINE faceDirs #-}

-- | Map from corner Z-index to the directions of incident faces.
sectorDirs :: Integral i => i -> [Dir 2]
sectorDirs 0 = [West, South]
sectorDirs 1 = [South, East]
sectorDirs 2 = [West, North]
sectorDirs 3 = [North, East]
sectorDirs _ = []
{-# INLINE sectorDirs #-}

-- | Use the given direction to index an element.
dirElem :: Dir 2 -> V4 a -> a
dirElem West  = (^._x)
dirElem East  = (^._y)
dirElem South = (^._z)
dirElem North = (^._w)
dirElem d     = error $ printf "P4est.Base.dirElem: invalid direction: %d" (fromEnum d)
{-# INLINE dirElem #-}

-- ** Face-transformation functions
------------------------------------------------------------------------------
toAxes :: Num a => Dir 2 -> V2 a
toAxes West  = V2 (-1)  0
toAxes East  = V2   1   0
toAxes South = V2   0 (-1)
toAxes North = V2   0   1
toAxes d     = error $ printf "P4est.Base.toAxes: invalid direction: %d" (fromEnum d)
{-# INLINE toAxes #-}

fromAxes :: (Eq a, Num a) => V2 a -> Maybe (Dir 2)
fromAxes (V2 (-1)  0 ) = Just West
fromAxes (V2   1   0 ) = Just East
fromAxes (V2   0 (-1)) = Just South
fromAxes (V2   0   1 ) = Just North
fromAxes _             = Nothing
{-# INLINE fromAxes #-}

-- | Encoded such that 'West' is rotated to have the given rotation, and then
--   an x-reflection applied, if requested.
toTransform :: Num a => Orient 2 -> M22 a
toTransform (Orient o) = case o `testBit` 2 of
  False -> rot
  _     -> mir !*! rot
  where
    rot = case o .&. 3 of
      0 -> identity
      1 -> V2   0   1  `V2` V2 (-1)  0
      2 -> V2 (-1)  0  `V2` V2   0 (-1)
      3 -> V2   0 (-1) `V2` V2   1   0
    mir  = V2 (-1)  0  `V2` V2   0   1
{-# INLINE toTransform #-}

-- ** Local-forest helpers
------------------------------------------------------------------------------
-- | Convert a 2-cell to a quadrant.
toquad :: Fkey 2 -> Quad
toquad k = Quad (xy :: V2 Int32) l 0 0 (i32 t) $ toEnum (-1) where
  Leaf xy l t = lpath k
{-# INLINE toquad #-}

tofkey :: Quad -> Fkey 2
tofkey (Quad xy l _ _ t _) = reify $ Leaf xy l $ fromIntegral t
{-# INLINE tofkey #-}

------------------------------------------------------------------------------
-- | Convert between quadrants and leaf-nodes.
toleaf :: Quad -> Leaf 2
toleaf (Quad xy l _ _ t _) = Leaf xy l (TreeID t)
{-# INLINE toleaf #-}

q2leaf :: Quad -> Leaf 2
q2leaf  = toleaf
{-# INLINE q2leaf #-}

-- | Convert between quadrants and leaf-nodes.
l2quad :: Leaf 2 -> Quad
l2quad (Leaf xy l t) = Quad xy l (-1) (-1) (fromIntegral t) (-1)
{-# INLINE l2quad #-}

-- ** Extra output
------------------------------------------------------------------------------
-- | Display a quad.
showQuad :: Quad -> String
showQuad (Quad (V2 xi yi) li _ _ ti _) =
  let t = fromIntegral ti :: Int
      l = fromIntegral li :: Int
      x = fromIntegral xi >>% (NodeMaxLevel-l) :: Int
      y = fromIntegral yi >>% (NodeMaxLevel-l) :: Int
      d = 1 <<% l :: Int
  in  printf "Q(t=%1d,l=%02d): (%d/%d, %d/%d)" t l x d y d

-- ** Logging helper functions
------------------------------------------------------------------------------
trace' :: MonadIO m => Text -> m ()
trace'  = (>> flushLog) . liftIO . debugS "P4est<pforest>"
{-# INLINE trace' #-}

trc :: MonadIO m => String -> m ()
trc  = trace' . pack
{-# INLINE trc #-}
