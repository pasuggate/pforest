This is the *mid-level API* for working with parallel forests, as it still uses quite low-level abstractions (and functions) so that it remains as general-purpose as the API of the underlying `p4est` library. There are some convenience data-types and a monadic execution environment so that a user is less-frequently required to explicitly call destructors, nor having to store some mesh components to named variables.

The top-level modules for the mid-level API are:

```haskell
  module Data.Forest.P4est.P4est -- ^ parallel forests of quadrilateral elements
  module Data.Forest.P8est.P8est -- ^ parallel forests of hexahedral elements
```

where the exposed `pforest` mid-level API's have the following abstractions and groupings (and therefore subsections in this document):

#. [A First Basic Example](#sec:first-basic)

#. [The `P4estM` Environment](#sec:p4estm)

#. [Constructors and Destructors](#sec:condes)

#. [Mesh Shape and Partitioning](#sec:mesh-shape)

#. [Forest Navigation and Queries](#sec:queries-api)

#. [Forest Traversal Functionality](#sec:traverse-api)

#. [Indices, Coordinates, and Cells](#sec:index-coords)

#. [Import and Export](#sec:midlevel-io)

There are [higher-level data-types and functions](#sec:hilevel-api) exposed by additional modules of this library, and they are detailed elsewhere. The primary purpose for this library to expose the mid-level API is for users of this library to implement additional high-level functionality for specific data-types and use-cases; e.g., for RUB-spline meshes and any related FEA tools.

\clearpage

## A First Basic Example ## {#sec:first-basic}

A very simple example of how to write a program using these functions is given by the following program code:

```haskell
  module Main where

  import System.Environment (getArgs)
  import Control.Monad.IO.Class
  import Text.Printf
  import Data.Scientific.SC as SC
  import Data.Forest.P4est as P4

  main :: IO ()
  main  = do
    _ <- getArgs >>= \args -> P4.withSC args $ do
      P4.withSquare $ do
        rank <- fromEnum <$> SC.mpiRank
        liftIO $ printf "basic 'square' example: MPI rank = %d\n" rank
        P4.vtkWriteFile "square"
    pure ()
```

The `SC.`{.haskell} and `P4.`{.haskell} prefixes were used to show which libraries that the various functions came from.

\clearpage

## The `P4estM` Environment ## {#sec:p4estm}

Monadic `P4estM`{.haskell} actions are evaluated in an environment that contains stateful parallel-forest data, and can also include ghost-layers and mesh-nodes information. The data type has the following definition:

```haskell
  newtype P4estM a = P4estM { unP4estM :: StateT P4estState SCM a }
```

and supports `IO`{.haskell} (via the `SCM a`{.haskell} "base" monad) and stateful actions (via the `StateT s m a`{.haskell} state-transformer monad).

Mid-level API actions are performed within this `P4estM (d :: Nat)`{.haskell} environment, and have access to the underlying `p4est` data types.

### Data Type for Forest State ###

The state-variable:

```haskell
  data P4estState = P4estState { _p4est :: P4estT
                               , _p4raw :: Maybe P4estRaw
                               , _ghost :: Maybe GhostT
                               , _nodes :: Maybe NodesT }
```

stores a pointer to the `p4est` data structure (with data type `P4estT`{.haskell}). It optionally also stores a Haskell-ised version of this, `P4estRaw (d :: Nat)`{.haskell} (to reduce the number of conversions required for some operations), as well as a ghost-layer and mesh-nodes data if they have been constructed.

\clearpage

## Constructors and Destructors ## {#sec:condes}

The top-level API functions for constructing and destroying parallel forests are:

+ `withP4estM :: ConnectivityT -> P4estM a -> SCM a`{.haskell} \
   Perform the given action within a parallel-forest context. \
  *Inputs*:
  + `ConnectivityT`{.haskell} -- macro-mesh object that is shared amongst all MPI processes;
  + `P4estM a`{.haskell} -- monadic parallel-forest action to perform within the new context;

  *Outputs*:
  + `a` -- the result of executing the given monadic action (and returned within a *'Scientific Computation'* (`SC`) monad/environment, which is the "base" monad of `P4estM a`{.haskell}). \

+ `withSquare :: P4estM a -> SCM a`{.haskell} \
  Convenience version of `withP4estM` above, but also calls `meshDestroy` to deallocate the macro-mesh when the computation has finished. \
  *Inputs*:
  + `P4estM a`{.haskell} -- monadic parallel-forest action to perform within the new context;

  *Outputs*:
  + `a` -- the result of executing the given monadic action (and returned within a *'Scientific Computation'* (`SC`) monad/environment, which is the "base" monad of `P4estM a`{.haskell}). \

+ `meshDestroy :: MonadIO m => ConnectivityT -> m ()`{.haskell} \
  Routine for destroying/deallocating a connectivity structure. \
  *Inputs*:
  + `ConnectivityT`{.haskell} -- macro-mesh object that is shared amongst all MPI processes, and that will be deallocated by this function-call;

  *Outputs*:
  + *none*

To build a parallel forest, first its *"connectivity structure"* [@burstedde2011p4est] is required, and this defines the *'macro mesh'* that is shared amongst all MPI processes. Some [constructors for macro meshes](#sec:build-macro) are given in a later subsection.

There are additional constructors that add *"layers"* to the forest, and these include:

#. [*"Ghost"* & *"mirror"* layers](#sec:ghost-layers) of cells that are adjacent to the subforest boundaries between local subforests that are stored within each MPI process.

#. [Global *'Node'*-index labelling](#sec:mesh-nodes) data structures that are used to give each independent node of the mesh a unique label.

### Macro-Mesh Construction ### {#sec:build-macro}

Functionality and data types for importing, exporting, and creating macro-meshes is defined within the following module:

```haskell
  module Data.Forest.P4est.Connectivity
```

Built-in macro-mesh builders include the following:

+ `unitSquare :: MonadIO m => m ConnectivityT`{.haskell} \
  Constructs a connectivity for a unit square.

+ `meshLShape :: MonadIO m => m ConnectivityT`{.haskell} \
  Generates the macro-mesh for an (inverted) L-shaped domain, and composed from three unit-squares.

The `ConnectivityT`{.haskell} data type is based closely off of `p4est_connectivity_t`, that is defined and used by `p4est`.

### Ghost Layers ### {#sec:ghost-layers}

Functions for constructing ghost-layers:

+ `addGhost :: ConnectT -> P4estM ()`{.haskell} \
  Adds a *'ghost layer'* that contains all of the cells adjacent to the local subforest, as well as a layer of *'mirrors'* which are the cells adjacent to the boundary of the local subforest.

+ `expandGhost :: P4estM ()`{.haskell} \
  Increase the "thickness" of the ghost-layer by one.

+ `delGhost :: P4estM ()`{.haskell} \
  Delete a ghost layer, if it exists, and also deletes a nodes-labelling first if one exists.

### Mesh Nodes ### {#sec:mesh-nodes}

Once the macro-mesh and subdivision structure of the parallel forest have been finalised, a next step is often to compute a globally-unique labelling for each *'independent node'* of the mesh. The backend `p4est` library provides two different methods and data-structures for this task, but we will only discuss the functions and data structures that are used to give each independent node of the mesh a unique label:

+ `addNodes :: P4estM ()`{.haskell} \
  Generates a unique node-labelling for every *'independent node'* of the subforest. Every local subforest stores its *'owned'* independent nodes, as well as any *'shared'* independent nodes, that it received from other (MPI) processes, due to them having *"support"* within any of the local cells.

+ `delNodes :: P4estM ()`{.haskell} \
  Removes any nodes-labelling if it exists.

The other method supports *'Lobatto points'*, which we do not use.

<!--
### Lower-Level Constructors and Destructors ###

For more control, there are also even lower-level functions, and these are basically just wrappers to the equivalent `p4est` functions and data types. Some of these functions are:

#. `create :: ConnectivityT -> P4estM ()`{.haskell} -- low-level routine for creating an initial parallel forest.

#. `destroy :: MonadIO m => P4estT -> m ()`{.haskell} -- low-level routine for destroying/deallocating a parallel forest.
-->

\clearpage

## Mesh Shape and Partitioning ## {#sec:mesh-shape}

The following sections describe functions that modify the "shape" of the forest, or change how the cells of the (trees of the) forest are distributed, amongst the (MPI) processes:

#. [Balancing and Partitioning](#sec:balance-parts)

#. [Refinement and Coarsening](#sec:refine-coarse)

### Balancing and Partitioning ### {#sec:balance-parts}

The following functions modify the "shape" of the forest, or change how the cells of the (trees of the) forest are distributed, amongst the (MPI) processes:

+ `balance :: ConnectT -> P4estM ()`{.haskell} \
  Uses the efficient, parallel algorithms from [@isaac2012low], and the 2:1 balance mode depends on the `ConnectT`{.haskell} argument.

+ `partition :: P4estM ()`{.haskell} \
  Equidistributes the cells amongst all (MPI) processes.

There are also the following convenience functions:

+ `balanceFace :: P4estM ()`{.haskell} \
  Just the `balance`{.haskell} function that always performs a face-based 2:1 balancing of the parallel forest.

+ `balanceFull :: P4estM ()`{.haskell} \
  Just the `balance`{.haskell} function that always performs a face-based 2:1 balancing of the parallel forest.

### Refinement and Coarsening ### {#sec:refine-coarse}

In addition to any refinement performed by the `balance :: ConnectT -> P4estM ()`{.haskell} function, there are the following functions to control the level of refinement of parallel forest's leaf-nodes:

+ `refineWith :: (Quad -> Bool) -> Bool -> P4estM ()`{.haskell} \
  A [traversal](#sec:traverse-api) that uses a *'predicate'* to determine whether each cell should be subdivided, or not. The operation is applied recursively if the boolean argument is `True`{.haskell}.

+ `coarsenBy :: (Quad -> Bool) -> Bool -> P4estM ()`{.haskell} \
  Apply the predicate to each leaf-node, and coarsen it (and its *'siblings'*) if it evaluates to `True`{.haskell}.

And versions the allow a predicate to perform an IO action:

+ `refineWithIO :: (Quad -> IO Bool) -> Bool -> P4estM ()`{.haskell} \
  A [traversal](#sec:traverse-api) that uses a *'predicate'* to determine whether each cell should be subdivided, or not. The operation is applied recursively if the boolean argument is `True`{.haskell}.

+ `coarsenByIO :: (Quad -> IO Bool) -> Bool -> P4estM ()`{.haskell} \
  Apply the predicate to each leaf-node, and coarsen it (and its *'siblings'*) if it evaluates to `True`{.haskell}.

\clearpage

## Forest Navigation and Queries ## {#sec:queries-api}

Due to `p4est` supporting arbitrary topologies (via its `p4est_connectivity_t` structures), there is no general, efficient, and convenient method to compute global coordinates. But the `p4est` library contains numerous routines for the traversal and querying of forests, and mostly by generating, testing, and searching for adjacent quadrants, from any given quadrant.

This section includes the following subsections:

#. [Tree-based Queries](#sec:tree-based)

#. [Quadrant and Coordinate Functions](#sec:quad-coords)

#. [Coordinate Transformations](#sec:face-transforms)

Most of the `p4est` quadrant-based functions are not (tree-)boundary aware, but some of them are, and this is covered within the definitions of the functions given in these subsections. These functions are frequently used for the "building blocks" of the algorithms presented in the following sections; for example, the traversal algorithms in Subsection \ref{sec:traverse-api}.

### Tree-based Queries ###

Tree containment & ownership queries:

```haskell
  quadExists          :: Quad              -> P4estM Bool
  quadFindOwner       :: Quad -> Maybe Dir -> P4estM (Maybe MpiRank)
  findOverlappedRanks :: Quad              -> P4estM (Int, Int)
  quadIsContained     :: Quad -> Int       -> P4estM Bool
  quadFaceOnBoundary  :: Quad -> Dir       -> P4estM Bool
```

Simple membership-based queries:

+ `isInside      :: Quad -> Bool`{.haskell} \
  Returns `True`{.haskell} if the given quad is inside the unit-tree.

+ `outsideFace   :: Quad -> Bool`{.haskell} \
  Returns `True`{.haskell} if the given quad is outside of the unit-tree, and across a face.

+ `outsideCorner :: Quad -> Bool`{.haskell} \
  Returns `True`{.haskell} if the given quad is outside of the unit-tree, and (diagonally) across from a corner.


### Quadrant and Coordinate Functions ### {#sec:quad-coords}

Quad/coordinate functions for forest-traversal:

```haskell
  quadFaceNeighbourExtra :: Quad -> Dir -> P4estM (Maybe (Dir, Quad))
  quadToCoord :: Quad -> V3 Int
  coordToQuad :: TreeID -> MpiRank -> V3 Int -> Quad
  lastCoordOffset :: (Integral i, Num a) => i -> a
  faceNbr :: Quad -> Dir -> Quad
  cornerNbr :: Quad -> Int -> Quad
  faceNbrs :: Quad -> V4 Quad
  lastQuad :: Quad -> Quad
```

Quadrant intersection tests:

+ `overlaps :: Quad -> Quad -> Bool`{.haskell} \
  Tests if two quadrants overlap, and also tests that the quadrants belong to the same trees as well.

+ `disjoint :: Quad -> Quad -> Ordering`{.haskell} \
  Compare two quadrants in their Morton ordering, with equivalence if the two quadrants overlap, and also compares the owning trees as well.

### Coordinate Transformations ### {#sec:face-transforms}

Functions for finding transforms:

```haskell
  findFaceTransform   :: ConnectivityT -> Int -> Dir -> Maybe (FTransform 2)
  findCornerTransform :: ConnectivityT -> Int -> Int -> Maybe (CornerInfo 2)
```

and calls to these functions will often need to be preceded by issuing:

```haskell
  getConnectivity :: P4estM ConnectivityT
```

To apply a face- or corner- transform, then use one of:

```haskell
  quadFaceTransform   :: Quad -> FTransform -> Quad
  quadCornerTransform :: Quad -> CornerInfo -> Quad
```

\clearpage

## Forest Traversal Functionality ## {#sec:traverse-api}

Traversal of local regions of a forest, as if they are *"Cartesian"*. And this works more generally for *'planar connectivities'* that have *'no extraordinary points'* [@stam1998exact].

+ `buildFaceTransforms' :: EmbeddingT 2 a -> Box (FTransforms 2)`{.haskell} \
  Build a box of coordinate-transforms, to be applied when crossing tree-faces.

+ `iterate :: (a -> b) -> Forest a -> Forest b`{.haskell} \
  Function that iterates over the entire subforest.

The full `iterate()` routine (of the `p4est` library) traverses the entire forest, and can execute callbacks for each cell, face, edge, and node.

### Searching and Gathering ### {#sec:searching}

+ `gatherNodesToShare :: (Node 2 -> [Leaf 2]) -> LocalForest 2 -> Sharers`{.haskell} \
  Use the given "support" function to determine which MPI processes to share each of the (locally-owned) independent nodes, from the mirrors-layer of the local subforest. \
  *Inputs*:
  + a *node-support* function to gather leaves that are within the support of the input node;
  + the (process-)local region of the parallel forest;

  *Output*:
  + sets of node-indices for nodes to be shared with other MPI processes. \

+ `floodFillM :: (V3 Int -> Bool) -> V3 Int -> Quad -> P4estM [Quad]`{.haskell} \
  Uses the given predicate to perform a "flood search" of the neighbourhood.

+ `euclidSearchM :: (V3 Int -> Bool) -> (a -> Quad -> P4estM a) ->`{.haskell} \
  `V3 Int -> a -> Quad -> P4estM [a]`{.haskell} \
  Uses the given predicate to perform a *"Euclidean, flood search"* of the neighbourhood, with the limits set by the *Euclidean*-coordinates predicate, `(V3 Int -> Bool)`{.haskell}, and using the given monadic accumulator, `(a -> Quad -> P4estM a)`{.haskell} to compute the result.

+ `euclidSearchL :: (V3 Int -> Bool) -> (a -> Quad -> a) -> V3 Int -> a -> Quad -> P4estM a`{.haskell} \
  Performs a local, Euclidean search while accumulating the result of each evaluation.

### Folding over Forests ### {#sec:folding}

Fold over the (local sub-)forest, using a pure accumulator:

+ `dfoldr :: (Fkey 2 -> a -> a) -> a -> P4estM a`{.haskell} \
  Depth-first (right-)fold over the forest.

+ `dfoldl :: (a -> Fkey 2 -> a) -> a -> P4estM a`{.haskell} \
  Depth-first (left-)fold over the forest.

+ `bfoldr :: (Fkey 2 -> a -> a) -> a -> P4estM a`{.haskell} \
  Breadth-first (right-)fold over the forest.

+ `bfoldl :: (a -> Fkey 2 -> a) -> a -> P4estM a`{.haskell} \
  `bfoldl :: (a -> Fkey 3 -> a) -> a -> P8estM a`{.haskell} \
  Breadth-first (left-)fold over the forest.

And monadic versions:

+ `bfoldrM :: (Fkey 2 -> a -> P4estM a) -> a -> P4estM a`{.haskell} \
  Breadth-first (right-)fold over the forest, and using the given monadic accumulator.

+ `bfoldlM :: (a -> Fkey 2 -> P4estM a) -> a -> P4estM a`{.haskell} \
  `bfoldlM :: (a -> Fkey 3 -> P8estM a) -> a -> P8estM a`{.haskell} \
  Breadth-first (left-)fold over the forest, and using the given monadic accumulator.

\clearpage

## Sharing Independent Nodes ## {#sec:indep-nodes}

Independent nodes are often required to be shared amongst MPI processes that fall within the supports of these nodes. A general process may consist of the following steps:

#. computing the set of local-indices to share (which is outside of the scope of this library, but perhaps using `gatherNodesToShare`{.haskell} from Section \ref{sec:searching});

#. sharing the number of nodes that each other MPI process will receive from this node; and

#. performing the actual sharing of the `Indep`{.haskell}'s.

### Sharing Nodes ### {#sec:sharing-nodes}

The following functions are for this purpose:

+ ```haskell
  shareNodesAndUpdate ::
    (MonadIO m, MonadReader Comm m) =>
    Sharers -> LocalForest 2 -> m (LocalForest 2)
  ```
  Share the locally-owned independent nodes with other MPI processes, and using the given node-sharing information. This function also receives nodes from other MPI processes, and then updates the `LocalForest 2`{.haskell} data structure with these received nodes. \
  *Inputs*:
  + sets of node-indices for nodes to be shared with other MPI processes;

  *Output*:
  + the updated `LocalForest 2`{.haskell} data structure. \

+ `shareNodes :: Sharers -> P4estM Shareds`{.haskell} \
  Share the locally-owned independent nodes with other MPI processes, and using the given node-sharing information. This function also receives nodes from other MPI processes, and returns these. \
  *Inputs*:
  + sets of node-indices for nodes to be shared with other MPI processes;

  *Output*:
  + map from ranks to arrays of received nodes.

### Node-Sharing Helper Functions ### {#sec:share-help}

To assist with building custom node-sharing functions, there are several lower-level sharing helper-functions, and these are the following functions:

+ `shareSharedCounts :: (MonadIO m, MonadReader Comm m) => Sharers -> m (Vector CInt)`{.haskell} \
  Send all of the shared-dofs counts, for each MPI process, amongst all MPI processes. \
  *Inputs*:
  + map from (MPI) ranks to local-indices;
  
  *Output*:
  + array containing the number of nodes to receive from other processes. \

+ `shareSharers :: Sharers -> Vector CInt -> P4estM Shareds`{.haskell} \
  Using MPI, `isend`/`irecv` all shared nodes needed by this process, from other ranks, and vice versa. \
  *Inputs*:
  + sets of node-indices for nodes to be shared with other MPI processes;
  + array containing the number of nodes to receive from other processes;

  *Output*:
  + map from ranks to an array of received nodes. \

+ `buildShareds :: Sharers -> P4estM Shareds`{.haskell} \
  Computes the array of independent nodes to share, for each rank. This function looks up the independent-node data for each given local node index. \
  *Inputs*:
  + sets of node-indices for nodes to be shared with other MPI processes;

  *Output*:
  + map from ranks to an array of locally-owned independent nodes to be shared. \

+ `shareShareds :: MonadIO m => Comm -> Tag -> Shareds -> Vector CInt -> m Shareds`{.haskell} \
  Using MPI, `isend`/`irecv` all shared nodes needed by this process, from other ranks, and vice versa. \
  *Inputs*:
  + sets of node-indices for nodes to be shared with other MPI processes;
  + array containing the number of nodes to receive from other processes;

  *Output*:
  + map from ranks to an array of received nodes.

### Node-Sharing Data Types ### {#sec:share-data}

The default *MPI Tag* used for these operations is the following:

```haskell
  pattern MpiTagNodesShared :: MPI.Tag
```

which is defined to be the next free tag after those allocated by the `SC` and `p4est` libraries.

Sets of nodes to be (or already) shared nodes have the following types:

```haskell
  newtype RankSet = RankSet { _rankSet :: IntSet }
  type Sharers = IntMap RankSet
  type Shareds = IntMap (Vector Indep)
```

\clearpage

## Indices, Coordinates, and Cells ## {#sec:index-coords}

This API represents cells and nodes using the following data types:

+ `Quad`{.haskell} -- leaf-node elements of a parallel-forest representation for a quadrilateral mesh.

+ `Key (d :: Nat)`{.haskell} -- data type for (hash/map) keys, for tree nodes and cells.

+ `Leaf (d :: Nat)`{.haskell} -- leaf-node elements of a parallel-forest representation for a $d$-cube mesh.

+ `Fkey (d :: Nat)`{.haskell} -- data type for (hash/map) keys, for forest cells.

+ `Indep`{.haskell} -- *'independent node'* data type for the vertices of the mesh.

+ `Hangs`{.haskell} -- *'hanging-node'* data type which represent interpolated nodes of the mesh, which are the *"T-junctions"* of a 2D forest.

### Leaf Nodes ###

Quadrant (of a quadtree) data type:

```haskell
  data Quad = Quad { _qLocation :: !(V2 Int32)
                   , _qLevel    :: !Int8
                   , _qPad8     :: !Int8
                   , _qPad16    :: !Int16
                   , _qTree     :: !Int32
                   , _qRank     :: !Rank -- sometimes 'local_num'; e.g., for ghosts
                   } deriving (Show, Generic)
```

### Node Representations ### {#sec:node-reprs}

Data types for independent nodes, where (only) corners of cells meet are defined as:

```haskell
  data Indep = Indep { _ix, _iy :: Int
                     , _ilevel  :: Int8
                     , _ipad8   :: Int8  -- #sharers
                     , _ipad16  :: Int16 -- i for sharers[i]
                     , _itree   :: Int
                     , _ilocal  :: Rank
                     } deriving (Eq, Show, Generic)
```

This is a fairly low-level data type for *'independent nodes'* and this needs to be generalised to work in at least 2D & 3D.

Hanging-nodes are those that form *"T-junctions"* along edges of cells:

```haskell
  data Hangs = Hangs { _hx, _hy :: Int
                     , _hlevel  :: Int8
                     , _hpad8   :: Int8
                     , _hpad16  :: Int16
                     , _htree   :: Int
                     , _h1, _h2 :: Int -- independent nodes being "hung" off-of
                     } deriving (Eq, Show, Generic)
```

### Z-Indices ###

The `p4est` library defaults to using upto 29-bits for the depth of a quad, and two extra bits can be used for node-coordinates, allowing them to be on the (tree-)boundary or even outside of it.

For 2D forests:

+ `keyToQuad :: TreeID -> Rank -> Key 2 -> Quad`{.haskell} \
  Convert the given key (representing a Z-index) into a cell-coordinate. \
  *Inputs*:
  + `TreeID`{.haskell} -- index of the tree (within the forest) that contains the leaf-node;
  + `Rank`{.haskell} -- MPI rank of the owner of the leaf-node;
  + `Key 2`{.haskell} -- Z-index of the cell (within the tree), to be used for hash/map operations;

  *Outputs*:
  + `Quad`{.haskell} -- leaf-node of the forest. \

+ `quadToKey :: Quad -> Key 2`{.haskell} \
  Compute the Z-index 'key' in a form that is compatible with linear quadtrees (and particularly for use with `Ltree`/`Xtree` objects). \
  *Inputs*:
  + `Quad`{.haskell} -- leaf-node of the forest.

  *Outputs*:
  + `Key 2`{.haskell} -- Z-index of the cell (within the tree), to be used for hash/map operations;

\clearpage

## Import and Export ## {#sec:midlevel-io}

### `VTK` Export ###

Exporting a parallel forest to (parallel sets of) `VTK` files can be achieved using:

+ `vtkWriteFile :: FilePath -> P4estM ()`{.haskell} \
  Export the given forest and (optionally) its geometry to the indicated file.

These generated `VTK` files can then be visualised using [*ParaView*](https://www.paraview.org/).

### `SVG` Export ###

2D-only, and via the funtion:

+ `svgWriteFile :: FilePath -> P4estM ()`{.haskell} \
  Write the local subforest to an SVG file.

Examples of the output generated by this function is shown in Figure \ref{fig:subforests} and Figure \ref{fig:simple-forest} , where each of the individual images is one of the generated `SVG` outputs (of which there are five in each figure).

### `JSON` Import and Export ###

Primarily for debugging?

+ `saveP4estRaw :: MonadIO m => FilePath -> P4estRaw -> m ()`{.haskell} \
  Write the `P4estRaw`{.haskell} data as `JSON` and to the file given by the `FilePath`{.haskell}.

+ `loadP4estRaw :: MonadIO m => FilePath -> m (Maybe P4estRaw)`{.haskell} \
  Read `P4estRaw`{.haskell} data (as `JSON`) from the file given by the `FilePath`{.haskell}.
