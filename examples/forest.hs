{-# LANGUAGE TemplateHaskell, DeriveGeneric, DataKinds
  #-}
------------------------------------------------------------------------------
-- |
-- Module      : Main
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Demonstration of some parallel-forest functionality.
-- 
-- Changelog:
--  + 25/05/2019  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Main where

import GHC.Generics (Generic)
import System.Environment
import System.Random
import Control.Lens (makeLenses, (^.))
import Control.Monad.Reader
import Control.Monad.State
import Options.Applicative
import Data.Maybe (fromJust)
import Text.Printf
-- import Graphics.Svg (renderToFile)

import Data.Index.ZCurve
import qualified Data.Scientific.SC as SC
import Data.Forest.P4est (Quad, P4estM)
import qualified Data.Forest.P4est as P4
import qualified Data.Forest.Local as Lo


-- * Data-types
------------------------------------------------------------------------------
-- | Command-line options.
data DemoOpts =
  DemoOpts { _maxLevel     :: Int
           , _refineRandom :: Bool
           , _faceOnly     :: Bool
           , _unitSquare   :: Bool
           , _jsonExport   :: Bool
           , _verbose      :: Bool
           } deriving (Eq, Show, Generic)

makeLenses ''DemoOpts


-- * Helper functions
------------------------------------------------------------------------------
-- | Command-line options parser.
parser :: Parser DemoOpts
parser  = DemoOpts
  <$> option auto (short 'l' <> long "max-level" <> metavar "LEVEL" <>
                   help "Choose the maximum refinement-level" <>
                   value 2 <> showDefault)
  <*> switch      (short 'r' <> long "random" <>
                   help "Random refinement")
  <*> switch      (short 'f' <> long "face-only" <>
                   help "Balance and connect only by face")
  <*> switch      (short 'u' <> long "unitsquare" <>
                   help "Use a unit-square instead of an L-shaped mesh")
  <*> switch      (short 'j' <> long "json" <>
                   help "Export subforests to JSON files if option is set")
  <*> switch      (short 'v' <> long "verbose" <>
                   help "Generate extra output")

-- ** Refinement functionality
------------------------------------------------------------------------------
-- | Refine the given quadrant if the result from this callback is `True`.
refineFn :: Int -> Quad -> IO Bool
refineFn lmax quad
  | depth quad>=lmax = return False
  | otherwise        = (|| depth quad<1) <$> randomIO


-- * Top-level functions
------------------------------------------------------------------------------
-- | Simple demo.
runDemo :: DemoOpts -> IO ()
runDemo opts = do
  args <- getArgs
  _    <- SC.withSC args $ do
    scrk <- SC.mpiRank
    rank <- pure $ fromEnum scrk

    -- process CLI options, to create initial mesh:
    mesh <- if opts^.unitSquare
      then P4.unitSquare
      else P4.meshLShape
    cnct <- pure $ if opts^.faceOnly
      then P4.ConnectFace
      else P4.ConnectFull

    lofo <- P4.withP4estM mesh $ do
      -- refine, balance, and partition the initial mesh:
      if opts^.refineRandom
        then P4.refineWithIO (refineFn $ opts^.maxLevel) True
        else P4.refineWith   ((<opts^.maxLevel) . depth) True
      P4.partition
      P4.balance cnct
      P4.partition

      -- add a ghost-layer:
      P4.addGhost cnct

      -- build a globally-numbered set of nodes:
      P4.addNodes

      -- export to VTK and SVG:
      P4.vtkWriteFile "out/forest"
      P4.svgWriteFile "out/forest"
      when (opts^.jsonExport) $ do
        lowLevelJSON rank

      Lo.mkLocalForest

    when (opts^.jsonExport) $ do
      Lo.yamlSaveLocalForest (printf "out/local-forest%03d.yaml" rank) $ lofo

    P4.meshDestroy mesh
  pure ()

-- | Write JSON files for the various layers and data-structures.
lowLevelJSON :: Int -> P4estM ()
lowLevelJSON rank = do
  p4st <- get >>= P4.fromP4estT . P4._p4est
  ndes <- get >>= P4.fromNodesT . fromJust . P4._nodes
  ghst <- get >>= P4.fromGhostT . fromJust . P4._ghost
  liftIO $ do
    let fp = printf "out/p4est%03d.json" rank :: FilePath
    -- P4.saveNodes (printf "out/nodes%03d.json" rank) ndes
    P4.yamlSaveNodes (printf "out/nodes%03d.yaml" rank) ndes
    P4.saveGhost (printf "out/ghost%03d.json" rank) ghst
    P4.saveP4estRaw fp p4st


-- * Main program entry-point
------------------------------------------------------------------------------
main :: IO ()
main  = execParser opts >>= runDemo
  where
    opts = info parser mempty
