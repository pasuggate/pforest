The `pforest` library contains higher-level bindings to `p4est` (see Figure @fig:lshape-mesh for a mesh produced via VTK-export, using `p4est`), for the *"parallel forest"* functionality required for the Haskell implementation of RUB-spline meshes, `rubs-mesh`.

![*ParaView* screen-capture that shows an L-shaped mesh, via VTK export from `p4est`. The mesh has been (randomly, recursively) refined, and showing the local-mesh regions for each of five processors.](../rubs-base/images/lshape-mesh.png "L-shaped mesh that has been refined"){#fig:lshape-mesh}

## The `pforest` Library ##

Functionality.

### Macro Meshes ###

Uses `p4est`'s notion of *"macro mesh"* [@burstedde2011p4est] for:

+ (indirectly) storing topology via the `Connectivity`{.haskell};

### Parallel Forests ###

Algorithms and data structures for:

+ constructing parallel forests;

+ partitioning (for load balancing);

+ parallel 2:1 balancing (see Figure \ref{fig:simple-forest}) [@isaac2012low];

+ generating global indices;

+ applying coarsening and refinement;

+ traversal;

### Import and Export ###

Mesh export:

#. VTK

#. JSON

#. SVG (2D only)

### Tests and Examples ###

Contains additional code for:

#. performance & regression tests; and

#. usage examples.

## Scope ##

The `pforest` library is primarily intended to:

+ Wrap the `p4est` functions with:

  #. a [mid-level API](#sec:midlevel-api) that brings an improved degree of abstraction for writing meshing, visualisation, and FEA tools to sit on top of `pforest`;

  #. automates that allocation and freeing of memory for `p4est` objects; and

  #. data-structures that are more suitable for manipulating in Haskell, and with more type-safety.

+ Include additional higher-level (forest and related) data structures for parallel forests:

  #. along with a collection of higher-level functions and API's;

  #. for example, the `Lforest (d :: Nat)`{.haskell} (and related) data types that have been implemented using more standard Haskell data types, and with stronger type-checking; and
   
  #. in order to support more convenient functions and operators, like those from:
     ```haskell
       module Math.Sets         -- ^ handy operators for set- & map- like data
       module Data.Index.ZCurve -- ^ Z-indices and coordinate conversions
     ```

+ Provide additional, general-purpose functionality; e.g., *"flood-fills"* and other traversals.

+ Implement mesh import/export and via common formats (currently `VTK`, `SVG`, and `JSON`).

But `pforest` itself will not be tied to representations for node, and cell contents, nor explicitly handle boundary values and conditions.

## Document Overview ##

Subsequent sections of this document are the following:

#. [Overview of parallel forests](#sec:pforests).

#. [The `pforest` High-Level API](#sec:hilevel-api).

#. [The Common API](#sec:common-api).

#. [Mid-Level API of the `pforest` Library](#sec:midlevel-api).

#. [Additional `pforest` Examples](#sec:examples).

#. [The Low-Level Wrappers for the `p4est` Library](#sec:backend).

#. [Future Work for this Library](#sec:future-work).

These are followed by several appendices, and then the bibliography:

#. [Appendix: `pforest` API](#app:pforest-api)

#. [Appendix: Macro-Mesh Structures](#sec:structures)

#. [Appendix: `p4est` Indexing Conventions](#app:z-indices)

#. [Bibliography](#bibliography)
