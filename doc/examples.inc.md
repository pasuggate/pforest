This section contains examples that show how to use the `pforest` library:

#. [Uniformly-Refined Mesh](#uniformly-refined-mesh)

and source-code for these examples can be found within the `examples/` subdirectory of the [`pforest` repository](https://bitbucket.org/patsphd/pforest/).

## Uniformly-Refined Mesh ##

This first example constructs a uniform, and uniformly subdivided and partitioned mesh. Then a ghost-layer is constructed, and then a nodes-labelling is generated. This results in a uniform version of submeshes like that shown in Figure \ref{fig:subforests}. The listing for this example is:

```haskell
  module Main where

  import GHC.Generics (Generic)
  import System.Environment (getArgs)
  import Control.Monad.IO.Class
  import Control.Lens (makeLenses, (^.))
  import Data.Scientific.SC as SC
  import Data.Forest.P4est as P4

  -- | Command-line options.
  data SimpleOpts = SimpleOpts { _maxLevel  :: Int
                               , _useSquare :: Bool
                               , _verbose   :: Bool
                               } deriving (Eq, Show, Generic)

  makeLenses ''SimpleOpts

  -- | Simple demo.
  runDemo :: SimpleOpts -> IO ()
  runDemo opts = do
    args <- getArgs
    _    <- P4.withSC args $ do
      mesh <- if opts^.useSquare
        then P4.unitSquare
        else P4.meshLShape

      P4.withP4estM mesh $ do
        rank <- fromEnum <$> SC.mpiRank
        liftIO $ printf "simple: MPI rank = %d\n" rank
        -- refine & balance
        P4.refineWith ((<opts^.maxLevel) . depth) False
        P4.partition
        P4.refineWith ((<opts^.maxLevel) . depth) True
        P4.partition
        P4.balanceFull
        P4.partition
        -- add ghosts and then nodes:
        P4.addGhost P4.ConnectFull
        P4.addNodes
        -- export to VTK and SVG:
        P4.vtkWriteFile "out/simple"
        P4.svgWriteFile "out/simple"
      P4.connDestroy mesh
    pure ()
```

Compared to the [basic example](#sec:first-basic) shown in Subsection \ref{sec:first-basic}, this new example first adds a *'ghosts' layer* of adjacent cells, for each of the MPI processes. Then `addNodes` generates globally-unique indices for each of the [*'independent nodes'*](#sec:node-reprs) of the mesh.

Figure \ref{fig:simple-forest} shows a uniform, balanced, and partitioned mesh, and as might be generated using:

```bash
  $ MAX_LEVEL=2 NUM_CORES=5 make simple
```

where the `MAX_LEVEL=2`{.bash} sets an environment variable to override the value (for the maximum refinement-level for the forest) that was given within the `Makefile`, and likewise with `NUM_CORES=5`{.bash} to set/override the number of MPI processors.

<!--
![Mesh has been partitioned over five processors.](images/forestparts-x6.pdf "Partitioned over five processors"){#fig:simple-forest}
-->

\begin{figure}[htb]
\centering
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/simple000.pdf}
  \label{fig:simple000}
\end{minipage}%
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/simple001.pdf}
  \label{fig:simple001}
\end{minipage}%
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/simple002.pdf}
  \label{fig:simple002}
\end{minipage}\\
\vspace{5mm}
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/simple003.pdf}
  \label{fig:simple003}
\end{minipage}%
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/simple004.pdf}
  \label{fig:simple004}
\end{minipage}%
\begin{minipage}{.32\textwidth}
  \mbox{}%
\end{minipage}%
\caption{\label{fig:simple-forest}An inverted, subdivided L-shaped domain that has been (2:1, corner-) balanced, partitioned, and distributed across five processes. Included in this image are the 'ghost' cells (shaded in grey), and the 'independent nodes' (colour if locally-owned).}
\end{figure}

For completeness, the command-line options parser, and the `main :: IO ()`{.haskell} routine are now given as well:

```haskell
  -- | Command-line options parser.
  parser :: Parser SimpleOpts
  parser  = SimpleOpts
    <$> option auto (short 'l' <> long "max-level" <> metavar "LEVEL" <>
                     help "Choose the maximum refinement-level" <>
                     value 2 <> showDefault)
    <*> switch      (short 'u' <> long "unitsquare" <>
                     help "Use a unit-square instead of an L-shaped mesh")
    <*> switch      (short 'v' <> long "verbose" <>
                     help "Generate extra output")

  main :: IO ()
  main  = execParser opt >>= runDemo where
    opt = info parser mempty
```

and the full source-code can be found within:

```
  examples/simple.hs
```

of the `pforest` repository.
