{-# LANGUAGE TemplateHaskell, DeriveGeneric, DataKinds
  #-}
------------------------------------------------------------------------------
-- |
-- Module      : Main
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Demonstration of some parallel-forest functionality.
-- 
-- Changelog:
--  + 25/05/2019  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Main where

import GHC.Generics (Generic)
import GHC.Int
import System.Environment
import System.Random
import Control.Lens (makeLenses, (^.), (.~)) -- , (%~))
import Control.Monad.Reader
import Control.Monad.State
-- import Control.Monad.IO.Class
import Control.Distributed.MPI as MPI
import Options.Applicative
import Data.Maybe (fromJust)
import Data.Bits.Handy ((<<%))
import Text.Printf
-- import Graphics.Svg (renderToFile)
import Linear (V2 (..)) -- , V3 (..))

-- import Data.Tree.Linear as Ltree
-- import Data.Tree.Linear.Export as Ltree
import Data.Index.ZCurve
import qualified Data.Scientific.SC as SC
import Data.Forest.P4est (Quad, P4estM)
import qualified Data.Forest.P4est as P4
import qualified Data.Forest.Local as Lo
import qualified Data.Forest.Parallel as P4

import Foreign.Storable
import Foreign.Ptr
import Foreign.ForeignPtr
import Data.Scientific.SC.Types (LogHandler (..), c'SC_LP_ESSENTIAL)


-- * Data-types
------------------------------------------------------------------------------
-- | Command-line options.
data DemoOpts =
  DemoOpts { _maxLevel     :: Int
           , _refineRandom :: Bool
           , _faceOnly     :: Bool
           , _unitSquare   :: Bool
           , _jsonExport   :: Bool
           , _verbose      :: Bool
           } deriving (Eq, Show, Generic)

makeLenses ''DemoOpts


-- * Helper functions
------------------------------------------------------------------------------
-- | Command-line options parser.
parser :: Parser DemoOpts
parser  = DemoOpts
  <$> option auto (short 'l' <> long "max-level" <> metavar "LEVEL" <>
                   help "Choose the maximum refinement-level" <>
                   value 2 <> showDefault)
  <*> switch      (short 'r' <> long "random" <>
                   help "Random refinement")
  <*> switch      (short 'f' <> long "face-only" <>
                   help "Balance and connect only by face")
  <*> switch      (short 'u' <> long "unitsquare" <>
                   help "Use a unit-square instead of an L-shaped mesh")
  <*> switch      (short 'j' <> long "json" <>
                   help "Export subforests to JSON files if option is set")
  <*> switch      (short 'v' <> long "verbose" <>
                   help "Generate extra output")

-- ** Refinement functionality
------------------------------------------------------------------------------
-- | Refine the given quadrant if the result from this callback is `True`.
refineFn :: Int -> Quad -> IO Bool
refineFn lmax quad
  | depth quad>=lmax = return False
  | otherwise        = (|| depth quad<1) <$> randomIO
  

-- * Top-level functions
------------------------------------------------------------------------------
-- | Simple demo.
runDemo :: DemoOpts -> IO ()
runDemo opts = do
  MPI.init
  let comm = MPI.commWorld
      Comm co = comm
  print comm
  MPI.commRank comm >>= print
  withForeignPtr co $ \p -> do
    q <- peek (castPtr p :: Ptr (Ptr ()))
    print p
    print q
  SC.scInit comm 1 1 (LogHandler nullPtr) c'SC_LP_ESSENTIAL
  SC.scFinalise
  MPI.finalize


-- * Main program entry-point
------------------------------------------------------------------------------
main :: IO ()
main  = execParser opts >>= runDemo
  where
    opts = info parser mempty
