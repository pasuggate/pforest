## Connectivities ##

For an explanation of the `p4est` connectitivity data-structures, refer to [@burstedde2011p4est].

### Constructors and Destructors ###

To build a custom connectitivity, the *"corner-to-corner"* field is the most difficult to understand, and this is just an index into a table of permutations, for quadrants/octants that do not connect via simple translation [see @burstedde2011p4est].

## Geometry ##

Store *"geometry maps"*, which is handy when using `pforest` for IGA?

### Constructors and Destructors ###

#. `setGeometry :: Vec -> P4estM 2 ()`{.haskell} -- collective, using the *'PETSc'* `Vec` object.

## Forests ##

Briefly [covered earlier](#sec:pforest-api), but the API is given a more complete and detailed coverage below.

### Constructors and Destructors ###

### Traversal ###

Basic queries:

+ `child_id` $: Q \rightarrow \mathbb{Z}$

+ `parent` $: Q \rightarrow \mathbb{Z}$

+ `octant` $: \mathbb{Z} \times \mathbb{Z} \rightarrow Q$ (or $\mathbb{R}^3$)

Adjacency queries:

+ `descendants` $: Q \rightarrow \mathbb{R}^3 \times \mathbb{R}^3$

Ownership queries:

+ `find_owner` $: \mathbb{Z} \times Q \rightarrow B$

The `iterate()` routine traverses the entire forest, and can execute callbacks for each cell, face, edge, and node. Therefore, `iterate()` can easily be specialised for different traversal-types.

### Coarsening and Refinement ###

### Export ###

The `p4est` library provides:

+ SVG -- for *"sanity-checking"* the local meshes;

+ VTk -- allows *ParaView* visualisation of meshes;

+ Generic mesh -- for building additional exporters(?);

+ PLEX -- for export to `petsc`;

+ forest of `Ltree`s -- from `tree-linear`;

## Trees ##

### Constructors and Destructors ###

## Quads ##

Mirrors; e.g., see Figure @fig:mirrors.

![Mirrors are like the "inward ghosts" that are shared when computing the ghosts for each processor-local region of the mesh.](images/mirrors.pdf "Mirrors are like the 'inward ghosts' that are shared when computing the ghosts for each processor-local region of the mesh"){#fig:mirrors}

## Nodes ##

### Constructors and Destructors ###
