{-# LANGUAGE DataKinds #-}

module Main where

import qualified Data.Forest.Local.Sharing as Shary
import qualified Data.Forest.Local.Tests   as Local
import qualified Data.Forest.Query.Tests   as Query
import           Test.Tasty                (defaultMain, testGroup)


main :: IO ()
main  = do
  Shary.manualTests
  defaultMain $ testGroup "Forest/Tests" [ Local.tests
--                                          , Shary.tests
                                         , Query.tests
                                         ]
