# Source Markdown files:
MD	:= $(wildcard *.md) $(wildcard rubs-base/doc/*.md)

# Include-files:
INC	:= $(filter %.inc.md, $(MD))
REF	:= rubs-base/tex/rubrefs.bib
GLS	:= rubs-base/tex/glossary.inc.latex
TMP	?= rubs-base/tex/_layout.latex

# Images:
PNG	:= $(wildcard doc/images/*.png) $(wildcard rubs-base/images/*.svg)
SVG	:= $(wildcard out/*.svg) $(wildcard doc/images/*.svg) $(wildcard rubs-base/images/*.svg)
PIC	:= $(SVG:.svg=.pdf)
OUT	:= $(wildcard out/*.svg) $(wildcard out/*.pdf) $(wildcard out/*.json) $(wildcard out/*.yaml)

# Top-level documents:
DOC	:= $(filter-out %.inc.md, $(MD))
PDF	:= $(DOC:.md=.pdf)

# Pandoc settings:
FLT	?= --filter=pandoc-include --filter=pandoc-fignos --filter=pandoc-citeproc --filter=pandoc-filter-graphviz
OPT	?= --number-sections --bibliography=$(REF)

# Run-time settings:
GHC		?= ghc-8.6.4
NUM_JOBS	?= 10
MAX_LEVEL	?= 2
NUM_CORES	?= 3
MPI_INC_DIR	?= /usr/lib/x86_64-linux-gnu/openmpi/include/
EXTRA_INC_DIR	?= /usr/local/include/
BUILD_DIR	?= dist-newstyle/build/x86_64-linux/${GHC}/pforest-0.1.0.0

LVL	?= --max-level $(MAX_LEVEL)
#MPI	?= time mpiexec -n $(NUM_CORES)
MPI	?= time mpirun --hostfile hostfile -np $(NUM_CORES)


.PHONY:	all doc basic simple step4 grid2 unit2 build clean

all:	doc $(PDF) $(PIC) $(PNG) $(INC)

doc:	$(PDF) $(PIC)
	+make -C doc all

build:	hello
	cabal new-build -j$(NUM_JOBS) --extra-include-dirs=$(MPI_INC_DIR) --extra-include-dirs=$(EXTRA_INC_DIR) basic
	cabal new-build -j$(NUM_JOBS) --extra-include-dirs=$(MPI_INC_DIR) --extra-include-dirs=$(EXTRA_INC_DIR) simple
	cabal new-build -j$(NUM_JOBS) --extra-include-dirs=$(MPI_INC_DIR) --extra-include-dirs=$(EXTRA_INC_DIR) forest
#	cabal new-build -j$(NUM_JOBS) --extra-include-dirs=$(MPI_INC_DIR) basic
#	cabal new-build -j$(NUM_JOBS) --extra-include-dirs=$(MPI_INC_DIR) simple
#	cabal new-build -j$(NUM_JOBS) --extra-include-dirs=$(MPI_INC_DIR) forest

basic:	build
	$(MPI) $(BUILD_DIR)/x/basic/build/basic/basic $(LVL)

simple:	build
#	$(MPI) $(BUILD_DIR)/x/simple/build/simple/simple $(LVL) --unitsquare
	$(MPI) $(BUILD_DIR)/x/simple/build/simple/simple $(LVL)

forest:	build
	$(MPI) $(BUILD_DIR)/x/forest/build/forest/forest $(LVL) --json
#	$(MPI) $(BUILD_DIR)/x/forest/build/forest/forest $(LVL) --unitsquare --random --json
#	$(MPI) $(BUILD_DIR)/x/forest/build/forest/forest $(LVL) --random --json

step4:	build
	$(MPI) $(BUILD_DIR)/x/step4/build/step4/step4 $(LVL) --random

grid2:	build
	$(MPI) $(BUILD_DIR)/x/step4/build/step4/step4 $(LVL)

unit2:	build
	$(MPI) $(BUILD_DIR)/x/step4/build/step4/step4 $(LVL) --random # --unitsquare

hello:
	+rm -f $(OUT)

mpitest:
	cabal new-build -j --extra-include-dirs=/usr/lib/x86_64-linux-gnu/openmpi/include/ mpitest
	time mpiexec -n 5 dist-newstyle/build/x86_64-linux/ghc-8.6.4/pforest-0.1.0.0/x/mpitest/build/mpitest/mpitest --max-level 2


clean:
	rm -f $(PDF) $(PIC) *.pvtu *.vtu *.visit graphviz-images
	+make -C doc clean

# Implicit rules:
%.pdf: %.md $(PIC) $(PNG) $(INC) $(GLS)
	+pandoc --template=$(TMP) $(FLT) $(OPT) -f markdown+tex_math_double_backslash -t latex -V papersize:a4 $< -o $@

%.pdf: %.svg
	+inkscape --export-area-drawing --export-text-to-path --export-pdf=$@ $<
