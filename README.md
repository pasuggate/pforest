---
title: "README for the `pforest` Library"
author: Patrick Suggate <<patrick.suggate@gmail.com>>
date: 22 May, 2019
geometry: margin=2cm
colorlinks: true
toc: true
toc-depth: 3
toccolor: purple
lof: true
biblio-title: Bibliography
---

\clearpage

# Introduction #

Mid-level API for the `bindings-p4est` package, so that `p4est` parallel forests can be used from Haskell code.

## P4est Flow ##

#. Load/Make a connectivity

#. Add happiness

#. Refine

#. Export

# Install #

Depending on where your OpenMPI (or other) headers can be found, modify the following command and then run it:

```bash
$ cabal new-build -j --extra-include-dirs=/usr/lib/x86_64-linux-gnu/openmpi/include/
```

OR equivalently:

```bash
$ C_INCLUDE_PATH=/usr/lib/x86_64-linux-gnu/openmpi/include/ cabal new-build -j
```

And if everything worked, then run one of the demos using:

```
$ mpiexec -n 2 dist-newstyle/build/x86_64-linux/ghc-8.6.4/pforest-0.1.0.0/x/step4/build/step4/step4
```

And play with the refinement-strategy using:

```
$ cabal new-build -j --extra-include-dirs=/usr/lib/x86_64-linux-gnu/openmpi/include/
$ mpiexec -n 2 dist-newstyle/build/x86_64-linux/ghc-8.6.4/pforest-0.1.0.0/x/step4/build/step4/step4 \ 
    --max-level 5 --random
```

## Submodules ##

When cloning the repository, the `git submodule`s can also be cloned at the same time by using:

```bash
$ git clone git@bitbucket.org:patsphd/pforest.git --recursive
```

\clearpage

# Run #

![](doc/images/forestparts-x6.pdf "Partitioned over five processors")

## MPI Tips ##

```bash
$ MAX_LEVEL=2 NUM_CORES=7 make -j forest
```

You may see a message:

```bash
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 7 slots
that were requested by the application:
  dist-newstyle/build/x86_64-linux/ghc-8.6.4/pforest-0.1.0.0/x/forest/build/forest/forest

Either request fewer slots for your application, or make more slots available
for use.
--------------------------------------------------------------------------
```

To increase the number of MPI slots; e.g., to allocate 25 slots:

```bash
$ echo "localhost slots=25" > hostfile
$ mpirun --hostfile hostfile -np 25 <CMD>
```

\clearpage

# Building the Documentation #

!include rubs-base/doc/pandoc.inc.md

# Bibliography #
