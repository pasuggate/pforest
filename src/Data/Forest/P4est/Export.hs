{-# LANGUAGE DataKinds, DeriveGeneric, FlexibleContexts, FlexibleInstances,
             GeneralizedNewtypeDeriving, OverloadedStrings,
             ScopedTypeVariables, StandaloneDeriving, TemplateHaskell,
             TypeFamilies #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.Export
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- SVG & VTK exporters for `p4est` meshes; i.e., forests of linear-trees.
--
-- TODO:
--  + move to `rubs-mesh` as this module makes assumptions about supported
--    mesh topologies, and that are more restrictive than that of 'p4est';
--  + still to many magic-numbers throughout the code -- mostly related to
--    scaling & depths -- and I need to clean this up;
--
------------------------------------------------------------------------------

module Data.Forest.P4est.Export
  ( SvgQuad (..)
  , SvgProps (..)
  -- forest data with export metadata:
  , LocalExport (..)
  , mkLocalExport
  , saveLocalExport
  , loadLocalExport
  , yamlSaveLocalExport
  -- exporters:
  , writeP4estAsSVG
  , localForestToSVG
  , svgWriteFile
  , vtkWriteFile
  , vtkWriteFile'
  ) where

import           Data.Forest.P4est.Export.SVG
import           Data.Forest.P4est.Export.VTK
