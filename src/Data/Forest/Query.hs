{-# LANGUAGE CPP, DataKinds, FlexibleInstances, InstanceSigs,
             MultiParamTypeClasses, NoMonomorphismRestriction,
             OverloadedStrings, PatternSynonyms, PolyKinds, RankNTypes,
             TypeFamilies #-}

{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Query
-- Copyright   : (C) Patrick Suggate 2019
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Functions for working with the "connectivities" of parallel forests.
--
-- Changelog:
--  + 26/05/2019  --  initial file;
--
-- TODO:
--  + additional queries for working with boundaries and corners;
--
------------------------------------------------------------------------------

module Data.Forest.Query
  ( TreeQueries (..)
  , LeafQueries (..)
  , NodeQueries (..)
  , getDiag
  , treesOfCorner
  , findCorner
  , faceBoundary2
  )
where

import           Control.Lens             ((^.))
import           Data.Bits.Handy          (int)
import           Data.Bool                (bool)
import           Data.Foldable            (toList)
import           Data.Forest.Mesh
import           Data.Forest.Mesh.Macro2D
import           Data.Maybe               (isNothing)
import           Data.Vector.Helpers
import qualified Data.Vector.Storable     as Vec
import           Linear.Handy             hiding (unit)


-- * Classes of queries
------------------------------------------------------------------------------
class TreeQueries e where
  isOnFace :: e -> Bool
  isOnEdge :: e -> Bool
  isCorner :: e -> Bool

class LeafQueries t where
  existsIn       :: Leaf (DimsOf t)                   -> t -> Bool
  isContained    :: Leaf (DimsOf t) -> TreeID         -> t -> Bool
  faceOnBoundary :: Leaf (DimsOf t) -> Dir (DimsOf t) -> t -> Bool
--   findOwner      :: Leaf (DimsOf t) -> Dir (DimsOf t) -> t -> Maybe MpiRank

class NodeQueries t where
  onBoundaryOf :: Node (DimsOf t) -> t -> Bool


-- * Some orphan instances
------------------------------------------------------------------------------
instance TreeQueries (Node 2) where
  isOnFace n@(Node (V2 x y) _ _) = x `elem` bs || y `elem` bs where
    bs = [0, maxWidth n]
  isCorner n@(Node (V2 x y) _ _) = x `elem` bs && y `elem` bs where
    bs = [0, maxWidth n]
  -- | Uses the p4est convention that 2D cells have "faces", not edges.
  isOnEdge  = error "isOnEdge_Node_2: 2D cells do not have edges"
  {-# INLINE isOnFace #-}
  {-# INLINE isCorner #-}

{-- }
instance CellQueries (LocalForest 2) 2 where
  existsIn
--}

instance TreeQueries (Leaf 2) where
  isCorner :: Leaf 2 -> Bool
  isCorner c =
    let (h, w) = (width c, maxWidth c)
        V2 x y = fromIntegral <$> coord c
    in  (x==0 || x+h==w) && (y==0 || y+h==w)
  {-# INLINE isCorner #-}
  isOnFace :: Leaf 2 -> Bool
  isOnFace c =
    let (h, w) = (width c, maxWidth c)
        V2 x y = fromIntegral <$> coord c
    in  x==0 || x+h==w || y==0 || y+h==w
  {-# INLINE isOnFace #-}
  isOnEdge :: Leaf 2 -> Bool
  isOnEdge  =
    error "2D trees do not have edges, only faces (a 'p4est' convention)"

instance NodeQueries (MacroMesh 2) where
  onBoundaryOf :: Node 2 -> MacroMesh 2 -> Bool
  onBoundaryOf  = onBoundary2
  {-# INLINE onBoundaryOf #-}


-- * Helper functions
------------------------------------------------------------------------------
onBoundary2 :: Node 2 -> MacroMesh 2 -> Bool
onBoundary2 n mm
  | isCorner n = cornerBoundary2 n mm
  | isOnFace n = faceBoundary2 n mm
  | otherwise  = False

cornerBoundary2 :: Node 2 -> MacroMesh 2 -> Bool
cornerBoundary2 n mm = case findCorner n mm of
  Nothing -> faceBoundary2 n mm
  Just cx -> (<4) . length $ treesOfCorner mm cx

faceBoundary2 :: Node 2 -> MacroMesh 2 -> Bool
faceBoundary2 n mm = or $ zipWith (&&) bs ts where
  ds = [minBound..maxBound]
  bs = btest n <$> ds
  ts = isNothing . findTransform mm (treeId n) <$> ds

------------------------------------------------------------------------------
-- | Return the list of tree-indices for the given corner.
treesOfCorner :: MacroMesh 2 -> Int -> [TreeID]
treesOfCorner mm c =
  let mx = mm^.macroMesh
      (s, e) = (_cttOffset mx!c, _cttOffset mx!(c+1))
  in  map fromIntegral $ Vec.toList $ int s `slc` int (e-s) $ mx^.cornerToTree
{-# INLINE treesOfCorner #-}

-- | Find the corner of the quadrant for the given node, if the corner exists.
findCorner :: Node 2 -> MacroMesh 2 -> Maybe Int
findCorner n mm
  | nul tc    = Nothing
  | otherwise = Just (int cx) `bool` Nothing $ cx < 0
  where
    di = fromIntegral $ getDiag n
    tc = mm^.macroMesh.treeToCorner
    cx = toList (tc!fromIntegral (treeId n)) !! di
{-# INLINE findCorner #-}

------------------------------------------------------------------------------
-- | Calculate which quadrant the given node lies within.
getDiag :: Node 2 -> Diag 2
getDiag n@(Node (V2 x y) _ _)
  |     s &&     w = SW
  |     s && not w = SE
  | not s &&     w = NW
  | not s && not w = NE
  where
    w = (x+x) < maxWidth n
    s = (y+y) < maxWidth n
{-# INLINE getDiag #-}

-- ** 'Node'-query helper functions
------------------------------------------------------------------------------
btest :: Node 2 -> Dir 2 -> Bool
btest n d = case d of
  West  -> x == 0
  East  -> x == maxWidth n
  South -> y == 0
  North -> y == maxWidth n
  where
    V2 x y = n^.pos
