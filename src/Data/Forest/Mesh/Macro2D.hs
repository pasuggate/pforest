{-# LANGUAGE CPP, DataKinds, DeriveGeneric, FlexibleContexts, FlexibleInstances,
             GeneralizedNewtypeDeriving, MultiParamTypeClasses,
             NoMonomorphismRestriction, OverloadedStrings, PatternSynonyms,
             PolyKinds, RankNTypes, StandaloneDeriving, TemplateHaskell,
             TupleSections, TypeFamilies, TypeSynonymInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Mesh.Macro2D
-- Copyright   : (C) Patrick Suggate 2019
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Functions for working with the "connectivities" of parallel forests.
--
-- Changelog:
--  + 16/06/2019  --  initial file;
--
------------------------------------------------------------------------------

module Data.Forest.Mesh.Macro2D
  ( module Data.Forest.Mesh.Macro
  -- Embeddings:
  -- ------------
  , MacroMesh (..)
  , Macro2D (..)
  --   getters & setters:
  , treeToTree
  , treeToFace
  , treeToCorner
  , cttOffset
  , cornerToTree
  , cornerToCorner
  -- conversions & constructors:
  , decomposeConn2D
  , macroLShape
  , macroSquare
  , macroGrid
  , fromConnT
  , macroToConn2D
  , toConnectivityT
  , aabb
  -- queries:
  , findTransform
  , treeCoord
  , treeCornerCoord
  --   load & save:
  , loadMacroMesh
  , saveMacroMesh
  , yamlMacroMesh
  )
where

import           Control.Arrow                  (first, (<<<))
import           Control.DeepSeq
import           Control.Lens                   (makeLenses, (.~), (^.))
import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Bits.Handy                (int)
import qualified Data.ByteString                as BS
import qualified Data.ByteString.Lazy           as Lazy
import           Data.Foldable                  (toList)
import           Data.IntSet.Sets
import           Data.Vector.Helpers
import           Data.Vector.Storable           (Vector)
import qualified Data.Vector.Storable           as Vec
import qualified Data.Yaml.Pretty               as Y
import           GHC.Generics                   (Generic)
import           GHC.Int
import           Linear.Handy                   hiding (unit)
import           System.IO.Unsafe

import           Data.Forest.Mesh
import           Data.Forest.Mesh.Macro
import           Data.Forest.P4est.Connectivity (Conn2D, ConnectivityT,
                                                 FTransform (..), embedLShape,
                                                 toConn2D)
import qualified Data.Forest.P4est.Connectivity as Con
import           Data.Index.ZCurve


-- * Type-instances
------------------------------------------------------------------------------
type instance MacroType 2 = Macro2D


-- * Macro-mesh data-types
------------------------------------------------------------------------------
-- | Stores the connectivity-structure of a quadrilateral mesh.
--   Where:
--    + treeToCorner  ->  4 * num_trees
data Macro2D
  = Macro2D
      { _treeToTree     :: !(Vector (FaceVec 2 TreeID))
        -- (Orient 2))
      , _treeToFace     :: !(Vector (FaceVec 2 Int8)) -- (Orient 2))
      , _treeToCorner   :: !(Vector (NodeVec 2 Int32))
        -- corners:
      , _cttOffset      :: !(Vector Int32)
      , _cornerToTree   :: !(Vector Int32)
      , _cornerToCorner :: !(Vector Int8)
      }
  deriving (Eq, Show, Generic)


-- * Lenses and instances
------------------------------------------------------------------------------
makeLenses ''Macro2D

------------------------------------------------------------------------------
deriving instance Eq   (MacroMesh 2)
deriving instance Show (MacroMesh 2)

instance ToJSON   Macro2D
instance FromJSON Macro2D

instance ToJSON   (MacroMesh 2)
instance FromJSON (MacroMesh 2)

-- ** Strictness instances
------------------------------------------------------------------------------
instance NFData Macro2D
instance NFData (MacroMesh 2)

-- ** Transform-helper instances
------------------------------------------------------------------------------
-- | Find the coordinate transform, for crossing the tree-face in the given
--   direction.
{--}
instance SetElems (TreeID, Dir 2) (Maybe (FTransform, Dir 2)) (MacroMesh 2) where
  (t, d) <~ mm = findTransform mm t d
  {-# INLINE (<~) #-}
--}

{-- }
instance SetElems (Fkey 2, Dir 2) (Maybe (Fkey 2)) (MacroMesh 2) where
  (k, d) <~ mm = reify . leafTransform (lpath k) . fst <$> findTransform mm (_ktree k) d
  {-# INLINE (<~) #-}
--}


-- * Queries
------------------------------------------------------------------------------
findTransform :: MacroMesh 2 -> TreeID -> Dir 2 -> Maybe (FTransform, Dir 2)
findTransform mac t d
  | t==s && d==e = Nothing
  | otherwise    = Just (ft, e)
  where
    mm = mac^.macroMesh
    ft = toList (_transforms mac!r^.ftransforms) !! i
    r = fromEnum t
    i = int d
    s = toList (_treeToTree mm!r) !! i
    e = Dir $ toList (_treeToFace mm!r) !! i


-- * Constructors
------------------------------------------------------------------------------
decomposeConn2D :: Conn2D -> (Vertices 2 Double, Macro2D)
decomposeConn2D (Con.Conn2D vs vt tt tf tc co ct cc) = (vx, mm) where
  vx = Vec.map (^._xy) vs `Vertices` Vec.map (int <$>) vt
  mm = Macro2D (Vec.unsafeCast tt) tf tc co ct cc

------------------------------------------------------------------------------
-- | Construct the reference "L-shape" mesh for testing.
macroLShape :: MacroMesh 2
macroLShape  = MacroMesh m2 fx $ Just vx where
  (vx, m2) = first macroGrid $ decomposeConn2D embedLShape
  fx = unsafePerformIO $ buildFaceTransforms <$> Con.fromConn2D embedLShape

-- | Construct the reference "L-shape" mesh for testing.
macroSquare :: MacroMesh 2
macroSquare  = MacroMesh m2 fx $ Just vx where
  (vx, m2) = first macroGrid $ decomposeConn2D Con.embedSquare
  fx = unsafePerformIO $ buildFaceTransforms <$> Con.fromConn2D Con.embedSquare

{-- }
macroGrid ::
     Integral i
  => RealFrac a
  => Functor (Vec d)
  => Storable (Vec d i)
  => Vertices d a
  -> Vertices d i
--}
macroGrid ::
     Vertices 2 Double
  -> Vertices 2 Int32
macroGrid (Vertices cs vi) =
  let cs' = fmap go `Vec.map` cs
      go x | x''-x < e = x'
           | otherwise = error "macroGrid: roundoff error too great"
        where
          x'  = round x
          x'' = fromIntegral x'
          e   = 1e-8 -- acceptable round-off error
  in  Vertices cs' vi

-- ** Conversions
------------------------------------------------------------------------------
-- | Construct from a connectivity data-structure.
fromConnT :: ConnectivityT -> MacroMesh 2
fromConnT co = MacroMesh m2 fx $ Just vx where
  (vx, m2) = first macroGrid $ decomposeConn2D em
  fx = buildFaceTransforms co
  em = unsafePerformIO $ toConn2D co

------------------------------------------------------------------------------
-- | Convert into a @Conn2D@, which may be useful as a first step to building
--   a @ConnectivityT@ from a @MacroMesh@ .
macroToConn2D :: MacroMesh 2 -> Conn2D
macroToConn2D mm = case mm^.embedding of
  Just  _ -> toConn2D' mm
  Nothing -> error "Macro2D.toConn2D: no embedding info for MacroMesh"

toConn2D' :: MacroMesh 2 -> Conn2D
toConn2D' mm = Con.Conn2D vx tj tt tf tc co ct cc where
  Just (Vertices vs vi) = mm^.embedding
  go :: V2 Int32 -> V3 Double
  go = flip (_xy .~) 0 <<< fmap fromIntegral
  vx = Vec.map go vs :: Vector (V3 Double)
  tj = Vec.map (fromIntegral <$>) vi :: Vector (V4 Int32)
  Macro2D mt tf tc co ct cc = mm^.macroMesh
  tt = Vec.map (fromIntegral . fromEnum <$>) mt

------------------------------------------------------------------------------
-- | Converts a @MacroMesh@ into a 'p4est' @ConnectivityT@ .
toConnectivityT :: MonadIO m => MacroMesh 2 -> m ConnectivityT
toConnectivityT  = Con.fromConn2D . macroToConn2D
{-# INLINABLE toConnectivityT #-}


-- * Queries
------------------------------------------------------------------------------
-- | Compute the Axis-Aligned Bounding-Box (AABB) for the Embedding.
aabb :: MacroMesh 2 -> AABB Double
aabb mm =
  let vs = Vec.map (\a -> _xy .~ a $ 0) em :: Vector (V3 Double)
      em = fmap fromIntegral `Vec.map` (er (mm^.embedding)^.coordinates)
      er = error "Macro2D.aabb: no embedding" `maybe` id
      {-# INLINE er #-}
      xs = Vec.map (^._x) vs
      ys = Vec.map (^._y) vs
      zs = Vec.map (^._z) vs
      (x0, x1, dx) = (Vec.minimum xs, Vec.maximum xs, x1-x0)
      (y0, y1, dy) = (Vec.minimum ys, Vec.maximum ys, y1-y0)
      (z0, z1, dz) = (Vec.minimum zs, Vec.maximum zs, z1-z0)
  in  V3 x0 y0 z0 `AABB` V3 dx dy dz

-- ** Tree-coordinate queries
------------------------------------------------------------------------------
-- | Find the coordinates of the first corner, for the given tree index.
treeCoord :: MacroMesh 2 -> TreeID -> Maybe (V2 Int)
treeCoord (MacroMesh _ _  Nothing               ) _ = Nothing
treeCoord (MacroMesh _ _ (Just (Vertices cs vi))) t
  | fromEnum t < len vi = Just $ fromEnum <$> cs!fromEnum i
  | otherwise           = Nothing
  where
    i = vi!fromEnum t^._x :: Int
{-# INLINE treeCoord #-}

treeCornerCoord :: Enum i => MacroMesh 2 -> TreeID -> i -> Maybe (V2 Int)
treeCornerCoord (MacroMesh _ _  Nothing               ) _ _ = Nothing
treeCornerCoord (MacroMesh _ _ (Just (Vertices cs vi))) t j =
  let i = toList (vi!fromEnum t) !! fromEnum j
  in  Just $ fromEnum <$> cs!fromEnum i
{-# INLINE treeCornerCoord #-}


-- * Import/export
------------------------------------------------------------------------------
saveMacroMesh :: MonadIO m => FilePath -> MacroMesh 2 -> m ()
#ifdef __USE_JSON
saveMacroMesh fp = liftIO . Lazy.writeFile fp . encode
#else /* !__USE_JSON */
saveMacroMesh fp =
  let ycfg = Y.setConfCompare compare Y.defConfig
  in  liftIO . BS.writeFile fp . Y.encodePretty ycfg
#endif /* __USE_JSON */

loadMacroMesh :: MonadIO m => FilePath -> m (Maybe (MacroMesh 2))
loadMacroMesh  = liftIO . fmap decode . Lazy.readFile

------------------------------------------------------------------------------
-- | YAML export.
yamlMacroMesh :: MonadIO m => FilePath -> MacroMesh 2 -> m ()
yamlMacroMesh fp = liftIO . BS.writeFile fp . Y.encodePretty ycfg
  where
    ycfg = Y.setConfCompare compare Y.defConfig
