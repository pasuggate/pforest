{-# LANGUAGE DataKinds, DeriveGeneric, FlexibleContexts, FlexibleInstances,
             GeneralizedNewtypeDeriving, InstanceSigs, KindSignatures,
             MultiParamTypeClasses, NoMonomorphismRestriction,
             OverloadedStrings, PolyKinds, ScopedTypeVariables,
             StandaloneDeriving, TemplateHaskell, TupleSections,
             TypeFamilies #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.Local.Base
-- Copyright   : (C) Patrick Suggate, 2020
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Basic functionality for local (sub)forests and nodes.
--
------------------------------------------------------------------------------

module Data.Forest.Local.Base
  ( module Data.Forest.Mesh
  , Rebaseable (..)
  , BoundaryTestable (..)
  , LocalForest (..)
  , macroStruct
  , localForest
  , firstLeaves
  , ghostsLayer
  , mirrorLayer
  , forestNodes
  , LocalNodes (..)
  , localNodes
  , localCells
  , cellOfNode
  , globalIdxs
  , nodeOwners
  , ownedCount
  , sharedWith
  , RankSet
  , rankSet
  --   import & export:
  , saveLocalForest
  , loadLocalForest
  , yamlSaveLocalForest
  , yamlLoadLocalForest
  , saveLocalNodes
  , loadLocalNodes
  , yamlSaveLocalNodes
  , yamlLoadLocalNodes
  --   helper functions:
  , dbg
  , trace'
  , trc
  , indep2node
  , node2indep
  , firstLeaf
  , lastLeaf
  , makeNodeOf
  )
where

import           Control.Arrow                 ((&&&))
import           Control.DeepSeq
import           Control.Distributed.MPI.Handy as MPI
import           Control.Lens                  (makeLenses, (.~), (^.))
import           Control.Logging
import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Bits.Handy               (int)
import qualified Data.ByteString               as BS
import qualified Data.ByteString.Lazy          as Lazy
import           Data.Foldable                 (foldl')
import           Data.Forest.Mesh
import           Data.Forest.Mesh.Macro2D
import           Data.Forest.P4est.Base        hiding (trace', trc)
import           Data.IntMap.Maps
import           Data.MonoTraversable          (Element, MonoFoldable (..))
import           Data.Text                     (Text, pack)
import           Data.Tree.Linear.Coords       (BoundaryTestable (..))
import           Data.Vector.Storable          (Vector)
import qualified Data.Yaml                     as Y
import qualified Data.Yaml.Pretty              as Y
import           GHC.Generics                  (Generic)
import           GHC.TypeNats
import           Linear.Handy                  (V2 (..))


-- * Forest data-types
------------------------------------------------------------------------------
-- | Stores all of the information needed to traverse the local subforest.
--
--   TODO: fix up the face-transforms so that it can support 3D
--   TODO: do I want this to be able to store ghosts, mirrors, and/or nodes?
data LocalForest (d :: Nat)
  = LocalForest
      { _macroStruct :: !(MacroMesh d)
      , _localForest :: !(Lforest d)
      , _firstLeaves :: !(Vector (Leaf d))
      , _ghostsLayer :: !(Lforest d)
      , _mirrorLayer :: !(Lforest d)
      , _forestNodes :: !(LocalNodes d)
      }
  deriving (Generic)

deriving instance Eq   (LocalForest 2)
deriving instance Show (LocalForest 2)

type instance Element (LocalForest 2) = Fkey 2
type instance Element (LocalForest 3) = Fkey 3

------------------------------------------------------------------------------
-- | Local nodes data structure that stores the independent nodes required
--   for the local subforest.
--
--   TODO: are the local-indices (corresponding to 'ilocal') properly stored
--     by this data structure? (So that 'Indep's can be built as needed.) Yes,
--     because:
--       ilocal = iglobal - globalOwnedOffset[rank]
--
data LocalNodes (d :: Nat)
  = LocalNodes
      { _localNodes :: !(Vector (Node d)) -- ^ local DoF- & interpolated- nodes
      , _localCells :: !(Xforest d (NodeVec d Int))
        -- TODO: todo?
      , _cellOfNode :: !(IntMap [(Dir d, Leaf d)]) -- TODO: todo?
        -- ^ local-to-global map
      , _globalIdxs :: !(Vector Int) -- ^ local-to-global map
        -- ^ local-to-rank map
      , _nodeOwners :: !(Vector Rank) -- ^ local-to-rank map
        -- ^ num-owned per MPI rank
      , _ownedCount :: !(Vector Int) -- ^ num-owned per MPI rank
      , _sharedWith :: !(IntMap RankSet)
      }
  deriving (Generic)

deriving instance Eq   (LocalNodes 2)
deriving instance Show (LocalNodes 2)

-- ** Strictness instances
------------------------------------------------------------------------------
instance NFData (LocalNodes  2)
instance NFData (LocalForest 2)


-- * Lenses and instances
------------------------------------------------------------------------------
makeLenses ''LocalForest
makeLenses ''LocalNodes

-- ** JSON import/export
------------------------------------------------------------------------------
instance ToJSON   (LocalForest 2)
instance FromJSON (LocalForest 2)

instance ToJSON   (LocalNodes 2)
instance FromJSON (LocalNodes 2)

-- ** Boundary-tests
------------------------------------------------------------------------------
-- NOTE: only does tile-boundary tests, so if the tile-boundary is an interior
--   edge of the macro-mesh, then these functions will ignore this fact.
instance BoundaryTestable (Node 2) where
  inside  p@(Node (V2 x y) _ _) =
    let w = maxWidth p
    in  x>0 && x<w && y>0 && y<w
  border  p@(Node (V2 x y) _ _) =
    let w = maxWidth p
    in  closure p && (x==0 || x==w || y==0 || y==w)
  outside p@(Node (V2 x y) _ _) =
    let w = maxWidth p
    in  x<0 || x>w || y<0 || y>w

-- | Apply the tests to leaf-cells, to determine whether their coordinates are
--   within their owning-trees, or not.
instance BoundaryTestable (Leaf 2) where
  inside  p = x>0 && x+lw<dw && y>0 && y+lw<dw where
    V2 x y = int <$> p ^. _coord
    dw = maxWidth p
    lw = width p
  outside p = x<0 || x+lw>dw || y<0 || y+lw>dw where
    V2 x y = int <$> p ^. _coord
    lw = width p
    dw = maxWidth p
  border  p = closure p && (x==0 || x+lw==dw || y==0 || y+lw==dw) where
    V2 x y = int <$> p ^. _coord
    dw = maxWidth p
    lw = width p

-- ** Queries
------------------------------------------------------------------------------
instance SetIndex (Fkey 2) (LocalForest 2) where
  k <? lf = k <? lf^.localForest || k <? lf^.ghostsLayer
  (<\) = error "LocalForest_2.(<\\): unimplemented"
  {-# INLINE (<?) #-}

-- ** Additional instances
------------------------------------------------------------------------------
instance MonoFoldable (LocalForest 2) where
  ofoldMap  f = foldMap  f . otoList
  ofoldr  f s = foldr  f s . otoList
  ofoldl' f s = foldl' f s . otoList
  otoList = flat . _localForest
  onull   = onull . _localForest
  olength = olength . _localForest
  ofoldr1Ex  f = foldr1 f . otoList
  ofoldl1Ex' f = uncurry (foldl' f) . (head &&& tail) . otoList


-- * Import and export
------------------------------------------------------------------------------

-- ** JSON save/load
------------------------------------------------------------------------------
saveLocalForest :: MonadIO m => FilePath -> LocalForest 2 -> m ()
saveLocalForest fp = liftIO . Lazy.writeFile fp . encode

loadLocalForest :: MonadIO m => FilePath -> m (Maybe (LocalForest 2))
loadLocalForest  = liftIO . fmap decode . Lazy.readFile

yamlSaveLocalForest :: MonadIO m => FilePath -> LocalForest 2 -> m ()
yamlSaveLocalForest fp = liftIO . BS.writeFile fp . Y.encodePretty ycfg
  where
    ycfg = Y.setConfCompare compare Y.defConfig

yamlLoadLocalForest ::
     MonadIO m
  => FilePath
  -> m (Either Y.ParseException (LocalForest 2))
yamlLoadLocalForest fp = liftIO $ Y.decodeFileEither fp

------------------------------------------------------------------------------
-- | Save the local node-info to a JSON file of the given name.
saveLocalNodes ::
  (MonadIO m, ToJSON (LocalNodes d)) => FilePath -> LocalNodes d -> m ()
saveLocalNodes fp = liftIO . Lazy.writeFile fp . encode

-- | Load the local node-info from a JSON file of the given name.
loadLocalNodes ::
  (MonadIO m, FromJSON (LocalNodes d)) => FilePath -> m (Maybe (LocalNodes d))
loadLocalNodes  = liftIO . fmap decode . Lazy.readFile

yamlSaveLocalNodes :: MonadIO m => FilePath -> LocalNodes 2 -> m ()
yamlSaveLocalNodes fp = liftIO . BS.writeFile fp . Y.encodePretty ycfg
  where
    ycfg = Y.setConfCompare compare Y.defConfig

yamlLoadLocalNodes ::
     MonadIO m
  => FilePath
  -> m (Either Y.ParseException (LocalNodes 2))
yamlLoadLocalNodes fp = liftIO $ Y.decodeFileEither fp


-- * Helper functions
------------------------------------------------------------------------------
dbg :: MonadIO m => String -> m ()
dbg  = debug' . pack
{-# INLINE dbg #-}

-- ** Conversion helpers
------------------------------------------------------------------------------
indep2node :: Indep -> Node 2
indep2node (Indep x y l _ _ t _) = Node (V2 x y) (int l) t
{-# INLINE indep2node #-}

node2indep :: Node 2 -> Indep
node2indep (Node (V2 x y) l t) =
  Indep x y (fromIntegral l) (-1) (-1) t (Rank (-1))
{-# INLINE node2indep #-}

-- ** Leaf-cell generators
------------------------------------------------------------------------------
-- TODO: make more general.
lastLeaf :: Leaf 2 -> Leaf 2
lastLeaf  = toleaf . lastQuad . l2quad
{-# INLINE lastLeaf #-}

firstLeaf :: forall d. Layered (Leaf d) => Leaf d -> Leaf d
firstLeaf  = _level .~ maxl where
  maxl = fromIntegral $ maxDepth (undefined :: Leaf d) - 1
{-# INLINE firstLeaf #-}

-- ** Node generators
------------------------------------------------------------------------------
-- | Construct one of the four possible corner-nodes of the indicated leaf-
--   cell, using the given Z-index representation of the corner-index.
makeNodeOf :: Diag 2 -> Fkey 2 -> Node 2
makeNodeOf d k = Node xy (int l') (int t') where
  Leaf ox' l' t' = lpath k
  V2 x y = int <$> ox'
  dw = width k
  xy = [V2 u v | v<-[y, y+dw], u<-[x, x+dw]] !! int d

-- ** Logging helper functions
------------------------------------------------------------------------------
trace' :: MonadIO m => Text -> m ()
trace'  = (>> flushLog) . liftIO . debugS "Local<pforest>"
{-# INLINE trace' #-}

trc :: MonadIO m => String -> m ()
trc  = trace' . pack
{-# INLINE trc #-}
