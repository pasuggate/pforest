---
title: "The `pforest` Library"
author: Patrick Suggate <<patrick.suggate@gmail.com>>
date: 23 March, 2019
geometry: margin=2cm
colorlinks: true
toc: true
toc-depth: 3
toccolor: purple
lof: true
secnumdepth: 3
fontsize: 11pt
biblio-title: Bibliography
---

\clearpage

!include ../rubs-base/tex/glossary.inc.latex

\clearpage

# Introduction #

!include intro.inc.md

\clearpage

# Parallel Forests # {#sec:pforests}

!include overview.inc.md

\clearpage

# High-Level Forest API # {#sec:hilevel-api}

!include hilevel-api.inc.md

\clearpage

# The Common API {#sec:common-api}

!include common-api.inc.md

\clearpage

# Mid-Level API for Parallel Forests # {#sec:midlevel-api}

!include midlevel-api.inc.md

\clearpage

# `pforest` Examples # {#sec:examples}

!include examples.inc.md

\clearpage

<!--
TODO:
# Other `pforest` API Functions # {#sec:backend}

!include backend.inc.md

\clearpage
-->

# Future Work # {#sec:future-work}

## Macro-Mesh Improvements ##

Referring to Figure \ref{fig:bad-macro-mesh} we can see the "islands" within some of the regions of the partition. This probably means that a better ordering and macro-mesh could be found, but this could be fairly difficult to automate?

\begin{figure}[htb]
\centering
\begin{minipage}{.24\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/bad-macro-mesh000.pdf}
  \label{fig:bad-macro-mesh000}
\end{minipage}%
\begin{minipage}{.24\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/bad-macro-mesh001.pdf}
  \label{fig:bad-macro-mesh001}
\end{minipage}%
\begin{minipage}{.24\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/bad-macro-mesh002.pdf}
  \label{fig:bad-macro-mesh002}
\end{minipage}%
\begin{minipage}{.24\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/bad-macro-mesh003.pdf}
  \label{fig:bad-macro-mesh003}
\end{minipage}\\
\vspace{5mm}
\begin{minipage}{.24\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/bad-macro-mesh004.pdf}
  \label{fig:bad-macro-mesh004}
\end{minipage}%
\begin{minipage}{.24\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/bad-macro-mesh005.pdf}
  \label{fig:bad-macro-mesh005}
\end{minipage}%
\begin{minipage}{.24\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{images/bad-macro-mesh006.pdf}
  \label{fig:bad-macro-mesh006}
\end{minipage}%
\begin{minipage}{.24\textwidth}
  \mbox{}%
\end{minipage}%
\caption{\label{fig:bad-macro-mesh}An inverted, subdivided L-shaped domain that has been (2:1, corner-) balanced, partitioned, and distributed across seven processes. Included in this image are the 'ghost' cells (shaded in grey), and the 'independent nodes' (colour if locally-owned).}
\end{figure}

## Support for Reduced-Continuity Features ##

Enforcing ($C^2$) continuity everywhere within a domain may reduce representational and solver efficiency, for some problems; e.g., perhaps when solving certain EM problems. By allowing for additional "virtual" nodes to be inserted, then would it be possible/realistic to support features with reduced continuity? E.g., $C^{-1}$ and $C^0$ across certain element-boundaries?

\clearpage

# Appendix: `pforest` API # {#app:pforest-api}

!include app-pforest.inc.md

\clearpage

# Appendix: Macro-Mesh Structures # {#sec:structures}

!include structures.inc.md

\clearpage

# Appendix: `p4est` Indexing Conventions # {#app:z-indices}

!include app-indices.inc.md

\clearpage

# Appendix: Select `pforest` Algorithms #

## Node-Level Calculation ## {#app:node-level}

!include ../rubs-base/doc/algo-node-level.inc.md

\clearpage

## Area Rule Mesh Constraint ## {#app:arearule}

!include algo-arearule.inc.md

\clearpage

## Remote Dependencies ## {#app:remotes}

!include ../rubs-base/doc/algo-remote-deps.inc.md

\clearpage

## Local "Cartesian" Traversals ## {#sec:traverse}

Even for fairly bizarre topologies, the `p4est` structure can be traversed as if it were (in some sense) *'Cartesian'*, as long as all "moves" are relative; e.g., west/east, and north/south. Then we can define a map from a Cartesian grid to quadrants of our forest, and keeping track of "visited" quadrants & coordinates so that each is only traversed/processed once. This allows traversals to be performed even in regions of the mesh where no consistent orientation can be assigned to all of its quadrants; e.g., the (surface-)traversals near the corner of a cube.

For the following algorithm to support cases like that of the corner of a cube, the traversal function needs:

#. a *'predicate'* that sets the *"domain"* of the 'Cartesian'-traversal region;
#. the `connectivity` structure that allows neighbours to be found across tree-boundaries;
#. coordinate transforms so that "moves" in 'Cartesian' coordinates can be transformed into local, quadrant-based directions;
#. state variables to track the set of quadrants that need visiting, and their (relative) orientations; and
#. state variables that track the set of quadrants that have already been visited,

all while building the output-list in the actual traversal-ordering.

### Tasks ###

#. Decide on a *"canonical traversal ordering"*, perhaps using the ordering defined for `Quad`{.haskell}'s?
   #. this is the ordering that quads are removed from the *'queue'*;
   #. decide between a `Map`{.haskell} or `Seq`{.haskell};
   #. For the *'queue'* data-structure, is it better to use `Map`{.haskell}, as it has better cache-locality?

#. Maps from *"local, Cartesian"* coordinates to:
   #. new *"local, Cartesian"* coordinates;
   #. quadrant at the given location;
   #. information that will be needed to compute the next map; and
   #. unit-tests for the various coordinate transform functions.

#. Variable quad-size version for building `Jaboc`{.haskell} meshes?

#. If I use the `Stream`{.haskell} data-type (along with `Step`{.haskell}), are there any opportunities for *'stream-fusion'*? E.g., for `Vector`{.haskell} $\rightarrow$ `Stream`{.haskell} $\rightarrow$ `Vector`{.haskell} type operations?

#. Can I use a more general function; e.g., like: \
   `bfoldl :: (a -> Fkey 2 -> a) -> a -> P4estM a`{.haskell}?

### Flood-Fill Algorithm ###

Function has definition:

```haskell
floodFillM :: (V3 Int -> Bool) -> V3 Int -> Quad -> ForestM [Quad]
```

Traversal algorithm:

#. Set up the state-variables, given the initial quadrant, $q_i$.

#. Test each of $q_i$'s neighbours, and store them in the traversal-"queue" on `True`{.haskell}, for:
   #. "Cartesian" coordinates that pass the inclusion-test;
   #. quads that are within the domain/boundary; and
   #. quads not present in the `visits`{.haskell} list.

#. Compute and store the relative ("Cartesian") coordinates, plus its orientation (as an `Orient`{.haskell}), for each new quad, and relative to that of the initial quadrant, with respect to its (local) coordinate-system.

#. Store visited quads in `visits`{.haskell}.

\clearpage

# Bibliography #
