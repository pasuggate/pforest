{-# LANGUAGE BangPatterns, CPP, DeriveGeneric, FlexibleInstances,
             GeneralizedNewtypeDeriving, OverloadedStrings, PatternSynonyms,
             TemplateHaskell, TypeFamilies, ViewPatterns #-}
{-# OPTIONS_GHC -Wall -fno-warn-orphans #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.Types
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Mappings between Haskell and C representations of `p4est` data-types.
--
-- Changelog:
--  + 31/07/2017  --  initial file;
--  + 18/03/2019  --  started a fairly major refactoring so that this mid-
--                    level interface can be used with RUB-spline meshes;
--
------------------------------------------------------------------------------

#include <p4est.h>
#include <sc.h>
#include <bindings.dsl.h>

module Data.Forest.P4est.Types
  ( CSize
  , module Data.Forest.Mesh
  , module Data.Forest.P4est.Types
  , module SC
  , P4estConnectType (..)
  , pattern ConnectFace
  , pattern ConnectFull
  )
where

import           Control.DeepSeq
import           Control.Distributed.MPI          as MPI
import           Control.Lens                     (lens, makeLenses)
import           Control.Monad.State
import           Data.Aeson                       as Aeson
import           Data.Bits.Handy                  ((<<%), (>>%))
import           Data.Vector.Storable             (Vector)
import qualified Data.Vector.Storable             as Vec
import qualified Data.Vector.Storable.Mutable     as Mut
import           Foreign.C.Types
import           Foreign.Marshal.Utils
import           Foreign.Ptr
import           Foreign.Storable
import           GHC.Generics                     (Generic)
import           GHC.Int
import           GHC.Types                        (SPEC (..))
import           Linear.Handy
import           System.IO.Unsafe
import           Text.Printf

import           Bindings.P4est.P4est
import           Bindings.P4est.P4estBase
import           Bindings.P4est.P4estBits
import           Bindings.P4est.P4estConnectivity
import           Bindings.P4est.P4estGeometry
import           Bindings.P4est.P4estGhost
import           Bindings.P4est.P4estNodes
import           Data.Forest.Mesh
import           Data.Index.ZCurve
import           Data.Scientific.SC               as SC
import           Data.Scientific.SC.Array


-- * Convenience patterns
------------------------------------------------------------------------------
-- | Maximum refinement level for a quadrant.
--   NOTE: any quad with level _equal_ to this is classed as a 'node' by the
--     'p4est' routines.
{-- }
pattern QuadMaxLevel :: Integral i => i
pattern QuadMaxLevel <- ((== c'P4EST_QMAXLEVEL) -> True)
  where QuadMaxLevel  = c'P4EST_QMAXLEVEL
--}

pattern NodeMaxLevel :: Integral i => i
pattern NodeMaxLevel <- ((== c'P4EST_MAXLEVEL) -> True)
  where NodeMaxLevel  = c'P4EST_MAXLEVEL

pattern RubsMaxLevel :: Integral i => i
pattern RubsMaxLevel <- ((== 14) -> True)
  where RubsMaxLevel  = 14

-- ** MPI tags used by @p4est@
------------------------------------------------------------------------------
pattern MpiTagP4estFirst :: MPI.Tag
pattern MpiTagP4estFirst <- ((== MPI.Tag c'P4EST_COMM_COUNT_PERTREE) -> True)
  where MpiTagP4estFirst  = MPI.Tag c'P4EST_COMM_COUNT_PERTREE

pattern MpiTagP4estLast :: MPI.Tag
pattern MpiTagP4estLast <- ((== MPI.Tag (succ c'P4EST_COMM_LNODES_ALL)) -> True)
  where MpiTagP4estLast  = MPI.Tag (succ c'P4EST_COMM_LNODES_ALL)

{-- }
pattern MpiTagP4estFirst :: MPI.Tag
pattern MpiTagP4estFirst <- ((== MPI.Tag c'P4EST_COMM_TAG_FIRST) -> True)
  where MpiTagP4estFirst  = MPI.Tag c'P4EST_COMM_TAG_FIRST

pattern MpiTagP4estLast :: MPI.Tag
pattern MpiTagP4estLast <- ((== MPI.Tag c'P4EST_COMM_TAG_LAST) -> True)
  where MpiTagP4estLast  = MPI.Tag c'P4EST_COMM_TAG_LAST
--}


-- * Core data-types
------------------------------------------------------------------------------
-- | A "connectivity" stores the connectivity of the forest (of octrees).
newtype ConnectivityT =
  ConnectivityT { _unConnectivityT :: Ptr C'p4est_connectivity_t }
  deriving (Eq, Show, Storable, Generic, NFData)

-- ** Ghost data-types
------------------------------------------------------------------------------
-- | Stores the quads that need to be shared with other processes.
newtype GhostT =
  GhostT { _unGhostT :: Ptr C'p4est_ghost_t }
  deriving (Eq, Show, Storable, Generic)

-- ** Node data-types
------------------------------------------------------------------------------
-- | Independent nodes, where (only) corners of cells meet.
newtype IndepT =
  IndepT { _unIndepT :: Ptr C'p4est_indep_t }
  deriving (Eq, Show, Storable, Generic)

-- | Hanging-nodes are those that form "T-junctions" along edges of cells.
newtype Hang2T =
  Hang2T { _unHang2T :: Ptr C'p4est_hang2_t }
  deriving (Eq, Show, Storable, Generic)

-- | Pointer to a data-structure containing complete information about local,
--   shared, and (any) ghost nodes.
newtype NodesT =
  NodesT { _unNodesT :: Ptr C'p4est_nodes_t }
  deriving (Eq, Show, Storable, Generic)


-- * Forest data-types
------------------------------------------------------------------------------
-- | Parallel forest data-type.
--   NOTE: State information/container for a `p4est` instance.
newtype P4estT = P4estT { _unP4estT :: Ptr C'p4est_t }
  deriving (Eq, Show, Storable, Generic, NFData)


-- ** Nodes data-types
------------------------------------------------------------------------------
-- | Independent nodes, where (only) corners of cells meet.
data Indep
  = Indep
      { _ix, _iy :: Int
      , _ilevel  :: Int8
        -- #sharers
      , _ipad8   :: Int8 -- #sharers
        -- i for sharers[i]
      , _ipad16  :: Int16 -- i for sharers[i]
      , _itree   :: Int
      , _ilocal  :: Rank
      }
  deriving (Eq, Show, Generic)

-- | Hanging-nodes are those that form "T-junctions" along edges of cells.
data Hangs
  = Hangs
      { _hx, _hy :: Int
      , _hlevel  :: Int8
      , _hpad8   :: Int8
      , _hpad16  :: Int16
      , _htree   :: Int
        -- independent nodes being "hung" off-of
      , _h1, _h2 :: Int -- independent nodes being "hung" off-of
      }
  deriving (Eq, Show, Generic)

-- ** Leaf data-types
------------------------------------------------------------------------------
newtype QuadrantT = QuadrantT { _unQuadrantT :: Ptr C'p4est_quadrant_t }
  deriving (Eq, Show, Storable, Generic)

data Quad
  = Quad
      { _qLocation :: !(V2 Int32)
      , _qLevel    :: !Int8
      , _qPad8     :: !Int8
      , _qPad16    :: !Int16
      , _qTree     :: !Int32
        -- sometimes 'local_num'; e.g., for ghosts
      , _qRank     :: !Rank -- sometimes 'local_num'; e.g., for ghosts
      }
  deriving (Show, Generic)

-- ** Tree data-types
------------------------------------------------------------------------------
newtype TreeT
  = TreeT { _unTreeT :: Ptr C'p4est_tree_t }
  deriving (Eq, Show, Storable, Generic)

data Tree
  = Tree
      { _tQuads      :: !(Vector Quad)
      , _tFirst      :: !Quad
      , _tLast       :: !Quad
      , _tOffset     :: !Int32
      , _tQuadLevels :: !(Vector Int32)
      , _tMaxLevel   :: !Int8
      }
  deriving (Eq, Show, Generic)


-- ** Geometry data structures
------------------------------------------------------------------------------
newtype GeometryT
  = GeometryT { _getGeometryT :: Ptr C'p4est_geometry_t }
  deriving (Eq, Show, Storable, Generic)

-- | Null geometry.
pattern GeomNull :: GeometryT
pattern GeomNull <- ((== GeometryT nullPtr) -> True)
  where GeomNull  = GeometryT nullPtr


-- * Aliases for C/p4est types.
------------------------------------------------------------------------------
-- | Alias for "topological" indices.
-- #integral_t p4est_topidx_t
type TInt = C'p4est_topidx_t

-- | Alias for "local" indices.
-- #integral_t p4est_locidx_t
type LInt = C'p4est_locidx_t

-- | Alias for "global" indices.
-- #integral_t p4est_gloidx_t
type GInt = C'p4est_gloidx_t

-- | Alias for "quadrant coordinate" indices.
-- #integral_t p4est_qcoord_t
type QInt = C'p4est_qcoord_t


------------------------------------------------------------------------------
type PBool = CInt

pbool :: Bool -> PBool
pbool False = 0
pbool True  = 1

data UserPtr
  = UserPtr
      { _getUserPtr :: Ptr ()
      }
  | UserNil
  deriving (Eq, Show, Generic)


-- * Callback types.
------------------------------------------------------------------------------
-- | Initialises a quadrant's user-data.
#callback_t InitFn , P4estT -> TInt -> QuadrantT -> IO ()
newtype InitFn  = InitFn { unInitFn :: C'InitFn }
  deriving (Eq, Show, Storable, Generic)

pattern NoInitFn :: InitFn
pattern NoInitFn <- ((== InitFn nullFunPtr) -> True)
  where NoInitFn  = InitFn nullFunPtr

------------------------------------------------------------------------------
-- | Needs to be marshalled.
data InitFun
  = InitFun
      { _getInitFun :: P4estT -> TInt -> QuadrantT -> QuadrantT
      }
  | InitNil
  deriving (Generic)

data WeightFun
  = WeightFun
      { _getWeightFun :: P4estT -> TInt -> QuadrantT -> TInt
      }
  | WeightNil
  deriving (Generic)

------------------------------------------------------------------------------
-- TODO: The return-type represents a boolean? Set by the user, so can be made
--   to always be `\in {0,1}`?
-- #callback_t RefineFn , P4estT -> TInt -> QuadrantT -> IO CInt
type    RefineFnT = P4estT -> TInt -> QuadrantT -> IO Bool
-- newtype RefineFn  = RefineFn { unRefineFn :: C'RefineFn }
newtype RefineFn  = RefineFn { unRefineFn :: C'p4est_refine_t }
  deriving (Eq, Show, Storable, Generic)

pattern NoRefineFn :: RefineFn
pattern NoRefineFn <- ((== RefineFn nullFunPtr) -> True)
  where NoRefineFn  = RefineFn nullFunPtr

------------------------------------------------------------------------------
-- #callback_t CoarsenFn , P4estT -> TInt -> Ptr QuadrantT -> IO CInt
type    CoarsenFnT = P4estT -> Int -> Ptr QuadrantT -> IO Bool
-- newtype CoarsenFn  = CoarsenFn { unCoarsenFn :: C'CoarsenFn }
newtype CoarsenFn  = CoarsenFn { unCoarsenFn :: C'p4est_coarsen_t }
  deriving (Eq, Show, Storable, Generic)

pattern NoCoarsenFn :: CoarsenFn
pattern NoCoarsenFn <- ((== CoarsenFn nullFunPtr) -> True)
  where NoCoarsenFn  = CoarsenFn nullFunPtr

------------------------------------------------------------------------------
#callback_t WeightFn , P4estT -> TInt -> QuadrantT -> IO CInt
type    WeightFnT = P4estT -> TInt -> QuadrantT -> IO CInt
newtype WeightFn  = WeightFn { unWeightFn :: C'WeightFn }
  deriving (Eq, Show, Storable, Generic)

pattern NoWeightFn :: WeightFn
pattern NoWeightFn <- ((== WeightFn nullFunPtr) -> True)
  where NoWeightFn  = WeightFn nullFunPtr


-- * Instances
------------------------------------------------------------------------------
instance ToJSON   Indep
instance FromJSON Indep

instance ToJSON   Hangs
instance FromJSON Hangs

-- ** Moar 'p4est' instances
------------------------------------------------------------------------------
instance ToJSON   P4estConnectType
instance FromJSON P4estConnectType

------------------------------------------------------------------------------
-- | Instance is here, because 'Quad' depends on one of the above instances.
instance ToJSON   Quad
instance FromJSON Quad

instance ToJSON   Tree
instance FromJSON Tree

-- ** Coordinate/index instances
------------------------------------------------------------------------------
instance HasTreeID Quad where
  treeId  = fromIntegral . _qTree
  _treeId = lens treeId (\r s -> r { _qTree = fromIntegral s })
  {-# INLINE treeId #-}
  {-# INLINE _treeId #-}

instance HasTreeID Indep where
  treeId  = fromIntegral . _itree
  _treeId = lens treeId (\r s -> r { _itree = fromIntegral s })
  {-# INLINE treeId #-}
  {-# INLINE _treeId #-}

instance HasTreeID Hangs where
  treeId  = fromIntegral . _htree
  _treeId = lens treeId (\r s -> r { _htree = fromIntegral s })
  {-# INLINE treeId #-}
  {-# INLINE _treeId #-}

instance HasCoords Quad where
  type CoordVec Quad = V3 Int
  coords = quadToCoord
  {-# INLINE coords #-}

------------------------------------------------------------------------------
-- | Compute the width of a quad, based upon its quad-level.
instance HasWidth Quad where
  maxWidth _ = 1 <<% treeMaxLevel
  width = quadrantWidth . _qLevel
  {-# INLINE maxWidth #-}
  {-# INLINE width #-}

------------------------------------------------------------------------------
instance Layered Indep where
  maxDepth _ = quadMaxLevel
  depth = fromIntegral . _ilevel
  {-# INLINE maxDepth #-}
  {-# INLINE depth #-}

instance Layered Hangs where
  maxDepth _ = quadMaxLevel
  depth = fromIntegral . _hlevel
  {-# INLINE maxDepth #-}
  {-# INLINE depth #-}

-- ** Storable instances
------------------------------------------------------------------------------
-- | Independent nodes are typical tpological and FEA nodes.
--   NOTE: these may or may not be "inferred nodes", of a RUB-spline mesh.
instance Storable Indep where
  sizeOf  _ = sizeOf (undefined :: C'p4est_indep)
  alignment = const $ alignment (undefined :: C'p4est_indep)
  peek p'   = do
    let p = castPtr p' :: Ptr C'p4est_indep
    x   <- fmap fromIntegral . peek $ p'p4est_indep'x p
    y   <- fmap fromIntegral . peek $ p'p4est_indep'y p
    lv  <- fmap fromIntegral . peek $ p'p4est_indep'level p
    p8  <- fmap fromIntegral . peek $ p'p4est_indep'pad8 p
    p16 <- fmap fromIntegral . peek $ p'p4est_indep'pad16 p
    wt  <- fmap fromIntegral . peek $ p'p4est_indep'p'piggy3'which_tree p
    ln  <- fmap fromIntegral . peek $ p'p4est_indep'p'piggy3'local_num p
    return $ Indep x y lv p8 p16 wt ln
  poke p' (Indep x y l a b t n) = do
    let p = castPtr p' :: Ptr C'p4est_indep
    p'p4est_indep'x p `poke` fromIntegral x
    p'p4est_indep'y p `poke` fromIntegral y
    p'p4est_indep'level p `poke` fromIntegral l
    p'p4est_indep'pad8 p `poke` fromIntegral a
    p'p4est_indep'pad16 p `poke` fromIntegral b
    p'p4est_indep'p'piggy3'which_tree p `poke` fromIntegral t
    p'p4est_indep'p'piggy3'local_num p `poke` fromIntegral n
  {-# INLINE sizeOf #-}
  {-# INLINE alignment #-}

------------------------------------------------------------------------------
-- | Hanging-node data-type is one that is positioned halfway along the edge
--   of a coarse cell.
--   NOTE: these nodes are one type of "inferred-node", of a RUB-spline mesh.
instance Storable Hangs where
  sizeOf  _ = sizeOf (undefined :: C'p4est_hang2)
  alignment = const $ alignment (undefined :: C'p4est_hang2)
  peek p'   = do
    let p = castPtr p' :: Ptr C'p4est_hang2
    x   <- fmap fromIntegral . peek $ p'p4est_hang2'x p
    y   <- fmap fromIntegral . peek $ p'p4est_hang2'y p
    lv  <- fmap fromIntegral . peek $ p'p4est_hang2'level p
    p8  <- fmap fromIntegral . peek $ p'p4est_hang2'pad8 p
    p16 <- fmap fromIntegral . peek $ p'p4est_hang2'pad16 p
    wt  <- fmap fromIntegral . peek $ p'p4est_hang2'p'piggy'which_tree p
    d0  <- fmap fromIntegral . peek $ p'p4est_hang2'p'piggy'depends_0_ p
    d1  <- fmap fromIntegral . peek $ p'p4est_hang2'p'piggy'depends_1_ p
    return $ Hangs x y lv p8 p16 wt d0 d1
  poke p' (Hangs x y l a b t d e) = do
    let p = castPtr p' :: Ptr C'p4est_hang2
    p'p4est_hang2'x p `poke` fromIntegral x
    p'p4est_hang2'y p `poke` fromIntegral y
    p'p4est_hang2'level p `poke` fromIntegral l
    p'p4est_hang2'pad8 p `poke` fromIntegral a
    p'p4est_hang2'pad16 p `poke` fromIntegral b
    p'p4est_hang2'p'piggy'which_tree p `poke` fromIntegral t
    p'p4est_hang2'p'piggy'depends_0_ p `poke` fromIntegral d
    p'p4est_hang2'p'piggy'depends_1_ p `poke` fromIntegral e
  {-# INLINE sizeOf #-}
  {-# INLINE alignment #-}

-- ** Quad instances
------------------------------------------------------------------------------
-- | Store quadrants in a format that is compatible with 'p4est'.
instance Storable Quad where
  sizeOf _ = sizeOf (undefined :: C'p4est_quadrant)
  alignment _ = 4
  peek p' = do
    let p = castPtr p' :: Ptr C'p4est_quadrant
    x   <- fmap fromIntegral . peek $ p'p4est_quadrant'x p
    y   <- fmap fromIntegral . peek $ p'p4est_quadrant'y p
    lv  <- fmap fromIntegral . peek $ p'p4est_quadrant'level p
    p8  <- fmap fromIntegral . peek $ p'p4est_quadrant'pad8 p
    p16 <- fmap fromIntegral . peek $ p'p4est_quadrant'pad16 p
    t   <- fmap fromIntegral . peek $ p'p4est_quadrant'p'piggy3'which_tree p
    r   <- fmap fromIntegral . peek $ p'p4est_quadrant'p'piggy3'local_num  p
--     t   <- fmap fromIntegral . peek $ p'p4est_quadrant'p'piggy1'which_tree p
--     r   <- fmap fromIntegral . peek $ p'p4est_quadrant'p'piggy1'owner_rank p
    return $ Quad (V2 x y) lv p8 p16 t r
  poke p' (Quad (V2 x y) l a b t r) = do
    let p = castPtr p' :: Ptr C'p4est_quadrant
    p'p4est_quadrant'x p `poke` fromIntegral x
    p'p4est_quadrant'y p `poke` fromIntegral y
    p'p4est_quadrant'level p `poke` fromIntegral l
    p'p4est_quadrant'pad8 p `poke` fromIntegral a
    p'p4est_quadrant'pad16 p `poke` fromIntegral b
--     p'p4est_quadrant'p'piggy1'which_tree p `poke` fromIntegral t
--     p'p4est_quadrant'p'piggy1'owner_rank p `poke` fromIntegral r
    p'p4est_quadrant'p'piggy3'which_tree p `poke` fromIntegral t
    p'p4est_quadrant'p'piggy3'local_num  p `poke` fromIntegral r
  {-# INLINE sizeOf    #-}
  {-# INLINE alignment #-}

------------------------------------------------------------------------------
-- | Compare whether two quads are equal, and using the 'which_tree' values.
--   TODO: instances for 'Indep' and 'Hangs'?
instance Eq Quad where
  (==) = quadEq
  {-# INLINE (==) #-}

-- | Compute the relative ordering of two quads, and using the 'which_tree'
--   values.
--   TODO: instances for 'Indep' and 'Hangs'?
instance Ord Quad where
  compare = quadCompare
  {-# INLINE compare #-}

instance Layered Quad where
  maxDepth _ = treeMaxLevel
  depth = fromIntegral . _qLevel
  {-# INLINE maxDepth #-}
  {-# INLINE depth #-}

-- ** Tree instances
------------------------------------------------------------------------------
instance Storable Tree where
  sizeOf    _ = sizeOf (undefined :: C'p4est_tree)
  alignment _ = 8
  peek        = peekTreeT
  poke    _ _ = error "P4est.Types: Storable.poke_Tree -- unimplemented"
  {-# INLINE sizeOf    #-}
  {-# INLINE alignment #-}


-- * Helper functions
------------------------------------------------------------------------------
peekTreeT :: Ptr Tree -> IO Tree
peekTreeT p' = do
  let p   = castPtr p' :: Ptr C'p4est_tree
      ml' = c'P4EST_MAXLEVEL + 1
      pl  = castPtr $ p'p4est_tree'quadrants_per_level p
  qs <- toVector . ScArray $ p'p4est_tree'quadrants p
  qf <- peek . castPtr $ p'p4est_tree'first_desc p
  ql <- peek . castPtr $ p'p4est_tree'last_desc p
  qo <- fmap fromIntegral . peek $ p'p4est_tree'quadrants_offset p
  lc <- Mut.new ml' >>= \ar -> Mut.unsafeWith ar $ \q -> do
    copyBytes q pl (ml'*sizeOf (undefined :: LInt))
    Vec.unsafeFreeze ar
  ml <- fmap fromIntegral . peek $ p'p4est_tree'maxlevel p
  return $ Tree qs qf ql qo lc ml

-- | Read in a raw 'p4est' tree so that it is easy to process.
fromTreeT :: MonadIO m => TreeT -> m Tree
fromTreeT (TreeT p) = liftIO $ do
  let ml' = c'P4EST_MAXLEVEL
      pl  = castPtr $ p'p4est_tree'quadrants_per_level p
  -- trees:
  qs <- toVector . ScArray $ p'p4est_tree'quadrants p
  qf <- peek . castPtr $ p'p4est_tree'first_desc p
  ql <- peek . castPtr $ p'p4est_tree'last_desc p
  qo <- fmap fromIntegral . peek $ p'p4est_tree'quadrants_offset p
  lc <- Mut.new ml' >>= \ar -> Mut.unsafeWith ar $ \q -> do
    copyBytes q pl (ml'*sizeOf (undefined :: LInt))
    Vec.unsafeFreeze ar
  ml <- fmap fromIntegral . peek $ p'p4est_tree'maxlevel p
  return $ Tree qs qf ql qo lc ml

------------------------------------------------------------------------------
-- | Chop the given vector up into segments, using the given "segment-offset"
--   vector.
chop :: Storable a => Vector a -> Vector Int -> [Vector a]
chop xs ks
  | Vec.null ks = error "chop: indices-vector must have non-zero length"
  | otherwise   = go SPEC (Vec.head ks) (Vec.tail ks)
  where
    go !_ i js
      | Vec.null js = []
      | otherwise   = x:go SPEC j (Vec.tail js) where
          (j, x) = (Vec.head js, Vec.slice i (j-i) xs)
{-# INLINE chop #-}

-- ** Comparisons
------------------------------------------------------------------------------
-- | Compare whether two quads are equal, and using the 'which_tree' values.
quadEq :: Quad -> Quad -> Bool
quadEq q0 q1 = unsafePerformIO $ do
  with q0 $ \p0 -> do
    with q1 $ \p1 -> do
      (0/=) <$> c'p4est_quadrant_is_equal_piggy (castPtr p0) (castPtr p1)
{-# INLINE quadEq #-}

-- | Compute the relative ordering of two quads, and using the 'which_tree'
--   values.
--   NOTE: requires that 'q0' & 'q1' both be "extended".
quadCompare :: Quad -> Quad -> Ordering
quadCompare q0 q1 = unsafePerformIO $ do
  with q0 $ \p0 -> with q1 $ \p1 -> do
    flip compare 0 <$> c'p4est_quadrant_compare_piggy (castPtr p0) (castPtr p1)
{-# INLINE quadCompare #-}

-- | Test if a quadrant has valid Morton indices in the 3x3 box around root.
quadIsExtended :: Quad -> Bool
quadIsExtended q = unsafePerformIO $ do
  with q $ \p -> fmap (/=0) . c'p4est_quadrant_is_extended $ castPtr p
{-# INLINE quadIsExtended #-}

------------------------------------------------------------------------------
-- | Test if two quadrants overlap.
quadsOverlap :: Quad -> Quad -> Bool
quadsOverlap q0 q1 = unsafePerformIO $ do
  with q0 $ \p0 -> with q1 $ \p1 -> do
    (0/=) <$> c'p4est_quadrant_overlaps (castPtr p0) (castPtr p1)
{-# INLINE quadsOverlap #-}


-- * Helper functions
------------------------------------------------------------------------------
-- | Calculate the width of the quadrant for the given quadrant-level.
quadrantWidth :: (Integral i, Num a) => i -> a
quadrantWidth  = fromIntegral . c'P4EST_QUADRANT_LEN . fromIntegral
{-# INLINE quadrantWidth #-}

-- | Extract and convert the coordinates of a `p4est` quadrant to a form that
--   is compatible with `Ltree`.
quadToCoord :: Quad -> V3 Int
quadToCoord (Quad (V2 i j) l _ _ _ _)
  | d<=maxDepth2 = V3 x y d
  | otherwise    = error $ printf "quadToCoord: too deep (%d)" d
  where (x, y) = (fromIntegral i>>%o, fromIntegral j>>%o)
        (o, d) = (treeMaxLevel - maxDepth2, fromIntegral l)
{-# INLINE quadToCoord #-}


-- * Lenses
------------------------------------------------------------------------------
makeLenses ''P4estT
makeLenses ''ConnectivityT
makeLenses ''GhostT
makeLenses ''IndepT
makeLenses ''Hang2T
makeLenses ''NodesT
makeLenses ''TreeT
makeLenses ''QuadrantT

makeLenses ''Indep
makeLenses ''Hangs
makeLenses ''Quad
makeLenses ''Tree
