{-# LANGUAGE DataKinds, DeriveGeneric, FlexibleContexts, FlexibleInstances,
             GeneralizedNewtypeDeriving, OverloadedStrings,
             ScopedTypeVariables, StandaloneDeriving, TemplateHaskell,
             TypeFamilies #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.Export.SVG
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- SVG exporters for `p4est` meshes; i.e., forests of linear-trees.
--
-- TODO:
--  + move to `rubs-mesh` as this module makes assumptions about supported
--    mesh topologies, and that are more restrictive than that of 'p4est';
--  + still to many magic-numbers throughout the code -- mostly related to
--    scaling & depths -- and I need to clean this up;
--
------------------------------------------------------------------------------

module Data.Forest.P4est.Export.SVG
  ( SvgQuad (..)
  , SvgProps (..)

  -- forest data with export metadata:
  , LocalExport (..)
  , mkLocalExport
  , saveLocalExport
  , loadLocalExport
  , yamlSaveLocalExport

  -- exporters:
  , writeP4estAsSVG
  , localForestToSVG
  , svgWriteFile

  -- testing & examples:
  , testExport
  )
where

import           Control.Arrow            ((&&&), (>>>))
import           Control.Lens             (makeLenses, (%~), (.~), (^.))
import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Bits.Handy          (int, (<<%), (>>%))
import           Data.Bool                (bool)
import qualified Data.ByteString          as BS
import qualified Data.ByteString.Lazy     as Lazy
import           Data.Foldable            (foldl')
import           Data.Forest.Linear
import           Data.Forest.Local        hiding (ghostsLayer, localForest,
                                           mirrorLayer, pos)
import           Data.Forest.Mesh.Macro2D
import           Data.Forest.P4est.P4est  (P4estM)
import           Data.Forest.P4est.Types  hiding (pos)
import           Data.Index.ZCurve        (Key (..), maxDepth2)
import           Data.IntSet.Sets
import           Data.Maybe               (fromJust)
import qualified Data.Scientific.SC       as SC
import           Data.Text                as Text (Text)
import           Data.Tree.Linear.Export  (txt)
import qualified Data.Tree.Linear.Export  as Ltree
import           Data.Vector.Storable     ((!))
import qualified Data.Vector.Storable     as Vec
import qualified Data.Yaml.Pretty         as Y
import           GHC.Generics             (Generic)
import           GHC.TypeNats
import           Graphics.Svg             as Svg
import           Linear                   (V2 (..), V3 (..), (*^), (^+^), (^-^),
                                           _x, _xy, _y, _z)
import           Text.Printf


-- * Data-types to use while exporting
------------------------------------------------------------------------------
data SvgQuad
  = SvgQuad
      { _qpos :: V3 Int
      , _qsiz :: Int
      , _qtyp :: QuadType
      , _qclr :: Text
      }
  deriving (Eq, Show, Generic)

-- | Quadrant types and attributes.
type QuadType = [QuadAttr]

data QuadAttr
  = QuadCoarse
  | QuadGhost
  | QuadMirror [(Int, Either Int (Dir 2))]
  deriving (Eq, Show, Generic)

------------------------------------------------------------------------------
-- | Set the drawing-parameters for the current SVG element.
data SvgProps
  = SvgProps
      { _strokeWidth   :: Int
      , _strokeColour  :: Text
      , _imageBorder   :: Int
      , _treeTileSize  :: Int
      , _borderWidth   :: Int
      , _quadTileSize  :: Int
      , _quadBorder    :: Int
      , _nodeBorder    :: Int
      , _maxNodeRadius :: Int
      , _sharedColour  :: Text
      , _borderColour  :: Text
      , _ghostsColour  :: Text
      }
  deriving (Eq, Show, Generic)

-- ** Forest data-types
------------------------------------------------------------------------------
-- | Store the local portion of the forest as a map of sets: from tree-index
--   to tree, then a set of that tree's cell-(Z-)indices.
--
--   TODO:
--    + not much use without the 'Embedding'?
--
data LocalExport (d :: Nat)
  = LocalExport
      { _localForest :: Lforest d
      , _ghostsLayer :: Lforest d
      , _mirrorLayer :: Lforest d
      , _coarseCells :: Lforest d
      , _dofNodeInfo :: LocalNodes d
      , _embedded    :: MacroMesh d
      }
  deriving (Generic)

deriving instance Show (LocalExport 2)


-- * Some lenses and instances
------------------------------------------------------------------------------
-- | Lenses that are used when exporting parallel forests.
makeLenses ''SvgProps
makeLenses ''LocalExport

------------------------------------------------------------------------------
instance ToJSON   SvgProps
instance FromJSON SvgProps

instance ToJSON   QuadAttr
instance FromJSON QuadAttr

instance ToJSON   SvgQuad
instance FromJSON SvgQuad

instance ToJSON   (LocalExport 2)
instance FromJSON (LocalExport 2)


-- * JSON save/load
------------------------------------------------------------------------------
saveLocalExport :: MonadIO m => FilePath -> LocalExport 2 -> m ()
saveLocalExport fp = liftIO . Lazy.writeFile fp . encode

loadLocalExport :: MonadIO m => FilePath -> m (Maybe (LocalExport 2))
loadLocalExport  = liftIO . fmap decode . Lazy.readFile

yamlSaveLocalExport :: MonadIO m => FilePath -> LocalExport 2 -> m ()
yamlSaveLocalExport fp = liftIO . BS.writeFile fp . Y.encodePretty ycfg
  where
    ycfg = Y.setConfCompare compare Y.defConfig


-- * Settings
------------------------------------------------------------------------------
defaultProps :: SvgProps
defaultProps  = SvgProps 1 "black" 20 1032 4 1024 4 2 20
  "lightgrey" "lightgrey" (Ltree.shade 4)
{-# INLINE defaultProps #-}


-- * SVG export/conversion
------------------------------------------------------------------------------
-- | Extract, convert, and export the local subforest as an SVG image.
writeP4estAsSVG :: FilePath -> P4estM ()
writeP4estAsSVG fp = do
  rank <- fromEnum <$> SC.mpiRank
  l4st <- mkLocalExport <$> mkLocalForest
  liftIO . Svg.renderToFile fp $ localForestToSVG (rank+1) l4st

------------------------------------------------------------------------------
-- | Write the local subforest to an SVG file.
svgWriteFile :: FilePath -> P4estM ()
svgWriteFile fp = do
  rank <- fromEnum <$> SC.mpiRank
  writeP4estAsSVG (printf "%s%03d.svg" fp rank :: FilePath)

------------------------------------------------------------------------------
-- | Convert the (augmented) local forest to SVG.
--
--   TODO:
--    + handle coarse-cells, ghosts/mirrors, and node-labelings;
--
localForestToSVG :: Int -> LocalExport 2 -> Element
localForestToSVG rank lf =
  let (V2 h v, xy) = calcTreeOrigins ps lf
      ps  = defaultProps
      -- compute and draw the local-nodes (owned & shared):
      dof = lf^.dofNodeInfo
      ns  = dof^.localNodes
      own = Vec.toList $ Vec.imap (\i _ -> i <? dof^.cellOfNode) ns
      clr = bool (ps^.sharedColour) (Ltree.colour rank)
      pts = zipWith (\n o -> drawNode ps (xy!!_tId n) n (clr o)) (Vec.toList ns) own
      -- compute and draw the local, mirror, and ghost quads:
      (ts, gs) = (flat $ lf^.localForest^._ltrees, flat $ lf^.ghostsLayer^._ltrees)
      fun t lt = let t' = toEnum t :: TreeID
                 in  drawQuad lf ps (xy!!t) rank t' <$> flat lt
      qds = map mconcat $ flip map ts $ uncurry fun
      gss = map mconcat $ flip map gs $ uncurry fun
      els = drawTreeBorder ps `fmap` xy ++ qds ++ gss
      pic = rect_ [ Width_ <<- "100%", Height_ <<- "100%", "white" ->> Fill_ ]
  in  Ltree.svgSize h v $ pic <> mconcat els <> mconcat pts

-- ** Quad-export helpers
------------------------------------------------------------------------------
drawQuad :: LocalExport 2 -> SvgProps -> V2 Int -> Int -> TreeID -> Key 2 ->
            Element
drawQuad lf ps oo rank t = quadToElement ps oo . makeQuad lf rank t

makeQuad :: LocalExport 2 -> Int -> TreeID -> Key 2 -> SvgQuad
makeQuad lf rank t k = SvgQuad xy w qt (Ltree.colour rank) where
  sc = maxDepth2 - 10
  xy = _xy %~ fmap (>>% sc) $ lpath k
  w  = 1 <<% (maxDepth2 - xy^._z - sc)
  qt = quadType lf t k

quadType :: LocalExport 2 -> TreeID -> Key 2 -> QuadType
quadType lf t k =
  let fs = [ maybe id (bool id (QuadGhost:)     . (k<?)) . (<~lf^.ghostsLayer^._ltrees)
           , maybe id (bool id (QuadMirror []:) . (k<?)) . (<~lf^.mirrorLayer^._ltrees)
           , maybe id (bool id (QuadCoarse:)    . (k<?)) . (<~lf^.coarseCells^._ltrees)
           ]
  in  foldr ($ int t) [] fs

-- ** Tree-export helpers
------------------------------------------------------------------------------
-- | Compute all of the origin-positions for the forest's trees.
--
--   NOTE:
--    + SVG uses "upside-down" y-axis;
--    + only uses the connectivity information of the 'LocalForest';
--
calcTreeOrigins :: SvgProps -> LocalExport 2 -> (V2 Int, [V2 Int])
calcTreeOrigins ps lf = (2*^off ^+^ fmap round hv, xy') where
  (hv, xy) = treeCoords scl $ lf^.embedded
  (off, o) = (V2 o o, ps^.imageBorder)
  scl = fromIntegral $ ps^.treeTileSize
  xy' = (off ^+^) . fmap round <$> [V2 x (hv^._y-y-scl) | V2 x y <- xy]

-- ** Macro-mesh helper functions
------------------------------------------------------------------------------
-- | Parse the embedding info to build the geometry and topology of the mesh.
treeCoords :: Double -> MacroMesh 2 -> (V2 Double, [V2 Double])
treeCoords scl mm =
  let (ab, dx, ox) = (aabb mm, scl*^ab^.delta^._xy, scl*^ab^.lower^._xy)
      js  = [0..Vec.length cs-1]
      Vertices vs cs = fromJust $ mm^.embedding
      vs' = fmap fromIntegral `Vec.map` vs
      xys = [scl*^xy^-^ox | j <- js, xy <- [vs'!int (cs!j^._x)]]
  in  (dx, xys)


-- * "General purpose" functions
------------------------------------------------------------------------------
quadToElement :: SvgProps -> V2 Int -> SvgQuad -> Element
quadToElement ps oo (SvgQuad xy dx ty co) = foldl (<>) qud $ go <$> ty
  where
    go t = case t of
      QuadGhost    -> gst
      QuadCoarse   -> crs
      QuadMirror _ -> mrr
    (pos, siz) = (xy^._xy, V2 dx dx)
    -- normal:
    qud = drawRect ps oo pos siz co
    -- ghosts:
    gst = drawRect ps oo pos siz $ ps^.ghostsColour -- Ltree.shade 4 -- "grey"
    (dx4, dx2) = (dx >>% 2, dx >>% 1)
    (xy0, xy1) = (pos ^+^ V2 dx2 dx4, pos ^+^ V2 dx4 dx2)
    -- coarse:
    crs = drawLine ps oo xy0 (xy0 ^+^ V2 0 dx2) <>
          drawLine ps oo xy1 (xy1 ^+^ V2 dx2 0)
    -- mirrors:
    lw  = max 2 $ 20 - xy^._z<<%2
    lw2 = fromIntegral $ lw >>% 1 + 1
    ps' = strokeWidth .~ lw $ strokeColour .~ ps^.borderColour $ ps
    mrr = drawRect ps' oo (pos ^+^ lw2) (siz ^-^ 2*^lw2) co

------------------------------------------------------------------------------
-- | Compute the properties and then draw the given node, using the selected
--   colour.
drawNode :: SvgProps -> V2 Int -> Node 2 -> Text -> Element
drawNode ps oo (Node (V2 x y) l _) clr =
  let qwid = ps^.quadTileSize
      bwid = ps^.borderWidth
      -- move node to better align with tree borders:
      f u | u <=    0 = -bwid
          | u >= qwid = bwid
          | otherwise = 0
      off = f (pos^._x) `V2 ` f (pos^._y)
      scl = NodeMaxLevel - 10 :: Int
      pos = (>>% scl) <$> V2 x y
      rad = max 8 $ ps^.maxNodeRadius - 2*fromIntegral l
      dot = drawCirc ps oo (pos^+^off) (max 4 (rad-6)) clr
  in  drawCirc ps oo (pos^+^off) rad (ps^.sharedColour) <> dot

-- ** Tree-export helpers
------------------------------------------------------------------------------
-- | Draw the boundaries of a tree.
--
--   NOTE:
--    + 'SvgProps' supplies the box width/height information;
--
drawTreeBorder :: SvgProps -> V2 Int -> Element
drawTreeBorder ps oo = bg where
  bg = rect_ [ X_ <<- txt (oo^._x), Y_ <<- txt (oo^._y)
             , Width_ <<- txt qwid, Height_ <<- txt qwid
             , Stroke_ <<- "black", Stroke_width_ <<- txt swid
             , Fill_ <<- "none" ]
  swid = ps^.nodeBorder
  qwid = ps^.treeTileSize

-- ** SVG helpers
------------------------------------------------------------------------------
-- | Draws a line within a quad/tile.
--
--   Inputs:
--    oo    - origin of the tile?
--    x0,y0 - coordinate of line start point;
--    x1,y1 - coordinate of line end point;
--
drawLine :: SvgProps -> V2 Int -> V2 Int -> V2 Int -> Element
drawLine ps oo (V2 x0 y0) (V2 x1 y1) =
  line_ [ X1_ <<- txt (dw+x0), Y1_ <<- txt (dh+qwid-y0)
        , X2_ <<- txt (dw+x1), Y2_ <<- txt (dh+qwid-y1)
        , Stroke_ <<- strk, Stroke_width_ <<- txt swid ]
  where
    strk = ps^.strokeColour
    swid = ps^.strokeWidth
    qwid = ps^.quadTileSize
    V2 dw dh = (oo ^+^) . fromIntegral $ (ps^.treeTileSize - qwid) >>% 1

drawRect :: SvgProps -> V2 Int -> V2 Int -> V2 Int -> Text -> Element
drawRect ps (V2 ox oy) (V2 x y) (V2 dx dy) co =
  rect_ [ X_ <<- txt (ox+dw+x+bwid), Y_ <<- txt (oy+dh+qwid-y-dy+bwid)
        , Width_ <<- txt (dx-2*bwid), Height_ <<- txt (dy-2*bwid)
        , Stroke_ <<- strk, Stroke_width_ <<- txt swid, Fill_ <<- co ]
  where
    strk = ps^.strokeColour
    swid = ps^.strokeWidth
    qwid = ps^.quadTileSize
    bwid = ps^.quadBorder
    V2 dw dh = fromIntegral $ (ps^.treeTileSize - qwid) >>% 1

------------------------------------------------------------------------------
-- | Draws a circle within a quad/tile.
--
--   Inputs:
--    oo  - origin of the tile?
--    x,y - coordinate of line start point;
--    r   - radius;
--    clr - colour used to fill the circle;
--
--   NOTE:
--    + stroke colour is set by 'SvgProps', as are some other things;
--
drawCirc :: SvgProps -> V2 Int -> V2 Int -> Int -> Text -> Element
drawCirc ps oo (V2 x y) r clr =
  circle_ [ Cx_ <<- txt (dw+x), Cy_ <<- txt (dh+qwid-y), R_ <<- txt r
          , Stroke_ <<- strk, Stroke_width_ <<- txt swid, Fill_ <<- clr]
  where
    strk = ps^.strokeColour
    swid = ps^.strokeWidth
    qwid = ps^.quadTileSize
    V2 dw dh = (oo ^+^) . fromIntegral $ (ps^.treeTileSize - qwid) >>% 1


-- * Local-forest functions
------------------------------------------------------------------------------
-- | Construct the local-forest information (in an easy-to-use Haskell
--   representation).
mkLocalExport :: LocalForest 2 -> LocalExport 2
mkLocalExport lf@(LocalForest em fs _ gs ms ns) =
  let go cf k | isCoarse lf k = k~>cf
              | otherwise     = cf
      cs = foldl' go bare $ flat fs
  in  LocalExport fs gs ms cs ns em

-- TODO: not very fast?
isCoarse :: LocalForest 2 -> Fkey 2 -> Bool
isCoarse lf =
  let fn = \l -> halfFaceNbrs mm l `concatMap` [minBound..maxBound]
      {-# INLINE fn #-}
      mm = lf^.macroStruct
  in  lpath >>> fn &&& halfDiagNbrs mm >>> uncurry (++) >>> any ((<?lf) . reify)
{-# INLINE isCoarse #-}


-- * Testing & examples
------------------------------------------------------------------------------
testExport :: IO ()
testExport  = do
  let q = Quad (V2 1048576 1048576) 5 (-1) (-1) 0 0
  printf "Quad: %s\n" $ show q
  printf "Coord: %s\n" $ show $ quadToCoord q
