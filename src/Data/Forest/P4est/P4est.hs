{-# LANGUAGE ConstraintKinds, DataKinds, DeriveGeneric, FlexibleContexts,
             FlexibleInstances, GeneralizedNewtypeDeriving, KindSignatures,
             ScopedTypeVariables, StandaloneDeriving, TemplateHaskell,
             TupleSections #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Forest.P4est.P4est
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Functions for working with `p4est_t` @P4est@ objects.
--
-- TODO:
--
------------------------------------------------------------------------------

module Data.Forest.P4est.P4est
  ( module Data.Forest.Mesh
  , module Data.Forest.P4est.Base
  , module Data.Forest.P4est.Connectivity
  , module Data.Forest.P4est.Ghost
  , module Data.Forest.P4est.Nodes
  , HasP4estT (..)
  , WithP4est
  , P4estRaw (..)
  , fFirstLocalTree
  , fLastLocalTree
  , fLocalNumQuads
  , fGlobalNumQuads
  , fGlobalFirstQuad
  , fGlobalFirstPos
  , fConnectivity
  , fTrees
  , P4estState (..)
  , P4estM
  , _unP4estM
  , getConnectivityT
  -- constructors & modifiers:
  , fromP4estT
  , runP4estM
  , getP4raw
  , addGhost
  , delGhost
  , addNodes
  , delNodes
  , getNodes
  , withP4estM
  --   connectivities stuff:
  , withSquare
  , unitSquare
  , meshLShape
  , meshDestroy
  -- shape-modification interface:
  , refineWith
  , refineWithIO
  , coarsenBy
  , coarsenByIO
  , partition
  , balance
  --   helpers:
  , withClean
  , balanceFull
  , balanceFace
  -- queries:
  , quadExists
  , quadFindOwner
  , findOverlappedRanks
  , findOverlappedRanksUnsafe
  , quadIsContained
  , quadPropsInfo
  , quadIsCoarse
  , quadFaceNeighbourExtra
  , quadFaceOnBoundary
  , treeFaceOnBoundary
  -- nodes-sharing:
  , shareNodes
  , shareSharers
  , buildShareds
  -- conversions
  , n2quad
  , q2node
  -- import/export:
  , saveP4estRaw
  , loadP4estRaw
  , dumpP4estT
  ) where

import           Control.DeepSeq
import           Control.Distributed.MPI           as MPI
import           Control.Distributed.MPI.Handy     as MPI
import           Control.Lens                      (Lens', lens, makeLenses,
                                                    (.~), (^.))
import           Control.Monad.IO.Class
import           Control.Monad.Reader
import           Control.Monad.State
import           Data.Aeson
import           Data.Bits.Handy                   (i32, int, testBit)
import           Data.Bool                         (bool)
import qualified Data.ByteString.Lazy              as Lazy
import           Data.IntSet.Sets
import           Data.Maybe                        (fromJust, fromMaybe, isJust)
import qualified Data.Vector.Algorithms.Search     as Vec
import           Data.Vector.Storable              (Vector, (!))
import qualified Data.Vector.Storable              as Vec
import qualified Data.Vector.Storable.Mutable      as Mut
import           Foreign.C.Types
import           Foreign.Marshal.Utils
import           Foreign.Ptr
import           Foreign.Storable
import           GHC.Generics                      (Generic)
import           Linear                            (V2 (..), V4 (..), _w, _x,
                                                    _xy, _y)
import           Text.Printf
import           Unsafe.Coerce

import           Bindings.P4est.P4est
import           Bindings.P4est.P4estBits
import           Bindings.P4est.P4estCommunication
import           Bindings.P4est.P4estGhost
import           Data.Forest.Mesh
import           Data.Forest.P4est.Base
import           Data.Forest.P4est.Connectivity
import           Data.Forest.P4est.Ghost
import           Data.Forest.P4est.Nodes
import           Data.Scientific.SC.Array          as SC


-- * Type-class for a context with 'p4est' forest
------------------------------------------------------------------------------
class HasP4estT m where
  askP4estT :: m P4estT


-- * Data types
------------------------------------------------------------------------------
-- | Constraint for contexts that have a 'p4est' object.
type WithP4est m = (Monad m, MonadIO m, HasP4estT m)

-- ** Forest data-types
------------------------------------------------------------------------------
-- | Closely related to the `p4est_t` data type.
--   TODO:
data P4estRaw
  = P4estRaw
      { _fFirstLocalTree  :: CInt
      , _fLastLocalTree   :: CInt
      , _fLocalNumQuads   :: CInt
      , _fGlobalNumQuads  :: Int
      , _fGlobalFirstQuad :: Vector Int
      , _fGlobalFirstPos  :: Vector Quad
      , _fConnectivity    :: Conn2D
      , _fTrees           :: Vector Tree
      }
  deriving (Eq, Generic)

-- ** Forest-state monad data-types
------------------------------------------------------------------------------
-- | Monad for working within parallel-forest environments.
newtype P4estM a =
  P4estM { unP4estM :: StateT P4estState SCM a }
  deriving ( Functor, Applicative, Monad, MonadIO, MonadState P4estState
           , MonadReader Comm, MonadFail)

-- | Forest-state monad.
data P4estState
  = P4estState
      { _p4est :: !P4estT
      , _p4raw :: !(Maybe P4estRaw)
      , _ghost :: !(Maybe GhostT)
      , _nodes :: !(Maybe NodesT)
      }
  deriving (Eq, Show, Generic)


-- * Instances
------------------------------------------------------------------------------
instance Show P4estRaw where
  show (P4estRaw fl ll nq gq fq fp _ _) = -- _ _ _) =
    let a = printf "Forest:\n"
        b = printf " * local tree indices:\n"
        c = printf "   + first: %9d\n" (fromIntegral fl :: Int)
        d = printf "   + last:  %9d\n" (fromIntegral ll :: Int)
        e = printf " * number of quads:\n"
        f = printf "   + local:  %9d\n" (fromIntegral nq :: Int)
        g = printf "   + global: %9d\n" gq
        h = printf " * quads per processor:\n"
        i = printf "   + indices: %s\n" $ show fq
        j = printf "   + firsts:"
        k = concat $ ('\n':) . ('\t':) . show <$> Vec.toList fp
--         l = '\n':show ts
    in  a ++ b ++ c ++ d ++ e ++ f ++ g ++ h ++ i ++ j ++ k ++ "\n"

------------------------------------------------------------------------------
-- | Retrieve the 'p4est' object from the @P4estM@ context.
instance HasP4estT P4estM where
  askP4estT = fmap _p4est get

-- ** JSON import/export
------------------------------------------------------------------------------
instance ToJSON   P4estRaw
instance FromJSON P4estRaw

-- instance ToJSON   P4estState
-- instance FromJSON P4estState

-- ** Lenses
------------------------------------------------------------------------------
makeLenses ''P4estRaw
makeLenses ''P4estState


-- * High-level user-API functions
------------------------------------------------------------------------------
-- | Setup up a stateful environment that tracks the 'p4est' variables.
withP4estM :: forall a. NFData a => ConnectivityT -> P4estM a -> SCM a
withP4estM conn actn = do
  let newFstate :: ConnectivityT -> SCM P4estState
      newFstate con' = do
        p4st <- con' `deepseq` newP4est conn
        p4st `deepseq` pure (P4estState p4st Nothing Nothing Nothing)

      runFstate :: P4estM a -> P4estState -> SCM (a, P4estState)
      runFstate act' p4st = do
        (res, pst) <- runP4estM act' p4st
        res `deepseq` pure (res, pst)

      delFstate :: P4estState -> SCM ()
      delFstate p4st = do
        pure () `maybe` nodesDestroy $ p4st^.nodes
        pure () `maybe` ghostDestroy $ p4st^.ghost
        liftIO $ p4estDestroy $ p4st^.p4est

  newFstate conn >>= runFstate actn >>= \(x, s) -> do
    () <- delFstate s
    pure x

------------------------------------------------------------------------------
-- | Evaluation of a forest-monad action.
runP4estM :: P4estM a -> P4estState -> SCM (a, P4estState)
runP4estM  = runStateT . unP4estM
{-# INLINE runP4estM #-}

------------------------------------------------------------------------------
-- | Destroy/deallocate a macro-mesh.
meshDestroy :: MonadIO m => ConnectivityT -> m ()
meshDestroy  = connDestroy
{-# INLINE meshDestroy #-}

-- ** Macro-mesh constructors and actions
------------------------------------------------------------------------------
-- | Construct a unit-square macro-mesh.
unitSquare :: MonadIO m => m ConnectivityT
unitSquare  = connSquare
-- unitSquare  = liftIO $ fromConn2D embedSquare
{-# INLINE unitSquare #-}

meshLShape :: MonadIO m => m ConnectivityT
meshLShape  = liftIO $ fromConn2D embedLShape

-- | Evaluate the given parallel-forest action within a context that has a
--   unit square for its macro-mesh.
withSquare :: NFData a => P4estM a -> SCM a
withSquare k = unitSquare >>= \sq -> do
  x  <- withP4estM sq k
  () <- meshDestroy sq
  pure x

-- ** Accessors
------------------------------------------------------------------------------
getConnectivityT :: P4estM ConnectivityT
getConnectivityT  = do
  get >>= fmap ConnectivityT . liftIO . peek . p'p4est'connectivity . _unP4estT . _p4est

getP4raw :: P4estM P4estRaw
getP4raw  = get >>= \st -> case st^.p4raw of
  Nothing -> fromP4estT (st^.p4est) >>= \p4 -> do
    put st { _p4raw = Just p4 } >> pure p4
  Just p4 -> pure p4

getEmbedding :: P4estM Conn2D
getEmbedding  = _fConnectivity <$> getP4raw

getNodes :: P4estM (Maybe Nodes)
getNodes  = do
  l4st <- get
  case l4st^.nodes of
    Nothing -> pure Nothing
    Just np -> Just <$> fromNodesT np

-- ** Functions for working within a forest-environment
------------------------------------------------------------------------------
-- | Add a ghost-layer to each of the processor-local meshes/forests.
--
--   TODO: use `withClean` to cleanly remove stale state?
addGhost :: P4estConnectType -> P4estM ()
addGhost cnct = do
  fstm <- get
  when (isJust $ fstm^.ghost) $ do
    pure () `maybe` ghostDestroy $ fstm^.ghost
  ghst <- ghostNew (fstm^.p4est) cnct
  put $ ghost .~ Just ghst $ fstm

-- | Remove a ghosts-layer if it exists, and also removes any nodes-labelling
--   as well, as this depends on the ghost layer.
delGhost :: P4estM ()
delGhost  = do
  P4estState p4 r4 mg mn <- get
  maybe (pure ()) nodesDestroy mn
  maybe (pure ()) ghostDestroy mg
  put $ P4estState p4 r4 Nothing Nothing

------------------------------------------------------------------------------
-- | Add a (global) nodes-labelling to the mesh.
addNodes :: P4estM ()
addNodes  = do
  fstm <- get
  let ghst = fromMaybe (GhostT nullPtr) $ fstm^.ghost
  when (isJust $ fstm^.nodes) $ do
    pure () `maybe` nodesDestroy $ fstm^.nodes
  nods <- nodesNew (fstm^.p4est) ghst
  put $ nodes .~ Just nods $ fstm

-- | Removes any nodes-labelling if it exists.
delNodes :: P4estM ()
delNodes  = do
  P4estState p4 r4 mg mn <- get
  maybe (pure ()) nodesDestroy mn
  put $ P4estState p4 r4 mg Nothing

-- ** Shape-modification and partitioning
------------------------------------------------------------------------------
-- | Enforce 2:1 cell-size constraints for adjacent cells. If the
--   @P4estConnectType@ is @ConnectFull@, then this condition must hold
--   across corners, and not just faces.
balance :: P4estConnectType -> P4estM ()
balance cont = withClean $ \p4st -> do
  p4estBalance p4st cont InitNil

-- | Perform 2:1 balancing across the entire mesh; and enforcing the 2:1
--   constraint across faces and corners.
balanceFull :: P4estM ()
balanceFull  = balance ConnectFull
{-# INLINE balanceFull #-}

balanceFace :: P4estM ()
balanceFace  = balance ConnectFace
{-# INLINE balanceFace #-}

------------------------------------------------------------------------------
-- | Equidistribute the number of leaf-cells across each MPI process.
partition :: P4estM ()
partition  = withClean $ \p4st -> do
  p4estPartition p4st False WeightNil

------------------------------------------------------------------------------
-- | Refine the mesh using the given predicate.
--
--   Inputs:
--    1. `fun` -- refinement predicate, for each leaf-node; and
--    2. `rcs` -- if @True@, then recursively refine the mesh, else stop the
--                refinement process after each leaf-cell has been visited
--                once.
--
--   TODO: check.
--
refineWith :: (Quad -> Bool) -> Bool -> P4estM ()
refineWith fun rcs = withClean $ \p4st -> do
  let go _ t q = fromQuadrantT q >>= pure . fun . (qTree .~ fromIntegral t)
  rptr <- mkRefineFn go
  p4estRefine p4st rcs rptr InitNil

-- | Refine the mesh using the given predicate, and evaluate the predicate
--   within the IO monad.
refineWithIO :: (Quad -> IO Bool) -> Bool -> P4estM ()
refineWithIO act rcs = withClean $ \p4st -> do
  let go _ _ q = fromQuadrantT q >>= act
  rptr <- mkRefineFn go
  p4estRefine p4st rcs rptr InitNil

------------------------------------------------------------------------------
-- | Coarsen the mesh by applying the given predicate to each cell.
--
--   TODO: input should be an array of four quads?
coarsenBy :: (V4 Quad -> Bool) -> Bool -> P4estM ()
coarsenBy fun rcs = withClean $ \p4st -> do
  let go _ _ p = peek (castPtr p) >>= \q -> mapM fromQuadrantT q >>= pure . fun
  coarsenBy' p4st rcs go

-- | Coarsen the mesh by applying the given predicate to each cell, and
--   evaluate the predicate within the IO monad.
coarsenByIO :: (V4 Quad -> IO Bool) -> Bool -> P4estM ()
coarsenByIO act rcs = withClean $ \p4st -> do
  let go _ _ p = peek (castPtr p) >>= \q -> mapM fromQuadrantT q >>= act
  coarsenBy' p4st rcs go

coarsenBy' :: MonadIO m => P4estT -> Bool -> CoarsenFnT -> m ()
coarsenBy' p4st rcsv rfun = do
  rptr <- mkCoarsenFn rfun
  p4estCoarsen p4st rcsv rptr InitNil
{-# INLINE coarsenBy' #-}


-- * Queries
------------------------------------------------------------------------------
-- | Checks if quadrant exists in the local forest or the ghost layer.
--   NOTE: requires that the given quads have accurate 'which_tree' values.
quadExists :: Quad -> P4estM Bool
quadExists quad = do
  fstm <- get
  let p = fstm^.p4est
      g = fromMaybe (GhostT nullPtr) $ fstm^.ghost
      t = fromIntegral $ quad^.qTree
  liftIO $ withQuadrantT quad $ \q -> quadrantExists p g t q
{-# INLINABLE quadExists #-}

{-- }
-- TODO
-- | Checks if quadrant exists across a face, of a quad within the local
--   forest or the ghost layer.
faceQuadExists :: Quad -> P4estM Bool
faceQuadExists quad = do
  fstm <- get
  let p = fstm^.p4est
      g = fromMaybe (GhostT nullPtr) $ fstm^.ghost
      t = fromIntegral $ quad^.qTree
  liftIO $ withQuadrantT quad $ \q -> faceQuadrantExists p g t q
--}

------------------------------------------------------------------------------
-- | Find the quad that is across the given face.
--
--   NOTE: this function is capable of crossing tree-boundaries.
--   NOTE: uses the tree-id from the quad, and returns the tree-id via the
--     quad as well.
quadFaceNeighbourExtra :: Quad -> Dir 2 -> P4estM (Maybe (Dir 2, Quad))
quadFaceNeighbourExtra q d = do
  co <- get >>= liftIO . peek . p'p4est'connectivity . _unP4estT . _p4est
  liftIO $ with q $ \qp -> do
    rp <- new q
    let q' = castPtr qp
        t' = fromIntegral $ q^.qTree
        r' = castPtr rp
        d' = fromDir d
    with d' $ \dp -> do
      to <- c'p4est_quadrant_face_neighbor_extra q' t' d' r' dp co
      di <- toDir <$> peek dp
      ro <- peek rp >>= \r -> do
        pure $ if to < 0
          then Nothing
          else Just (di, qTree .~ fromIntegral to $ r)
      pure ro
{-# INLINABLE quadFaceNeighbourExtra #-}

------------------------------------------------------------------------------
-- | Checks if a quadrant's face is on the boundary of the forest.
--   NOTE: assumes that the quad's tree-id is correct.
quadFaceOnBoundary :: Quad -> Dir 2 -> P4estM Bool
quadFaceOnBoundary q d = do
  let di = fromDir d :: Int
      xy | testBit di 1 = q^.qLocation^._x
         | otherwise    = q^.qLocation^._y
      dw | testBit di 0 = lastCoordOffset $ q^.qLevel -- :: Int32
         | otherwise    = 0
  (&& dw==xy) <$> fromEnum (treeId q) `treeFaceOnBoundary` d

-- | Checks if the tree face (in the given direction, 'd') is on the boundary
--   of the forest.
treeFaceOnBoundary :: Int -> Dir 2 -> P4estM Bool
treeFaceOnBoundary t d = do
  em <- getEmbedding
  let tt = dirElem d . (!t) $ _treeToTree em
      tf = dirElem d . (!t) $ _treeToFace em
  return $ fromIntegral t==tt || fromDir d==tf
{-# INLINE treeFaceOnBoundary #-}

-- ** Quadrant-owner queries
------------------------------------------------------------------------------
-- | Search the forest for the given quad, and if found then return the MPI
--   rank of the processor.
--   NOTE: requires that the given quads have accurate 'which_tree' values.
quadFindOwner :: Quad -> Maybe (Dir 2) -> P4estM (Maybe Rank)
quadFindOwner quad mdir = do
  let d = maybe (-1) fromDir mdir
      t = fromIntegral $ quad^.qTree
  P4estT p <- fmap _p4est get
  rank <- liftIO $ withQuadrantT quad $ \(QuadrantT q) -> do
    fmap fromIntegral $ c'p4est_quadrant_find_owner p t d q
  return $ if rank < 0 then Nothing else Just rank
{-# INLINE quadFindOwner #-}

------------------------------------------------------------------------------
-- | Find the ranks of any local-regions of the forest that intersect with the
--   given quad.
--
--   NOTE: returns the lower- and upper- (MPI) ranks that are covered, and
--     where the upper-value is up to, but not including this rank.
findOverlappedRanks :: Quad -> P4estM (Int, Int)
findOverlappedRanks q
  | isInside q = findOverlappedRanksUnsafe q
  | otherwise  = error "P4est.findOverlappedRanks: outside unit tree"

-- | Find the ranks of any local-regions of the forest that intersect with the
--   given quad.
findOverlappedRanksUnsafe :: Quad -> P4estM (Int, Int)
findOverlappedRanksUnsafe q = do
  fmap _fGlobalFirstPos getP4raw >>= \gq -> do
    let q' = lastQuad q
    (l, u) <- liftIO $ do
      ar <- Vec.thaw gq
      (,) <$> Vec.binarySearch ar q <*> Vec.binarySearch ar q'
    let l' = bool l (l-1) (gq!l > q')
        u' = bool u (u-1) (gq!u > q' && u>l')
    pure (l', u')
{-# INLINE findOverlappedRanksUnsafe #-}

------------------------------------------------------------------------------
-- | Test whether a quadrant is fully contained within a rank's owned region.
--
--   NOTE: requires that the tree-id is stored in the quad, and is accurate.
--   NOTE: only found in a later versions of 'p4est'.
quadIsContained :: Quad -> Int -> P4estM Bool
quadIsContained quad rank = do
  P4estT p <- _p4est <$> get
  liftIO $ with quad $ \q' -> do
    let q = castPtr q'
        t = fromIntegral $ quad^.qTree
    (/=0) <$> c'p4est_comm_is_contained p t q (fromIntegral rank)
{-# INLINE quadIsContained #-}

{-- }
-- TODO:
------------------------------------------------------------------------------
-- | Find the quad that is across the given corner, and this function is
--   capable of crossing tree-boundaries.
--
--   NOTE: uses the tree-id from the quad, and returns the tree-id via the
--     quad as well.
quadCornerNeighbourExtra :: Quad -> Dir 2 -> P4estM (Maybe (Dir, Quad))
quadCornerNeighbourExtra q d = do
  co <- get >>= liftIO . peek . p'p4est'connectivity . _unP4estT . _p4est
  liftIO $ with q $ \qp -> do
    rp <- new q
    let q' = castPtr qp
        t' = fromIntegral $ q^.qTree
        r' = castPtr rp
        d' = fromDir d
    with d' $ \dp -> do
      to <- c'p4est_quadrant_corner_neighbor_extra q' t' d' r' dp co
      di <- toDir <$> peek dp
      ro <- peek rp >>= \r -> do
        pure $ if to < 0
          then Nothing
          else Just (di, qTree .~ fromIntegral to $ r)
      pure ro
{-# INLINABLE quadCornerNeighbourExtra #-}
--}

-- ** RUB-spline mesh queries
------------------------------------------------------------------------------
-- | Check the neighbouring quads to see if any of them are finer than the
--   given quad.
quadIsCoarse :: Quad -> P4estM Bool
quadIsCoarse q
  | treeMaxLevel<l1 = pure False
  | otherwise       = do
      let es = quadFaceNbrsHalf q        <$> faceDirs :: V4 (V2 Quad)
          cs = cornerNbrHalf q . fromDir <$> faceDirs :: V4 Quad
      fs <- mapM (fmap or . mapM quadExists) es
      ds <- mapM quadExists cs
      pure $ or fs || or ds
  where l1 = q^.qLevel + 1

-- | Check for half-sized neighbours across faces and corners.
quadPropsInfo :: Quad -> P4estM (Maybe CoarseInfo, Maybe InterpInfo)
quadPropsInfo q = do
  let xs = sequenceA $ quadFaceNbrsAll q <$> faceDirs :: V4 (V4 Quad)
      (bs, es) = (xs^._w, sequenceA $ xs^._xy)
      cs = cornerNbrHalf q . fromDir <$> faceDirs :: V4 Quad
      ns = cornerNbrTwice q . fromDir <$> faceDirs :: V4 (Maybe Quad)
  fs <- mapM (fmap or . mapM quadExists) es
  ds <- mapM quadExists cs
  ci <- pure $ if or fs || or ds
    then Just $ CoarseInfo fs ds
    else Nothing
  hs <- mapM quadExists bs
  nx <- maybe (pure False) quadExists `mapM` ns
  fi <- pure $ Nothing `bool` Just (InterpInfo hs nx) $ or hs || or nx
  pure (ci, fi)


-- * Nodes-sharing functions
------------------------------------------------------------------------------
-- | Share the locally-owned independent nodes with other MPI processes, and
--   using the given node-sharing information. This function also receives
--   nodes from other MPI processes, and returns these.
--   Inputs:
--   + sets of node-indices for nodes to be shared with other MPI processes;
--   Output:
--   + map from ranks to arrays of received nodes.
shareNodes :: Sharers -> P4estM Shareds
shareNodes ss = shareSharedCounts ss >>= shareSharers ss

------------------------------------------------------------------------------
-- | Using MPI, @isend@/@irecv@ all shared nodes needed by this process, from
--   other ranks, and vice versa.
--   Inputs:
--    + sets of node-indices for nodes to be shared with other MPI processes;
--    + array containing the number of nodes to receive from other processes;
--   Output:
--    + map from ranks to an array of received nodes.
--
--   NOTE: OBSOLETE -- replaced by 'shareNodesAndUpdate' from 'Local'.
shareSharers :: Sharers -> Vector CInt -> P4estM Shareds
shareSharers ls nums = do
  -- construct the lists of @Indep@s:
  shaz <- buildShareds ls
  comm <- ask
  shareShareds comm MpiTagNodesShared shaz nums
{-# INLINE shareSharers #-}

-- | Computes the array of independent nodes to share, for each rank.
--
--   NOTE: OBSOLETE -- replaced by 'shareNodesAndUpdate' from 'Local'.
buildShareds :: Sharers -> P4estM Shareds
buildShareds ls = do
  p4st <- get
  locs <- _nodesIndep <$> case p4st^.nodes of
    Nothing -> addNodes >> fromJust . _nodes <$> get >>= fromNodesT
    Just ns -> fromNodesT ns
  let go = Vec.fromList . map (locs!) . flat
      {-# INLINE go #-}
  pure $ go <$> invert (rankSet <$> ls)


-- * Mid-level User API functions
------------------------------------------------------------------------------
newP4est :: ConnectivityT -> SCM P4estT
newP4est conn = do
  comm <- ask
  liftIO $ p4estNew comm conn 0 InitNil UserNil

{-- }
-- OBSOLETE: ununsed?
delP4est :: MonadIO m => P4estT -> m ()
delP4est p4st = do
  p4estDestroy p4st
--}

------------------------------------------------------------------------------
-- | Run the given forest-modifying action, after first clearing any state
--   that would be invalidated by the subsequent changes to the forest.
withClean :: (P4estT -> P4estM a) -> P4estM a
withClean k = do
  P4estState p4 _ mg mn <- get
  maybe (pure ()) nodesDestroy mn
  maybe (pure ()) ghostDestroy mg
  put $ P4estState p4 Nothing Nothing Nothing
  k p4
{-# INLINE withClean #-}


-- * Low-level bindings.
------------------------------------------------------------------------------
p4estNew :: Comm -> ConnectivityT -> CSize -> InitFun -> UserPtr -> IO P4estT
p4estNew comm (ConnectivityT conn) sizt ifun idat = do
  iptr <- marshalInitFun ifun
  uptr <- case idat of
    UserNil -> return nullPtr
    _       -> error "p4estNew: user data unimplemented"
  -- since `mpi-hs-0.5.2.x`:
  P4estT <$> c'p4est_new comm conn sizt iptr uptr
{-- }
  with comm $ \cptr -> do
--   withForeignPtr comm $ \cptr -> do
    ptr' <- peek (castPtr cptr :: Ptr Word64)
    P4estT <$> c'p4est_new (unsafeCoerce ptr') conn sizt iptr uptr
--}

p4estDestroy :: P4estT -> IO ()
p4estDestroy (P4estT p4) = c'p4est_destroy p4

------------------------------------------------------------------------------
-- TODO:
marshalInitFun :: InitFun -> IO C'p4est_init_t
marshalInitFun InitNil = return nullFunPtr
marshalInitFun _       = error "marshalInitFun: not a 'Nil' init fun"

------------------------------------------------------------------------------
-- | Balance and (optionally) using the given leaf-node initialisation
--   function.
p4estBalance :: MonadIO m => P4estT -> P4estConnectType -> InitFun -> m ()
p4estBalance (P4estT p4) ct ifun = liftIO $ do
  iptr <- marshalInitFun ifun
  c'p4est_balance p4 ct iptr

------------------------------------------------------------------------------
p4estPartition :: MonadIO m => P4estT -> Bool -> WeightFun -> m ()
p4estPartition (P4estT p4) a4c wfun = liftIO $ do
  wptr <- marshalWeightFun wfun
  c'p4est_partition p4 (fromBool a4c) wptr

marshalWeightFun :: WeightFun -> IO C'p4est_weight_t
marshalWeightFun WeightNil = return nullFunPtr
marshalWeightFun _         = error "marshalWeightFun: not a 'Nil' weight fun"

-- ** Modifiers & refinement.
------------------------------------------------------------------------------
p4estRefine :: MonadIO m => P4estT -> Bool -> RefineFn -> InitFun -> m ()
p4estRefine (P4estT p4) rcsv (RefineFn refn) infn = liftIO $ do
  iptr <- marshalInitFun infn
  c'p4est_refine p4 (pbool rcsv) refn iptr

mkRefineFn :: MonadIO m => RefineFnT -> m RefineFn
mkRefineFn refn = liftIO $ do
  let go p i q = fromBool <$> refn (P4estT p) i (QuadrantT q)
  RefineFn <$> mk'p4est_refine_t go

------------------------------------------------------------------------------
p4estCoarsen :: MonadIO m => P4estT -> Bool -> CoarsenFn -> InitFun -> m ()
p4estCoarsen (P4estT p4) rcsv (CoarsenFn cofn) infn = liftIO $ do
  iptr <- marshalInitFun infn
  c'p4est_coarsen p4 (pbool rcsv) cofn iptr

mkCoarsenFn :: MonadIO m => CoarsenFnT -> m CoarsenFn
mkCoarsenFn cofn = liftIO $ do
  let go p i q = fromBool <$> cofn (P4estT p) (fromIntegral i) (unsafeCoerce q)
  CoarsenFn <$> mk'p4est_coarsen_t go


-- * Conversions
------------------------------------------------------------------------------
fromP4estT :: forall m. MonadIO m => P4estT -> m P4estRaw
fromP4estT (P4estT p) = liftIO $ do
  mn <- fmap fromIntegral . peek $ p'p4est'mpisize p
--   mr <- fmap fromIntegral . peek $ p'p4est'mpirank p
  nq <- peek $ p'p4est'local_num_quadrants p
  let mn' = fromIntegral (mn :: Int) + 1
  fl <- peek $ p'p4est'first_local_tree p
  ll <- peek $ p'p4est'last_local_tree p
  ng <- fmap fromIntegral . peek $ p'p4est'global_num_quadrants p
  fq <- Mut.new mn' >>= \ar -> Mut.unsafeWith ar $ \q -> do
      s <- castPtr <$> peek (p'p4est'global_first_quadrant p)
      copyBytes q s (mn'*sizeOf (undefined :: GInt))
      Vec.unsafeFreeze ar
  fp <- Mut.new mn' >>= \ar -> Mut.unsafeWith ar $ \q -> do
      s <- castPtr <$> peek (p'p4est'global_first_position p)
      copyBytes q s (mn'*sizeOf (undefined :: Quad))
      Vec.unsafeFreeze ar
  cp <- fmap ConnectivityT $ peek (p'p4est'connectivity p)
  em <- fromConnectivityT cp
  -- read the trees (stored as an array of ScArray's):
  ts <- peek (p'p4est'trees p) >>= \q -> do
    toVector (ScArray q :: ScArray Tree)
  return $ P4estRaw fl ll nq ng fq fp em ts

-- ** JSON save/load
------------------------------------------------------------------------------
saveP4estRaw :: MonadIO m => FilePath -> P4estRaw -> m ()
saveP4estRaw fp = liftIO . Lazy.writeFile fp . encode

loadP4estRaw :: MonadIO m => FilePath -> m (Maybe P4estRaw)
loadP4estRaw  = liftIO . fmap decode . Lazy.readFile

-- ** Conversion helpers
------------------------------------------------------------------------------
-- | Convert the given node to the data type suitable for being used with
--   'p4est' "node" functions.
--
--   NOTE: discards the node-level information, as 'p4est' uses 'NodeMaxLevel'
--     to indicate that a quadrant is to be treated as a node.
n2quad :: Node 2 -> Quad
n2quad (Node xy _ t) = Quad (i32 <$> xy) NodeMaxLevel (-1) (-1) (i32 t) (-1)
{-# INLINE n2quad #-}

q2node :: Quad -> Node 2
q2node (Quad xy l _ _ t _) = Node (int <$> xy) (int l) (int t)
{-# INLINE q2node #-}


-- * Accessors
------------------------------------------------------------------------------
-- | Lens/accessor for the value of the internal representation for a @P4estM@
--   monadic action.
_unP4estM :: Lens' (P4estM a) (StateT P4estState SCM a)
_unP4estM  = lens unP4estM (\r s -> r { unP4estM = s })
{-# INLINE _unP4estM #-}


-- * Miscellaneous functions
------------------------------------------------------------------------------
dumpP4estT :: MonadIO m => P4estT -> m ()
dumpP4estT (P4estT pf) = liftIO $ do
--   mc <- pure SC.commWorld
  mr <- fmap fromIntegral . peek $ p'p4est'mpirank pf
  ms <- fmap fromIntegral . peek $ p'p4est'mpisize pf
  printf "MPI:{\"rank\":%d, \"size\":%d}\n" (mr :: Int) (ms :: Int)
  gq <- fmap fromIntegral . peek $ p'p4est'global_num_quadrants pf
  lq <- fmap fromIntegral . peek $ p'p4est'local_num_quadrants pf
  printf "Quads:{\"global\":%d, \"local\":%d}\n" (gq :: Int) (lq :: Int)
  ft <- fmap fromIntegral . peek $ p'p4est'first_local_tree pf
  lt <- fmap fromIntegral . peek $ p'p4est'last_local_tree pf
  printf "Local trees:{\"first\":%d, \"last\":%d}\n" (ft :: Int) (lt :: Int)
