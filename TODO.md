---
title: "`pforest` *Tasks to Complete*"
author: Patrick Suggate <<patrick.suggate@gmail.com>>
date: 17 March, 2019
geometry: margin=2cm
biblio-title: Bibliography
colorlinks: true
toc: true
toc-depth: 2
toccolor: purple
fontsize: 11pt
---

\clearpage

# Introduction #

Build mid-level Haskell wrapper/bindings to `p4est`, while preserving its strong parallel-scaling performance.

## Bugs/Issues ## {#sec:bugs}

#. `refineWith :: (Quad -> Bool) -> Bool -> P4estM ()`{.haskell} (and `refineWithIO`) pass a `Quad`{.haskell} value to the refinement-predicate that does not contain the correct value for its owning tree? Currently, it seems to always be set to zero?

#. Often very difficult to find the desired function(s), because *Haddock* documentation does not exist, and the library is a mess.

## Goals ##

#. Use `p4est` to provide:

   #. support for many topologies;
   #. the framework for parallel execution; and
   #. AMR.

#. Additional Haskell code/modules for:

   #. import/export of meshes that are compatible with *'connectivities'*;
   #. ~~generating data and interfacing with 'PETSc' for solving systems of equations?~~ *Can not support PETSc `Mat` objects, as determining their sparsity-patterns would require basis-functions or NIFs*
   #. ~~parallel runtimes for interfacing with other languages, and providing parallel execution?~~ **NO**
   #. data structures and functions for working with local forests, as these will form the mesh structures for the various (field-, basis-, geometry-) "layers" in higher-level libraries?
   #. more convenient queries than those provided by 'p4est';

#. Documentation:

   #. using some of the info and text from these design nodes;
   #. make the algorithm descriptions rigourous, if they are meant for the thesis;
   #. check that the code comments/documentation is accurate;
   #. add user-guides and implementation descriptions to the code repositories;
   #. provide citations to the appropriate sources from the literature;
   #. overview of the low-level stuff (mostly in Base and Types)?
   #. comments that can be used to build build Haddock docs;

## Plan ##

Broadly, the goal is to write Haskell bindings for `p4est` so that I can do *"L"*-shape FEM computations using RUB-splines. Another advantage is that performance will improve (vs `bspline-adaptive`), due to the parallel-forests (and algorithms) used by `p4est`.

Carrying out the first part of this plan involves:

#. [Determining the suitability of `p4est`](#sec:p4est) ~~(and PETSc)~~.

#. [Bindings and wrapper for `p4est`](#sec:wrapper) -- low-level bindings are basically done, but a better API is needed for working with `p4est`.

## Stages ##

**TODO:**

#. Nicer mid-level interface.

#. Testbenches, examples, and documentation that is suitable for supervisors.

**DONE:**

#. Haskell bindings to `p4est` and SVG export for forests.

\clearpage

# Task Lists by Category # {#sec:task-lists}

Improve:

#. High-level API.

#. Mid-level API.

#. Support for 3D parallel forests.

#. Improve the code quality.

#. Finish the documentation.

## Bugs and `TODO` ##

#. How `maxDepth` works is **inconsistent**, as sometimes it's inclusive (e.g., with `Ltree d`{.haskell} code), and sometimes exclusive (with `p4est`-based code)?

#. The macro-mesh meeds vertices for `SVG`-export? Are vertices needed for `VTK`-export?

#. Filter the nodes/ranks in `Sharers`{.haskell} depending on whether they have been shared before, and this new functionality probably belongs in `module Data.Forest.Local`{.haskell}.

## Tasks from `pforest.md` ##

#. Keep these tasks in sync. with those from `TODO.md`.

#. Design an API suitable for interfacing with a RUB-spline meshing package, including:
   #. abstractions to make it easier to generalise mesh backends (for 2D and 3D meshes);
   #. mesh-navigation, queries, etc.;
   #. how much direct support for boundaries and boundary-conditions does this library require?

#. Mesh import/export and via common formats:
   #. `Exodus II` export?
   #. `VTK`;
   #. `SVG`; and
   #. `JSON`.

#. Documentation tasks:
   #. annotate source for `haddock` documentation, and set up the `Makefile`'s;
   #. MPI example, using `mpi-hs`, `Tag`{.haskell}'s, etc.;
   #. face-transforms and mesh-navigation tutorial;
   #. `SC`{.haskell} documentation;
   #. section describing the `Mesh`{.haskell} and `Local`{.haskell} high-level, generic parallel-mesh data types and functionality;

#. Performance and regression tests.

## High-Level Tasks ##

#. what does the `Int`{.haskell} do/represent, for the type: \
   `, _cellOfNode :: IntMap [(Int, Leaf d)] -- ^ TODO: todo?`{.haskell} \
   which is a member of `data LocalNodes (d :: Nat)`{.haskell}?

## Mid-Level API Tasks ## {#sec:midlevel-tasks}

#. Code hygeine:

   #. sort out 2D/3D interfaces;

   #. remove the `(2 :: Nat)`{.haskell} parameters/annotations for the `P4est`{.haskell} data types, as these have dimensionality of 2 anyway;

   #. *clean up the dead code*, and especially regarding coordinate conversions;

#. Support user-supplied weighting function for the `partition`{.haskell} operations.

#. Usage examples.

#. Expose more functionality:

   #. macro-mesh subdivision?

   #. geometry maps?

   #. `lnodes` and *Lobatto points* stuff?

#. Simplify the exposed traversal functions:

   + flood-search traversals;

   + bread-first searches & traversals;

### General Tasks ###

#. Use `ForeignPtr`{.haskell}'s for some stuff, so that destructors are automatically triggered.

#. Examples that use the mid-level API to do some `p4est`-like stuff, and save/export the results.

#. Instances for the `class Relations t`{.haskell} methods, for the various tree-cell data types?

### Improving Traversal and Navigation ###

#. Fix up the `Dir`{.haskell} and `Orient`{.haskell} stuff, so that functions like:

   ```haskell
   findFaceNbr :: Quad -> Orient -> Dir -> P4estM 2 (Maybe (Quad, Orient))
   ```
   
   are less retarded.

#. Clean up the stuff in Section \ref{sec:arb-topo} for queries, coordinate-transformations, and mesh navigation.

### Mid-Level API Improvements ###

#. Finish `iterate :: (a -> b) -> Forest a -> Forest b`{.haskell} so that this function that iterates over the entire subforest.

#. Refinement and coarsening functions that more closely match those of the `p4est` library.

#. Improve the handling for *MPI Tags* and for (*MPI*) user-defined types, to support sharing.

## Refactor the Codebase ## {#sec:refactor}

Figure @fig:pforest-deps shows the (simplified) module dependencies, and this not all of this functionality is being used.

![Simplified module dependencies of 'pforest'.](doc/images/pforest-deps.pdf "Simplified module dependencies of 'pforest'"){#fig:pforest-deps}

### Tasks ###

#. Clean up the `SC`{.haskell} modules and with better abstractions and docs for `ScArray`{.haskell} objects;

#. Project clean-up tasks:
   #. *'Haddock'* annotations and build-scripts;
   #. `QuickCheck` tests and unit-tests;
   #. `criterion` benchmarks for finding regressions;
   #. improve library-dependency checks and the necessary scripts (and maybe `stack` support);

### Building the Dependencies Graph ###

This graph was constructed using:

```bash
$ aptitude install xdot
$ cd ~/tmp/
$ git clone https://github.com/yav/graphmod.git
```

and then the `graphmod/src/Utils.hs` file was modified so that the suffixes included support for `".hsc"`{.haskell} files:

```haskell
suffixes           :: [String]
suffixes            = [".hs", ".hsc", ".lhs", ".imports"]
```

Then

```bash
$ cabal new-install -j graphmod
$ cd ~/Dropbox/bitbucket/pforest
$ find src -iname '*.hs' -o -iname '*.hsc' | xargs graphmod -q | xdot -
$ find src -iname '*.hs' -o -iname '*.hsc' | xargs graphmod -q -p \
    | dot -Tsvg -o doc/images/pforest-deps.svg
```

where `xdot` is for quickly viewing the graph, and the `-p` option prunes a lot of "noisy" edges.

\clearpage

# Design the Required, New Functionality # {#sec:design}

#. "Audit" the existing `p4est` code-base and functions:

   #. what queries already exist?
   
   #. finish the API-audit spreadsheet, `doc/p4est-functions.ods`;

   #. design the mid-level API;

#. Evaluate the functionality provided by `p4est`, including:

   #. `p4est_geometry` objects (as they represent transformations/maps from parametric to geometric coordinates, so may be useful for IGA);

   #. `p8est_*` stuff, and how to support 3D meshes with the `pforest` types and functions;

   #. `p6est_*` of any use?

   #. 4D meshes?

## Design of the User API ##

Look for ideas for the API based on existing code/projects, including:

#. `p4est` examples and tutorials;

#. *'FEniCS'*, and `freefem++`; and

#. my other examples and test-bases.

\clearpage

# Bindings and Wrapper for `p4est` # {#sec:wrapper}

#. Haskell (GHC) bindings:

   #. ~~SC arrays;~~
   
   #. ~~Low-level bindings;~~
   
   #. Base functions;

   #. Mesh-level operations and traversals:

      + ~~`New`;~~

      + ~~`Destroy`;~~

      + `Partition` using:
        #. user-calculated weights; and
        #. ~~uniform weights;~~

      + `Balance` using 2:1 constraints for:
        #. faces; and
        #. ~~corners;~~

      + `Refine` by:
        #. node;
        #. ~~cell;~~
        #. edge; and
        #. using supplied `InitFn`{.haskell} for interpolation,

      + `Coarsen` by:
        #. node;
        #. cell;
        #. edge; and
        #. using supplied `InitFn`{.haskell} for interpolation,

      + `Iterate` by:
        #. node;
        #. cell;
        #. edge; and
        #. using supplied `InitFn`{.haskell} for interpolation,

   #. Mesh augmentation:

      + ~~`Ghost` layer functions;~~

      + ~~`Nodes` layer functions;~~

      + `LNodes` layer functions?

      + `Geometry` data?

   #. Connectivities:
   
      + ~~mid-level;~~ and
      
      + ~~high-level,~~
      
   #. ~~Communication (via 'mpi-hs');~~ **DONE**

   #. Queries by:

      + node;

      + face;

      + neighbour; and

      + neighbourhood queries,

   #. Code-quality:

      + unit-tests and `QuickCheck` testbenches;

      + check the contexts for when the various `piggy(x)` *"piggy-back"* data is used;

      + profile Haskell code vs. C-based implementations, and make sure parallelism and overhead are acceptable;

      + Haddock code annotations;

      + use `criterion` to measure the costs for various API calls -- in case there are better ways to use the `p4est` API's;

      + refactor the code so there are no orphan instances for data-types from external libraries; e.g., eliminate definitions like `instance ToJSON a => ToJSON (V3 a)`{.haskell};

      + probably some memory-leaks that need fixing (due to use of `new`, etc.);

   #. Build scripts:

      + find the correct headers for MPI, `libsc`, and `libpetsc`;

      + determine the versions, and use CPP to set the configuration;

#. Exporters:

   #. SVG:

      + Forests -- better handle the connectivities;

      + Trees -- needs rotations/flips based on 'tree' orientation & embedding;

      + Connectivities;

      + ~~Ghosts and mirrors;~~

      + Subgraph compositing and tiling;

   #. ~~VTK~~

   #. JSON:

      + Connectivities;

      + Ghosts;

      + Nodes;

#. Installation scripts and/or packages, perhaps using `docker` and/or `stack`.

\clearpage

# Appendix: Algorithms # {#app:algorithms}

This section describes a few additional functions needed to support the rest of the algorithms. First is a list of functions that were written/bound for traversing and querying the (local) forest structures.

## Support for Arbitrary Topologies ## {#sec:arb-topo}

Due to `p4est` supporting arbitrary topologies (via its `p4est_connectivity_t` structures), there is no general, efficient, and convenient method to compute global coordinates. But the `p4est` library contains numerous routines for the traversal and querying of forests, and mostly by generating, testing, and searching for adjacent quadrants, from any given quadrant.

Most of the `p4est` quadrant-based functions are not (tree-)boundary aware, but some of them are, and a list of useful functions follows:

```haskell
  -- quad/coordinate functions for forest-traversal:
  quadFaceNeighbourExtra :: Quad -> Dir -> ForestM (Maybe (Dir, Quad))
  quadToCoord :: Quad -> V3 Int
  coordToQuad :: TreeID -> MpiRank -> V3 Int -> Quad
  lastCoordOffset :: (Integral i, Num a) => i -> a
  faceNbr :: Quad -> Dir -> Quad
  cornerNbr :: Quad -> Int -> Quad
  faceNbrs :: Quad -> V4 Quad
  lastQuad :: Quad -> Quad

  -- functions for finding/applying transforms:
  getConnectivity :: ForestM Connectivity
  findFaceTransform :: Connectivity -> Int -> Dir -> Maybe FTransform
  quadFaceTransform :: Quad -> FTransform -> Quad
  findCornerTransform :: Connectivity -> Int -> Int -> Maybe CornerInfo
  quadCornerTransform :: Quad -> CornerInfo -> Quad

  -- tree containment & ownership queries:
  quadExists :: Quad -> ForestM Bool
  quadFindOwner :: Quad -> Maybe Dir -> ForestM (Maybe MpiRank)
  findOverlappedRanks :: Quad -> ForestM (Int, Int)
  quadIsContained :: Quad -> Int -> ForestM Bool
  quadFaceOnBoundary :: Quad -> Dir -> ForestM Bool
```

These are frequently used for the "building blocks" of the algorithms presented in the following sections.

\clearpage

## Rank-sets for `isend`/`irecv` of *"Sharers"* ## {#sec:irecv-sets}

!include rubs-base/doc/algo-sharing-ranks.inc.md

\clearpage

## Sharing *"Sharers"* ## {#sec:sharing}

!include rubs-base/doc/algo-sharing-nodes.inc.md

# Bibliography #
