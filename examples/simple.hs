{-# LANGUAGE TemplateHaskell, DeriveGeneric
  #-}
------------------------------------------------------------------------------
-- |
-- Module      : Main
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Demonstration of some parallel-forest functionality.
-- 
-- Changelog:
--  + 25/05/2019  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Main where

import GHC.Generics (Generic)
import System.Environment
import Control.Monad.IO.Class
import Control.Lens (makeLenses, (^.))
import Options.Applicative
import Text.Printf

import Data.Scientific.SC as SC
import Data.Forest.P4est as P4


-- * Data-types
------------------------------------------------------------------------------
-- | Command-line options.
data SimpleOpts =
  SimpleOpts { _maxLevel  :: Int
             , _useSquare :: Bool
             , _verbose   :: Bool
             } deriving (Eq, Show, Generic)

makeLenses ''SimpleOpts


-- * Helper functions
------------------------------------------------------------------------------
-- | Command-line options parser.
parser :: Parser SimpleOpts
parser  = SimpleOpts
  <$> option auto (short 'l' <> long "max-level" <> metavar "LEVEL" <>
                   help "Choose the maximum refinement-level" <>
                   value 2 <> showDefault)
  <*> switch      (short 'u' <> long "unitsquare" <>
                   help "Use a unit-square instead of an L-shaped mesh")
  <*> switch      (short 'v' <> long "verbose" <>
                   help "Generate extra output")
  

-- * Top-level functions
------------------------------------------------------------------------------
-- | Simple demo.
runDemo :: SimpleOpts -> IO ()
runDemo opts = do
  args <- getArgs
  _    <- P4.withSC args $ do
    mesh <- if opts^.useSquare
      then P4.unitSquare
      else P4.meshLShape

    P4.withP4estM mesh $ do
      rank <- fromEnum <$> SC.mpiRank
      liftIO $ printf "simple: MPI rank = %d\n" rank

      -- refine & balance
      P4.refineWith ((<opts^.maxLevel) . depth) False
      P4.partition
      P4.refineWith ((<opts^.maxLevel) . depth) True
      P4.partition
      P4.balanceFull
      P4.partition

      -- add ghosts and then nodes:
      P4.addGhost P4.ConnectFull
      P4.addNodes

      -- export to VTK and SVG:
      P4.vtkWriteFile "out/simple"
      P4.svgWriteFile "out/simple"
    P4.meshDestroy mesh
  pure ()


-- * Main program entry-point
------------------------------------------------------------------------------
main :: IO ()
main  = execParser opt >>= runDemo where
  opt = info parser mempty
